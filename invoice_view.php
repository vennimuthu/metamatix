<?php
include("config/config.php");

if(!isset($_SESSION["user_id"])){ // checking whether the usertype is logged in
	header('Location: logout.php');        						//REdirection to logout.php
	exit;
}

include("core/class/db_query.php");                             // Class where query generetion is written
include("core/class/db_helper.php");                            // Class where table and feilds 
include("core/function/common.php");   	
                                                                // are mentioned to generate query
ob_start();                                                     // to clear the internal output
global $db_helper_obj;
global $global_website_url;
$db_helper_obj=new db_helper();  

$inward_view=$db_helper_obj->invoice_edit($_GET["invoice_id"]);

	foreach(explode(",",$inward_view[0]["inward_ids"]) as $va=>$key){	
		$inward=$db_helper_obj->inwardapprove_edit($key);
		
		foreach($inward as $va1=>$key1){
			
			$products=convert_array($key1["products"]);
			$qtylist=convert_array($key1["qty_list"]);
			 
			$supplier=$key1["supplier"];
			foreach($products as $va2=>$key2){
				
				$invoice_dc[$key][$va2]["product_id"]=$key2["product_id"];
				$invoice_dc[$key][$va2]["dc_no"]=$key1["inv_no"];
				$invoice_dc[$key][$va2]["inward_no"]=$key1["inward_no"];
				$invoice_dc[$key][$va2]["qty_received"]=$key2["qty_received"];
				$invoice_dc[$key][$va2]["qty_approve"]=$key2["qty_approve"];
				$invoice_dc[$key][$va2]["qty_rejected"]=$key2["qty_rejected"];
				$invoice_dc[$key][$va2]["ins_remarks"]=$key2["ins_remarks"];
				$invoice_dc[$key][$va2]["product_price"]=$key2["product_price"];
			}
		}
		
	}//echo"<pre>";print_r($supplier);echo"</pre>";
	
?>
<html>
<head>

  <link rel="icon" type="images/png" href="">
  <link rel='stylesheet' href='css/font-awesome.css'>
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/responsive-menu.css">
  <link rel="stylesheet" href="css/style.css">
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap.js"></script>
  <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
  <link href="css/select2.min.css" rel="stylesheet" />
  <script src="js/select2.min.js"></script>
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" />
  <script src="js/modernizr-custom.js"></script>
  <script src="js/responsive-menu.js"></script>
</head>
<div class="listwrapper" id="height_id">

<div class="table-responsive"> 
<table class="table listhead table-bordered table-hover" align="center" width="50%" border="1" cellspacing="0" cellpadding="0">
<tr>	
<td width="70px"><label>Invoice&nbsp;No:</label></td>
<td colspan="3">
<label><?php echo $inward_view[0]["invoice_no"]; ?></label>
 </td>
<td ><label>Supplier&nbsp;Name:</label></td>
<td colspan="5">
<label><?php 
$prod=$db_helper_obj->supplier_edit($supplier);
echo $prod[0]["supplier_name"]; ?></label>
</td>
</tr>
<tr>
<td align="center"><label>S No</label></td>
<td align="center"><label>Dc&nbsp;No</label></td>
<td align="center"><label>Inward&nbsp;No</label></td>
<td align="center"><label>Item&nbsp;Name</label></td>
<td align="center"><label>Qty&nbsp;Received</label></td>
<td align="center"><label>Approved&nbsp;Qty</label></td>
<td align="center"><label>Rejected&nbsp;Qty</label></td>
<td align="center"><label>Rate</label></td>
<td align="center"><label>Amount</label></td>
<!--<td align="center"><label>Inspection&nbsp;Remarks</label></td>-->
</tr>
<?php 
$sno=1;
foreach($invoice_dc as $va=>$key){$count=1;
foreach($key as $va1=>$key1){ ?>
<tr>

<?php if($count==1){ ?>
<td align="center" rowspan="<?php echo count($key); ?>"><?php echo $sno; ?></td>
<td  align="center" style="padding: 4px;" rowspan="<?php echo count($key); ?>">
<label><?php echo $key1["dc_no"]; ?></label>
</td>
<td  rowspan="<?php echo count($key); ?>" align="center" style="padding: 4px;">
<label><?php echo $key1["inward_no"]; ?></label>
</td>
<?php $sno++;} ?>
<td style="padding: 4px;">
<label><?php $prod2=$db_helper_obj->total_product_edit($key1["product_id"]);
echo $prod2[0]["name"]; ?></label>
</td>
<td  align="center" style="padding: 4px;">
<label><?php $total_received+=$key1["qty_received"];echo $key1["qty_received"]; ?></label>
</td>
<td  align="center" style="padding: 4px;">
	<label><?php $total_approve+=$key1["qty_approve"]; echo $key1["qty_approve"]; ?></label>
</td>
<td align="center" style="padding: 4px;">
	<label><?php $total_rejected+=$key1["qty_rejected"]; echo $key1["qty_rejected"]; ?></label>
</td>

<td align="right" style="padding: 4px;">
	<label><?php echo $key1["product_price"]; ?></label>
</td>
<td align="right" style="padding: 4px;">
	<label><?php $total_amount+=$key1["qty_approve"]*$key1["product_price"]; echo 
	sprintf("%.2f",$key1["qty_approve"]*$key1["product_price"]); ?></label>
</td>
<!--<td  align="center" style="padding: 4px;">
	<label><?php echo $key1["ins_remarks"]; ?></label>
</td>-->
</tr>
<?php $count++; }  } ?>
<tr>
<td rowspan="3" colspan="3"> <label>Remarks:</label><br/><label><?php echo $inward_view[0]["admin_comment"]; ?></label></td>
<td align="center"><label>Total&nbsp;Qty&nbsp;Received</label></td>
<td align="center" > <label><?php echo $total_received; ?></label></td>
<td align="center" colspan="2">&nbsp;</td>
<td align="center"><label>Taxable&nbsp;Value</label></td>
<td align="right"><b><?php echo sprintf("%.2f",$total_amount); ?><b></td>
</tr>
<tr>

<td align="center"><label>Total&nbsp;Qty&nbsp;Approved</label></td>
<td align="center"> <label><?php echo $total_approve; ?></label></td>
<td align="center" colspan="2">&nbsp;</td>
<?php if($qtylist["cgst_percent"]!=""){ ?>
<td align="center"><label>CGST:&nbsp;<?php echo $qtylist["cgst_percent"]; ?>&nbsp;%</label></td>
<td align="right"> <label><?php echo $cgst=($total_amount*$qtylist["cgst_percent"])/100; ?></label></td>

<?php } ?>
</tr>
<tr>
<td align="center"><label>Total&nbsp;Qty&nbsp;Rejected</label></td>
<td align="center"> <label><?php echo $total_rejected; ?></label></td>
<td align="center" colspan="2">&nbsp;</td>
<?php if($qtylist["sgst_percent"]!=""){ ?>
<td align="center"><label>SGST:&nbsp;<?php echo $qtylist["sgst_percent"]; ?>&nbsp;%</label></td>
<td align="right"> <label><?php echo $sgst=($total_amount*$qtylist["sgst_percent"])/100; ?></label></td>

<?php } ?>
</tr>
<tr>
<?php if($qtylist["igst_percent"]!=""){ ?>
<td align="center"><label>IGST:&nbsp;<?php echo $qtylist["igst_percent"]; ?>&nbsp;%</label></td>
<td align="right"> <label><?php echo $igst=($total_amount*$qtylist["igst_percent"])/100; ?></label></td>

<?php } ?>
</tr>
<tr>
<td colspan="7"></td>

<td align="center"><label>GRAND&nbsp;TOTAL</label></td>
<td align="right"> <label><?php echo sprintf("%.2f",$total_amount+$cgst+$sgst+$igst); ?></label></td>
</tr>
</table>
</div>
</div>
<script type="text/javascript">
	setTimeout(function(){ parent.$("#iframe_show_vehicle_data1").attr("height",$("#height_id").height()+30+"px"); }, 100);
</script>
