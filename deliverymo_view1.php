<?php
include("config/config.php");

if(!isset($_SESSION["user_id"])){ // checking whether the usertype is logged in
	header('Location: logout.php');        						//REdirection to logout.php
	exit;
}

include("core/class/db_query.php");                             // Class where query generetion is written
include("core/class/db_helper.php");                            // Class where table and feilds 
include("core/function/common.php");
                                                               // are mentioned to generate query
ob_start();                                          			// to clear the internal output
global $db_helper_obj;
global $global_website_url;
$db_helper_obj=new db_helper();  

$deliverymi_edit=$db_helper_obj->deliverymi_edit($_GET["process_id"]);
foreach($deliverymi_edit as $va=>$key){
	$product_price[$key["product_id"]]=$key["rate"];
}	

?>
<html>
<head>
<link rel="icon" type="images/png" href="">
  <link rel='stylesheet' href='css/font-awesome.css'>
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
   <link rel="stylesheet" href="css/responsive-menu.css">
  <link rel="stylesheet" href="css/style.css">
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap.js"></script>
  <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
  <link href="css/select2.min.css" rel="stylesheet" />
  <script src="js/select2.min.js"></script>
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" />
  <script src="js/modernizr-custom.js"></script>
  <script src="js/responsive-menu.js"></script>
</head>
<div class="listwrapper" id="height_id">

<div class="table-responsive"> 
<table class="table listhead table-bordered table-hover" align="center" width="50%" border="1" cellspacing="0" cellpadding="0">
<tr>	
<td width="70px"><label>Date:</label></td>
<td  >
<label><?php 
 echo date('d-m-Y',strtotime($deliverymi_edit[0]["dated"]));
 ?></label>
</td>
<td width="70px"><label>Challan NO:</label></td>
<td colspan="2">
<label><?php echo $deliverymi_edit[0]["challan_no"]; ?></label>
 </td>
</tr>
<tr>
</td>
<td ><label>Supplier Name:</label></td>
<td colspan="4">
<label><?php 
$prod=$db_helper_obj->supplier_edit($deliverymi_edit[0]["supplier"]);
echo $prod[0]["supplier_name"]; ?></label>
</td>
</tr>

<tr>	
<td width="70px"><label>Supplier Invoice No:</label></td>
<td>
<label><?php echo $deliverymi_edit[0]["supp_invoice_no"]; ?></label>
</td>
<td width="70px"><label>Supplier Bill Date:</label></td>
<td colspan="2">
<label><?php echo date('d-m-Y',strtotime($deliverymi_edit[0]["supp_bill_date"])); ?></label>
 </td>
</tr>
 <tr>
<td width="70px"><label>Vehicle No</label></td>
<td>
<label><?php echo $deliverymi_edit[0]["vehicle_no"]; ?></label>
</td>
<td ><label>Delivery through:</label></td>
<td colspan="2"><label><?php echo $deliverymi_edit[0]["delivery_through"]; ?></label>
 </td>
</tr>
<td align="center"><label>S No</label></td>
<td align="center"><label>Item Name</label></td>
<td align="center"><label>Quantity</label></td>
<td align="center" style="width:90px;"><label>Rate</label></td>
<td align="center"><label>Amount</label></td>
</tr>
<?php $count=1;
if(!empty($deliverymi_edit)) { 
foreach($deliverymi_edit as $va=>$key){
	if($key["admin_approve"]==1)
		$deliver=convert_array($key["adminproduct_approve"]);
	else if($key["qc_approve"]==1)
		$deliver=convert_array($key["qcproduct_approve"]);
	foreach($deliver["product_id"] as $va1=>$key1){
		if($key["product_id"]==$key1){ 
?>
<tr>
<td align="center"><?php echo $count; ?></td>
<td align="center" style="padding: 4px;">
<label><?php
	$prod2=$db_helper_obj->total_product_edit($key1);
	echo $prod2[0]["name"]; ?></label>
</td>
<td  align="center" style="padding: 4px;">
<label><?php echo $deliver["qty_approve"][$va1]; ?></label>
</td>
<td  align="center" style="padding: 4px;">
<label><?php echo $product_price[$key1]; ?></label>
 </td>
<td  align="center" style="padding: 4px;">
<label><?php echo $deliver["qty_approve"][$va1]*$product_price[$key1];  ?></label>
</td>
</tr>
	<?php $count++; } } } }?>
</table>
</div>
</div>
<script type="text/javascript">
parent.$("#iframe_show_vehicle_data").attr("height",$("#height_id").height()+400+"px");
</script>
