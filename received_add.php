<?php function main(){
	
	  
	$db_query=new db_query();  
	global $db_helper_obj;
	$db_helper_obj=new db_helper();
	$product_list=$db_helper_obj->product_list();
	$supplier_list=$db_helper_obj->supplier_list();
	$roles=$db_helper_obj->roles();
	
	if(!empty($_POST)){
		$_POST["received_date"] = date("d-m-Y", strtotime($_POST["received_date"]));
		$inserted=$db_helper_obj->received_insert();
		header('Location:  received_list.php'); //REdirection to master_list.php
	}else{
		unset($_SESSION["cost"]);
	}
?>
<script type="text/javascript">

function add_order_row(order_row_id)
{	
	var order_item_index=$("#order_item_row_index_"+order_row_id).val();
	order_item_index++;
	var html_content;
	var options=document.getElementById('supplier_1').innerHTML;
//+'<td><select required="" class="form-control" name="name['+order_item_index+']" id="name_'+order_row_id+"_"+order_item_index+'"><option value="">Select</option><option value="Plastic">Plastic</option><option value="Rubber">Rubber</option><option value="Bolts &amp; Nuts">Bolts &amp; Nuts</option><option value="Red Fibre Gasket">Red Fibre Gasket</option></select></td>'
	
	var slect='<select id="supplier_'+order_item_index+'" name="supplier['+order_item_index+']" class="form-control" required>'+options+'</select>';

	html_content='<tr id="order_item_row_'+order_row_id+"_"+order_item_index+'" name="order_item_row_'+order_row_id+"_"+order_item_index+'">'
+'<td><input type="text" class="form-control" name="name['+order_item_index+']" id="name_'+order_item_index+'" value="" required /></td>'
+'<td> 	<input type="text" class="form-control" name="dimension['+order_item_index+']" id="dimension_'+order_item_index+'" value="" required=""></td>'
+'<td> 	<input type="number" class="form-control" name="qty['+order_item_index+']" id="qty_'+order_item_index+'" value="" required=""></td>'
+'<td><input type="text" class="form-control" name="price['+order_item_index+']" id="price_'+order_item_index+'" value="" required/></td>'
+'<td>'+slect+'</td>'
+'<td align="center"><input type="checkbox" name="process['+order_item_index+']" id="process_'+order_item_index+'" value="1"   data-toggle="modal" href="#myModal"  onclick="cost_add(this)"></td>'
+'<td><input type="button" class="btn btn-danger" value="X" onclick="delete_order_item_row(this);"></td></tr>';
	
	div = document.getElementById('order_item_body_'+order_row_id);	
	div.insertAdjacentHTML( 'beforeend', html_content);
	$("#order_item_row_index_"+order_row_id).val(order_item_index);
}

function delete_order_item_row(element)  
{
    var row = element.parentNode.parentNode;
    row.parentNode.removeChild(row);
}
function cost_add(obj){
var word=obj.id;
var number=word.replace('process_','');	
if(obj.checked==true)
	var check=1;
else if(obj.checked==false)	
	var check=0;
if(obj.checked==true&&document.getElementById('name_'+number).value!=''){
$.ajax({url: "master_costadd.php?sub_id="+number+"&check="+check+"",contentType:false, processData: false, success: function(result){
	var ser = document.getElementById('iframe_show_vehicle_data');
	ser.contentDocument.write(result);
	}
	});	
}else{ 
	$.ajax({url: "master_costadd.php?sub_id="+number+"&check="+check+"",contentType:false, processData: false, success: function(result){
	var ser = document.getElementById('iframe_show_vehicle_data');
	ser.contentDocument.write(result);
	}
	});	
}
}
function cancel_data(){
	var iframe = document.getElementById("iframe_show_vehicle_data");
	var html = "";
	iframe.contentWindow.document.open();
	iframe.contentWindow.document.write(html);
	iframe.contentWindow.document.close();
}

</script>


<style>
	.cost_form { }
	.cost_form input.form-control { margin-bottom:10px; }
	.tbox_vsmall {
     width:21%;
	}

</style>

<form class="form-horizontal" id="recruitment" name="recruitment" method="post">

	<div class="form-group">
	<div style="width: 700px;margin:0 auto;">
    <label for="inputEmail3" class="col-sm-3 control-label">Product Name</label>
    <div class="col-sm-9">
	<select id="product_id" name="product_id" class="form-control tbox_small" required>
				<option value="" selected disabled >Select</option>
				<?php if(!empty($product_list)) foreach($product_list as $va=>$key){ ?>
				<option value="<?php echo $key["id"];?>"><?php echo $key["product_name"];?></option>
				<?php } ?>
	</select>
	</div>
	</div>

	<div style="width: 700px;margin:0 auto;margin-top: 45px;">
    <label for="inputEmail3" class="col-sm-3 control-label">Quantity</label>
    <div class="col-sm-9"><input type="text" class="form-control tbox_small" id="qty"  name="qty" placeholder="Quantity"></div>
	</div>
	<!-- <div style="width: 700px;margin:0 auto;margin-top: 90px;">
    <label for="inputEmail3" class="col-sm-3 control-label">Price</label>
    <div class="col-sm-9"><input type="text" class="form-control tbox_small" id="price"  name="price" placeholder="Price"></div>
	</div>  -->
	<div style="width: 700px;margin:0 auto;margin-top: 90px;">
    <label for="inputEmail3" class="col-sm-3 control-label">Supplier</label>
    <div class="col-sm-9">
	<select id="supplier_id" name="supplier_id" class="form-control tbox_small" required>
				<option value="" selected disabled >Select</option>
				<?php if(!empty($supplier_list)) foreach($supplier_list as $va=>$key){ ?>
				<option value="<?php echo $key["id"];?>"><?php echo $key["supplier_name"];?></option>
				<?php } ?>
	</select>
	</div>
	</div>
	<div style="width: 700px;margin:0 auto;margin-top: 135px;">
    <label for="inputEmail3" class="col-sm-3 control-label">Received Date</label>
    <div class="col-sm-9"><input type="date" class="form-control tbox_small" id="received_date"  name="received_date" value="<?php echo date('Y-m-d'); ?>" placeholder="Price"></div>
	</div>

	<div class="col-sm-10" align="center" style="margin-top: 30px;">
			 <button type="button" onclick="window.location.href='received_list.php'" class="btn btn-primary">Cancel</button>
			 <button type="submit" class="btn btn-primary">Submit</button>
	</div>
	</div>
</form>
<?php } include("template.php"); ?>  