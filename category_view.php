<?php 
include("config/config.php");

if(!isset($_SESSION["user_id"])){ // checking whether the usertype is logged in
	header('Location: logout.php');        						//REdirection to logout.php
	exit;
}

include("core/class/db_query.php");                             // Class where query generetion is written
include("core/class/db_helper.php");                            // Class where table and feilds 
                                                                // are mentioned to generate query
ob_start();                                                     // to clear the internal output
global $db_helper_obj;
global $global_website_url;
$db_helper_obj=new db_helper();                                 // Creating object to get data from table
$current_page = basename($_SERVER['PHP_SELF']); // to get the page name from url
global $month_name_arr;
$month_name_arr=array('1'=>'Jan','2'=>'Feb','3'=>'Mar','4'=>'Apr','5'=>'May','6'=>'Jun','7'=>'Jul','8'=>'Aug','9'=>'Sep','10'=>'Oct','11'=>'Nov','12'=>'Dec');

?>
<link rel="icon" type="images/png" href="images/favicon.png">
  <link rel='stylesheet' href='css/font-awesome.css'>
  <link rel="stylesheet" href="css/bootstrap.css">
   <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/jquery.mCustomScrollbar.min.css">
  <link rel="stylesheet" href="css/responsive-menu.css">
  <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery.js"></script>
<?php 

	$db_query=new db_query();  
	global $db_helper_obj;
	$db_helper_obj=new db_helper();

	if(!empty($_REQUEST)){
	$master=$db_helper_obj->total_product_edit($_REQUEST["product_id"]);
	//echo"<pre>";print_r($master);echo"</pre>";
	}
?>
<style>
	.cost_form { }
	.cost_form input.form-control { margin-bottom:10px; }
</style>
							  
<div class="table-responsive" id="master_viewid"> 
<table class="table listhead table-bordered table-hover">
	<thead>
		<tr>
			<th>Product Name</th>
			<th>HSN/SAC</th>
			<th>Material Type</th>
			<th>Buffing Price / Hour</th>
			<th>Machining Price</th>
			<th>Product Price</th>
			<th>Process</th>
			<th>Machining</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><?php echo $master[0]["name"]; ?></td>	
			<td>  <?php echo $master[0]["hsn"];?></td>
			<td><?php 
			$material=$db_helper_obj->material_edit($master[0]["material"]);
			echo  $material[0]["category"]; ?></td>
			
			<td><?php if($master[0]["price"]!='')echo $master[0]["price"]; else  echo "Will Assigned."; ?></td>
			
			<td><?php if($master[0]["machining_price"]!='') { echo $master[0]["machining_price"]; } else  echo "Will Assigned."; ?></td>
			
			<td><?php if($master[0]["product_price"]!='') echo $master[0]["product_price"];else  echo "Will Assigned.";  ?></td>
			
			<td><?php if( $master[0]["process"]==1) echo "Buffing Involved."; else  echo "Buffing Not Involved."; ?></td>
			
			<td><?php if( $master[0]["machining"]==1) echo "Machining Involved."; else  echo "Machining Not Involved."; ?></td>
					</tr>
	</tbody>
</table>

</div>      
<script type="text/javascript">
	setTimeout(function(){ parent.$("#iframe_show_vehicle_data").attr("height",$("#master_viewid").height()+40+"px"); }, 100);
</script>				         
		