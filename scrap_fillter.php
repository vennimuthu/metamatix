<?php 
include("config/config.php");
include("core/class/db_query.php");                             // Class where query generetion is written
include("core/class/db_helper.php");                            // Class where table and feilds 
include("core/function/common.php");   	
                                                                // are mentioned to generate query
ob_start();                                                     // to clear the internal output
global $db_helper_obj;
global $global_website_url;

$db_helper_obj=new db_helper();

$steel_history=$db_helper_obj->steel_fillter($_REQUEST);

foreach($steel_history as $va=>$key){	
	$scrap[$key["steel_number"]]["created_date"]+=$key["created_date"];
	$scrap[$key["steel_number"]]["rm_wastage"]+=$key["rm_wastage"];		
	$scrap[$key["steel_number"]]["qty_approved"]+=$key["qty_approved"];
}

?>
<div class="table-responsive" id="table_append"> 
                            <table class="table listhead table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>S No</th>                                         
										<th>STN No</th>
										<th>Date</th>
										<th>Qty</th>										 
										<th>RM Wastage (G)</th>
										<th>Total Wastage (KG)</th>
										<th>Report</th>
									</tr>
                                </thead>
                                <tbody>
	<?php $count=1; $num=0;
		if(!empty($scrap)){
		foreach($scrap as $va1=>$key1){ 
	?>
	<tr id="product_tr_<?php echo $num; ?>" >	
	<td align="center" ><?php echo $count; ?></td>
	<td align="center"> 
	<?php echo $va1; ?></td>
	<td align="center"> 
	<?php echo date("d-m-Y",$key1["created_date"]); ?></td>
	<td align="center"><?php  echo $key1["qty_approved"]; ?></td>
	<td align="center"><?php  echo $key1["rm_wastage"]; ?></td>	  
	<td><?php $total_scrap+=number_format($key1["qty_approved"]*$key1["rm_wastage"],3); echo number_format($key1["qty_approved"]*$key1["rm_wastage"],3); ?></td>	 
	<td>
	<input type="checkbox" name="approve[]" price="<?php echo number_format($key1["qty_approved"]*$key1["rm_wastage"],3); ?>" id="approve_<?php echo $num; ?>" value="<?php echo $va1; ?>" >
	</td>	 
	</tr>
	<?php $count++; $num++;  }}  ?>
	<tr>
	<td align="right" colspan="6"><b>Total Scrap</b></td>
	<td><b><?php echo $total_scrap; ?></b>
	<input type="hidden" name="total_scrap" id="total_scrap" value="<?php echo $total_scrap; ?>" />
	</td>
	</tr>
<div aria-hidden="true" aria-labelledby="myModal" role="dialog" tabindex="-1" id="myModal" class="purchase modal fade">
<div class="modal-dialog">
  <div class="modal-content">
	  <div class="modal-header">
		  <button type="button" class="close"  onclick="cancel_data()"  data-dismiss="modal" aria-hidden="true">&times;</button>
		  <h3 class="modal-title text-center">Scrap Movement</h3>		  
	  </div>
	  <div class="modal-body">
<iframe id="iframe_show_vehicle_data" frameborder="0" marginwidth="0" marginheight="0"  scrolling="yes"  style="width:100%;border:0px;">
</iframe>
	  </div>                          
  </div>
</div>
</div>
	<input type="hidden" name="wavement" id="wavement" value="" />
	<input type="hidden" name="vehicle" id="vehicle" value="" />
	<input type="hidden" name="creation" id="creation" value="" />
	</tbody>
                            </table>
                        </div>  <!------End--- Table Responsive -->