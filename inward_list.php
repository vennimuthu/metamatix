<?php function main(){
	$db_query=new db_query();  
	global $db_helper_obj;
$db_helper_obj=new db_helper();

if($_POST["forward_id"][0]!=''){ 
$forward=$db_helper_obj->forwards($_POST["forward_id"][0]);
	unset($_POST["forward_id"]);
}
if($_POST["admin_approve"][0]!=''){
$forwards_admin=$db_helper_obj->forwards_admin($_POST["admin_approve"][0]);
	unset($_POST["admin_approve"]);
}
$inward_list=$db_helper_obj->inward_list();
$inward_approve_list=$db_helper_obj->inward_approve_list();

foreach($inward_list as $va=>$key){
foreach($inward_approve_list as $va1=>$key1){
	if($key["id"]==$key1["inward_id"])
		$inward_list[$va]["admin_approve"]=$key1["admin_approve"];
}
}

?>
      
	<form id="receipt_list" name="receipt_list" method="post">
	<div class="container-fluid">  
	<?php if($_SESSION["user_type"]==4){ ?>
			<a href="inward_add.php" class="list_add pull-right"><img src="images/edit.ico"/></a>
			 <?php } ?> 	
    	<div class="row">
        	<div class="col-sm-12">
			
            	<!--<h4 class="inline-heading">Inward List</h4>-->
						
				<div class="listwrapper">
                	<div class="table-responsive" id="table_append"> 
                            <table id="example" class="table listhead table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>S No</th>
										<th>GRN No</th>
                                        <th class="no-sort">Date</th>
										<th class="no-sort">Total Qty</th>
										
										 <?php if($_SESSION["user_type"]==2){ ?>
										 <th class="no-sort">Payment</th>  
										 <?php } ?> 
										 <?php if($_SESSION["user_type"]==1){ ?>
										  <th class="no-sort">Admin Approve</th>  
										 <?php } ?>
										 <th class="no-sort">Status</th>
										 <th class="no-sort">Edit</th> 										 
                                    </tr>
                                </thead>
                                <tbody>
                    <?php
						$count=1; if(!empty($inward_list)){
						foreach($inward_list as $va=>$key){
							
						/*if($_SESSION["user_type"]==1){
						if($key["forward_approved"]==0){
						continue;
						}
						if($key["admin_approved"]==1){
						continue;
						}							
						}*/
							
						$receipts=convert_array($key["products"]);		
						$total=0;	
						$qty=0;
						$amount=0;					
						foreach($receipts as $va2=>$key2){	
						$qty+=$key2["qty"];
						$amount+=$key2["amount"];
						}
						
					?>  
					 <tr>
                        <td>
                         <?php echo $count; ?>
                        </td>
						<td>
						<a data-toggle="modal" href="#myModal" data-backdrop="static" data-keyboard="false" onclick="show_order_data(<?php echo $key["id"]; ?>)" >
                        <?php echo $key["inward_no"]; ?>
                        </a>
						</td>
                        <td>
						<?php echo date('d-m-Y',$key["dated"]);	?>
						</td>
						
						<td><?php echo $qty; ?></td>
						
						<?php if($_SESSION["user_type"]==1){ ?>
						<td>
						<?php if($key["forward_approved"]==1&&$key["admin_approved"]==0){ ?>
						<input type="hidden" name="admin_approve[]" id="admin_approve" value="" > 
						<input type="submit" name="approves" id="approves" class="btn btn-primary" value="Approve" onclick="Admin_forward(<?php echo $key["id"];?>)" >
						<?php } ?>
						</td>
						<?php } else if($_SESSION["user_type"]==2){ ?>						
						<td>
						<?php if($key["approved"]==1&&$key["forward_approved"]==0&&$key["admin_approved"]==0){ ?> 
						<input data-ids="<?php echo $key["id"]; ?>" type="submit" name="forword" id="forward" class="btn btn-primary" value="Forward" onclick="create_hidden(<?php echo $key["id"];?>);">
						<input type="hidden" name="forward_id[]" id="forward_id" value=""> 
						<?php }else if($key["approved"]==1&&$key["forward_approved"]==1&&$key["admin_approved"]==0){ ?> 
						<label>Forwarded </label>
						<?php } else{ ?>
						<input type="checkbox" name="payment" id="payment" onclick="payment_update(this,<?php echo $key["id"];?>)" <?php if($key["payment"]==1){ ?>checked<?php } ?>>
						<?php } ?>
						</td>
						
						<?php } ?>
						<td>
						<?php if($key["admin_approve"]==1) $key["status"]=3;
						echo $key["status"]; ?></td>
						
						<td>
						<?php if($key["status"]==1){ ?>
                          <a href="inward_edit.php?inward_id=<?php echo $key["id"]; ?>" class="list_add"><Img src="images/edit.png"> </a>
                        <?php } ?>
						</td>
						
                        </tr>
					<?php $count++; } }else{ ?>
					 <tr>
                        <td align="center" colspan="4">There are no list generated</td>
					</tr>
					<?php } ?>				
					            </tbody>
                            </table>
                        </div>  <!------End--- Table Responsive -->
                </div>
				
            </div>
        </div>
    </div>
	</form>
<div aria-hidden="true" aria-labelledby="myModal" role="dialog" tabindex="-1" id="myModal" class="purchase modal fade">
<div class="modal-dialog">
  <div class="modal-content">
	  <div class="modal-header">
		  <button type="button" class="close"  onclick="cancel_data()"  data-dismiss="modal" aria-hidden="true">&times;</button>
		  <h3 class="modal-title text-center">Inward View</h3>
		  
	  </div>
	  <div class="modal-body">
<iframe id="iframe_show_vehicle_data" frameborder="0" marginwidth="0" marginheight="0"  scrolling="yes"  style="width:100%;border:0px;">
</iframe>
	  </div>                          
  </div>
</div>
</div>
<?php } include("template.php"); ?>  
<script type="text/javascript">

function Admin_forward(admin){
  document.getElementById("admin_approve").value=admin;
}
function filter_receipt()
{
	var form = document.getElementById("receipt_list");
	 $.ajax({url: "receipt_ajax.php", type: 'POST',data: new FormData(form),contentType:false, processData: false, success: function(result){
	$("#table_append").html(result);
	}
}); 
}
function payment_update(obj,buffid){
	var check=0;
	if(obj.checked==true)
		var check=1;		
	 $.ajax({url: "process_ajax.php?check="+check+"&buffid="+buffid+"", type: 'POST' ,contentType:false, processData: false, success: function(result){
	   //$("#table_append").html(result);
	}
}); 
}
function show_order_data(supp)
{
	try 
	{		
		 $.ajax({url: "inward_view.php?process_id="+supp,contentType:false, processData: false, success: function(result){
    
	//$("#iframe_show_vehicle_data").innerHTML=result;
	var ser = document.getElementById('iframe_show_vehicle_data');
	//ser.contentDocument.write(result);
	ser.contentDocument.write(result);
	}
});
	 //var form = document.getElementById("subproduct_list");
	 //var arr=new FormData(form);
	 //alert(arr);
	 
		//var url='subproduct_add.php?data='+arr;
		//$("#iframe_show_vehicle_data").attr('src', url);		
		//$("#button_model_show_vehicle_data").click(); 
	}
	catch(err) 
	{
		
	}

}
	function cancel_data()
{
	var iframe = document.getElementById("iframe_show_vehicle_data");
var html = "";

iframe.contentWindow.document.open();
iframe.contentWindow.document.write(html);
iframe.contentWindow.document.close();
}
function create_hidden(ids){
  document.getElementById("forward_id").value=ids;
}

</script>
