<?php
include("config/config.php");

if(!isset($_SESSION["user_id"])){ // checking whether the usertype is logged in
	header('Location: logout.php');        						//REdirection to logout.php
	exit;
}

include("core/class/db_query.php");                             // Class where query generetion is written
include("core/class/db_helper.php");                            // Class where table and feilds 
include("core/function/common.php");   	
                                                                // are mentioned to generate query
ob_start();                                                     // to clear the internal output
global $db_helper_obj;
global $global_website_url;
$db_helper_obj=new db_helper();  

$inward_view=$db_helper_obj->inwardapprove_edit($_GET["process_id"]);

foreach($inward_view as $va=>$key){
	$receipt=convert_array($key["products"]);
	$qtylist=convert_array($key["qty_list"]);	
}
unset($receipt["igst"]);
$total=0;
foreach($receipt as $va2=>$key2){	
$total+=$key2["amount"];
}
//echo"<pre>";print_r($qtylist);echo"</pre>";
?>
<html>
<head>

  <link rel="icon" type="images/png" href="">
  <link rel='stylesheet' href='css/font-awesome.css'>
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/responsive-menu.css">
  <link rel="stylesheet" href="css/style.css">
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap.js"></script>
  <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
  <link href="css/select2.min.css" rel="stylesheet" />
  <script src="js/select2.min.js"></script>
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" />
  <script src="js/modernizr-custom.js"></script>
  <script src="js/responsive-menu.js"></script>
</head>
<div class="listwrapper" id="height_id">

<div class="table-responsive"> 
<table class="table listhead table-bordered table-hover" align="center" width="50%" border="1" cellspacing="0" cellpadding="0">
<tr>	
<td width="70px"><label>GRN&nbsp;NO:</label></td>
<td colspan="3">
<label><?php echo $inward_view[0]["inward_no"]; ?></label>
 </td>
<td ><label>Supplier&nbsp;Name:</label></td>
<td colspan="5">
<label><?php 
$prod=$db_helper_obj->supplier_edit($inward_view[0]["supplier"]);
echo $prod[0]["supplier_name"]; ?></label>
</td>

</tr>
<tr>	
<td ><label>GRN&nbsp;DATE:</label></td>
<td colspan="3">
<label><?php echo date("d-m-Y",$inward_view[0]["dated"]); ?></label>
 </td>
<td ><label>Inspection&nbsp;date:</label></td>
<td colspan="5">
<label><?php  echo date("d-m-Y",$inward_view[0]["ins_date"]);  ?></label>
</td>

</tr>
<tr>
<td><label>DC/INV&nbsp;NO</label></td>

<td  colspan="3">
<label><?php 
echo $inward_view[0]["inv_no"];
 ?></label>
</td>
<td width="70px"><label>Inv&nbsp;Date:</label></td>
<td colspan="5">
<label><?php 
  echo date("d-m-Y",$inward_view[0]["inv_date"]);  
 ?></label>
</td>
</tr>

<tr>
<td width="70px"><label>Received&nbsp;By</label></td>
<td colspan="3">
<label><?php 

echo $inward_view[0]["received_by"];
 ?></label>
</td>
<td width="70px"><label>Inspected By</label></td>
<td colspan="5">
<label><?php echo $inward_view[0]["ins_by"];
 ?></label>
</td>
</tr>
<tr>
<td align="center"><label>S No</label></td>
<td align="center"><label>Item&nbsp;Name</label></td>
<td align="center"><label>Qty&nbsp;Received</label></td>
<td align="center" style="width:90px;"><label>Approved&nbsp;Qty</label></td>
<td align="center"><label>Rejected&nbsp;Qty</label></td>
<td align="center"><label>Rate</label></td>
<td align="center"><label>Amount</label></td>
<td align="center"><label>Inspection&nbsp;Remarks</label></td>

</tr>
<?php $count=1;
 foreach($receipt as $va=>$key){?>
<tr >
<td align="center"><?php echo $count; ?></td>
<td style="padding: 4px;">
<label><?php $prod2=$db_helper_obj->total_product_edit($key["product_id"]);
echo $prod2[0]["name"]; ?></label>
</td>
<td  align="center" style="padding: 4px;">
<label><?php echo $key["qty_received"]; ?></label>
</td>
<td  align="center" style="padding: 4px;">
<label><?php echo $key["qty_approve"]; ?></label>
 </td>
<td  align="center" style="padding: 4px;">
<label><?php echo $key["qty_rejected"]; ?></label>
</td>

<td  align="right" style="padding: 4px;">
<label><?php echo $key["product_price"]; ?></label>
</td>
<td  align="right" style="padding: 4px;">
<label><?php $total_amount+=$key["qty_approve"]*$key["product_price"]; echo $key["qty_approve"]*$key["product_price"]; ?></label>
</td>
<td  align="center" style="padding: 4px;">
<label><?php echo $key["ins_remarks"]; ?></label>
</td>
</tr>
	<?php $count++;  }?>
<tr>
<td rowspan="3" colspan="3"> <label>Remarks:</label><br/><label><?php echo $inward_view[0]["admin_comment"]; ?></label></td>
<td align="center"><label>Total&nbsp;Qty&nbsp;Received</label></td>
<td align="right" > <label><?php echo $qtylist["total_received"]; ?></label></td>
<td align="center"><label>Taxable Value</label></td>
<td align="right"><b><?php echo $total_amount; ?><b></td>
</tr>
<tr>

<td align="center"><label>Total&nbsp;Qty&nbsp;Approved</label></td>
<td align="right"> <label><?php echo $qtylist["total_approve"]; ?></label></td>
<?php if($qtylist["cgst_percent"]!=""){ ?>
<td align="center"><label>CGST:&nbsp;<?php echo $qtylist["cgst_percent"]; ?>&nbsp;%</label></td>
<td align="right"> <label><?php echo $cgst=($total_amount*$qtylist["cgst_percent"])/100; ?></label></td>
<td></td>
<?php } ?>
</tr>
<tr>
<td align="center"><label>Total&nbsp;Qty&nbsp;Rejected</label></td>
<td align="right"> <label><?php echo $qtylist["total_reject"]; ?></label></td>
<?php if($qtylist["sgst_percent"]!=""){ ?>
<td align="center"><label>SGST:&nbsp;<?php echo $qtylist["sgst_percent"]; ?>&nbsp;%</label></td>
<td align="right"> <label><?php echo $sgst=($total_amount*$qtylist["sgst_percent"])/100; ?></label></td>
<td></td>
<?php } ?>
</tr>
<tr>
<td align="center" colspan="2"><label>PROCESSED BY</label></td>
<td align="left" colspan="4"> <label><?php echo $inward_view[0]["admin_processed"]; ?></label></td>
<?php if($qtylist["igst_percent"]!=""){ ?>
<td align="center"><label>IGST:&nbsp;<?php echo $qtylist["igst_percent"]; ?>&nbsp;%</label></td>
<td align="right"> <label><?php echo $igst=($total_amount*$qtylist["igst_percent"])/100; ?></label></td>
<td></td>
<?php } ?>
</tr>
<tr>
<td colspan="5"></td>
<td align="center"><label>GRAND&nbsp;TOTAL</label></td>
<td align="right"> <label><?php echo $total_amount+$cgst+$sgst+$igst; ?></label></td>
<td></td>
</tr>
</table>
</div>
</div>
<script type="text/javascript">
	setTimeout(function(){ parent.$("#iframe_show_vehicle_data").attr("height",$("#height_id").height()+30+"px"); }, 300);
</script>
