<?php function main(){
	$db_query=new db_query();  
	global $db_helper_obj;
	$db_helper_obj=new db_helper();
	$material_list=$db_helper_obj->material_category();	
	if(!empty($_POST)){
		$db_helper_obj->product_update($_GET["product_id"]);
		header('Location:  master_list.php');	//REdirection to master_list.php
    }
	if(!empty($_GET)){
		$master=$db_helper_obj->product_edit($_GET["product_id"]);
	}
	$product_list=$db_helper_obj->total_product_list();
?>
<script type="text/javascript">
function intializeSelect(){
    // this "initializes the boxes"
	$('.removes').each(function(box) {
      var value = $('.removes')[box].value;
      if (value) {
        $('.removes').not(this).find('option[value="' + value + '"]').hide();
      }
    });
};  

// this is called every time a select box changes
$('.removes').on('change', function(event) {
$('.removes').find('option').show();
intializeSelect();
});
function add_order_row(order_row_id)
{	
	var order_item_index=$("#order_item_row_index_"+order_row_id).val();
	order_item_index++;
	var html_content;
	var options=document.getElementById('product_select').innerHTML;
	
//+'<td><select ="" class="form-control" name="name['+order_item_index+']" id="name_'+order_row_id+"_"+order_item_index+'"><option value="">Select</option><option value="Plastic">Plastic</option><option value="Rubber">Rubber</option><option value="Bolts &amp; Nuts">Bolts &amp; Nuts</option><option value="Red Fibre Gasket">Red Fibre Gasket</option></select></td>'
	
	var slect='<select  required onchange="intializeSelect()" id="name_new_'+order_item_index+'" name="name_new['+order_item_index+']" class="form-control removes" >'+options+'</select>';
	html_content='<tr id="order_item_row_'+order_row_id+"_"+order_item_index+'" name="order_item_row_'+order_row_id+"_"+order_item_index+'">'
+'<td>'+slect+'</td>'
+'<td><input type="number" required class="form-control" name="qty_new['+order_item_index+']" id="qty_new_'+order_item_index+'" min=1 value=""></td>'
+'<td><input type="button" class="btn btn-danger remove_class" value="X" onclick="delete_order_item_row(this);remove();"></td></tr>';
	
	div = document.getElementById('order_item_body_'+order_row_id);	
	div.insertAdjacentHTML( 'beforeend', html_content);
	intializeSelect();
	$("#order_item_row_index_"+order_row_id).val(order_item_index);
	remove();
}
function showmaterial(obj){
	if(obj.value==3)
		document.getElementById("material_id").style.display="none";
	else{
		document.getElementById("material_id").style.display="";
	}

}
function delete_order_item_row(element)  
{   
    var row = element.parentNode.parentNode;
    row.parentNode.removeChild(row);
	remove();
}
function cost_add(obj){
var word=obj.id;
var number=word.replace('process_','');	
if(obj.checked==true)
	var check=1;
else if(obj.checked==false)	
	var check=0;
if(obj.checked==true&&document.getElementById('name_'+number).value!=''){
$.ajax({url: "master_costadd.php?sub_id="+number+"&check="+check+"",contentType:false, processData: false, success: function(result){
	var ser = document.getElementById('iframe_show_vehicle_data');
	ser.contentDocument.write(result);
	}
	});	
}else{ 
	$.ajax({url: "master_costadd.php?sub_id="+number+"&check="+check+"",contentType:false, processData: false, success: function(result){
	var ser = document.getElementById('iframe_show_vehicle_data');
	ser.contentDocument.write(result);
	}
	});	
}
}
function cancel_data(){
	var iframe = document.getElementById("iframe_show_vehicle_data");
	var html = "";
	iframe.contentWindow.document.open();
	iframe.contentWindow.document.write(html);
	iframe.contentWindow.document.close();
}
</script>
<style>
	.cost_form { }
	.cost_form input.form-control { margin-bottom:10px; }
</style>
  <form class="form-horizontal" id="recruitment" name="recruitment" method="post">
  <div class="form-group">
	<div class="table-responsive" style="padding-left: 16px;padding-right: 16px;">
	<table id="" class="table listhead table-bordered table-hover">
	
	<tr >
	<td><label>Product Name</label></td>
	<td><input type="text" class="form-control" id="title"  name="title" placeholder="Product Name" required value="<?php echo $master[0]["product_name"] ?>"></td>
	<td><label>Product Series</label></td>
	<td><input type="text" class="form-control" id="series"  name="series" placeholder="Product Series" required value="<?php echo $master[0]["series"] ?>"></td>
	<td><label>Opening Qty</label></td>
	<td><input type="text" class="form-control" id="open_qty"  name="open_qty" placeholder="Opening Qty" required value="<?php echo $master[0]["open_qty"] ?>"></td>
	<td><label>Product Price</label></td>
	<td><input type="text" class="form-control" id="product_price"  name="product_price" placeholder="Product Price" required value="<?php echo $master[0]["product_price"]; ?>"></td>
	</tr>
	</table>
	</div>
  
  <section class="main_content">
	<div class="container-fluid">
    	
    	<div class="row">
        	<div class="col-sm-12">
            	
				<form name="master_add" id="master_add" method="post" target="iframe">
            	<div class="listwrapper">
	<div class="table-responsive"> 
                            <table id="order_item_table_1" class="table listhead table-bordered table-hover">
                                <thead>
								<tr>
									<th>Sub Component Name</th>
									<th>Qty</th>	
									<th></th>
								</tr>
                                </thead>
            <tbody id="order_item_body_<?php echo count($master) ?>">
            <?php $count=1;
			if(!empty($master)){foreach($master as $va=>$key){
			?>
				<tr id="order_item_row_<?php echo count($master) ?>_<?php echo $count; ?>" name="order_item_row_<?php echo count($master) ?>_<?php echo $count; ?>">
				<td>
					<select onchange="intializeSelect()" class="form-control removes" name="name[<?php echo $count; ?>]" required>
							<option value="">Select	</option>
							<?php foreach($product_list as $va1=>$key1){ ?>
							<option <?php if($key["name"]==$key1["id"]){ ?> selected <?php } ?> value="<?php echo $key1["id"]; ?>"><?php echo $key1["name"]; ?></option>
							<?php } ?>
					</select>
					</td>
					
				    <td>
                        <input type="number" min=1 class="form-control" name="qty[<?php echo $count; ?>]" id="qty_<?php echo $count; ?>" value="<?php echo $key["qty"]; ?>" required />
                    </td>					
                    
					<td style="text-align:left;">
						<input type="button" class="btn btn-danger remove_class" value="X" onclick="delete_order_item_row(this);"/>
					</td>	
					<div id="product_select" style="display:none;">				
						<option value="">Select	</option>
						<?php foreach($product_list as $va1=>$key1){ ?>
							<option value="<?php echo $key1["id"]; ?>"><?php echo $key1["name"]; ?></option>
						<?php } ?>
					</div>
					
                				
               <input type="hidden" name="product_id" id="product_id"	value="<?php echo $key["id"]; ?>"/>
			  </tr>
			  <input type="hidden" name="sub_product_id[<?php echo $count; ?>]" id="sub_product_id<?php echo $count; ?>"	value="<?php echo $key["sub_product_id"]; ?>"/>
			<?php $count++;} } ?>
            </tbody>
                            </table>
							
       <input type="hidden" value="<?php echo count($master) ?>" id="order_item_row_index_<?php echo count($master) ?>" name="order_item_row_index_<?php echo count($master) ?>">
    </div>
	  <div class="addnew_wrapp clearfix">
		 <div class="addnew" align="right">       
		<a href="#" onclick="add_order_row(<?php echo count($master) ?>);" class="" style="padding-top: 8px;padding-bottom: 8px;padding-left: 3px;padding-right: 3px;"> ADD NEW</a>
	    </div></div>
	    <br/>
		<br/>
                  </div>  <!------End--- Table Responsive -->
                </div>
				<div class="col-md-12">
			 <button type="button" onclick="window.location.href='master_list.php'" class="btn btn-primary">Cancel</button>
			 <button type="submit" class="btn btn-primary">Submit</button>
		</div>
		</form>
            </div>
        </div>
	</section>
	<div aria-hidden="true"  aria-labelledby="myModal" role="dialog" tabindex="-1" id="myModal" class="purchase modal fade">
	  <div class="modal-dialog">
		  <div class="modal-content">
			  <div class="modal-header">
				  <button type="button" class="close" onclick="cancel_data()" data-dismiss="modal" aria-hidden="true">&times;</button>
				  <h3 class="modal-title text-center">Master View</h3>
				  
			  </div>
			  <div class="modal-body">
			  <iframe id="iframe_show_vehicle_data" frameborder="0" marginwidth="0" marginheight="0"  scrolling="no"  style="width:100%;border:0px;">
			  </iframe>						
			  <div class="modal-footer">
			  </div>                          
			  </div>                          
		  </div>
	  </div>
	</div>
	</div>
      </form>
<?php } include("template.php"); ?>  
<script type="text/javascript">
remove();
function remove(){
	var count=0;
	$(".remove_class").each(function(){		
		if(count==0)
			$(this).first().attr("disabled", true);
		count++;
	});
	if(count>=2)
			$(".remove_class").attr("disabled", false);
}
</script>