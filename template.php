<?php 
include("config/config.php");
include("lng/language.php");

if(!isset($_SESSION["user_id"])&&!isset($_SESSION["paswrd"])){ // checking whether the usertype is logged in
	header('Location: logout.php');        						//REdirection to logout.php
	exit;
}

include("core/class/db_query.php");                             // Class where query generetion is written
include("core/class/db_helper.php");                            // Class where table and feilds 
include("core/function/common.php");     
                                                  
ob_start();                                                     // to clear the internal output
global $db_helper_obj;
global $global_website_url;
$db_helper_obj=new db_helper();                                 // Creating object to get data from table
$current_page = basename($_SERVER['PHP_SELF']); // to get the page name from url
global $month_name_arr;
$month_name_arr=array('1'=>'Jan','2'=>'Feb','3'=>'Mar','4'=>'Apr','5'=>'May','6'=>'Jun','7'=>'Jul','8'=>'Aug','9'=>'Sep','10'=>'Oct','11'=>'Nov','12'=>'Dec');
//echo"<pre>";print_r($_SESSION["user_type"]);echo"</pre>";
$pages_list=$db_helper_obj->pages_list($_SESSION["user_type"]);

foreach($pages_list as $va=>$key){
	//echo"<pre>";print_r($key["page_name"].".php");echo"</pre>";
	$pagename[]=$key["page_name"].".php";
	$currentname=str_replace("/metamatix/","",$_SERVER["SCRIPT_NAME"]);
	
		
}
if(!in_array($currentname,$pagename)){
	header("Location:error.php");
	
}

$dash_sty="";
$master_sty="";
$supplier_sty="";
$purchase_sty="";
$settings_sty="";
$home_sty="";
$inward_sty="";
$process_sty="";
if (strpos($_SERVER["SCRIPT_NAME"], 'master_') !== false){
	$master_sty="style='background:#da5527; color:#fff;'";
	$header="Category Master";
}else if(strpos($_SERVER["SCRIPT_NAME"], 'supplier_') !== false){
	$supplier_sty="style='background:#da5527; color:#fff;'";
	$header="Supplier Master";
}else if(strpos($_SERVER["SCRIPT_NAME"], 'employee') !== false){
	$emp_sty="style='background:#da5527; color:#fff;'";
	$header="Employee List" ;
}
else if(strpos($_SERVER["SCRIPT_NAME"], 'machiningappr_list') !== false){
	$emp_sty="style='background:#da5527; color:#fff;'";
	$header="Machining Approval" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'empdelete_his') !== false){
	$emp_sty="style='background:#da5527; color:#fff;'";
	$header="Employee Delete Log" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'order_history') !== false){
	$receipt_sty="style='background:#da5527; color:#fff;'";
	$header="Order History" ;
}
else if(strpos($_SERVER["SCRIPT_NAME"], 'received') !== false){
	$rece_sty="style='background:#da5527; color:#fff;'";
	$header="Received List" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'dashboard') !== false){
	$dash_sty="style='background:#da5527; color:#fff;'";
	$header="Dashboard" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'inward_reg') !== false){
	$inwardreg_sty="style='background:#da5527; color:#fff;'";
	$header="Raw Material Inward Register" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'inward_') !== false){
	$inward_sty="style='background:#da5527; color:#fff;'";
	$header="Inward" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'inwardappr') !== false){
	$inwardappr_sty="style='background:#da5527; color:#fff;'";
	$header="Inward Approval" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'buffingappr_') !== false){
	$buffingappr_sty="style='background:#da5527; color:#fff;'";
	$header="Buffing Approval" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'payment_') !== false){
	$payment_sty="style='background:#da5527; color:#fff;'";
	$header="Payment List" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'invent') !== false){
	$invent_sty="style='background:#da5527; color:#fff;'";
	$header="Inventory Approval" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'buffing_reject') !== false){
	$buffingrjct_sty="style='background:#da5527; color:#fff;'";
	$header="Buffing Rejection" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'reject_reg') !== false){
	$rejectreg_sty="style='background:#da5527; color:#fff;'";
	$header="Rejection List" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'finishedapprove_rej') !== false){
	$reject_sty="style='background:#da5527; color:#fff;'";
	$header="FG Rejection" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'reject') !== false){
	$reject_sty="style='background:#da5527; color:#fff;'";
	$header="Inward Rejection" ;
}
else if(strpos($_SERVER["SCRIPT_NAME"], 'process') !== false){
	$process_sty="style='background:#da5527; color:#fff;'";
	$header="Buffing" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'receipt') !== false){
	$receipt_sty="style='background:#da5527; color:#fff;'";
	$header="Receipt" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'order') !== false){
	$order_sty="style='background:#da5527; color:#fff;'";
	$header="Purchase Order" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'fgreport_history') !== false){
	$report_sty="style='background:#da5527; color:#fff;'";
	$header="FG History" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'report_his') !== false){
	$reporthis_sty="style='background:#da5527; color:#fff;'";
	$header="Bill Process History" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'finished_report') !== false){
	$report_sty="style='background:#da5527; color:#fff;'";
	$header="FG Report" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'report') !== false){
	$report_sty="style='background:#da5527; color:#fff;'";
	$header="Bill Process Report" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'stock_reg') !== false&&$_GET["mate"]==2){
	$receipt_sty="style='background:#da5527; color:#fff;'";
	$header="Plastic & Rubber Stock" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'stock_reg') !== false&&$_GET["mate"]==3){
	$receipt_sty="style='background:#da5527; color:#fff;'";
	$header="Consumable Stock" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'stock_reg') !== false&&$_GET["mate"]==4){
	$receipt_sty="style='background:#da5527; color:#fff;'";
	$header="Bought out items Stock" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'stock_') !== false){
	$stock_re_sty="style='background:#da5527; color:#fff;'";
	$header="Open Stock Master" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'category_') !== false){
	$receipt_sty="style='background:#da5527; color:#fff;'";
	$header="Sub Product Master" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'finish') !== false){
	$receipt_sty="style='background:#da5527; color:#fff;'";
	$header="FG Approval" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'rdproduct_') !== false){
	$receipt_sty="style='background:#da5527; color:#fff;'";
	$header="R & D Products" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'stock') !== false&&$_GET["mate"]==1){
	$receipt_sty="style='background:#da5527; color:#fff;'";
	$header="Raw Material Stock" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'stock') !== false&&$_GET["mate"]==2){
	$receipt_sty="style='background:#da5527; color:#fff;'";
	$header="Machined Stock" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'stock') !== false&&$_GET["mate"]==3){
	$receipt_sty="style='background:#da5527; color:#fff;'";
	$header="Buffed Stock" ;
}
else if(strpos($_SERVER["SCRIPT_NAME"], 'stock') !== false&&$_GET["mate"]==4){
	$receipt_sty="style='background:#da5527; color:#fff;'";
	$header="Consolidation Stock" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'machiningapprove_') !== false){
	$receipt_sty="style='background:#da5527; color:#fff;'";
	$header="Machining Approval" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'account') !== false){
	$receipt_sty="style='background:#da5527; color:#fff;'";
	$header="Accounts" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'pointent_list') !== false){
	$receipt_sty="style='background:#da5527; color:#fff;'";
	$header="Purchase Intent";
}else if(strpos($_SERVER["SCRIPT_NAME"], 'pointentapprove_') !== false){
	$receipt_sty="style='background:#da5527; color:#fff;'";
	$header="Purchase Intent" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'consumableappr_') !== false||strpos($_SERVER["SCRIPT_NAME"], 'consumable_list') !== false){
	$receipt_sty="style='background:#da5527; color:#fff;'";
	$header="Consumables" ;
}

else if(strpos($_SERVER["SCRIPT_NAME"], 'jobout_history') !== false){
	$receipt_sty="style='background:#da5527; color:#fff;'";
	$header="Job Out History" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'jobin_history') !== false){
	$receipt_sty="style='background:#da5527; color:#fff;'";
	$header="Job In History" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'consumable_history') !== false){
	$receipt_sty="style='background:#da5527; color:#fff;'";
	$header="Consumable History" ;
}
else if(strpos($_SERVER["SCRIPT_NAME"], 'deliverymo_list') !== false){
	$receipt_sty="style='background:#da5527; color:#fff;'";
	$header="Job Out" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'deliverymi_list') !== false){
	$receipt_sty="style='background:#da5527; color:#fff;'";
	$header="Job In" ;
}
else if(strpos($_SERVER["SCRIPT_NAME"], 'steel_list') !== false){
	$receipt_sty="style='background:#da5527; color:#fff;'";
	$header="Steel List" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'steelapprove_list') !== false){
	$receipt_sty="style='background:#da5527; color:#fff;'";
	$header="Steel Approve List" ;
}
else if(strpos($_SERVER["SCRIPT_NAME"], 'scrap_list') !== false){
	$receipt_sty="style='background:#da5527; color:#fff;'";
	$header="Scrap History" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'scrap_history') !== false){
	$receipt_sty="style='background:#da5527; color:#fff;'";
	$header="Scrap Consolidation History" ;
}else if(strpos($_SERVER["SCRIPT_NAME"], 'scrap_movement') !== false){
	$receipt_sty="style='background:#da5527; color:#fff;'";
	$header="Scrap Movement" ;
}
else{
	$purchase_sty="style='background:#da5527; color:#fff;'";
	$header="MASTER" ;
}
?>
<!DOCTYPE html>
<html>
<head>
<style>
.logoutbtn
{
	position:fixed;
	right:15px;
	top:25px;
}
</style>
  <meta charset="UTF-8">
  <title><?php echo META; ?></title>

  <link rel="icon" type="images/png" href="">
  <link rel='stylesheet' href='css/font-awesome.css'>
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/responsive-menu.css">
  <link rel='stylesheet' href='https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css'>
  <link rel="stylesheet" href="css/style.css">
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap.js"></script>
  <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
  <link href="css/select2.min.css" rel="stylesheet" />
  <script src="js/select2.min.js"></script>
     <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	 <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>

<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" />
  <script src="js/modernizr-custom.js"></script>
  <script src="js/responsive-menu.js"></script>
  <script>window.jQuery || document.write('<script src="js/jquery.js"><\/script>')</script>
	<script>
		jQuery(function ($) {
			var menu = $('.rm-nav').rMenu({				              
			});
		});
		$('select').select2();
	</script>
</head>

<body>
<header>
	<div class="navbar navbar-default" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
		  <?php if($_SESSION["user_type"]==1){
					$url='dashboard.php';
					$user='Super Admin';					
		  }else if($_SESSION["user_type"]==2){
					$url='inward_register.php';
					$user='Admin & Accounts Department';	
		  }else if($_SESSION["user_type"]==3){
					$url='inwardappr_list.php';
					$user='Quality Department';
		  }else if($_SESSION["user_type"]==4){
					$url='inward_list.php';
					$user='Production Department';
		  } ?>
          <a class="navbar-brand"  href="<?php echo $url; ?>" ><img src="images/Metamatix_logo.png" style="width: 190px;height: 52px;" alt="metamatix" class="img-responsive" /></a>
        </div>
        <div class="navbar-collapse collapse in">
        <ul class="nav navbar-nav">
		<?php
			if($_SESSION["user_type"]==1){ ?>
			<li style="padding-top: 12px;"><a id="dLabel" role="button" class="userbtn btn btn-primary" data-target="#"  href="dashboard.php">Dashboard</a></li>
			<li style="padding-top: 12px;"><a id="dLabel" role="button" class="userbtn btn btn-primary" data-target="#"  href="inward_register.php">Home</a></li>
			<li style="padding-top: 12px;" class="dropdown">
			<a  <?php echo $inwardreg_sty; ?> <?php echo $reject_sty; ?>  id="dLabel" role="button" data-toggle="dropdown" class="userbtn btn btn-primary" data-target="#" href="#">
			Inward <span class="caret"></span>
			</a>
			<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
			<li class="dropdown-submenu">
			<a tabindex="-1" href="#">Inventory Approval</a>
			<ul class="dropdown-menu">

			<li><a tabindex="-1" <?php echo $inwardreg_sty; ?> href="inward_register.php" >Inward Register</a></li>
			<li><a tabindex="-1" <?php echo $reject_sty; ?> href="reject_list.php">Inward Rejection</a></li>
			</ul>
			</li>
			</ul>
			</li>
			<li class="dropdown" style="padding-top: 12px;"><a <?php echo $process_sty; ?> id="dLabel" role="button" class="userbtn btn btn-primary" data-target="#"  href="finished_stocks.php">Finished Stocks</a></li>
           
           
			<li class="dropdown" style="padding-top: 12px;"><a <?php echo $process_sty; ?> id="dLabel" role="button" class="userbtn btn btn-primary" data-target="#"  href="process_lists.php">Buffing</a></li>
			
           			
			<li class="dropdown" style="padding-top: 12px;">
			<a id="dLabel"  <?php echo $reporthis_sty; ?>  role="button" data-toggle="dropdown" class="userbtn btn btn-primary" data-target="#" href="#">
			Report <span class="caret"></span>
			</a>
			<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
			<li><a <?php echo $reporthis_sty; ?> href="report_history.php">Bill Process History</a></li>
			</ul>
			</li>
			<li class="dropdown" style="padding-top: 12px;">
			<a id="dLabel"  <?php echo $master_sty; ?> <?php echo $supplier_sty; ?>  role="button" data-toggle="dropdown" class="userbtn btn btn-primary" data-target="#" href="#">
			Master <span class="caret"></span>
			</a>
			<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
			<li><a  <?php echo $master_sty; ?> href="master_list.php">Category Master</a></li>
			<li><a  <?php echo $master_sty; ?> href="openstock_list.php">Open Stock Master</a></li>
			<li><a  <?php echo $master_sty; ?> href="category_list.php">Sub Product Master</a></li>
			<li><a  <?php echo $supplier_sty; ?> href="supplier_list1.php">Supplier Master</a></li>
			<li><a  <?php echo $supplier_sty; ?> href="stock_adjust.php">Stock Adjustment  Entry							
</a></li>
			</ul>
			</li>
			<li style="padding-top: 12px;"><a <?php echo $inward_sty; ?>  id="dLabel" role="button" class="userbtn btn btn-primary" data-target="#"  href="rdproduct_list.php">R & D Products</a></li>
			
			
			<?php }else if($_SESSION["user_type"]==2){  ?>
			<li style="padding-top: 12px;"><a id="dLabel" role="button" class="userbtn btn btn-primary" data-target="#"  href="inward_register.php">Home</a></li>
			<li class="dropdown" style="padding-top: 12px;">
			<a id="dLabel"  <?php echo $invent_sty; ?> <?php echo $stock_re_sty; ?> <?php echo $inwardreg_sty; ?> <?php echo $reject_sty; ?>  role="button" data-toggle="dropdown" class="userbtn btn btn-primary" data-target="#" href="#">
			Inward<span class="caret"></span>
			</a>
			<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
			<li class="dropdown-submenu">
			<a tabindex="-1" href="#">Inventory Approval</a>
			<ul class="dropdown-menu">
			<li><a <?php echo $invent_sty; ?> href="inventory_list.php">Approval</a></li>
			<li><a <?php echo $invent_sty; ?> href="invoice_list.php">Invoice Generation</a></li>
			<li><a <?php echo $inwardreg_sty; ?> href="inward_register.php">Inward Register</a></li>
			<li><a <?php echo $reject_sty; ?> href="reject_list.php">Inward Rejection</a></li>
			</ul>
			</li>
			
			<li class="dropdown-submenu" >
			<a tabindex="-1" href="#">Stock Register</a>
			<ul class="dropdown-menu">
			<li><a href="stock.php?mate=4">Consolidation Stock</a></li>
			<li><a href="stock.php?mate=1">Raw Material Stock</a></li>
			<li><a href="stock.php?mate=2">Machined Stock</a></li>
			<li><a href="stock.php?mate=3">Buffed Stock </a></li>
			
			<!-- <li><a href="stock_register.php?mate=1">Raw Material</a></li> -->
			<li><a href="stock_register.php?mate=2">Plastic & Rubber</a></li>
			<li><a href="stock_register.php?mate=3">Consumables</a></li>
			<!--<li class="dropdown-submenu">
			<a tabindex="-1" href="#">Consumables</a>
			<ul class="dropdown-menu">
			<li><a href="stock_register.php?mate=3">Consumables</a></li>
			<li><a href="consumptionmate.php">Consumables Approve</a></li>
			</ul>-->
			</li>
			<!--<li><a href="stock_register.php?mate=3">Consumables</a></li>-->
			<li><a href="stock_register.php?mate=4">Bought Out Items</a></li>
			<li><a href="stock_register.php?mate=6">Fixed Assets</a></li>
			</ul>
			</li>
			</ul>
			</li>
			
			<li class="dropdown" style="padding-top: 12px;">
			<a id="dLabel" <?php echo $emp_sty; ?> <?php echo $master_sty; ?> <?php echo $order_sty; ?>  role="button" data-toggle="dropdown" class="userbtn btn btn-primary" data-target="#" href="#">
			Master <span class="caret"></span>
			</a>
			<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
			<li class="dropdown-submenu">
			<a tabindex="-1" href="#">Employee Master</a>
			<ul class="dropdown-menu">
			<li><a tabindex="-1" href="employee_list.php">Employee list</a></li>
			<li><a tabindex="-1" href="empdelete_history.php">Employee Delete Log</a></li>
			</ul>
			</li>
			<!--<li><a href="master_request.php">Product Master</a></li>-->
			<li><a href="order_list.php">Purchase Order</a></li>
			</ul>
			</li>
			 
			<li class="dropdown" style="padding-top: 12px;">
			<a id="dLabel" <?php echo $report_sty; ?> <?php echo $reporthis_sty; ?> <?php echo $payment_sty; ?> <?php echo $buffingrjct_sty; ?> role="button" data-toggle="dropdown" class="userbtn btn btn-primary" data-target="#" href="#">
			Buffing <span class="caret"></span>
			</a>
			<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
			<li><a href="payment_list.php">Buffing Approval</a></li>
			<li><a href="buffing_reject.php">Buffing Rejection</a></li>
			<li class="dropdown-submenu">
			<a tabindex="-1" href="#">Report</a>
			<ul class="dropdown-menu">
			<li><a tabindex="-1" href="report.php">Bill Process Report</a></li>
			<li><a tabindex="-1" href="report_history.php">Bill Process History</a></li>
			</ul>
			</li>
			</ul>
			</li>
			
			<li class="dropdown" style="padding-top: 12px;">
			<a id="dLabel" <?php echo $report_sty; ?> <?php echo $reporthis_sty; ?> <?php echo $payment_sty; ?> <?php echo $buffingrjct_sty; ?> role="button" data-toggle="dropdown" class="userbtn btn btn-primary" data-target="#" href="#">
			Machining<span class="caret"></span>
			</a>
			<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
			<li><a href="machiningapprove_list.php">Machining Approval</a></li>
			<!-- <li><a href="machining_reject.php">Machining Rejection</a></li>	-->
			
			</ul>
			</li>
			
			<li class="dropdown" style="padding-top: 12px;">
			<a id="dLabel" <?php echo $emp_sty; ?> <?php echo $master_sty; ?> <?php echo $order_sty; ?>  role="button" data-toggle="dropdown" class="userbtn btn btn-primary" data-target="#" href="#">
			Scrap <span class="caret"></span>
			</a>
			<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">			
			<li><a href="scrap_history.php">Scrap Management</a></li>
			<li><a href="scrap_movement.php">Scrap Movement</a></li>			 
			</ul>
			</li>
			
			
			
			<!--
			<li class="dropdown" style="padding-top: 12px;"><a <?php echo $process_sty; ?> id="dLabel" role="button" class="userbtn btn btn-primary" data-target="#"  href="machiningapprove_list.php">Machining</a></li> -->
			
			<li class="dropdown" style="padding-top: 12px;">
			<a id="dLabel" <?php echo $report_sty; ?> <?php echo $reporthis_sty; ?> <?php echo $payment_sty; ?> <?php echo $buffingrjct_sty; ?> role="button" data-toggle="dropdown" class="userbtn btn btn-primary" data-target="#" href="#">
			Finished Goods <span class="caret"></span>
			</a>
			<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
			<li><a href="finishedapprove_list.php">FG Approval</a></li>
			<li><a href="finishedapprove_reject.php">FG Rejection</a></li>
			<li class="dropdown-submenu">
			<a tabindex="-1" href="#">Report</a>
			<ul class="dropdown-menu">
			<li><a tabindex="-1" href="finished_report.php">FG Report</a></li>
			<li><a tabindex="-1" href="finished_stocks.php?from=exl">FG Report Excel</a></li>
			<li><a tabindex="-1" href="fgreport_history.php">FG History</a></li>
			</ul>
			</li>
			</ul>
			</li>
			<li class="dropdown" style="padding-top: 12px;">
			<a id="dLabel" <?php echo $inward_sty; ?> role="button" data-toggle="dropdown" class="userbtn btn btn-primary" data-target="#" href="#">
			Accounts<span class="caret"></span>
			</a>
			<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
			<li><a href="account.php">Accounts</a></li>
			<!--<li><a href="pointent_report.php">Consolidation Report</a></li>	-->
			</ul>
			</li>
			
			<li class="dropdown" style="padding-top: 12px;">
			<a id="dLabel" <?php echo $inward_sty; ?> role="button" data-toggle="dropdown" class="userbtn btn btn-primary" data-target="#" href="#">
			Purchase<span class="caret"></span>
			</a>
			<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
			<li><a <?php echo $inward_sty; ?>  id="dLabel" role="button" class="userbtn btn btn-primary" data-target="#"  href="pointentapprove_list.php">Purchase Intent</a></li>
			<li><a <?php echo $inward_sty; ?>  id="dLabel" role="button" class="userbtn btn btn-primary" data-target="#"  href="rdproduct_list.php">R & D Products</a></li>
			</ul>
			</li>
			
			<!--<li style="padding-top: 12px;"><a <?php echo $inward_sty; ?>  id="dLabel" role="button" class="userbtn btn btn-primary" data-target="#"  href="pointent_report.php">Consolidation Report</a></li>
			<li style="padding-top: 12px;"><a <?php echo $inward_sty; ?>  id="dLabel" role="button" class="userbtn btn btn-primary" data-target="#"  href="account.php">Accounts</a></li>-->
			
			<li style="padding-top: 12px;"><a id="dLabel" role="button" class="userbtn btn btn-primary" data-target="#" href="consumableappr_list.php">Consumables</a></li>
			
			<li class="dropdown" style="padding-top: 12px;">
			<a id="dLabel"  role="button" data-toggle="dropdown" class="userbtn btn btn-primary" data-target="#" href="#">
			History <span class="caret"></span>
			</a>
			<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
			<li><a  href="order_history.php">Order History</a></li>
			<li><a href="jobout_history.php">Job Out History</a></li>
			<li><a href="jobin_history.php">Job In History</a></li>
			<li><a href="consumable_history.php">Consumable History</a></li>			
			<li><a href="scrap_list.php">Scrap History</a></li>
			
			</ul>
			</li>			
			
			<?php }else if($_SESSION["user_type"]==3){ ?>
			<li style="padding-top: 12px;"><a id="dLabel" role="button" class="userbtn btn btn-primary" data-target="#"  href="inwardappr_list.php">Home</a></li>
			<li style="padding-top: 12px;"><a <?php echo $inwardappr_sty; ?> id="dLabel" role="button" class="userbtn btn btn-primary" data-target="#"  href="inwardappr_list.php">INWARD APPROVAL</a></li>
			<li style="padding-top: 12px;"><a <?php echo $inward_sty; ?>  id="dLabel" role="button" class="userbtn btn btn-primary" data-target="#"  href="steelapprove_list.php">Steel Approval</a></li>
			<li style="padding-top: 12px;"><a <?php echo $buffingappr_sty; ?>  id="dLabel" role="button" class="userbtn btn btn-primary" data-target="#"  href="buffingappr_list.php">Buffing APPROVAL</a></li>
			<li style="padding-top: 12px;"><a <?php echo $buffingappr_sty; ?>  id="dLabel" role="button" class="userbtn btn btn-primary" data-target="#"  href="machiningappr_list.php">MACHINING APPROVAL</a></li>
			<li style="padding-top: 12px;" ><a <?php echo $process_sty; ?> id="dLabel" role="button" class="userbtn btn btn-primary" data-target="#"  href="finishedappr_list.php">FG Movement</a></li>
			<?php }else if($_SESSION["user_type"]==4){ ?>
			<li style="padding-top: 12px;"><a id="dLabel" role="button" class="userbtn btn btn-primary" data-target="#"  href="inward_list.php">Home</a></li>
			<li style="padding-top: 12px;"><a <?php echo $inward_sty; ?>  id="dLabel" role="button" class="userbtn btn btn-primary" data-target="#" href="inward_list.php">Inward</a></li>
			 
			
			<li class="dropdown" style="padding-top: 12px;">
			<a id="dLabel" <?php echo $report_sty; ?> <?php echo $reporthis_sty; ?> <?php echo $payment_sty; ?> <?php echo $buffingrjct_sty; ?> role="button" data-toggle="dropdown" class="userbtn btn btn-primary" data-target="#" href="#">
			Stock Register<span class="caret"></span>
			</a>
			<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
			<li><a href="stock.php?mate=1">Raw Material Stock</a></li>
			<li><a href="stock.php?mate=2">Machined Stock</a></li>
			<li><a href="stock.php?mate=3">Buffed Stock </a></li>
			
			<li><a href="stock_register.php?mate=2">Plastic & Rubber</a></li>
			<li><a href="stock_register.php?mate=3">Consumables</a></li>
			
			<li><a href="stock_register.php?mate=4">Bought Out Items</a></li>
			</ul>
			</li>
			 
			
			
			
			<li style="padding-top: 12px;"><a <?php echo $inward_sty; ?>  id="dLabel" role="button" class="userbtn btn btn-primary" data-target="#" href="steel_list.php">Steel</a></li>
			
			<li style="padding-top: 12px;" ><a <?php echo $process_sty; ?> id="dLabel" role="button" class="userbtn btn btn-primary" data-target="#" href="process_lists.php">Buffing</a></li>
			
			<!-- <li style="padding-top: 12px;" ><a <?php echo $process_sty; ?> id="dLabel" role="button" class="userbtn btn btn-primary" data-target="#"  href="machining_lists.php">Machining</a></li> -->
			
			<li class="dropdown" style="padding-top: 12px;">
			<a id="dLabel" <?php echo $report_sty; ?> <?php echo $reporthis_sty; ?> <?php echo $payment_sty; ?> <?php echo $buffingrjct_sty; ?> role="button" data-toggle="dropdown" class="userbtn btn btn-primary" data-target="#" href="#">
			JOB WORK<span class="caret"></span>
			</a>
			<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
			<li><a href="deliverymo_list.php">Job Out</a></li>
			<li><a href="deliverymi_list.php">Job In</a></li>	
			<li class="dropdown-submenu">
			<a tabindex="-1" href="#">History</a>
			<ul class="dropdown-menu">
			<li><a tabindex="-1" href="jobout_history.php">Job Out History</a></li>
			<li><a tabindex="-1" href="jobin_history.php">Job In History</a></li>
			<!--<li><a tabindex="-1" href="consolidatejob_history.php">Consolidate Job History</a></li>-->
			</ul>
			</li>
			</ul>
			</li>.
			<li class="dropdown" style="padding-top: 12px;"><a <?php echo $process_sty; ?> id="dLabel" role="button" class="userbtn btn btn-primary" data-target="#"  href="finished_stocks.php">Finished Stocks</a></li>			
			<li style="padding-top: 12px;" ><a <?php echo $process_sty; ?> id="dLabel" role="button" class="userbtn btn btn-primary" data-target="#"  href="finished_list.php">FG Movement</a></li>
			<li class="dropdown" style="padding-top: 12px;">
			<a id="dLabel" <?php echo $inward_sty; ?> role="button" data-toggle="dropdown" class="userbtn btn btn-primary" data-target="#" href="#">
			Purchase<span class="caret"></span>
			</a>
			<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
			<li><a <?php echo $inward_sty; ?>  id="dLabel" role="button" class="userbtn btn btn-primary" data-target="#"  href="pointent_list.php">Purchase Intent</a></li>
			<li><a <?php echo $inward_sty; ?>  id="dLabel" role="button" class="userbtn btn btn-primary" data-target="#"  href="rdproduct_list.php">R & D Products</a></li>
			</ul>
			</li>
			
			<!-- <li style="padding-top: 12px;"><a <?php echo $inward_sty; ?>  id="dLabel" role="button" class="userbtn btn btn-primary" data-target="#"  href="pointent_list.php">Purchase Intent</a></li>
			<li style="padding-top: 12px;"><a <?php echo $inward_sty; ?>  id="dLabel" role="button" class="userbtn btn btn-primary" data-target="#"  href="rdproduct_list.php">R & D Products</a></li>
			-->
			<li style="padding-top: 12px;"><a id="dLabel" role="button" class="userbtn btn btn-primary" data-target="#" href="consumable_list.php">Consumables</a></li>
			<?php } ?>		
		
			 <a style="padding-top: 13px;" class="navbar-brand"  href="logout.php" ><input type="image" src="images/logout.png" href="logout.php" alt="logout" width="37" height="46" class="img-responsive" /></a>
		</ul>

         <!-- <div class="dropdown clientinfo">
			 <a style="padding-top: 0px;" class="navbar-brand"  href="logout.php" ><input type="image" src="images/logout.png" href="logout.php" alt="logout" width="46" height="46" class="img-responsive" /></a>
		 <a class="navbar-brand"  href="logout.php"><img src="images/logout.png" style="width: 225px;height: 52px;" alt="logout" class="img-responsive" /><?php echo LOUT; ?></a>
           <a id="dLabel" style="" role="button" class="userbtn btn btn-primary" data-target="#" href="logout.php"  ><?php //echo LOUT; ?></a>-->
         </div>
         
        </div><!--/.nav-collapse -->
        
      </div>
    </div>
</header>

	<div class="container-fluid nopadd">
	<h3 class="" style="color:#DE610E;margin-left: 14px;"><?php echo $user; ?></h3>
         <h2 class="text-center" style="margin-top: -2.5%;"><?php echo $header; ?></h2>
    </div>
<section class="main_content">
	 <?php main();?> 
</section>
<div id="temp" style="margin-bottom: 70px;"></div>          
<footer>
	<div class="container-fluid nopadd">
        <div class="footer">
        	<p class="text-center">&copy; <?php echo date("Y"); ?> All Rights Reserved. <a class="footer_jtrack" href="#"><!--Metamatix--></a>  </p>
        </div>
    </div>
</footer>
</body>
</html>
<script> 
function scrollbotm(){
$("html, body").animate({ scrollTop: $(document).height() },"slow"); 
add_order_row(1);
}
$(document).ready(function(){
 
			// For the Second level Dropdown menu, highlight the parent	
			$( ".dropdown-menu" )
			.click(function() {
				$(this).parent('li').addClass('active');
			})
			.mouseleave(function() {
				$(this).parent('li').removeClass('active');
			});
			$('#example').DataTable({
				"columnDefs": [{
				"targets": 'no-sort',		   
				"orderable": false,
				scrollY: 20,
				scroller: {
				loadingIndicator: true
				}
				}]
			});	
		 
		});
	 
		 function isNumberKey(evt){
          var charCode = (evt.which) ? evt.which : event.keyCode
          if (((charCode == 46 && charCode > 31)||(charCode == 45 && charCode > 31 )) 
            && (charCode < 48 || charCode > 57))
             return false;
          return true;
       }

</script>