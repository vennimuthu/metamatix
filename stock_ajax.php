<?php 
//error_reporting(0);
include("config/config.php");
if(!isset($_SESSION["user_id"])){
	//checking whether the usertype is logged in
	header('Location: logout.php');
	exit;
}
include("core/class/db_query.php");//Class where query generetion is written
include("core/class/db_helper.php");//Class where table and feilds 
include("core/function/common.php"); 
$db_query=new db_query();  
global $db_helper_obj;
$db_helper_obj=new db_helper();
$report_list=$db_helper_obj->report_list();

	$inward_approve_list=$db_helper_obj->inward_approve_list();	
	$buffing_list1=$db_helper_obj->buffing_list();	
	$product_list=$db_helper_obj->total_product_edit($_GET["product_id"]); 
	$delivery_list=$db_helper_obj->deliverymi_list();
	$price_historylist=$db_helper_obj->price_historylist();

	foreach($delivery_list as $va=>$key){
		$material=convert_array($key["products"]);
		foreach($material["product_id"] as $va1=>$key1){
			$material_inqty[$key1]=$material["qty"][$va1];
		}
	}
	
	foreach($buffing_list1 as $va=>$key){
		if($key["admin_approved"]==1){
			$buffing_list[]=$key;	
		}
	}
	
	// $buffing_list=$db_helper_obj->buffing_list();
	// foreach($buffing_list as $va=>$key){
	// $key["dated"]=date("d-m-Y",$key["buffing_date"]);	
	// $approval_list[$key["dated"]][]=$key;
	// }
	foreach($buffing_list as $va1=>$key1){		
		$products=convert_array($key1["products_approve"]);
		
		foreach($products as $va2=>$key2){
			$approve_arry[$key2["product_id"]]["qty"]+=$key2["approve"];
			$buff_list[$key1["bpl_no"]][$va2]["date"]=date("d-m-Y",$key1["buffing_date"]);
			$buff_list[$key1["bpl_no"]][$va2]["product_id"]=$key2["product_id"];
			$buff_list[$key1["bpl_no"]][$va2]["qty"]=$key2["approve"];
		}
	
	}	
    $order_list=$db_helper_obj->order_qty();
	
	foreach($order_list as $va=>$key){
		$order_list1[$key["product_id"]]["qty"]+=$key["qty"];
		$order_ids[]=$key["product_id"];
		$order_date=$db_helper_obj->order_dates($key["order_id"]);
	}
	 
	$inward_view=$db_helper_obj->inward_approve_admin();
	$qty=array();
	$price=array();
	if(empty($buffing_list)){
	foreach($inward_view as $va=>$key){
		$receipt[$key["id"]]=convert_array($key["products"]);	 
		foreach($receipt[$key["id"]] as $va1=>$key1){
			//echo"<pre>";print_r($key1);echo"</pre>";
			$price_history=$db_helper_obj->price_history($key1["product_id"]);			
			$qty[$key1["product_id"]]+=$key1["qty_approve"];
			$price[$key1["product_id"]]+=$key1["qty_approve"]*$key1["product_price"];
		}
	}
	}
  
	
	foreach($price as $va1=>$key1){
		$actual_price[$va1]=$key1/$qty[$va1];
	}
	 
	$final_arry=array();
	foreach($product_list as $va=>$key){
		$products=$db_helper_obj->openstock_edit1($key["id"]);		
		$totalproduct=$db_helper_obj->total_product_edit($key["id"]);
		if(!empty($order_list1)){
			$final_arry[$key["id"]]["name"]=$key["name"];
			if($products[0]["processed_qty"]>$approve_arry[$key["id"]]["qty"]){
			
			if($totalproduct[0]["process"]==1)
				$final_arry[$key["id"]]["qty"]=$products[0]["processed_qty"];
			else
				$final_arry[$key["id"]]["qty"]=$products[0]["processed_qty"]-$products[0]["finished_qty"];
			$final_arry[$key["id"]]["buff"]=($products[0]["buffed_qty"]+$approve_arry[$key["id"]]["qty"])-($products[0]["finished_qty"]);
			 
			$final_arry[$key["id"]]["notbuff"]=$products[0]["processed_qty"];
			}else{
			$final_arry[$key["id"]]["qty"]=$approve_arry[$key["id"]]["qty"];	
		    
			$final_arry[$key["id"]]["buff"]=($products[0]["buffed_qty"]+$approve_arry[$key["id"]]["qty"]);
			$final_arry[$key["id"]]["notbuff"]=$products[0]["processed_qty"];
			}
		}
		else{
		
		$final_arry[$key["id"]]["name"]=$key["name"];
		$final_arry[$key["id"]]["qty"]=$products[0]["processed_qty"]-$approve_arry[$key["id"]]["qty"];
	 
		if(!empty($approve_arry))		
			$final_arry[$key["id"]]["buff"]=($products[0]["buffed_qty"]+$approve_arry[$key["id"]]["qty"]);	 
		else
			$final_arry[$key["id"]]["buff"]=$products[0]["buffed_qty"];
		
			$final_arry[$key["id"]]["notbuff"]=$products[0]["processed_qty"];
		}
		$final_arry[$key["id"]]["total_qty"]=($final_arry[$key["id"]]["buff"]+$final_arry[$key["id"]]["notbuff"]);
		$final_arry[$key["id"]]["finished"]=$products[0]["finished_qty"];
		$final_arry[$key["id"]]["price"]=$key["price"];
		$final_arry[$key["id"]]["product_price"]=$key["product_price"];
		$final_arry[$key["id"]]["machining_price"]=$key["machining_price"];
		$final_arry[$key["id"]]["update_machinprice"]=$key["update_machinprice"];
		$final_arry[$key["id"]]["updated_price"]=$key["updated_price"];
		$final_arry[$key["id"]]["updated_product_price"]=$key["updated_product_price"];
		$final_arry[$key["id"]]["machining"]=$key["machining"];
		//maching qty
		$final_arry[$key["id"]]["maching_qty"]=$products[0]["maching_qty"];
	
	}
	
$buffing_list=$db_helper_obj->buffing_list1();
$final_array=array();
	foreach($buffing_list as $va=>$key){
		$bpl_no[$key["name"]]["name"]=$key["bpl_no"];
		$key["dated"]=date("d-m-Y",$key["buffing_date"]);	
		$approval_list[$key["name"]][$key["dated"]][]=$key;
		foreach($approval_list[$key["name"]][$key["dated"]] as $va1=>$key1){
			$products=convert_array($key1["products"]);	
			$products_approve=convert_array($key1["products_approve"]);
			$final_array1[$key["name"]][$key["dated"]][$key["bpl_no"]]["countss"]=0;
		foreach(convert_array($key1["products"]) as $va2=>$key2){
			$final_array[$key["name"]][$key["dated"]][$key["bpl_no"]][$key2["product_id"]]["emp_type"]=$key["emp_type"];
			$final_array[$key["name"]][$key["dated"]][$key["bpl_no"]][$key2["product_id"]]["bpl_no"]=$key["bpl_no"];
			$final_array[$key["name"]][$key["dated"]][$key["bpl_no"]][$key2["product_id"]]["product_id"]=$key2["product_id"];
			$final_array[$key["name"]][$key["dated"]][$key["bpl_no"]][$key2["product_id"]]["stocks"]=$key2["stocks"];
			$final_array[$key["name"]][$key["dated"]][$key["bpl_no"]][$key2["product_id"]]["buffed_qty"]=$key2["buffed_qty"];
			$final_array[$key["name"]][$key["dated"]][$key["bpl_no"]][$key2["product_id"]]["balance"]=$key2["stocks"]-$key2["buffed_qty"];
			$final_array[$key["name"]][$key["dated"]][$key["bpl_no"]][$key2["product_id"]]["price"]=$key2["price"];
			$final_array1[$key["name"]][$key["dated"]][$key["bpl_no"]]["countss"]++;
		}
		foreach($products_approve as $va3=>$key3){
			$final_array[$key["name"]][$key["dated"]][$key["bpl_no"]][$key3["product_id"]]["approve"]=$key3["approve"];
			$final_array[$key["name"]][$key["dated"]][$key["bpl_no"]][$key3["product_id"]]["reject"]=$key3["reject"];
			$final_array[$key["name"]][$key["dated"]][$key["bpl_no"]][$key3["product_id"]]["amount"]=$final_array[$key["name"]][$key["dated"]][$key["bpl_no"]][$key3["product_id"]]["price"]*$key3["approve"];
		}	
		}
	}
	$couns=0;
foreach($inward_approve_list as $va=>$key){
	$products_appr=convert_array($key["products"]); 
}
foreach($products_appr as $va=>$key){
	$product_idsappr[]=$key["product_id"];
}
$return_list=$db_helper_obj->return_list();	
	foreach($return_list as $va=>$key){
		$product_ids[]=$key["product_id"];
	}
	foreach($return_list as $va2=>$key2){
		
		if(!in_array($key["product_id"],$product_idsappr)){
		if($final_arry[$key2["product_id"]]["notbuff"]==0){
			$final_arry[$key2["product_id"]]["notbuff"]=0;
		}
		else{
			$final_arry[$key2["product_id"]]["notbuff"]+=$key2["qty"];
		}
	}
	}
	foreach($product_list as $va4=>$key4){
		$final_arry1[$key4["id"]]["id"]=$key4["id"];	
		$inward_approve_list=$db_helper_obj->inward_approve_list($key4["id"]);
		$inwardaddproveproduct=convert_array($inward_approve_list[$key4["products"]]);
		
		foreach($inward_approve_list as $va5=>$key5){		 
			$final_arry2[$key5["id"]]["products"]=convert_array($key5["products"]);
		}
	}
 
	foreach($final_arry as $va1=>$key1){
		$stock_history[$va1]=$db_helper_obj->price_history($va1);
		$name=$key1["name"];
	} 
	$deliverymo_list=$db_helper_obj->deliverymo_list();
	$deliverymi_list=$db_helper_obj->deliverymi_list();
	foreach($deliverymo_list as $va2=>$key2){	
	$product_machin=convert_array($key2["products"]);
	foreach($product_machin["product_id"] as $va3=>$key3){
	if($key3==$_GET["product_id"]){
	$job_out[$key2["challan_no"]][$va3]["supplier_id"]=$product_machin["supplier"];
	$job_out[$key2["challan_no"]][$va3]["qty"]=$product_machin["qty"][$va3];
	$job_out[$key2["challan_no"]][$va3]["rate"]=$product_machin["rate"][$va3];
	$job_out[$key2["challan_no"]][$va3]["amount"]=$product_machin["amount"][$va3];
	$job_out[$key2["challan_no"]][$va3]["date"]=date("d-m-Y",strtotime($product_machin["dated"]));
	}
	}
	}
	 
	$available_qty=$_GET["total_qty"];
	
	foreach($job_out as $va6=>$key6){
	if($available_qty>=$key6[0]["qty"]){
		unset($job_out[$va6]);
	}
	$available_qty=$_GET["total_qty"]-$key6[0]["qty"];
	}
	 
	foreach($deliverymi_list as $va2=>$key2){
		$product_jobin=convert_array($key2["adminproduct_approve"]);
		foreach($product_jobin["product_id"] as $va3=>$key3){
			if($key3==$_GET["product_id"]){
				$job_in[$key2["challan_no"]][$va3]["date"]=date("d-m-Y",strtotime($key2["dated"]));
				$job_in[$key2["challan_no"]][$va3]["supplier_id"]=$product_jobin["supplier"];
				$job_in[$key2["challan_no"]][$va3]["qty_approve"]=$product_jobin["qty_approve"][$va3];
			}
		}
	}
	
	$availab_qty=$_GET["total_qty"];
	foreach($job_in as $va6=>$key6){
		$availab_qty=$availab_qty-$key6[0]["qty_approve"];
		if($available_qty>=$_GET["total_qty"]||$_GET["outward_qty"]==0){
			unset($job_in[$va6]);
		}		
	}
	//echo"<pre>";print_r($final_arry);echo"</pre>";
?>	
<?php  

if($_GET["material_type"]==1||$_GET["material_type"]==4){ ?>
	<div class="table-responsive"> 
	<table class="table listhead table-bordered table-hover">
		<thead>
			<tr>
				<th>No</th>
				<th>Date</th>
				<th>JobOut Number</th>
				<th>Supplier Name</th>
				<th>Qty</th>
				<th>Rate</th>
				<th>Amount</th>
			</tr>
		</thead>
		<tbody>
			<?php			
			$count=1; 
			if(!empty($job_out)){
			foreach($job_out as $va5=>$key5){ 			
			?>
				<tr>
					<td><?php echo $count; ?></td>	
					<td><?php echo $key5[0]["date"]; ?></td>
					<td><?php echo $va5; ?></td>
					<td><?php  
					$supplier=$db_helper_obj->supplier_edit($key5[0]["supplier_id"]);
					echo $supplier[0]["supplier_name"];?></td>
					<td><?php echo $key5[0]["qty"]; ?></td>
					<td><?php echo $key5[0]["rate"];  ?></td>
					<td><?php echo $key5[0]["amount"]; ?></td>
				</tr>
			<?php $count++; } }else{?>
				<tr>
					<td align="center" colspan="12">There are no Records found</td>
				</tr>
			<?php } ?>
		</tbody>
	</table>	
</div>
<?php } ?>
<div class="table-responsive"> 
	<table class="table listhead table-bordered table-hover">
		<thead>
			<tr>
				<th>No</th>
				<th>Date</th>
				<th>JobIn Number</th>
				<th>Supplier Name</th>
				<th>Qty</th>				 
			</tr>
		</thead>
		<tbody>
			<?php
			$count=1;if(!empty($job_in)){ foreach($job_in as $va5=>$key5){		
			?>
				<tr>
					<td><?php echo $count; ?></td>
					<td><?php echo $key5[0]["date"]; ?></td>
					<td><?php echo $va5; ?></td>
					<td><?php $supplier=$db_helper_obj->supplier_edit($key5[0]["supplier_id"]);
					echo $supplier[0]["supplier_name"];?></td>
					<td><?php echo $key5[0]["qty_approve"]; ?></td>
				</tr>
			<?php $count++; }}else{ ?>
			<tr>
				<td align="center" colspan="12">There are no Records found</td>
			</tr>
			<?php } ?>
		</tbody>
	</table>	
</div>	
<?php if($_GET["material_type"]==2||$_GET["material_type"]==4){ ?>
<div class="table-responsive"> 
	<table class="table listhead table-bordered table-hover">
		<thead>
			<tr>
				<th>No</th>
				<th>Date</th>
				<th>Bpl Number</th>
				<th>Qty</th>				 
			</tr>
		</thead>
		<tbody>
			<?php
			 
			$count=1;if(!empty($buff_list)){  foreach($buff_list as $va5=>$key5){	
			if($_GET["outward_qty"]>0){
			?>
				<tr>
					<td><?php echo $count; ?></td>
					<td><?php echo $key5[0]["date"]; ?></td>
					<td><?php echo $va5; ?></td>
					<td><?php echo $key5[0]["qty"]; ?></td>
				</tr>
			<?php $count++; }}}else{  ?>
			<tr>
				<td align="center" colspan="12">There are no Records found</td>
			</tr>
			<?php } ?>
		</tbody>
	</table>	
</div>
<?php } ?>