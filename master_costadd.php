<?php 
include("config/config.php");

if(!isset($_SESSION["user_id"])){ // checking whether the usertype is logged in
	header('Location: logout.php');        						//REdirection to logout.php
	exit;
}

include("core/class/db_query.php");                             // Class where query generetion is written
include("core/class/db_helper.php");                            // Class where table and feilds 
                                                                // are mentioned to generate query
ob_start();                                                     // to clear the internal output
global $db_helper_obj;
global $global_website_url;
$db_helper_obj=new db_helper();                                 // Creating object to get data from table
$current_page = basename($_SERVER['PHP_SELF']); // to get the page name from url
global $month_name_arr;
$month_name_arr=array('1'=>'Jan','2'=>'Feb','3'=>'Mar','4'=>'Apr','5'=>'May','6'=>'Jun','7'=>'Jul','8'=>'Aug','9'=>'Sep','10'=>'Oct','11'=>'Nov','12'=>'Dec');

?>
<link rel="icon" type="images/png" href="images/favicon.png">
<link rel='stylesheet' href='css/font-awesome.css'>
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/jquery.mCustomScrollbar.min.css">
<link rel="stylesheet" href="css/responsive-menu.css">
<link rel="stylesheet" href="css/style.css">
 <script src="js/jquery.js"></script>
  <script src="js/bootstrap.js"></script>
  <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
  <link href="css/select2.min.css" rel="stylesheet" />
  <script src="js/select2.min.js"></script>
	<script src="js/modernizr-custom.js"></script>
  <script src="js/responsive-menu.js"></script>
  <script>window.jQuery || document.write('<script src="js/jquery.js"><\/script>')</script>
  <?php

  if($_REQUEST["check"]==0){
	unset($_SESSION["cost"][$_REQUEST["sub_id"]]);  
  ?>
<script>
//parent.$('#myModal').modal('hide');
</script> 
  <?php } ?>
<?php 
	$db_query=new db_query();  
	global $db_helper_obj;
	$db_helper_obj=new db_helper();
	if(!empty($_REQUEST)){
	$master=$db_helper_obj->product_edit($_REQUEST["product_id"]);
	}
	
	if(!empty($_POST)){
		unset($_POST["sub_id"]);
		foreach($_POST["cost_check"] as $va=>$key){		
		$_POST["cost_of"][$va]=$_POST["cost_of"][$va];
		}

		$_SESSION["cost"][$_REQUEST["sub_id"]]=$_POST["cost_of"];
		//$insert=$db_helper_obj->product_cost_update();
	}
?>
<?php if(!empty($_POST)){?>

<script>
//parent.$('#myModal').modal('hide');
</script>
<?php } ?>
<style>
	.cost_form { }
	.cost_form input.form-control { margin-bottom:10px; }
</style>
<?php if($_REQUEST["check"]==1){ ?>
<div id="height_id">	
<form id="master_costadd" action="master_costadd.php" name="master_costadd" method="post">						  
<div class="table-responsive"> 
<table class="table listhead table-bordered table-hover">
	<tr>
		<td align="center"><input type="checkbox" name="cost_check[2]" value="1" id="" /><label>Fettling</label></td>		
		<td align="center"><input type="checkbox" name="cost_check[3]" value="1" id="" /><label>Polishing</label></td>
		<td align="center"><input type="checkbox" name="cost_check[4]" value="1" id="" /><label>Assembling</label></td>
		<td align="center"><input type="checkbox" name="cost_check[5]" value="1" id="" /><label>Laser Printing</label></td>
		<td align="center"><input type="checkbox" name="cost_check[6]" value="1" id="" /><label>Packaging</label></td>	
	</tr>
	<tr>
		<td><input type="text" placeholder="Fettling" value="<?php echo $key["piece_rate"] ?>" name="cost_of[2]" id="cost_of" class="form-control"/></td>
		<td><input type="text" placeholder="Polishing" value="<?php echo $key["piece_rate"] ?>" name="cost_of[3]" id="cost_of" class="form-control"/></td>
		<td><input type="text" placeholder="Assembling" value="<?php echo $key["piece_rate"] ?>" name="cost_of[4]" id="cost_of" class="form-control"/></td>
		<td><input type="text" placeholder="Laser Printing" value="<?php echo $key["piece_rate"] ?>" name="cost_of[5]" id="cost_of" class="form-control"/></td>
		<td><input type="text" placeholder="Packaging" value="<?php echo $key["piece_rate"] ?>" name="cost_of[6]" id="cost_of" class="form-control"/></td>
	</tr>
	<input type="hidden" name="sub_id" value="<?php echo $_REQUEST["sub_id"]; ?>">
</table>
</div>  
<div align="center">
<button type="button" onclick="parent.$('#myModal').modal('hide');" class="btn btn-primary">Cancel</button>
<button type="submit" style="margin-right: 63px;"  class="btn btn-primary">Save</button>
</div>  
</form>           
</div>
<?php } ?>
<script type="text/javascript">
parent.$("#iframe_show_vehicle_data").attr("height",$("#height_id").height()+10+"px");
</script>