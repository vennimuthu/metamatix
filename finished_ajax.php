<?php
include("config/config.php");
include("core/class/db_query.php");                             // Class where query generetion is written
include("core/class/db_helper.php");                            // Class where table and feilds 
include("core/function/common.php");   	
                                                                // are mentioned to generate query
ob_start();                                                     // to clear the internal output
global $db_helper_obj;
global $global_website_url;
$db_helper_obj=new db_helper();  

$finished_list=$db_helper_obj->finished_list();
$product_list=$db_helper_obj->product_list();
	foreach($finished_list as $va=>$key){
		$products_arr=convert_array($key["products"]);
		foreach($products_arr as $va1=>$key1){
		$already_processed[$key1["product_id"]]["produced_qty"]+=$key1["produced_qty"];
		}
	}
$product_list=$db_helper_obj->product_list();
$subproduct_list=$db_helper_obj->subproduct_list();
$openstock_list=$db_helper_obj->openstock_list();
$fgreturn_list=$db_helper_obj->fgreturn_list();

foreach($fgreturn_list as $va5=>$key5){
$fgreturn[$key5["product_id"]]["return_qty"]+=$key5["qty"];
}

foreach($openstock_list as $va5=>$key5){
$openstock[$key5["subproduct_id"]]["id"]=$key5["subproduct_id"];
$total_product_edit=$db_helper_obj->total_product_edit($key5["subproduct_id"]);
if($total_product_edit[0]["process"]==0){
$openstock[$key5["subproduct_id"]]["processed_qty"]=$key5["processed_qty"];
}
$openstock[$key5["subproduct_id"]]["finished_qty"]=$key5["finished_qty"];
$openstock[$key5["subproduct_id"]]["processed_buff"]=$key5["processed_buff"];
}

$total_product_list=$db_helper_obj->total_product_list();
$buffing_list=$db_helper_obj->buffing_list();
foreach($buffing_list as $va=>$key){
$products=convert_array($key["products_approve"]);
foreach($products as $va1=>$key1){
$buffed[$key1["product_id"]]["product_id"]=$key1["product_id"];
$buffed[$key1["product_id"]]["approve"]+=$key1["approve"];
$buffed[$key1["product_id"]]["reject"]+=$key1["reject"];	
}	
}

foreach($openstock as $va2=>$key2){
$total_buffed[$key2["id"]]["product_id"]=$key2["id"];
if(($key2["processed_qty"]+$buffed[$key2["id"]]["approve"]+$key2["processed_buff"])>=$key2["finished_qty"])
$total_buffed[$key2["id"]]["approve"]=($key2["processed_qty"]+$buffed[$key2["id"]]["approve"]+$key2["processed_buff"])-$key2["finished_qty"];	
}

foreach($_POST["product_id"] as $va7=>$key7){
	$products_choose[$key7]=$key7;
}

foreach($product_list as $va=>$key){
$total_arr[$key["id"]]["product_id"]=$key["id"];
$total_arr[$key["id"]]["product_name"]=$key["product_name"];
$total_arr[$key["id"]]["stocks"]=$key["open_qty"];
$total_arr[$key["id"]]["series"]=$key["series"];
foreach($subproduct_list as $va3=>$key3){

if($key["id"]==$key3["product_id"]){
$total_arr[$key["id"]]["subproducts"][]=$key3;
if($key["id"]==$products_choose[$key["id"]]){	
$subproducts[]=$key3["name"];	
}
}
}
}

foreach($_POST["produced_qty"] as $va5=>$key5){
$produced_qty+=$key5;
}

foreach($total_arr as $va5=>$key5){
foreach($key5["subproducts"] as $va6=>$key6){
if(in_array($key6["name"],$subproducts)&&(($total_buffed[$key6["name"]]["approve"])>($produced_qty*$key6["qty"])))
{
$product[$key6["name"]]=floor($total_buffed[$key6["name"]]["approve"]-($produced_qty*$key6["qty"]));
}
else if((($total_buffed[$key6["name"]]["approve"])>($produced_qty*$key6["qty"])))
	$product[$key6["name"]]=$total_buffed[$key6["name"]]["approve"];
}	
}

foreach($total_arr as $va5=>$key5){
foreach($key5["subproducts"] as $va6=>$key6){	
$posibleqty[$va5][$key6["name"]]=floor($product[$key6["name"]]/$key6["qty"]);
}	
}

foreach($posibleqty as $va7=>$key7){
$min = null;
foreach ($key7 as $a) {
    if ($min === null) 
        $min = $a;
    else if ($min > $a) 
        $min = $a;

}
$possible[$va7]=$min;	
}
foreach($possible as $va4=>$key4){	
$possible[$va4]=$key4;
}
?>
<option value="" selected disabled >Select</option>
<?php if(!empty($total_arr)) foreach($total_arr as $va=>$key){
if( $possible[$key["product_id"]]!=0){
?>
<option value="<?php echo $key["product_id"];?>" prdct_price="<?php echo $key["price"];?>" 
stock="<?php echo $key["stocks"];	?>" possible="<?php echo $possible[$key["product_id"]];	?>" series="<?php echo $key["series"];	?>" ><?php
echo $key["product_name"];?></option>
<?php } } ?>