<?php function main(){
	$db_query=new db_query();  
	global $db_helper_obj;
	$db_helper_obj=new db_helper();
	
	$supplier=$db_helper_obj->supplier_edit($_GET["supplier_id"]);
	if(!empty($_POST)){
	$inserted=$db_helper_obj->supplier_update();
	header('Location:  supplier_list1.php');	//REdirection to master_list.php
	}
?>


<style>
	.cost_form { }
	.cost_form input.form-control { margin-bottom:10px; }
</style>
  <form class="form-horizontal" id="supplier" name="supplier" method="post">

	<div class="container-fluid">
    	
    	<div class="row">
        	<div class="col-sm-12">
            
            	<div class="listwrapper">
			<div class="table-responsive"> 
                    <table id="order_item_table_1" class="table listhead table-bordered table-hover">
					<font size="5" color="red"> * Required Field.</font>
                  <thead>
                    <tr>
					<th colspan="2">Supplier Edit</th>
					                  				
                    </tr>
                    </thead>
					<tr id="order_item_row_1_1" name="order_item_row_1_1">
					<td width="15%">
                    <label for="" class="control-label">Supplier Name<font size="5" color="red">*</font></label>				
					</td>
					<td>
                        <input type="text" required  placeholder="Supplier Name" class="form-control tbox_small" name="supplier_name" id="supplier_name" value="<?php echo $supplier[0]["supplier_name"];  ?>">	
                    </td>
					</tr>
					<tr>
					<td width="15%">                					
                    <label for="" class="control-label">Supplier Id<font size="5" color="red">*</font></label>				
					</td>
					<td>
                        <input type="text" required placeholder="Supplier Id" class="form-control tbox_small" name="vendor_id" id="vendor_id" value="<?php echo $supplier[0]["vendor_id"];  ?>">	
                    </td>
					</tr>
					<tr>
					<td width="15%">                					
                    <label for="" class="control-label">GST Number<font size="5" color="red">*</font></label>				
					</td>
					<td>
                        <input type="text" required value="<?php echo $supplier[0]["gst_no"];  ?>"  placeholder="GST Number" class="form-control tbox_small" name="gst_no" id="gst_no">	
                    </td>
					</tr>
					<tr id="order_item_row_1_1" name="order_item_row_1_1">
					<td >                     
                    <label for="" class="control-label">Address<font size="5" color="red">*</font></label>		
					</td>
					<td>
                        <textarea required placeholder="Address" name="address" class="form-control tbox_small" id="address"><?php echo $supplier[0]["address"];  ?></textarea>		
                    </td>
					</tr>
					<tr id="order_item_row_1_1" name="order_item_row_1_1">
					<td >                     
                    <label for="" class="control-label">Pin Code<font size="5" color="red">*</font></label>		
					</td>
					<td>
					<input type="text" required value="<?php echo $supplier[0]["pincode"];  ?>"  placeholder="Pin Code" class="form-control tbox_small" name="pincode" id="pincode">
                    </td>
					</tr>
					<tr id="order_item_row_1_1" name="order_item_row_1_1">
					<td >                     
                    	 <label for="" class="control-label">City<font size="5" color="red">*</font></label>			
					</td>
					<td>
                        <input type="text" required placeholder="City" class="form-control tbox_small" name="city" id="city"  value="<?php echo $supplier[0]["city"];  ?>" required />
                    </td>
					</tr>
					<tr id="order_item_row_1_1" name="order_item_row_1_1">
					<td>                     
                    	 <label for="" class="control-label">State<font size="5" color="red">*</font></label>				
					</td>
					<td>
                        <input type="text" required placeholder="State" class="form-control tbox_small" name="state" id="state"  value="<?php echo $supplier[0]["state"];  ?>" required />
                    </td>
					</tr>
					<tr id="order_item_row_1_1" name="order_item_row_1_1">
					<td>                     
                    	 <label for="" class="control-label">Payment Terms<font size="5" color="red">*</font></label>				
					</td>
					<td>
                        <input type="text" required placeholder="Payment Terms" class="form-control tbox_small" name="payment_terms" value="<?php echo $supplier[0]["payment_terms"];  ?>"  id="payment_terms" value="" required />
                    </td>
					</tr>
                    </table>
    </div> 
	<div align="center">
		 <button type="button" onclick="window.location.href='supplier_list1.php'" class="btn btn-primary">Cancel</button>
		 <button type="submit" class="btn btn-primary">Submit</button>
	</div>
                  </div>  <!------End--- Table Responsive -->
                </div>
				</form>
            </div>
        </div>
    </div>

</form>
<?php } include("template.php"); ?>  