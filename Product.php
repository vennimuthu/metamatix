<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MY_Controller{
	public function __construct(){
		parent::__construct();
		user_validate(); // session validation.
		user_type_validate(array(1));
	}
	public function index(){
		$this->middle = 'product_list1'; // passing middle to function.
		$this->layout();
	}
	 
	public function listdata($sortby_name="id",$sort_by_val="asc"){
		$this->load->model('product_model');
		$categ=$this->product_model->get_category();

		$categ_sub=$this->product_model->get_sub_category();
		foreach($categ_sub as $va=>$key){
			$categ_sub_arr[$key->id]=$key->sub_category_name;
		}
		$data=$this->product_model->get_product_list($sortby_name,$sort_by_val);
		$json_data=array();
		$json_data['category']=$categ;
		foreach($data as $va=>$key){
			$json_data['product'][$key->category][$key->sub_category]["p_arr"][]=$key;
			$json_data['product'][$key->category][$key->sub_category]["sub_cate_name"]=$categ_sub_arr[$key->sub_category];
		}
		echo json_encode($json_data);
	}
	public function add($to_do="Save"){
	 
		$this->load->helper(array('form','common_helper'));

		$this->middle = 'product_add'; // passing middle to function.
		$this->data["to_do"]=$to_do;
		
		$this->load->model('product_model');
		$this->data["category"]=$this->product_model->get_category();
		$this->data["sub_category"]=$this->product_model->get_sub_category();
        $this->layout();
		
	}
	public function sub_cate($id){ 
		$this->load->model('product_model');
		$data=$this->product_model->get_sub_category($id); 
		echo json_encode($data);
	}
	public function edit($id){
		$this->load->model('product_model');
		$this->data["edit_data"]=$this->product_model->get_product($id);
		
		$file=$this->product_model->get_file($id);
		foreach ($file as $va=>$key){
			$this->data["file"][$key->reference_id][]=$key;
		}	//echo"<pre>sssssss";print_r($this);echo"</pre>";exit;
		$this->add("Update");
		$this->data["to_do"]="Update";
		
	}
	public function detail($id){
		$this->load->model('product_model');
		$this->data["edit_data"]=$this->product_model->get_product($id);
		$file=$this->product_model->get_file($id);
		//echo"<pre>";print_r($file);echo"</pre>";
		foreach ($file as $va=>$key){
			$this->data["file"][$key->reference_id][]=$key;
		}
		$this->middle = 'product_dv'; // passing middle to function.
		$this->layout();
	}
	public function add_submit(){
		
		$err=1;
		$data=array();
		
		if(isset($_POST["save"]) && ($_POST["save"]=="Save" || $_POST["save"]=="Update"))
		{
		
		$this->load->helper('form');
			$this->load->library(array('form_validation'));
			$this->form_validation->set_rules('category', 'Category', 'required');
			$this->form_validation->set_rules('sub_category_name', 'Sub Catogory', 'required');
			$this->form_validation->set_rules('p_name', 'Product Name', 'required');
			$this->form_validation->set_rules('ref', 'Reference', 'required');
			$this->form_validation->set_rules('price', 'Price', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
			$this->form_validation->set_rules('height', 'Height', 'required');
			$this->form_validation->set_rules('width', 'Width', 'required');
			$this->load->library('file_upload');
		
			$valid_file_prod_img=$this->file_upload->validate_multiple_file("prod_img",array("required"=>false,"allow_file_type"=>"jpg,png,gif,jpeg,pdf,doc,docx","max_file_size"=>'1000000'));
			$valid_file_prod_doc=$this->file_upload->validate_multiple_file("tech_document",array("required"=>false,"allow_file_type"=>"jpg,png,gif,jpeg,pdf,doc,docx","max_file_size"=>'1000000000000'));
			$valid_file_prod_test=$this->file_upload->validate_multiple_file("testing_document",array("required"=>false,"allow_file_type"=>"jpg,png,gif,jpeg","max_file_size"=>'1000000000000'));
		
			$valid_file_prod_pattern=$this->file_upload->validate_multiple_file("prod_pattern_file",array("required"=>false,"allow_file_type"=>"jpg, png, gif,jpeg,pdf,doc,docx","max_file_size"=>'1000000000000'));
			
			if ($this->form_validation->run() && $valid_file_prod_img=="" && $valid_file_prod_doc=="" && $valid_file_prod_test=="" && $valid_file_prod_pattern==""){
				$err=0;
				$this->load->model('product_model');
				
				//echo"<pre>";print_r($_FILES);echo"</pre>";
				
		
				if($_POST["save"]=="Save")
				
		
				$product_id=$this->product_model->add_product();
			// print_r($product_id);
				//	echo"<pre>";print_r($_FILES);echo"</pre>";
					//echo"<pre>";print_r($_POST);echo"</pre>";

//catch (Exception $e) {
    //alert the user then kill the process
//echo $e->getMessage();
 //exit;
//}
				
				
				if($_POST["save"]=="Update"){
						echo ($save);
					$product_id=$_POST["product_id"];
					$this->product_model->update_product($product_id);
					
					if(!isset($_FILES["prod_3dview_file_edit"]))
						$_FILES["prod_3dview_file_edit"]["name"]=array();					
					$key_vals5=array_keys($_FILES["prod_3dview_file_edit"]["name"]);			
					$lists5=$this->product_model->get_file_id($_POST["product_id"],6);				
						if(!empty($lists5))
					$delete_ids5=array_diff($lists5["id"],$key_vals5);					
					if(!empty($delete_ids5)){
						$lists25=$this->product_model->delete_product_file(implode(",",$delete_ids5),$lists5["file_path"]);	
					}

					if(isset($_FILES["prod_3dview_file_edit"])){					
						if(isset($_FILES["prod_3dview_file_edit"])){
							$files=$_FILES["prod_3dview_file_edit"];
							foreach($files['name'] as $key=>$va) {
								
								$_FILES['multi_images']['name']= $files['name'][$key];
								$_FILES['multi_images']['type']= $files['type'][$key];
								$_FILES['multi_images']['tmp_name']= $files['tmp_name'][$key];
								$_FILES['multi_images']['error']= $files['error'][$key];
								$_FILES['multi_images']['size']= $files['size'][$key];
								$data=$this->file_upload->move_upload("multi_images","documents/product","pdf");
								if($data["msg"]=="Upload success!"){
									$insert_data=array();
									$insert_data["file_type"]=$data["upload_data"]["file_type"];
									$insert_data["file_path"]="documents/product/".$data["upload_data"]["file_name"];
									$insert_data["file_name"]=$data["upload_data"]["client_name"];
									$this->product_model->update_product_file($insert_data,$key);
								}
							}
						}
					}
					if(isset($_POST["prod_3dview_desc_edit"])){
						foreach($_POST["prod_3dview_desc_edit"] as $va=>$key){
							$updt_arr_file=array();
							
							$updt_arr_file["description"]=$key;
							$this->product_model->update_product_file($updt_arr_file,$va);
						}
					}	
					
					
					if(!isset($_FILES["prod_img_edit"]))
						$_FILES["prod_img_edit"]["name"]=array();					
					$key_vals2=array_keys($_FILES["prod_img_edit"]["name"]);			
					$lists2=$this->product_model->get_file_id($_POST["product_id"],1);	
					if(!empty($lists2))
						$delete_ids2=array_diff($lists2["id"],$key_vals2);					
					if(!empty($delete_ids2)){
						$lists24=$this->product_model->delete_product_file(implode(",",$delete_ids2),$lists2["file_path"]);	
					}

					if(isset($_FILES["prod_img_edit"])){					
						if(isset($_FILES["prod_img_edit"])){
							$files=$_FILES["prod_img_edit"];
							foreach($files['name'] as $key=>$va) {
								
								$_FILES['multi_images']['name']= $files['name'][$key];
								$_FILES['multi_images']['type']= $files['type'][$key];
								$_FILES['multi_images']['tmp_name']= $files['tmp_name'][$key];
								$_FILES['multi_images']['error']= $files['error'][$key];
								$_FILES['multi_images']['size']= $files['size'][$key];
								$data=$this->file_upload->move_upload("multi_images","images/product","jpg|png|gif|jpeg|pdf|doc|docx");
								if($data["msg"]=="Upload success!"){
									$insert_data=array();
									$insert_data["file_type"]=$data["upload_data"]["file_type"];
									$insert_data["file_path"]="images/product/".$data["upload_data"]["file_name"];
									$insert_data["file_name"]=$data["upload_data"]["client_name"];
									$this->product_model->update_product_file($insert_data,$key);
								}
							}
						}
					}
					if(isset($_POST["prod_edit_desc"]))
					{
						foreach($_POST["prod_edit_desc"] as $va=>$key){
							$updt_arr_file=array();
							$updt_arr_file["description"]=$key;
							$this->product_model->update_product_file($updt_arr_file,$va);
						}
					}		
					if(!isset($_FILES["tech_document_edit"]))
						$_FILES["tech_document_edit"]["name"]=array();					
					$key_vals=array_keys($_FILES["tech_document_edit"]["name"]);			
					$lists=$this->product_model->get_file_id($_POST["product_id"],2);
					if(!empty($lists))
					$delete_ids=array_diff($lists["id"],$key_vals);					
					if(!empty($delete_ids)){
						$lists1=$this->product_model->delete_product_file(implode(",",$delete_ids),$lists["file_path"]);	
					}
					
					
					
					if(isset($_FILES["tech_document_edit"])){					
						$files=$_FILES["tech_document_edit"];
						foreach($files['name'] as $key=>$va){
							$_FILES['multi_doc']['name']= $files['name'][$key];
							$_FILES['multi_doc']['type']= $files['type'][$key];
							$_FILES['multi_doc']['tmp_name']= $files['tmp_name'][$key];
							$_FILES['multi_doc']['error']= $files['error'][$key];
							$_FILES['multi_doc']['size']= $files['size'][$key];
							$data=$this->file_upload->move_upload("multi_doc","images/product","jpg|png|gif|jpeg|pdf|doc|docx");
							if($data["msg"]=="Upload success!"){
								$insert_data=array();
								$insert_data["file_type"]=$data["upload_data"]["file_type"];
								$insert_data["file_path"]="images/product/".$data["upload_data"]["file_name"];
								$insert_data["file_name"]=$data["upload_data"]["client_name"];
								$this->product_model->update_product_file($insert_data,$key);
							}
						}
					}
					if(isset($_POST["tech_doc_desc_edit"])){
						foreach($_POST["tech_doc_desc_edit"] as $va=>$key){
							$updt_arr_file=array();
							$updt_arr_file["description"]=$key;
							$this->product_model->update_product_file($updt_arr_file,$va);
						}
					}
					//echo"<pre>";print_r($_FILES["testing_document_edit"]);echo"</pre>";
				  //echo"<pre>";print_r(implode(",",$delete_ids));echo"</pre>";
					
					
					//exit;
					if(!isset($_FILES["testing_document_edit"]))
						$_FILES["testing_document_edit"]["name"]=array();					
					$key_vals1=array_keys($_FILES["testing_document_edit"]["name"]);			
					$lists1=$this->product_model->get_file_id($_POST["product_id"],3);	
					if(!empty($lists1))					
					$delete_ids1=array_diff($lists1["id"],$key_vals1);					
					if(!empty($delete_ids1)){
						$lists2=$this->product_model->delete_product_file(implode(",",$delete_ids1),$lists1["file_path"]);	
					}
					if(isset($_FILES["testing_document_edit"])){
					
						$files=$_FILES["testing_document_edit"];
						foreach($files['name'] as $key=>$va){
							$_FILES['multi_doc']['name']= $files['name'][$key];
							$_FILES['multi_doc']['type']= $files['type'][$key];
							$_FILES['multi_doc']['tmp_name']= $files['tmp_name'][$key];
							$_FILES['multi_doc']['error']= $files['error'][$key];
							$_FILES['multi_doc']['size']= $files['size'][$key];
							$data=$this->file_upload->move_upload("multi_doc","images/product","jpg|png|gif|jpeg|pdf|doc|docx");
							if($data["msg"]=="Upload success!"){
								$insert_data=array();
								$insert_data["file_type"]=$data["upload_data"]["file_type"];
								$insert_data["file_path"]="images/product/".$data["upload_data"]["file_name"];
								$insert_data["file_name"]=$data["upload_data"]["client_name"];
								$this->product_model->update_product_file($insert_data,$key);
							}
						}
					}
					if(isset($_POST["testing_doc_desc_edit"])){
						foreach($_POST["testing_doc_desc_edit"] as $va=>$key){
							$updt_arr_file=array();
							$updt_arr_file["description"]=$key;
							$this->product_model->update_product_file($updt_arr_file,$va);
						}
					}
					if(!isset($_FILES["prod_pattern_file_edit"]))
						$_FILES["prod_pattern_file_edit"]["name"]=array();					
					$key_vals3=array_keys($_FILES["prod_pattern_file_edit"]["name"]);
									
					$lists3=$this->product_model->get_file_id($_POST["product_id"],5);	
					if(!empty($lists3))					
					$delete_ids3=array_diff($lists3["id"],$key_vals3);					
					if(!empty($delete_ids3)){
						$lists4=$this->product_model->delete_product_file(implode(",",$delete_ids3),$lists3["file_path"]);	
					}
					if(isset($_FILES["prod_pattern_file_edit"])){
						$files=$_FILES["prod_pattern_file_edit"];
					
						foreach($files['name'] as $key=>$va){
							$_FILES['multi_doc']['name']= $files['name'][$key];
							$_FILES['multi_doc']['type']= $files['type'][$key];
							$_FILES['multi_doc']['tmp_name']= $files['tmp_name'][$key];
							$_FILES['multi_doc']['error']= $files['error'][$key];
							$_FILES['multi_doc']['size']= $files['size'][$key];
							$data=$this->file_upload->move_upload("multi_doc","images/product","jpg|png|gif|jpeg|pdf|doc|docx");
							if($data["msg"]=="Upload success!"){
								$insert_data=array();
								$insert_data["file_type"]=$data["upload_data"]["file_type"];
								$insert_data["file_path"]="images/product/".$data["upload_data"]["file_name"];
								$insert_data["file_name"]=$data["upload_data"]["client_name"];
								$this->product_model->update_product_file($insert_data,$key);
							}
						}
					}
					if(isset($_POST["prod_patern_desc_edit"])){
						foreach($_POST["prod_patern_desc_edit"] as $va=>$key){
							$updt_arr_file=array();
							$updt_arr_file["description"]=$key;
							$this->product_model->update_product_file($updt_arr_file,$va);
						}
					}
				

				}
				//echo"<pre>";print_r($_FILES);echo"</pre>";
				//echo"<pre>";print_r($_POST);echo"</pre>";
				//exit;
				if(isset($_FILES["prod_3dview_file"])){
					$files=$_FILES["prod_3dview_file"];
					
					foreach($files['name'] as $key=>$va){
						$_FILES['multi_images']['name']= $files['name'][$key];
						$_FILES['multi_images']['type']= $files['type'][$key];
						$_FILES['multi_images']['tmp_name']= $files['tmp_name'][$key];
						$_FILES['multi_images']['error']= $files['error'][$key];
						$_FILES['multi_images']['size']= $files['size'][$key];
						$data=$this->file_upload->move_upload("multi_images","documents/product","pdf");
						
						if($data["msg"]=="Upload success!"){
							$insert_data=array();
							$insert_data["file_type"]=$data["upload_data"]["file_type"];
							$insert_data["file_path"]="documents/product/".$data["upload_data"]["file_name"];
							$insert_data["file_name"]=$data["upload_data"]["client_name"];
							$insert_data["reference_id"]=6;
							if(isset($_POST["prod_3dview_desc"][$va]))
							$insert_data["description"]=$_POST["prod_3dview_desc"][$va];
							$insert_data["primary_id"]=$product_id;
							$session_data = $this->session->userdata();
							$insert_data["created_by"]=$session_data["user_id"];
							$insert_data["created_datetime"]=date("Y-m-d H:i:s",time());
							$this->product_model->add_product_file($insert_data);
						}
					}
				}
				
				if(isset($_FILES["prod_img"])){
					$files=$_FILES["prod_img"];
					
					foreach($files['name'] as $key=>$va){
						$_FILES['multi_images']['name']= $files['name'][$key];
						$_FILES['multi_images']['type']= $files['type'][$key];
						$_FILES['multi_images']['tmp_name']= $files['tmp_name'][$key];
						$_FILES['multi_images']['error']= $files['error'][$key];
						$_FILES['multi_images']['size']= $files['size'][$key];
						$data=$this->file_upload->move_upload("multi_images","images/product","jpg|png|gif|jpeg|pdf|doc|docx");
						
						if($data["msg"]=="Upload success!"){
							$insert_data=array();
							$insert_data["file_type"]=$data["upload_data"]["file_type"];
							$insert_data["file_path"]="images/product/".$data["upload_data"]["file_name"];
							$insert_data["file_name"]=$data["upload_data"]["client_name"];
							$insert_data["reference_id"]=1;
							if(isset($_POST["prod_desc"][$va]))
							$insert_data["description"]=$_POST["prod_desc"][$va];
							$insert_data["primary_id"]=$product_id;
							$session_data = $this->session->userdata();
							$insert_data["created_by"]=$session_data["user_id"];
							$insert_data["created_datetime"]=date("Y-m-d H:i:s",time());
							$this->product_model->add_product_file($insert_data);
						}
					}
				}
				if(isset($_FILES["tech_document"])){
					$files=$_FILES["tech_document"];
					//echo"<pre>";print_r($files);echo"</pre>";
					//exit;
					foreach($files['name'] as $key=>$va){
						$_FILES['multi_doc']['name']= $files['name'][$key];
						$_FILES['multi_doc']['type']= $files['type'][$key];
						$_FILES['multi_doc']['tmp_name']= $files['tmp_name'][$key];
						$_FILES['multi_doc']['error']= $files['error'][$key];
						$_FILES['multi_doc']['size']= $files['size'][$key];
						$data=$this->file_upload->move_upload("multi_doc","images/product","jpg|png|gif|jpeg|pdf|doc|docx");
						
						if($data["msg"]=="Upload success!"){
							$insert_data=array();
							$insert_data["file_type"]=$data["upload_data"]["file_type"];
							$insert_data["file_path"]="images/product/".$data["upload_data"]["file_name"];
							$insert_data["file_name"]=$data["upload_data"]["client_name"];
							$insert_data["reference_id"]=2;
							$insert_data["primary_id"]=$product_id;
							if(isset($_POST["tech_doc_desc"][$va]))
								$insert_data["description"]=$_POST["tech_doc_desc"][$va];
							$session_data = $this->session->userdata();
							$insert_data["created_by"]=$session_data["user_id"];
							$insert_data["created_datetime"]=date("Y-m-d H:i:s",time());
							$this->product_model->add_product_file($insert_data);
						}
					}
				}
				if(isset($_FILES["testing_document"])){
					$files=$_FILES["testing_document"];
					foreach($files['name'] as $key=>$va){
						$_FILES['multi_doc']['name']= $files['name'][$key];
						$_FILES['multi_doc']['type']= $files['type'][$key];
						$_FILES['multi_doc']['tmp_name']= $files['tmp_name'][$key];
						$_FILES['multi_doc']['error']= $files['error'][$key];
						$_FILES['multi_doc']['size']= $files['size'][$key];
						$data=$this->file_upload->move_upload("multi_doc","images/product","jpg|png|gif|jpeg|pdf|doc|docx");
						if($data["msg"]=="Upload success!"){
							$insert_data=array();
							$insert_data["file_type"]=$data["upload_data"]["file_type"];
							$insert_data["file_path"]="images/product/".$data["upload_data"]["file_name"];
							$insert_data["file_name"]=$data["upload_data"]["client_name"];
							$insert_data["reference_id"]=3;
							if(isset($_POST["testing_doc_desc"][$key]))
								$insert_data["description"]=$_POST["testing_doc_desc"][$key];
							$insert_data["primary_id"]=$product_id;
							$session_data = $this->session->userdata();
							$insert_data["created_by"]=$session_data["user_id"];
							$insert_data["created_datetime"]=date("Y-m-d H:i:s",time());
							$this->product_model->add_product_file($insert_data);
						}
					}
					
				}
				if(isset($_FILES["prod_pattern_file"])){
					$files=$_FILES["prod_pattern_file"];
					foreach($files['name'] as $key=>$va){
						$_FILES['multi_doc']['name']= $files['name'][$key];
						$_FILES['multi_doc']['type']= $files['type'][$key];
						$_FILES['multi_doc']['tmp_name']= $files['tmp_name'][$key];
						$_FILES['multi_doc']['error']= $files['error'][$key];
						$_FILES['multi_doc']['size']= $files['size'][$key];
						$data=$this->file_upload->move_upload("multi_doc","images/product","jpg|png|gif|jpeg|pdf|doc|docx");
						if($data["msg"]=="Upload success!"){
							$insert_data=array();
							$insert_data["file_type"]=$data["upload_data"]["file_type"];
							$insert_data["file_path"]="images/product/".$data["upload_data"]["file_name"];
							$insert_data["file_name"]=$data["upload_data"]["client_name"];
							$insert_data["reference_id"]=5;
							if(isset($_POST["prod_patern_desc"][$key]))
								$insert_data["description"]=$_POST["prod_patern_desc"][$key];
							$insert_data["primary_id"]=$product_id;
							$session_data = $this->session->userdata();
							$insert_data["created_by"]=$session_data["user_id"];
							$insert_data["created_datetime"]=date("Y-m-d H:i:s",time());
							$this->product_model->add_product_file($insert_data);
						}
					}
					
				}
				redirect('/product','location');
			}
						}
		if($err==1){
			$this->index($data);
		}
		
		
		
	}
}
