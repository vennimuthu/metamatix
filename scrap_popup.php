	 
	<link rel='stylesheet' href='css/font-awesome.css'>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/jquery.mCustomScrollbar.min.css">
	<link rel="stylesheet" href="css/responsive-menu.css">
	<link rel="stylesheet" href="css/style.css">
	<script src="js/jquery.js"></script>
 
            <div class="listwrapper">
			<div class="table-responsive"  id="master_viewid"> 
                <table id="order_item_table_1" class="table listhead table-bordered table-hover">
                    <thead>
                    <tr>
						<th style="text-align: center;">Total&nbsp;Scrap</th>	
						<th style="text-align: center;">Scrap&nbsp;Out</th>
						<th style="text-align: center;">Net&nbsp;Balance</th>						 
                    </tr>
                    </thead>
								
					<tbody  id="order_item_body_1"  name="order_item_row_1_1">
					<tr id="order_item_row_1_1" name="order_item_row_1_1">
					 
					<td align="center">
						<label id="total_scrap"><?php echo $_GET["total_wastage"]; ?></label>
					</td>
					<td  align="center">			 
						<label id="selected_scrap"><?php echo $_GET["selected_wastage"]; ?></label>
					</td>			
					<td  align="center">
						<label id="balance_scrap"><?php echo $_GET["total_wastage"]-$_GET["selected_wastage"]; ?></label>
					 
					</td>
					  
					</tr>
					 
				   </tbody>
				</table>
				<table id="order_item_table_1" class="table listhead table-bordered table-hover">
                    <thead>
                    <tr>
						<th style="text-align: center;">Vehicle&nbsp;Empty&nbsp;Weight</th>	
						<th style="text-align: center;">Vehicle&nbsp;Load&nbsp;Weight</th>
                    </tr>
                    </thead>
								
					<tbody  id="order_item_body_1"  name="order_item_row_1_1">
					<tr id="order_item_row_1_1" name="order_item_row_1_1">
					 
					
					 <td align="center">
						<input type="text" class="form-control" onkeyup="weight_calculate();" name="vehicle_empty[]" id="vehicle_empty" value="0.000" />
					</td>
					<td align="center">			 
						<input type="text" class="form-control" onkeyup="weight_calculate();" name="vehicle_load[]" id="vehicle_load" value="0.000"  />
					</td>			
					 
					</tr>
					 
				   </tbody>
				</table>
				<table id="order_item_table_1" class="table listhead table-bordered table-hover">
                    <thead>
                    <tr>
						<th style="text-align: center;">Net&nbsp;Weight</th>
						<th style="text-align: center;">Rate&nbsp;/&nbsp;Kg</th>	
						<th style="text-align: center;">Total&nbsp;Amount</th>
						<th style="text-align: center;">Vehicle&nbsp;No</th>
						<th style="text-align: center;">Date</th>						
                    </tr>
                    </thead>
								
					<tbody  id="order_item_body_1"  name="order_item_row_1_1">
					<tr id="order_item_row_1_1" name="order_item_row_1_1">
									
					<td align="center">
						<label id="net_weight">0.000</label>
						 <input type="hidden" value="1" id="net_weight_hidden" name="net_weight_val">
						 
					</td>
					<td align="center">			 
						<input type="text" class="form-control" name="rate_kg[]" id="rate_kg" onkeyup="weight_calculate()" value="0"  />
					</td>
					<td align="center">
						<label id="total_amount">0.000</label>
						 <input type="hidden" value="1" id="total_amount_val" name="total_amount_val">
					</td>	
					<td align="center">			 
						<input type="text" class="form-control" name="vehicle_no[]" id="vehicle_no" value=""  />
					</td>						
					<td align="center">
					<input type="date" name="creation_date" id="creation_date" class="form-control" value="<?php echo date('Y-m-d'); ?>">
					</td>
					</tr>
					<tr>
					<td colspan="6"><input type="button" onclick="pdf_generate()" name="report_ids" id="report_ids" class="btn btn-primary pull-right" value="Generate"></td>
					</tr>
				   </tbody>
				</table>
</div>
</div>
 
<script>
function weight_calculate(){
	var vehicle_empty=document.getElementById("vehicle_empty").value;
	var vehicle_load=document.getElementById("vehicle_load").value;
	var rate_kg=document.getElementById("rate_kg").value;
	document.getElementById("net_weight").innerHTML=Number(vehicle_load-vehicle_empty).toFixed(3);
	document.getElementById("total_amount").innerHTML=Number(document.getElementById("net_weight").innerHTML*rate_kg).toFixed(3);
}
function pdf_generate(){ 				   	
	parent.document.getElementById("vehicle").value=document.getElementById("vehicle_no").value;	
	parent.document.getElementById("creation").value=document.getElementById("creation_date").value;
	
	parent.document.getElementById("empty").value=document.getElementById("vehicle_empty").value;
	parent.document.getElementById("load").value=document.getElementById("vehicle_load").value;
	parent.document.getElementById("price").value=document.getElementById("rate_kg").value; 
	parent.document.getElementById("receipt_list").submit();
	setTimeout(function(){parent.location.reload()},200);	
}
setTimeout(function(){ parent.$("#iframe_show_vehicle_data").attr("height",$("#order_item_table_1").height()+300+"px"); }, 100);
</script>	