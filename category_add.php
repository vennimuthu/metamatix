<?php function main(){
	$db_query=new db_query();  
	global $db_helper_obj;
	$db_helper_obj=new db_helper();
	$supplier_list=$db_helper_obj->supplier_list();
	$material_list=$db_helper_obj->material_category();	
	
	if(!empty($_POST)){
		$inserted=$db_helper_obj->total_product_insert();
		header('Location:  category_list.php'); //REdirection to master_list.php
	}else{
		unset($_SESSION["cost"]);
	}
?>

<script type="text/javascript">

function show_hide(type,count){
	if(type==5||type==2||type==3){
	//	$('.hidden_id').hide();
		//$('.hidden_kg').show();
		 $(".hidden_id"+count).attr("disabled", true);
	}else{
		//$('.hidden_id').show();
		//$('.hidden_kg').hide();
		 $(".hidden_id"+count).attr("disabled", false);
	}
}

function add_order_row(order_row_id)
{
	var order_item_index=$("#order_item_row_index_"+order_row_id).val();
	order_item_index++;
	var html_content;
	var options1=document.getElementById('material_select').innerHTML;
	
	var slect1='<select id="material_'+order_item_index+'" name="material['+order_item_index+']" class="form-control" onchange="show_hide(this.value,'+order_item_index+')" >'+options1+'</select>';
	html_content='<tr id="order_item_row_'+order_row_id+"_"+order_item_index+'" name="order_item_row_'+order_row_id+"_"+order_item_index+'">'
+'<td><input type="text" class="form-control" name="name['+order_item_index+']" id="name_'+order_item_index+'" value="" =""></td>'
+'<td><input type="text" class="form-control" name="hsn['+order_item_index+']" id="hsn_'+order_item_index+'" value="" /></td>'+'<td>'+slect1+'</td>'
+'<td><input type="text" class="form-control" name="dimension['+order_item_index+']" id="dimension_'+order_item_index+'" value="" /></td>'
+'<td><input type="text" class="form-control hidden_id'+order_item_index+'" name="price['+order_item_index+']" id="price_'+order_item_index+'" value="" /></td>'+
'<td><input type="text" class="form-control hidden_id'+order_item_index+'" name="machining_price['+order_item_index+']" id="machining_price_'+order_item_index+'" value="" /></td>'
+'<td><input type="text" class="form-control" name="product_price['+order_item_index+']" id="product_price_'+order_item_index+'" value="" /></td>'
+'<td align="center"><input class="hidden_id'+order_item_index+'" type="checkbox" name="process['+order_item_index+']" id="process_'+order_item_index+'" value="1" ></td>'
+'<td align="center"><input class="hidden_id'+order_item_index+'" type="checkbox" name="machining['+order_item_index+']" id="machining_'+order_item_index+'" value="1" ></td>'
+'<td><input type="button" class="btn btn-danger" value="X" onclick="delete_order_item_row(this);"></td></tr>';
	
	div = document.getElementById('order_item_body_'+order_row_id);	
	div.insertAdjacentHTML( 'beforeend', html_content);
	$("#order_item_row_index_"+order_row_id).val(order_item_index);
}

function delete_order_item_row(element)  
{
    var row = element.parentNode.parentNode;
    row.parentNode.removeChild(row);
}
function showmaterial(obj){
	if(obj.value==3)
		document.getElementById("material_id").style.display="none";
	else{
		document.getElementById("material_id").style.display="";
	}

}
function cost_add(obj){
var word=obj.id;
var number=word.replace('process_','');	
if(obj.checked==true)
	var check=1;
else if(obj.checked==false)	
	var check=0;
if(obj.checked==true&&document.getElementById('name_'+number).value!=''){
$.ajax({url: "master_costadd.php?sub_id="+number+"&check="+check+"",contentType:false, processData: false, success: function(result){
	var ser = document.getElementById('iframe_show_vehicle_data');
	ser.contentDocument.write(result);
	}
	});	
}else{ 
	$.ajax({url: "master_costadd.php?sub_id="+number+"&check="+check+"",contentType:false, processData: false, success: function(result){
	var ser = document.getElementById('iframe_show_vehicle_data');
	ser.contentDocument.write(result);
	}
	});	
}
}
function cancel_data(){
	var iframe = document.getElementById("iframe_show_vehicle_data");
	var html = "";
	iframe.contentWindow.document.open();
	iframe.contentWindow.document.write(html);
	iframe.contentWindow.document.close();
}
function disabl(){
document.getElementById('button_id').disabled=true;	
}

</script>


<style>
	.cost_form { }
	.cost_form input.form-control { margin-bottom:10px; }
	.tbox_vsmall {
     width:21%;
	}

.required input {
  
   background-image: url(...);
   background-position: right top;
}	  

</style>

<form class="form-horizontal" id="recruitment" name="recruitment" method="post">

	
  <div class="form-group">
  
  <h4 class="inline-heading">Sub Products List</h4>
	
  <section class="main_content">
	<div class="container-fluid">
    	
    	<div class="row">
        	<div class="col-sm-12">
            	
				<form name="master_add" id="master_add" method="post" target="iframe">
            	<div class="listwrapper">
			<div class="table-responsive"> 
                <table id="order_item_table_1" class="table listhead table-bordered table-hover">
                    <thead>
                    <tr>
						<th>Sub Products </th>	
						<th>HSN/SAC</th>
						<th>Material Category</th>
						<th>Dimension</th>						
						<!-- <th>Qty</th>-->							
						<th>Buffing Price / Hour</th>
						<th>Machining Price </th>
						<th>Price</th>
						<th>Process Involve</th>
						<th>Machining Involve</th>					
						<th><label class="btn btn-danger">X</label></th>
                    </tr>
                    </thead>
								
					<tbody  id="order_item_body_1"  name="order_item_row_1_1">
					<tr id="order_item_row_1_1" name="order_item_row_1_1">
					<div id="product_select" style="display:none;">				
					<option value="">Select</option>
					<?php foreach($product_list as $va=>$key){ ?>
					<option value="<?php echo $key["id"]; ?>"><?php echo $key["name"]; ?></option>
					<?php } ?>
					</div>
					
					<div id="material_select" style="display:none;">				
						<option value="" selected disabled>Select</option>
						<?php if(!empty($material_list)) foreach($material_list as $va=>$key){	?>
						<option value="<?php echo $key["id"];?>"><?php echo $key["category"];?></option>
						<?php  } ?>
					</div>
					<td>
						<input type="text" class="form-control" name="name[1]" id="name_1" value="" />
					</td>
					<td>			 
						<input type="text" class="form-control" name="hsn[1]" id="hsn_1" value=""  />
					</td>			
					<td  style="padding: 4px;" >
						<select required id="material_1" name="material[1]" class="form-control" onchange="show_hide(this.value,1)" >
							<option value="" selected disabled >Select</option>
							<?php if(!empty($material_list)) foreach($material_list as $va=>$key){?>
							<option value="<?php echo $key["id"];?>"><?php echo $key["category"];?></option>
							<?php  } ?>
					</select>
					</td>
					<td>			 
					<input type="text" class="form-control" name="dimension[1]" id="dimension_1"value="" required />
					</td>
					<td>
					<input type="text" class="form-control hidden_id1" name="price[1]" id="price_1" value="" />
					</td>
					<td>
					<input type="text" class="form-control hidden_id1" name="machining_price[1]" id="machining_price_1" value="" />
					</td>
					<td>
					<input type="text" class="form-control" name="product_price[1]" id="product_price_1" value="" />
					</td>
					<td align="center">
					<input type="checkbox" class="hidden_id1"  name="process[1]" id="process_1" value="1"  >
					</td>
					<td align="center">
					<input type="checkbox" class="hidden_id1"  name="machining[1]" id="machining_1" value="1"   >
					</td>	
					<td></td>
					</tr>
				   </tbody>
				</table>							
        <input type="hidden" value="1" id="order_item_row_index_1" name="order_item_row_index_1">
    </div>
	
	 </div>  <!------End--- Table Responsive -->
	 <br/> <br/>
	 <div class="addnew_wrapp clearfix">
		 <div class="addnew" align="right">
		<a href="#" onclick="add_order_row(1)" class="" style="padding-top: 8px;padding-bottom: 8px;padding-left: 3px;padding-right: 3px;"> ADD NEW</a>
	   </div>
	  </div>
	 </div>
	 
	 <div class="col-md-12">
			 <button type="button" onclick="window.location.href='category_list.php'" class="btn btn-primary">Cancel</button>
			 <button type="submit" id="button_id" class="btn btn-primary">Submit</button>
		</div>
				</form>
            </div>
        </div>

    </div>
	<div aria-hidden="true"  aria-labelledby="myModal" role="dialog" tabindex="-1" id="myModal" class="purchase modal fade">
                  <div class="modal-dialog">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" onclick="cancel_data()" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h3 class="modal-title text-center">Master View</h3>
                              
                          </div>
                          <div class="modal-body">
                          <iframe id="iframe_show_vehicle_data" frameborder="0" marginwidth="0" marginheight="0"  scrolling="no"  style="width:100%;border:0px;">
				</iframe>
						
                          <div class="modal-footer">
                              
    </div>
    </div>
    </div>
    </div>
    </div>
	</section>



</form>
<?php } include("template.php"); ?>  