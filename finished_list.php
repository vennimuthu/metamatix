<?php function main(){
	$db_query=new db_query();  
	global $db_helper_obj;
	$db_helper_obj=new db_helper();


$finished_list=$db_helper_obj->finished_list();
?>
    
	<form id="receipt_list" name="receipt_list" method="post">
	<div class="container-fluid">
			<?php if($_SESSION["user_type"]==4){ ?>
			<a href="finished_add.php" class="list_add pull-right"><img src="images/edit.ico"></a>
			<?php } ?> 	
    	<div class="row">
        	<div class="col-sm-12">
			
            	<!--<h4  class="inline-heading">Finished Goods List</h4>-->
						
				<div class="listwrapper">
                	<div class="table-responsive" id="table_append"> 
                            <table id="example" class="table listhead table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Date</th>
										<th>Movement No</th>	
										<th>Edit</th>
                                    </tr>
                                </thead>
                                <tbody>
                    <?php
						$count=1; 
						if(!empty($finished_list)){
						foreach($finished_list as $va=>$key){
					?>  
					 <tr>
                        <td>
                         <?php echo $count; ?>
                        </td>
                        <td>
						<a data-toggle="modal" href="#myModal" data-backdrop="static" data-keyboard="false" onclick="show_order_data(<?php echo $key["id"]; ?>,<?php echo $key["status"]; ?>)" >
						<?php echo date('d-m-Y',$key["mov_date"]);	?>

						</a>
						</td>
						
						<td><?php echo $key["mov_no"]; ?></td>
						<td>
						<?php if($key["status"]==1)						
			             echo"
                          <a href='finished_edit.php?process_id=".$key["id"]."' class='list_add' ><Img src='images/edit.png'> </a> ";
												
						  ?>
						</td>
						
                        </tr>
					<?php  $count++; } }else{ ?>
					 <tr>
                        <td align="center" colspan="4">There are no list generated</td>
					</tr>
					<?php } ?>				
					            </tbody>
                            </table>
                        </div>  <!------End--- Table Responsive -->
                </div>
				
            </div>
        </div>
    </div>
	</form>
<div aria-hidden="true" aria-labelledby="myModal" role="dialog" tabindex="-1" id="myModal" class="purchase modal fade">
<div class="modal-dialog">
  <div class="modal-content">
	  <div class="modal-header">
		  <button type="button" class="close"  onclick="cancel_data()"  data-dismiss="modal" aria-hidden="true">&times;</button>
		  <h3 class="modal-title text-center">Finished View</h3>
		  
	  </div>
	  <div class="modal-body">
<iframe id="iframe_show_vehicle_data" frameborder="0" marginwidth="0" marginheight="0"  scrolling="no"  style="width:100%;border:0px;">
</iframe>
	  </div>                          
  </div>
</div>
</div>
<div aria-hidden="true" aria-labelledby="myModal1" role="dialog" tabindex="-1" id="myModal1" class="purchase modal fade">
<div class="modal-dialog">
  <div class="modal-content">
	  <div class="modal-header">
		  <button type="button" class="close"  onclick="cancel_data()"  data-dismiss="modal" aria-hidden="true">&times;</button>
		  <h3 class="modal-title text-center">Buffing Approve</h3>
		  
	  </div>
	  <div class="modal-body">
<iframe id="iframe_show_vehicle_data1" frameborder="0" marginwidth="0" marginheight="0"  scrolling="no"  style="width:100%;border:0px;">
</iframe>
	  </div>                          
  </div>
</div>
</div>
<?php } include("template.php"); ?>  
<script type="text/javascript">
function Admin_forward(admin){
  document.getElementById("admin_approve").value=admin;
}
function filter_receipt()
{
	var form = document.getElementById("receipt_list");
	 $.ajax({url: "receipt_ajax.php", type: 'POST',data: new FormData(form),contentType:false, processData: false, success: function(result){
	$("#table_append").html(result);
	}
}); 
}
function payment_update(obj,buffid){
	var check=0;
	if(obj.checked==true)
		var check=1;		
	 $.ajax({url: "process_ajax.php?check="+check+"&buffid="+buffid+"", type: 'POST' ,contentType:false, processData: false, success: function(result){
	   //$("#table_append").html(result);
	}
}); 
}
function show_order_data(supp,status)
{
			
	
if(status==1){
$.ajax({url: "finished_view.php?process_id="+supp,contentType:false, processData: false, success: function(result){
	var ser = document.getElementById('iframe_show_vehicle_data');
	ser.contentDocument.write(result);
	}
});	
}else if(status==2){
$.ajax({url: "finishedappr_view.php?process_id="+supp,contentType:false, processData: false, success: function(result){
	var ser = document.getElementById('iframe_show_vehicle_data');
	ser.contentDocument.write(result);
	}
});	
}else if(status==3){
$.ajax({url: "finishedapprove_view.php?process_id="+supp,contentType:false, processData: false, success: function(result){
	var ser = document.getElementById('iframe_show_vehicle_data');
	ser.contentDocument.write(result);
}});
}


}
function show_order_data1(supp){
	$.ajax({url: "process_approve.php?process_id="+supp,contentType:false, processData: false, success: function(result){
		var ser = document.getElementById('iframe_show_vehicle_data1');
		ser.contentDocument.write(result);
	}
	});
}
function cancel_data(){
		var iframe = document.getElementById("iframe_show_vehicle_data");
		var html = "";

		iframe.contentWindow.document.open();
		iframe.contentWindow.document.write(html);
		iframe.contentWindow.document.close();
}
function create_hidden(ids){
  document.getElementById("forward_id").value=ids;
}

</script>
