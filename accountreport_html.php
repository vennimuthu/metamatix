<table border="1" style="border-collapse:collapse;font-size: 16px;">
  <thead>
                                    <tr>
                                        <th>No</th>										
                                        <th>Invoice No</th>
										<th>GRN No</th>
										<th>Supplier Name</th>
										<th>Particular</th>
										<th>Qty Approved</th>
										<th>Rate</th>
										<th>Amount</th>
										<th>CGST</th>
										<th>SGST</th>
										<th>IGST</th>
										<th>GRAND TOTAL</th>										
									</tr>
                                </thead>
                                <tbody>
					<?php $count=1; $rotu=1;  if(!empty($account_list)){
					foreach($account_list as $va1=>$key1){
					foreach($key1 as $va=>$key){
					?>  
					<tr>
                        <td>
                         <?php echo $count; ?>
                        </td>
						<td><?php echo $key["inv_no"]; ?></td>
						<td><?php echo $key["grn_no"]; ?></td>
						<td>						
                        <?php
						$subname=$db_helper_obj->supplier_edit($key["supplier"]);
						echo $subname[0]["supplier_name"];
						?>
                        </td>
						<td><?php $subproductname=$db_helper_obj->total_product_edit($key["product_id"]);
						echo $subproductname[0]["name"]; ?></td>
						<td><?php echo $key["approve"]; ?></td>
						<td><?php echo $key["product_price"]["price"]; ?></td>
						<td><?php echo $totalamount=$key["approve"]*$key["product_price"]["price"];  $over_amount+=$totalamount; ?></td>
						<td><?php echo $cgst_amount=($totalamount*$key["cgst"])/100;  $over_cgstamount+=$cgst_amount;					
						?></td>
						<td><?php echo $sgst_amount=($totalamount*$key["sgst"])/100;  $over_sgstamount+=$sgst_amount; ?></td>
						<td><?php echo $igst_amount=($totalamount*$key["igst"])/100; $over_igstamount+=$igst_amount; ?></td>
						<td><?php echo $grand=$totalamount+$cgst_amount+$sgst_amount+$igst_amount; $grand_total+=$grand; ?></td>

				   </tr>
					
					<?php $count++; 
				
				   $cgst=$key["cgst"];
				   $sgst=$key["sgst"];
				   $igst=$key["igst"];				   
					} $rotu=1;				 
					?>
					<tr>					 
					<td colspan="8" align="right" ><label><?php echo $over_amount;  ?></label></td>
					<td><label><?php echo $over_cgstamount;  ?></label></td>
					<td><label><?php echo $over_sgstamount;  ?></label></td>
					<td><label><?php echo $over_igstamount;  ?></label></td>
					<td><label><?php echo $grand_total;  ?></label></td>
					 
					</tr>
					 
					<?php }}else{ ?>
					 <tr>
                        <td align="center" colspan="7">There are no list generated</td>
					</tr>
					<?php } ?>				
					            </tbody>
</table>