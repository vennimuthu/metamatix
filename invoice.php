<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>HTML</title>
<style>
.container {
  width: 800px;
}

.wrapper {
   width: 100%;
   border: 1px solid #000;
   margin: 10px 0; 
  
  .row{
    border-bottom: 1px solid #000;
    
    .border-right {
      border-right: 1px solid #000;
    }
  }
  
  .no-border {
    border-bottom: 0;
  }
}

.margin-0 {
  margin: 0;
}

.padding-0 {
  padding: 0;
}

img {
  height: 30px;
}

.table {
  width: 100%;
  margin: 0;
  > tbody > tr {
    > td {
      border: 1px solid #000;
      padding: 5px 0 5px 10px;
      width: 50%;
    }
    
    .first {
      border-top: 0;
    }
    
    .last {
      border-bottom: 0;
    }
    
    b {
      display: block;
    }
    
    span {
      font-size: 12px;
    }
  }
}

.invoice-bill {
  width: 100%;
  
  td {
    border: 1px solid #000;
    padding: 5px;
  }
  
  .serial-no {
    width: 95px;
  }
  
  .particulars {
    width: 300px;
  }
  
  .amount {
    width: 150px;
  }
  
  .particular-items {
    padding: 5px 10px;
  }
  
  .amount-items {
    padding: 5px 0;
  }
}

.entry-id {
  margin: 35px 0;
}

.bank-details {
  margin-top: 30px;
}

.authorised-sign-wrapper {
  width: 300px;
  border: 1px solid #000;
  padding: 5px 20px;
}
table.invoice-bill tr td { font-size: 20px; }
</style>
</head>
<body>
<div class="container">
  <div class="wrapper">
     
    <div class="row margin-0">
      <div class="col-sm-12 padding-0">
        <table style="overflow: auto | hidden | visible | wrap" class="invoice-bill"  border="1"  cellspacing="0" cellpadding="10" >
		<tr>
            <td align="center" valign="middle" colspan="7">
				 <b>PURCHASE ORDER</b>
            </td>
		</tr>
		<tr>
		<td align="" style="border-right:none;">   
		<img width="19%" height="15%" src="images/tally_logo.jpg" alt="metamatix" />
		</td>
		<td align="" style="border-left:0px;width:70px" colspan="2">    
			<b>Invoice&nbsp;To</b><br/>
			METAMATIX&nbsp;SOLUTIONS&nbsp;PRIVATE&nbsp;LIMITED,<br/>
			SHED NO:27, SIDCO INDUSTRIAL ESTATE,<br/>
			KURICHI<br/>
			COIMBATORE<br/>
			Tamil Nadu - 641 021<br/>
			<b>GSTIN: 33AAKCM7413N1Z5</b>
		</td>
		<td colspan="4" valign="top" >             
		  <b>PO&nbsp;Number&nbsp;&nbsp;:</b>&nbsp;&nbsp;&nbsp;<?php echo $_POST['pdfinfo']['pur_number']; ?><br/><br/>
		  <b>PO&nbsp;Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</b>&nbsp;&nbsp;<?php echo date('d-M-Y',strtotime($_POST["pdfinfo"]["dates"])); ?><br/><br/>
		  <b>Vendor&nbsp;ID&nbsp;&nbsp;&nbsp;&nbsp;:</b>&nbsp;&nbsp;&nbsp;<?php echo $_POST["pdfinfo"]["vendor_number"]; ?>
		</td>
        </tr>
		<tr><td colspan="7"><br/></td></tr>		
		<tr>
            <td colspan="3" align="center" >
              <b>Supplier:</b>             
            </td>
			<td colspan="4"  align="center">
              <b>Ship To:</b>              
            </td>
			</tr>
		 <?php echo $ship_purchase; ?>
         <tr><td colspan="7">&nbsp;</td></tr>
		<tr>
            <td colspan="2" align="center" >
              <b>Despatch Through</b>             
            </td>
			<td colspan="2"  align="center">
              <b>Payment Terms</b>              
            </td>
            <td colspan="3"  align="center">
              <b>Required By Date</b>             
            </td>
          </tr>
		  <tr>
            <td colspan="2"  align="center">            
              <p>By Road -Transport</p>
            </td>
			<td colspan="2"  align="center">             
              <p><?php echo $_POST["pdfinfo"]["payment_terms"]; ?></p>
            </td>
            <td colspan="3"  align="center">             
              <p><?php echo date('d-M-Y',strtotime($_POST["pdfinfo"]["required_date"])); ?></p>
            </td>
          </tr>
		  <tr><td colspan="7">&nbsp;</td></tr>
          <tr>
            <td class="serial-no">
              <b>S No.</b>
            </td>
            <td class="particulars">
              <b>Particulars</b>
            </td>
			<td class="">
              <b>HSN</b>
            </td>
			<td class="">
              <b>Dimension</b>
            </td>
			<td class="">
              <b>Qty</b>
            </td>
			<td class="">
              <b>Rate</b>
            </td>
            <td class="amount">
              <b>Amount</b>
            </td>
          </tr>
           <?php echo $arr; ?>
		  <tr>
		  <td rowspan="<?php echo $rowspan; ?>" colspan="5"><b>Amount chargeable (in words)</b>
          <b><p><b>INR <?php echo $_POST["pdfinfo"]["words"]; ?></b></p></td>
		  </tr>
          <tr>
            <td  align="right">
              <b>Sub&nbsp;Total</b>
            </td>
            <td><?php echo sprintf("%.2f",$_POST["pdfinfo"]["sub_totall"]); ?></td>
          </tr>
		  <?php echo $val; ?> 
		  <tr>           
            <td align="right">
            <b>Total</b>
            </td>
            <td><?php echo sprintf("%.2f",$_POST["pdfinfo"]["orders"]); ?></td>
          </tr>
		   <tr><td style="border-bottom: none;"  colspan="7">
		   
		   <?php 
			$counts=10-count($final_array);
		   for($i=0;$i<$counts;$i++){ echo "<br/><br/>";   } ?>
		   
		   
		   </td></tr>
		   <tr>
           
            <td style="border-top: 0px;" colspan="2">
             Company's PAN :  <b>AAKCM7413N</b><br/>
			<u> Declaration :</u><br/>
We declare that this invoice shows the actual price of the<br/>
goods described and that all particulars are true and correct.
            </td>  
		  <td colspan="5" align="right"><p><b>for METAMATIX SOLUTIONS PRIVATE LIMITED</b></p>
          <br> <br> <br> <br> <br>
          <p>Authorised Signature</p></td>			
          </tr>
        </table>
		<p align="center">This is a Computer Generated Document</p>
      </div>
    </div> 
  </div>
</div>
</body>
</html>
