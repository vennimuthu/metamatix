<?php function main(){
$db_query=new db_query();  
global $db_helper_obj;
$db_helper_obj=new db_helper();

$finished_list=$db_helper_obj->finished_list();
$fgreport_list=$db_helper_obj->fgreport_list();
foreach($finished_list as $va=>$key){
	$products_arr=convert_array($key["products"]);	
	foreach($products_arr as $va1=>$key1){
	$already_processed[$key1["product_id"]]["produced_qty"]+=$key1["produced_qty"];
	}
}

foreach($fgreport_list as $va=>$key){
	$products=convert_array($key["products"]);	
	foreach($products["list"] as $va1=>$key1){
		$fi_qty[$va1]+=$key1["billing_qty"];
	}
}

if(!empty($_POST)){
	$process=$db_helper_obj->finished_insert();	
	header('Location:  finished_list.php');
}
$product_list=$db_helper_obj->product_list();
$subproduct_list=$db_helper_obj->subproduct_list();

$openstock_list=$db_helper_obj->openstock_list();
$fgreturn_list=$db_helper_obj->fgreturn_list();

foreach($fgreturn_list as $va5=>$key5){
$fgreturn[$key5["product_id"]]["return_qty"]+=$key5["qty"];
}
foreach($openstock_list as $va5=>$key5){
$openstock[$key5["subproduct_id"]]["id"]=$key5["subproduct_id"];
$total_product_edit=$db_helper_obj->total_product_edit($key5["subproduct_id"]);
if($total_product_edit[0]["process"]==0&&$total_product_edit[0]["machining"]==0){
$openstock[$key5["subproduct_id"]]["processed_qty"]=$key5["processed_qty"];
}
if($total_product_edit[0]["process"]==0&&$total_product_edit[0]["machining"]==1){
$openstock[$key5["subproduct_id"]]["processed_maching"]=$key5["processed_maching"];
}
if($total_product_edit[0]["process"]==1&&$total_product_edit[0]["machining"]==0){
$openstock[$key5["subproduct_id"]]["processed_buff"]=$key5["processed_buff"];
}
if($total_product_edit[0]["process"]==1&&$total_product_edit[0]["machining"]==1){
$openstock[$key5["subproduct_id"]]["processed_buff"]=$key5["processed_buff"];
}
	$openstock[$key5["subproduct_id"]]["finished_qty"]=$key5["finished_qty"];
	//$openstock[$key5["subproduct_id"]]["processed_buff"]=$key5["processed_buff"];
	//$openstock[$key5["subproduct_id"]]["processed_maching"]=$key5["processed_maching"];
}

$total_product_list=$db_helper_obj->total_product_list();
$buffing_list=$db_helper_obj->buffing_list();
foreach($buffing_list as $va=>$key){
	$products=convert_array($key["products_approve"]);
	if($key["admin_approved"]==1){
		foreach($products as $va1=>$key1){
			$buffed[$key1["product_id"]]["product_id"]=$key1["product_id"];
			$buffed[$key1["product_id"]]["approve"]+=$key1["approve"];
			$buffed[$key1["product_id"]]["reject"]+=$key1["reject"];	
		}	
	}
}
 
foreach($openstock as $va2=>$key2){
	$total_buffed[$key2["id"]]["product_id"]=$key2["id"];
	if(($key2["processed_qty"]+$buffed[$key2["id"]]["approve"]+$key2["processed_buff"]+$key2["processed_maching"])>=$key2["finished_qty"])
		$total_buffed[$key2["id"]]["approve"]=($key2["processed_qty"]+$buffed[$key2["id"]]["approve"]+$key2["processed_buff"]+$key2["processed_maching"])-$key2["finished_qty"];	
}
foreach($product_list as $va=>$key){
$total_arr[$key["id"]]["product_id"]=$key["id"];
$total_arr[$key["id"]]["product_name"]=$key["product_name"];
$total_arr[$key["id"]]["stocks"]=($key["open_qty"]+$already_processed[$key["id"]]["produced_qty"])-$fi_qty[$key["id"]];
$total_arr[$key["id"]]["series"]=$key["series"];
foreach($subproduct_list as $va3=>$key3){
if($key3["product_id"]==$key["id"])	
$total_arr[$key["id"]]["subproducts"][]=$key3;
}
}
//echo"<pre>product_list"; print_r($product_list); echo"</pre>";
 
foreach($total_arr as $va5=>$key5){
	foreach($key5["subproducts"] as $va6=>$key6){
		$posibleqty[$va5][$key6["name"]]=floor($total_buffed[$key6["name"]]["approve"]/$key6["qty"]);
	}
}
 
foreach($posibleqty as $va7=>$key7){
$min = null;
foreach ($key7 as $a) {
    if ($min === null) 
        $min = $a;
    else if ($min > $a) 
        $min = $a;
}
$possible[$va7]=$min;	
}

foreach($possible as $va4=>$key4){	
	$possible[$va4]=$key4;
}
if(count($finished_list)>0)
	$inward_no="MMSPL-FG-".sprintf("%03s",count($finished_list)+1);
else
	$inward_no="MMSPL-FG-001";
?>
<form name="finished_add" id="finished_add" method="post" onsubmit="form_submit();">
<table class="table listhead table-bordered table-hover" align="center" cellspacing="0" cellpadding="0">
<tr>
<td colspan="6" align="center" valign="middle"><label>FINISHED GOODS MOVING TO STOCK REGISTER</label></td>
</tr>

<tr>
<td style="width: 10%;" ><label>Movement No:</label></td>
<td>
<label><?php echo $inward_no; ?>
<input type="hidden" name="mov_no" id="mov_no" value="<?php echo $inward_no; ?>" >
</label>
</td>
<td style="width: 10%;"><label>Movement Date:</label></td>
<td >
<input type="date" name="mov_date" id="mov_date" value="<?php echo date('Y-m-d'); ?>" required class="form-control">
</td>
</tr>

<tr>
<td colspan="6">
<table id="contractor_tb" style="width: 100%;" class="table listhead table-bordered table-hover" align="center" cellspacing="0" cellpadding="0">
<thead>
<th align="center" style="width: 8%;" >S No</th>
<th align="center" style="width: 20%;">Item Name</th>
<th align="center">Series</th>
<th align="center">Stock in Hand</th>
<th align="center">Qty Produced</th>
<th align="center">Remarks</th>
<th align="center">Approve</th>
</thead>
<?php $count=1; $num=0;
foreach($total_arr as $va1=>$key1){
if($count<=7){
?>
<tr <?php echo $style; ?> id="product_tr_<?php echo $num; ?>" >
<td align="center"><?php echo $count; ?></td>
<td style="padding: 4px;" align="center">

<select <?php if($count==1) echo "required"; ?> id="product_id_<?php echo $count ?>" name="product_id[]" class="form-control tbox_small removes" onchange="product_choose(this,<?php echo $count ?>);intializeSelect();">
	<option value="" selected disabled >Select</option>
	<?php if(!empty($total_arr)) foreach($total_arr as $va=>$key){
	if( $possible[$key["product_id"]]!=0){ ?>
	<script type="text/javascript">
		//product_choose(document.getElementById(''),<?php echo $count ?>);
		//intializeSelect();
	</script>
	
	<option value="<?php echo $key["product_id"];?>" prdct_price="<?php echo $key["price"];?>" 
	stock="<?php echo $key["stocks"]; ?>" possible="<?php echo $possible[$key["product_id"]];	?>" series="<?php echo $key["series"];	?>" ><?php
	echo $key["product_name"];?></option>
	<?php }	} ?>
</select>
</td>

<td align="center" style="padding: 4px;"><input type="text" name="series[]" id="series_<?php echo $count; ?>" onblur="calc_price(this,'<?php echo $count; ?>')" class="form-control tbox_small" value="" ></td>
<td align="center" style="padding: 4px;">
<input type="number" min="0" disabled name="qty[]" id="quatity_<?php echo $count; ?>" class="form-control tbox_small" value="">
<input type="hidden" name="stock_hand[]" id="stock_hand_<?php echo $count; ?>" value="">
</td>

<td  align="center" style="padding: 4px;">
<input type="number" min=1 name="produced_qty[]" data-backdrop="static" data-keyboard="false" onblur="find_max(this,'<?php echo $count; ?>',this.max);"  id="produced_qty_<?php echo $count; ?>" class="form-control tbox_small">
</td>
<td  align="center"style="padding: 4px;">
<input type="text" class="form-control tbox_small" name="remarks[]" id="remarks_<?php echo $count; ?>" value="<?php echo $key["amount"]; ?>">
</td>
<td>
<input type="checkbox" name="approve[]" id="approve_<?php echo $count; ?>" value="1" onclick="apprve_ajax('<?php echo $count; ?>')">
</td>

</tr>
<?php $count++; }}?>

<tr>
<td align="center" colspan="4"  rowspan="1">
<input type="text"class="form-control"  style="resize:none;" name="passed" id="passed" placeholder="Entry Passed By" required >
<br/>
<label>Entry Passed By</label>
</td>
	<td>
	<label>Total Qty :-</label>
	</td>
	<td colspan="2">
	<label id="total_buffed"></label>
	<input type="hidden" name="buffed_total" id="buffed_total">
	</td>
</tr>
</table>
</td>
</tr>
</table>
	<div align="center">
		<button type="button" onclick="window.location.href='finished_list.php'" class="btn btn-primary">Cancel</button>
		<button type="submit"  class="btn btn-primary" onclick="form_submit(event)" >Save</button>
		<button type="submit" style="display:none;" class="btn btn-primary"></button>
	</div>
	<div aria-hidden="true" aria-labelledby="myModal" role="dialog" tabindex="-1" id="myModal" class="purchase modal fade">
<div class="modal-dialog">
  <div class="modal-content">
	  <div class="modal-header">
		  <button type="button" class="close"  onclick="cancel_data()"  data-dismiss="modal" aria-hidden="true">&times;</button>
		  <h3 class="modal-title text-center">Finished Possible</h3>
	  </div>
	<div class="modal-body">
	<iframe id="iframe_show_vehicle_data" frameborder="0" marginwidth="0" marginheight="0"  scrolling="no" style="width:100%;border:0px;">
	</iframe>
	</div>                          
  </div>
</div>
</div>
</form>
<?php } include("template.php"); ?>  
<script>
function intializeSelect() {
    // this "initializes the boxes"
	$('.removes').each(function(box) {
      var value = $('.removes')[box].value;
      if (value) {
        $('.removes').not(this).find('option[value="' + value + '"]').hide();
      }
    });
  };  

  // this is called every time a select box changes
  $('.removes').on('change', function(event) {
    $('.removes').find('option').show();
    intializeSelect();
  });
  function apprve_ajax(count){
	 //if(document.getElementById("<?php echo $count; ?>"))
	var form=document.getElementById("finished_add");
	$.ajax({url:"finished_ajax.php",
		  data: new FormData(form),
		  type:"post",contentType:false,async:false,processData:false,cache:false,
		  error:function(err)
		  {
			  $("#order_transaction_error_msg").html("Transaction Failled"+err);
		  },
		  success:function(data)
		  {
			$('.removes').each(function(box) {
			var nameob=document.getElementsByName("product_id[]");
			var len=nameob.length;
			for(var i=0;i<len;i++){		
			if(("product_id_"+count!=nameob[i].id)&&($("#"+nameob[i].id+" option:selected").val()=="")){
			var slect_obj=document.getElementById(nameob[i].id);		
			slect_obj.innerHTML=data;
			}
			}
			});
			intializeSelect();
		  }
		});
}

function find_max(obj,count,maxi){
	
	var nameobj=document.getElementById("product_id_"+count);
	
	var len=nameobj.length;
	var select=0;	
	for(var i=0;i<len;i++){
		if(nameobj[i].selected==true&&nameobj[i].value!="")
			select=1;
		var productid=nameobj[i].value;
	}
	if(select==0){
		alert("Please Select Item Name First");
		obj.value="";
		nameobj.focus();
		return false;
	}
	var maxm=obj.getAttribute('max');

	if(Number(obj.value)>Number(maxm)){
		
		alert("Only "+maxm+" Quantity is Produced");
		$('#myModal').modal('show');
		var form=document.getElementById("finished_add");
		$.ajax({url:"finished_popup.php?product_id="+productid+"&maxi="+maxi+"",
		  data: new FormData(form),
		  type:"post",contentType:false,async:false,processData:false,cache:false,
		  error:function(err)
		  {
			  $("#order_transaction_error_msg").html("Transaction Failled"+err);
		  },
		  success:function(data)
		  { 
		  var ser = document.getElementById('iframe_show_vehicle_data');
	ser.contentDocument.write(data);
			intializeSelect();
		  }
		});
		obj.value="";
		return false;
	}	
	var qtyobj=document.getElementsByName("produced_qty[]");
	var len1=qtyobj.length;
    var total=0;
	
	for(var i=0;i<len1;i++){
		total+=Number(qtyobj[i].value);
	}
	document.getElementById("total_buffed").innerHTML=total;
	document.getElementById("buffed_total").value=total;
}

function product_choose(obj,count){
var series=obj.options[obj.selectedIndex].getAttribute('series');
var stocks=obj.options[obj.selectedIndex].getAttribute('stock');
document.getElementById('series_'+count).value=series;
document.getElementById('quatity_'+count).setAttribute("max", stocks);
document.getElementById('quatity_'+count).setAttribute("required", true);
document.getElementById('quatity_'+count).value=stocks;
document.getElementById('stock_hand_'+count).value=stocks;

var possible=obj.options[obj.selectedIndex].getAttribute('possible');
document.getElementById('produced_qty_'+count).setAttribute("max", possible);
}

function cancel_data()
{
var iframe = document.getElementById("iframe_show_vehicle_data");
var html = "";

iframe.contentWindow.document.open();
iframe.contentWindow.document.write(html);
iframe.contentWindow.document.close();
}
</script>