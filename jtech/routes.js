module.exports = {
	
  configure: function(app) {
	  
	app.post('/create_info/', function(req, res) {
		var info = require('./models/information');
		info.create_info(req, res);
	});
	app.post('/register/', function(req, res) {
		var info = require('./models/information');
		info.register(req, res);
	});
	app.post('/login', function(req, res) {
		var info = require('./models/information');
		info.login(req, res);
	});
	app.get('/get_info',function(req, res) {
		var info = require('./models/information');
		info.get_info(req,res);
	});
	app.get('/get_info/:id',function(req, res) {
		var info = require('./models/information');
		info.get_info_id(req,res);
	});
	app.post('/generate',function(req, res) {
		var info = require('./models/information');
		info.generate_pdf(req,res);
	});
	/* app.post('/view_pdf',function(req, res) {
		var info = require('./models/information');
		info.generate_pdf(req,res,'view');
	}); */
	app.post('/view_pdf',function(req, res) {
		var info = require('./models/information');
		info.view_pdf(req,res);
	});
        app.post('/generate_mob',function(req, res) {
		var info = require('./models/information');
		info.generate_mob_pdf(req,res);
	});
	

 }
};
