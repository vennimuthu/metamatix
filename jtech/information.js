function information() {
	//fn to register user
	this.create_info = function(req,res) {
		var data_to_return={'status':400,'msg':'Invalid Data','data':{}};
		//console.log(req.sanitize('update').escape().trim());
		req.assert('update_desc', 'Invalid Update').notEmpty();
		req.assert('best_worst', 'Invalid best or worst').notEmpty();
		req.assert('significance', 'Invalid Significance').notEmpty();
		req.assert('emotions', 'Invalid emotions').notEmpty();
		var errors = req.validationErrors();
		if (errors) {
			data_to_return['data']['error_msg']=errors[0]['msg'];
			// returning the error
			res.send(data_to_return);
			return;
		}
		var data_sant={};
		data_sant['update_desc']=req.sanitize('update_desc').escape().trim(); 
		data_sant['best_worst']=req.sanitize('best_worst').escape().trim();
		data_sant['significance']=req.sanitize('significance').escape().trim();
		data_sant['emotions']=req.sanitize('emotions').escape().trim();
		data_sant['user_id']=1;
		data_sant['creation_time']=Date.now();
		var insert_data_rtn=function(err, result2) {
			if (err==1){
				res.send({status: 500, msg: 'Query failed'});
				return;
			}else{
				data_to_return['status']=200;
				data_to_return['msg']="success";
				data_to_return['data']['ins_id']=result2.insertId;
				res.send(data_to_return);
			}
								}
		db_query.insert_query("information",data_sant,insert_data_rtn);
		
		
		
};

	this.get_info = function(req,res) {
		var data_to_return={'status':400,'msg':'Invalid Data','data':{}};
		// setup validation
		//req.assert('mob_number', 'Invalid Mobile Number').isInt().len(10,10);
		// validating errors
		var errors = req.validationErrors();
		if (errors) {
			data_to_return['data']['error_msg']=errors[0]['msg'];
			// returning the error
			res.send(data_to_return);
			return;
		}
		// data sanitisation  
		var data_sant={};
		//data_sant['mobile_no']=req.sanitize('mob_number').escape().trim();
		var user_select_qry_data=function(err , result){
			console.log(result);
				data_to_return['data']=result;
				data_to_return['status']=200;
				data_to_return['msg']="success";				
				res.send(data_to_return);
		}
		global.db_query.select_query("information","id,update_desc,best_worst,significance,emotions","user_id=?",1,"",user_select_qry_data);
	}

	
this.get_info_id = function(req,res) {
var data_to_return={'status':0,'msg':'Invalid Data','data':{}};
		// setup validation
		req.checkParams('id', 'Invalid value').notEmpty().isInt();
		
		// validating errors
		var errors = req.validationErrors();
		if (errors) {
			data_to_return['data']['error_msg']=errors[0]['msg'];
			// returning the error
			res.send(data_to_return);
			return;
		}
		// data sanitisation  
		var v_id=req.sanitizeParams('id').escape().trim(); // getting veh type id
		
		// data sanitisation  
		var data_sant={};
		//data_sant['mobile_no']=req.sanitize('mob_number').escape().trim();
		var user_select_qry_data=function(err , result){
	if (result==''){
									res.send({status: 400, msg: 'empty result'});
	return;}else{
				data_to_return['data']=result;
				data_to_return['status']=200;
				data_to_return['msg']="success";				
				res.send(data_to_return);
	}
		}
		global.db_query.select_query("information","id,update_desc,best_worst,significance,emotions","id=?",v_id,"",user_select_qry_data);
	
		
}
}
	
var self =module.exports = new information();
