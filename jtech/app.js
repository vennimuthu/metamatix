var express = require('express');
var bodyparser = require('body-parser');
var expressValidator = require('express-validator');
global.connection = require('./connection');
var routes = require('./routes');
global.db_query = require('./helper/db_query');
global.fs = require('fs');
var main_1=require('./routes.js');
//global.file_base_path="/etc/gaadizo_new/";
global.server_environment="staging";
global.base_url ="http://23.251.147.227/jtech";

var app = express();
app.use(bodyparser.urlencoded({extended: true}));
app.use(expressValidator());
app.use(bodyparser.json());

connection.init();
routes.configure(app);
var server = app.listen(8050, function() {
  console.log('Server listening on port ' + server.address().port);
});
