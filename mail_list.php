<div class="middle">
<section class="content clearfix" ng-app="myApp" ng-controller="mailCtrl" ng-cloak >

<div class="col-md-12" style="background-color:#ffffff">
 <div class="container cnt_height">
	<br><br>
<h1 class="page_head">
		  Quote History 
		</h1>
    <div class="product_table">
        <div class=" mar-b-30">
                <div>
				<div class="form-group col-xs-1 col-sm-1"   >
					</div>
					
					<div class="pull-left">
					<h4 class="page-title" style="margin-top: 25px;margin-bottom: 20px;">Quote Details</h4></div>
                </div>
				<br></br>
	<!-- TABLE: LATEST ORDERS -->

				<div  class="table-responsive"> 
						<div class="mb-3">
                                <div class="row">
                                    <div class="col-12 text-sm-center form-inline">
                                      
									  
												<div class="col-sm-3">
						<select class="form-control ng-pristine ng-valid ng-empty ng-touched" ng-model="search_user" ng-change="search_form()" name="email_user" style="margin-right:85px;margin-left:-12px;">
								<option value="" selected>Search by sender</option>
								<option ng-repeat="p in email_user" style="width:30px;margin-top: 16px;margin-bottom: 5px;" value="{{p.id}}">{{p.name}}</option>
							</select>
							</div>	
                                    </div>
                                </div>
                            </div>
							
						   
				<table class="table table-bordered ctom_tbl table-striped table-hover" data-page-size="7">
					<thead style="background-color:#4eb7eb">
					
					<tr >
						<!--<th> <span class="pull-left"><a class="fa" ng-click="sortBy('p_name')" ng-class="(propertyName == 'p_name') ? sort_by_class : '' ">
				<span class="footable-sort-indicator" style="color:#000000"><b style="margin-left: 12px;">Product Name</b></span></a></span>
						</th>-->
<th><span class="pull-left"><a class="fa" ng-click="sortBy('p_name')" ng-class="(propertyName == 'p_name') ? sort_by_class : '' "><span class="footable-sort-indicator" style="color:#fff; font-family:segoe ui;"><b style="margin-left: 12px;">S.No</b></span></a></span>
						</th>
						<th><span class="pull-left"><a class="fa" ng-click="sortBy('subject')"  ng-class="(propertyName == 'subject') ? sort_by_class : '' "><span style="color:#fff"><b style="margin-left: 29px;font-family:segoe ui;">Subject</b></span></a></span></th>
						<th > <span class="pull-left"><a class="fa " ng-click="sortBy('to')"  ng-class="(propertyName == 'to') ? sort_by_class : '' "><span style="color:#fff"><b style="margin-left: 47px;font-family:segoe ui;">Email Sent To</b></span></a></span>
						</th>
						<th > <span class="pull-left"><a class="fa " ng-click="sortBy('name')"  ng-class="(propertyName == 'name') ? sort_by_class : '' "><span style="color:#fff"> <b style="margin-left: 25px;font-family:segoe ui;">Email Sent By</b><span></a></span>
						</th>
						<th > <span class="pull-left"><a class="fa " ng-click="sortBy('created_datetime')"  ng-class="(propertyName == 'created_datetime') ? sort_by_class : '' "><span style="color:#fff;font-family:segoe ui;"><b style="margin-left: 43px;">Sent Date & Time</b><span></a></span>
						<th colspan="2"><span class="pull-left"><a class="fa " ><span style="color:#fff;font-family:segoe ui;"><b style="margin-left: 78px;">Action</b><span></a></span>
						</th>
</tr>
					</thead>
				  <!--<tr style="cursor: pointer;" ng-repeat="x in mail_history" data-target=".mail_details" data-toggle="modal"  ng-click="show_detail(x.mail_id)">-->
				  
			<tr ng-repeat="x in mail_history">
			 
			 
					<!--<td>{{ x.product_name }}</td>-->
				  <td align="center">{{ x.mail_id}}</td>
				  
					<td align="center">{{ x.subject }}</td>
					<td align="center">{{ x.to }}</td>
					<td align="center">{{ x.name }}</td>
					<td align="center" ng-bind="formatDate(x.created_datetime) |  date:'dd/M/yyyy @ h:mm a'" ></td>
					<td align="center" style="text-align:center;">
					<a ng-click="send_mail(x.mail_id);$event.stopPropagation()" class="btn btn-info" id="resend">Resend</a>
					<a data-target=".mail_details" data-toggle="modal"  ng-click="show_detail(x.mail_id)" class="btn btn-info" style="left: 9px;right: 52px;padding-right: 20px;">&nbsp;&nbsp;View</a></td>
			 </tr>
				  
				  <div><tr class="active">
					<td colspan="6">
						 <div>
							<ul class="pagination pagination-split justify-content-end footable-pagination m-t-10 m-b-0">
							  <li ng-class="prevPageDisabled()">
								<a href ng-click="prevPage()">« </a>
							  </li>
							  <li ng-repeat="n in range()" ng-class="{active: n == currentPage}" ng-click="setPage(n)">
								<a href="">{{n+1}}</a>
							  </li>
							  <li ng-class="nextPageDisabled()">
								<a href ng-click="nextPage()"> »</a>
							  </li>
							</ul>
						</div>
					</td>
				  </tr>
				  </div>
				</table>
				
				</div>
			</div>
	</div>
	</div>
</div>
<div class="modal send_mail_images" aria-hidden="false" >
		<div class="modal-dialog" style="width: 800px">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					×
				</button>
				<header class="">
					<h3>Image View</h3>
				</header>
			</div>
			<div class="modal-body">
				<img ng-src="{{modal_imagpath}}" height="500px" width="650px" />
			</div>
		</div>
		</div>
	</div>
<div id="mail_details" class="modal fade mail_details" tabindex="-1" role="dialog" 
aria-labelledby="custom-width-modalLabel" aria-hidden="true;" style="margin-left: 314px;">
		<div class="modal-dialog" id="welcomeDiv">
		<div class="modal-content" style="padding-top: 35px;">
			<div class="modal-header" style="background-color:#4eb7eb;">
			 
			<header  class="">
					<h3><center>Mail Details</center></h3>
				<button type="button" style="font-size: 30px;margin-top: 1px;border-left-width: 0px;border-right-width: 0px;left: 793px;right: 32px;top: 52px;border-top-width: -6px;border-bottom-width: 1px;" class="close" data-dismiss="modal" >
						×
						</button>			
		 
				</header>
			</div>
			<div class="modal-body">
		<h4 class="page-title" style="margin-top: 10px;"></h4> 
					<div class="row">
					<div class="col-xs-12">
						<div class="form-group col-xs-12 col-sm-12">
							<label for="email">Product Name</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							{{mail_details.product_name}}
						</div>
				
						
					
					</div>
				</div>
			<div class="row">
					<div class="col-xs-12">	
						<div class="form-group col-xs-12 col-sm-12">
							<label for="email">Email Sent to</label>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{mail_details.sent_to}}
						</div>
						
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group col-xs-12 col-sm-12">
							<label for="email">Subject</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{mail_details.subject}}
						</div>
				
						
					
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group col-xs-4 col-sm-12">
							<label for="email">Email Sent By</label>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{mail_details.sent_by}}
						 
						</div>
						
					</div>
				</div>
 <!-- /.popup-content-start -->
			<div ng-repeat="x in mail_details.dvdata">
				<div ng-repeat="y in choosed_prod">
				<div class="row">
					<div class="col-xs-12">
						
						<div class="form-group col-xs-5 col-sm-12"   >
							<label for="email">Product Name</label>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							{{y.p_name}}
																	
						</div>
						</div>
						
					</div>
				</div> 
				<div class="row" ng-show="x.reference?true:false">
					<div class="col-xs-12">
						<div class="form-group col-xs-1 col-sm-1"   >
							
						</div>
<div  class="form-group col-xs-11 col-sm-11"><b>Reference:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp{{x.reference}}</div>
						
					</div>
				</div>
				<div class="row" ng-show="x.description?true:false">
					<div class="col-xs-12">
						<div class="form-group col-xs-1 col-sm-1"   >
							
						</div>
						<div  class="form-group col-xs-11 col-sm-11">
							<b>Description:</b>&nbsp;&nbsp;&nbsp;{{x.description}}
						</div>
						
					</div>
				</div>
				

				<div class="row" ng-show="x.price?true:false">
					<div class="col-xs-12">
						<div class="form-group col-xs-1 col-sm-1"   >
							 
						</div>
<div  class="form-group col-xs-11 col-sm-11"><b>Price:</b>&nbsp;&nbsp;&nbsp;&nbsp;{{x.price}}
						</div>
						
					</div>
				</div>
				<div class="row" ng-show="checked_data[x.id]['data_arr'].indexOf('4') != -1">
					<div class="col-xs-12">
						<div class="form-group col-xs-1 col-sm-1"   >
							
						</div>
												
					</div>
				</div>
				<div class="row" ng-show="checked_data[x.id]['data_arr'].indexOf('5') != -1">
					<div class="col-xs-12">
						<div class="form-group col-xs-1 col-sm-1"   >
							
						</div>
						</div>
				</div>
				<div class="row" ng-show="x.file[1].length">
					<div class="form-group col-xs-1 col-sm-1">
						
					</div>
					<div class="form-group col-xs-11 col-sm-11">
						<b>Product Images</b>
					</div>
				</div>
				<div class="row" ng-show="x.file[1].length">
					<div class="col-xs-12">
						<div class="form-group col-xs-1 col-sm-1"   >
						</div>
						<div class="form-group col-xs-11 col-sm-11">
							<div ng-repeat="y in x.file[1]" ng-show="{{checked_data[x.id]['file_arr'].indexOf(y.id) != -1}}"  >
								<img   src="{{y.path}}" height="100px" width="100px" />
								<a href="{{y.path}}" target="_blank" class="btn btn-info btn-lg" >View</a>
							</div>
						</div>
					</div>
				</div>
				<div class="row" ng-show="x.file[2].length">
					<div class="form-group col-xs-1 col-sm-1"   >
					</div>
					<div class="form-group col-xs-11 col-sm-11" >
						<b>Technical Document</b>
					</div>
				</div>
				<div class="row" ng-show="x.file[2].length">
					<div class="col-xs-12">
						<div class="form-group col-xs-1 col-sm-1"   >
						</div>
						<div class="form-group col-xs-11 col-sm-11" >
							<div ng-repeat="y in x.file[2]" ng-show="{{checked_data[x.id]['file_arr'].indexOf(y.id) != -1}}" class="pull-left ng-scope">
								<img  src="{{y.path}}" height="100px" width="100px" />
								
								<a href="{{y.path}}" target="_blank" class="btn btn-info btn-lg" >View</a>
							</div>
						</div>
					</div>
				</div>
				<div class="row" ng-show="x.file[3].length">
					<div class="form-group col-xs-1 col-sm-1"   >
					</div>
					<div class="form-group col-xs-11 col-sm-11" >
						<b>Testing Document</b>
					</div>
				</div>
				<div class="row" ng-show="x.file[3].length">
					<div class="col-xs-12">
						<div class="form-group col-xs-1 col-sm-1"   >
						</div>
						<div class="form-group col-xs-11 col-sm-11" >
							<div ng-repeat="y in x.file[3]" ng-show="{{checked_data[x.id]['file_arr'].indexOf(y.id) != -1}}" class="pull-left ng-scope">
								<img   src="{{y.path}}" height="100px" width="100px" />
								<a href="{{y.path}}" target="_blank" class="btn btn-info btn-lg" >View</a>
							</div>
						</div>
					</div>
				</div>
				<div class="row" ng-show="x.file[5].length">
					<div class="form-group col-xs-1 col-sm-1"   >
					</div>
					<div class="form-group col-xs-11 col-sm-11" >
						<b>Patent Document</b>
					</div>
				</div>
	<div class="row" ng-show="x.file[5].length">
	<div class="col-xs-12">
	<div class="form-group col-xs-1 col-sm-1">
		</div
	<div class="form-group col-xs-11 col-sm-11" >
	<div ng-repeat="y in x.file[5]" ng-show="{{checked_data[x.id]['file_arr'].indexOf(y.id) != -1}}" class="pull-left ng-scope">
	<img   src="{{y.path}}" ng-init="file5shown=1" height="100px" width="100px" />
			<a href="{{y.path}}" target="_blank" class="btn btn-info btn-lg" >View</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.popup-content-end -->
			
	
			</div>
		</div>
		</div>
	</div>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">
          <h5 id="alert" style="margin-top:20px;align:center;"></h5>
		  <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-left: 389px;border-radius: 5px; background-color: #008CBA;">Close</button>
        </div>
        </div>
      
    </div>
  </div>
	</section>
</div>

<style>
#welcomeDiv {
		
    margin-right: 500px;
	margin-left: -10px;
	min-width: 700px;
	background-color: #ffffff;
}
</style>
 
<script>

$(document).keyup(function(e){
     if (e.keyCode == 27) {
			$('.mail_details').modal('hide');
		}
    });
	
var app = angular.module('myApp', []);
app.controller('mailCtrl', function($scope, $http) {
	$scope.itemsPerPage = 10;
	$scope.currentPage = 0;
	$scope.propertyName="mail_id";
	$scope.sort_by="desc";
	$scope.total_count_paginate=0;
	var indexer = {};
    $http.get("<?php echo site_url('mailhistory/listdata/'); ?>"+$scope.currentPage+"/"+$scope.itemsPerPage)
    .then(function (response) {
		$scope.mail_histroy_response(response);
	});
	$scope.mail_histroy_response= function(response){
		var rec_data=$scope.mail_history = response.data.mail_list;
		$scope.total_count_paginate=response.data.total_count;
		$scope.mail_history_count=!$scope.mail_history.length;
		for (var i = 0; i < rec_data.length; i++){
			indexer[rec_data[i].mail_id] = parseInt(i);
		}
		//$scope.show_detail(rec_data[0].mail_id);
		if($scope.total_count_paginate<=$scope.itemsPerPage)
			$scope.to_paginate=0;
		else
			$scope.to_paginate=1;
	}
	$http.get("<?php echo site_url('mailhistory/email_user'); ?>")
    .then(function (response){$scope.email_user = response.data;});
	$scope.search_form = function() {
		$scope.currentPage = 0;
		var data_to_pass="1";
		
		if($scope.search_user)
			data_to_pass+="&user_id=" + $scope.search_user;
		$http({
			method: 'POST',
			url: '<?php echo site_url('mailhistory/listdata/'); ?>'+$scope.currentPage+'/'+$scope.itemsPerPage,
			data:  data_to_pass,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		})
		.then(function (response) {
			$scope.sort_by="";
			$scope.propertyName="";
			$scope.sort_by_class="";
			$scope.mail_histroy_response(response);
			});
	};
	$scope.send_mail = function(mailid) {
		$scope.currentPage = 0;
		var data_to_pass="1";				
		data_to_pass+="&mailid=" + mailid;
		$http({
			method: 'POST',
			url: '<?php echo site_url('mail/resend_mail/'); ?>'+$scope.currentPage+'/'+$scope.itemsPerPage,
			data:  data_to_pass,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		})
		.then(function (response) {
			//$scope.sort_by="";
			//$scope.propertyName="";
			//$scope.sort_by_class="";
			//$scope.mail_histroy_response(response);
			$http({
			method: 'POST',
			url: '<?php echo site_url('mailhistory/listdata/'); ?>'+$scope.currentPage+'/'+$scope.itemsPerPage,
			data:  data_to_pass,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		})
		.then(function (response) {
			$scope.sort_by="";
			$scope.propertyName="";
			$scope.sort_by_class="";
			$scope.mail_histroy_response(response);
			});
			});
	};
	$scope.show_detail = function(mail_id) {
		var mail_details={};
		var checked_data={};
		mail_details["sent_to"]=$scope.mail_history[indexer[mail_id]]['to'];
		mail_details.product_name=$scope.mail_history[indexer[mail_id]]['product_name'];
		mail_details.subject=$scope.mail_history[indexer[mail_id]]['subject'];
		mail_details.sent_by=$scope.mail_history[indexer[mail_id]]['name'];
		checked_data=$scope.mail_history[indexer[mail_id]]['product_data'];
		$http.get("<?php echo site_url('mailhistory/mail_list_details'); ?>/"+
		mail_id).then(function (response){ 
			mail_details.dvdata = response.data;
			
		});
		$scope.mail_details= mail_details;
		$scope.checked_data= checked_data;
	};
	$scope.image_popup=function(path){
		$scope.modal_imagpath=path;
		//console.log(image_view);
	}
	// for sorting
	$scope.sortBy = function(propertyName) {
		$scope.currentPage=0;
		$scope.sort_by = ($scope.propertyName === propertyName && $scope.sort_by=='asc')? 'desc': 'asc';
		$scope.propertyName = propertyName;
		$scope.sort_by_class="fa-sort-amount-"+$scope.sort_by;
		$http.post("<?php echo site_url('mailhistory/listdata/'); ?>"+$scope.currentPage+"/"+$scope.itemsPerPage+"/"+propertyName+"/"+$scope.sort_by)
		.then(function (response) {
			$scope.search_user="";
			$scope.mail_histroy_response(response);
		});
	};
	// for pagination
	$scope.range = function(){
		var rangeSize =4;
		if(Math.ceil($scope.total_count_paginate/$scope.itemsPerPage)<4)
			rangeSize =Math.ceil($scope.total_count_paginate/$scope.itemsPerPage)
		var ret = [];
		var start;
		start = $scope.currentPage == 1;
		if ( start > $scope.pageCount()-rangeSize ){
		  start = $scope.pageCount()-rangeSize+1;
		}
		for (var i=start; i<start+rangeSize; i++){
		  ret.push(i);
		}
		return ret;
	};
	$scope.prevPage = function() {
		if ($scope.currentPage > 0) {
			$scope.currentPage--;
			$scope.setPage($scope.currentPage);
		}
	};
	$scope.prevPageDisabled = function() {
		return $scope.currentPage === 0 ? "disabled" : "";
	};
	$scope.pageCount = function() {
		//var dd=$scope.items.length;
		//console.log($scope.total_count_paginate);
		return Math.ceil($scope.total_count_paginate/$scope.itemsPerPage)-1;
	};
	$scope.nextPage = function() {
		if ($scope.currentPage < $scope.pageCount()) {
		  $scope.currentPage++;
		  $scope.setPage($scope.currentPage);
		}
	};
	$scope.nextPageDisabled = function() {
		return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
	};
	$scope.setPage = function(n) {
		$scope.currentPage = n;
		var data_to_pass="1";
		if($scope.search_user)
			data_to_pass+="&user_id=" + $scope.search_user;
		$http({
			method: 'POST',
			url: "<?php echo site_url('mailhistory/listdata/'); ?>"+$scope.currentPage+"/"+$scope.itemsPerPage+"/"+$scope.propertyName+"/"+$scope.sort_by,
			data:  data_to_pass,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		})
		.then(function (response) {
			$scope.mail_histroy_response(response);
		});
	};
	$scope.formatDate = function(date){
          var dateOut = new Date(date);
          return dateOut;
    };
});

 $('#resend').click(function(){
	 	$('#myModal').modal("show");
    $("#alert").html("Resend successfully");
     });
</script>    
    
  