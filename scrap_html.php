<style>
table td{
    border: 1px solid #000;
	height:40px;
}
</style>
<div align="center"><img width="200px" src="images/Metamatix_logo.png" alt="metamatix" /></div>
<div id="reportid">
<table style="border-collapse:collapse;" align="center"  width="100%" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="5" align="center" valign="middle">
      <b>Scrap Movement Report (<?php echo date("d-m-Y",strtotime($_POST["from_date"])).'&nbsp;To&nbsp;'.date("d-m-Y",strtotime($_POST["to_date"])); ?>)</b>
    </td>
  </tr>
 
          <tr>
            <td align="center">
             <b>S No</b>
            </td>
            <td align="center">
             <b>STN No</b>
            </td>
            <td align="center">
             <b>Qty</b>
            </td>
            <td align="center">
             <b>RM Wastage (G)</b>
            </td>
            <td align="center">
             <b>Total Wastage (KG)</b>
            </td>            
          </tr>
        <?php $count=1; $num=0;
		if(!empty($scrap)){
		foreach($scrap as $va1=>$key1){ 
	?>
	<tr id="product_tr_<?php echo $num; ?>" >	
	<td align="center" ><?php echo $count; ?></td>
	<td align="center"> 
	<?php echo $va1; ?></td>
	<td align="center"><?php  echo $key1["qty_approved"]; ?></td>
	<td align="center"><?php  echo $key1["rm_wastage"]; ?></td>	  
	<td align="center"><?php $selected_scrap+=number_format($key1["qty_approved"]*$key1["rm_wastage"],3); echo number_format($key1["qty_approved"]*$key1["rm_wastage"],3); ?></td>	 
  	 
	</tr>
	<?php $count++; $num++;  }}  ?>
   
</table>
<br/>
<table style="border-collapse:collapse;" id="contractor_tb"  width="100%"  cellspacing="0" cellpadding="0">
<tr>
	<td align="center"><b>Total Scrap</b></td>
	<td align="center"><b>Scrap Out</b></td>
	<td align="center"><b>Net Balance</b></td>	
</tr>
     <tr>
	 <td  align="center">	
	 <label id="totalpayable_id"><?php echo $_POST["total_scrap"]; ?></label></td>
	 <td align="center"><label><?php echo $selected_scrap; ?></label></td>
	 <td align="center"><?php echo $_POST["total_scrap"]-$selected_scrap; ?></td>	 
</table>
<br/>
<table style="border-collapse:collapse;" id="contractor_tb"  width="100%"  cellspacing="0" cellpadding="0">
<tr>
	<td align="center"><b>Vehicle Empty Weight</b></td>
	<td align="center"><b>Vehicle Load Weight</b></td>	
</tr>
     <tr>
	 <td  align="center">	
	 <label id="totalpayable_id"><?php echo $_POST["empty"]; ?></label></td>
	 <td align="center"><label><?php echo $_POST["load"]; ?></label></td>
	 </tr> 
</table>
<br/>
<table style="border-collapse:collapse;" id="contractor_tb"  width="100%"  cellspacing="0" cellpadding="0">
<tr>
	<td align="center"><b>Net Weight To Out</b></td>
	<td align="center"><b>Rate / Kg	</b></td>
	<td align="center"><b>Total Amount</b></td>
	<td align="center" ><b>Vehicle No</b></td>
	<td align="center" ><b>Date</b></td>
</tr>
     <tr>
	 <td  align="center">	
	 <label id="totalpayable_id"><?php echo $net_weight=number_format($_POST["load"]-$_POST["empty"],3); ?></label></td>
	 <td align="center"><label><?php echo $_POST["price"]; ?></label></td>
	 <td align="center"><?php echo number_format($net_weight*$_POST["price"],3); ?></td>
	 <td align="center"><?php echo $_POST["vehicle"]; ?></td>
	 <td align="center"><?php echo date("d-m-Y",strtotime($_POST["creation"])); ?></td>
	 </tr> 
</table>
</div>