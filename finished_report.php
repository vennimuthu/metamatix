<?php function main(){
	$db_query=new db_query();  
	global $db_helper_obj;
$db_helper_obj=new db_helper();
$product_list=$db_helper_obj->product_list();
$finished_list=$db_helper_obj->finished_list();
$fgreport=$db_helper_obj->fgreport_list();

foreach($fgreport as $va=>$key){
$products=convert_array($key["products"]);
foreach($products["list"] as $va1=>$key1){
$billing_qty[$va1]+=$key1["billing_qty"];
}
}

foreach($product_list as $va=>$key){	
	foreach($finished_list as $va1=>$key1){
		if($key1["status"]==3){
		$products_approve=convert_array($key1["products_approve"]);		
		$product_list[$va]["open_qty"]+=$products_approve[$key["id"]]["approve_qty"];
		}
	}
	$product_list[$va]["open_qty"]=$product_list[$va]["open_qty"]-$billing_qty[$key["id"]];
}
//echo"<pre>";print_r($product_list);echo"</pre>";
//echo"<pre>";print_r($total_price);echo"</pre>";
//echo"<pre>";print_r($final_array1);echo"</pre>";
//echo"<pre>";print_r($final_array2);echo"</pre>";

?>
  <body > 
  <!--<h4 class="inline-heading">FG Report Generation</h4>-->
 <form id="report_form" action="fgreport_generate.php" name="report_form" method="post" target="_blank">
	
	<!-- <table id="contractor_tb" class="table listhead table-bordered table-hover" align="center" cellspacing="0" cellpadding="0">
	<tr>
	<td align="center" ><label>From Date</label></td>
	<td align="center"><input type="date" name="from_date" id="from_date" value="<?php echo date("Y-m-d"); ?>"></td>
	<td align="center"><label>To Date</label></td>
	<td align="center"><input type="date" name="to_date" id="to_date" value="<?php echo date("Y-m-d"); ?>"></td>
	<td align="center"><input type="button" onclick="fillter_date()" name="report_ids" id="report_ids" class="btn btn-primary" value="Fillter"></td>
	</tr>
	</table>
	-->
	
	<input type="submit" name="report_ids" id="report_ids" class="btn btn-primary pull-right" style="margin-top: -14px;"value="generate">
	<div class="container-fluid">    	
	<div class="row">
	<div class="col-sm-13">

	<h4 class="inline-heading"> </h4>
	<div class="listwrapper" id="fillter_id">
	<div class="table-responsive" id="table_append"> 
	<table class="table listhead table-bordered table-hover" align="center" cellspacing="0" cellpadding="0">
	<tr>
	<td colspan="8" align="center" valign="middle"><label>Finished Goods Report</label></td>
	</tr>
	<tr>
	<td colspan="8">
	<table id="contractor_tb" style="width: 100%;" class="table listhead table-bordered table-hover" align="center" cellspacing="0" cellpadding="0">
	<thead>
	<th style="text-align: center;">S No</th>
	<th style="text-align: center;">Part Name</th>
	<th style="text-align: center;">Series</th>
	<th style="text-align: center;">Qty In Stock</th>
	<th style="text-align: center;">Qty To Billing</th>
	<th style="text-align: center;">Rate</th>
	<th style="text-align: center;">Amount</th>	
	<th style="text-align: center;">Report</th>
	</thead>
	<?php $count=1; $num=0;
	if(!empty($product_list)){
	foreach($product_list as $va1=>$key1){
	if($key1["open_qty"]!=0){	
	?>
	<tr id="product_tr_<?php echo $num; ?>" >	
	<td align="center" ><?php echo $count; ?></td>
	<td style="padding: 4px;" align="center"><label>
	<?php  echo $key1["product_name"]; ?></label></td>
	<td align="center" style="padding: 4px;"><label><?php echo  $key1["series"]; ?></label></td>
	<td align="center" style="padding: 4px;"><label><?php echo $key1["open_qty"]; ?></label></td>
	<td align="center" ><input type="number" min="1" onblur="calculate_price(this,<?php echo $key1["id"]; ?>)" oninput="validity.valid||find_max('<?php echo $key1["open_qty"]; ?>',this);" max="<?php echo $key1["open_qty"]; ?>" name="billing_qty[<?php echo $key1["id"];?>]" id="billing_qty_<?php echo $count; ?>" class="form-control tbox_small"></td>
	<td align="center"  ><label id="price_<?php echo $key1["id"]; ?>"><?php echo $key1["price"]; ?></label></td>
	<td align="center" ><label id="amount_<?php echo $key1["id"]; ?>"></label></td>
	<td align="center"><input type="checkbox" name="processed[]" onclick="find_pay(<?php echo $key1["id"]; ?>)" id="processed_<?php echo $key1["id"]; ?>" value="<?php echo $key1["id"]; ?>" ></td>
	</tr>
	<?php  $count++; 
	 $total_price+=$key1["open_qty"]*$key1["price"];
	} }  } else{  ?>
	 <tr>
         <td align="center" colspan="8">There are no Reports found</td>
	 </tr>
	<?php } ?>	
	</table>
	</td>
	</tr>
	</table>
	<table id="contractor_tb" style="width: 100%;" class="table listhead table-bordered table-hover" align="center" cellspacing="0" cellpadding="0">
	<thead>
	<th style="text-align: center;">Total Stock</th>
	<th style="text-align: center;">Billing Value</th>
	<th style="text-align: center;">Invoice No</th>
	<th style="text-align: center;">Date</th>
	<th style="text-align: center;">Closing Stock Value</th>
	</thead>
     <tr>
	  
	 <td align="center">
	 <input type="hidden" name="totalpay" id="totalpay" value="<?php echo $total_price; ?>">
	 <label id="totalpayable_id">	 
	 <?php echo $total_price; ?></label></td>
	 <input type="hidden" name="pay" id="pay" value="">
	 <td  align="center"><label id="payable_id">0</label></td>
	 <td  align="center"><input type="text" name="inst_no" id="inst_no" value="" class="form-control tbox_small"></td>
	 <td  align="center"><input type="date" name="inst_date" id="inst_date" value="<?php echo date("Y-m-d"); ?>" class="form-control"></td>
	 <td  align="center"><input type="hidden" name="balances" id="balances"><label id="balance_id"><?php echo $totalamount+$totalamount1; ?></label></td>
	 </tr> 
	</table>
	</div>
	</div>
	</div>
	</div>
	</div>
	</form>
	</body>

<?php } include("template.php"); ?>  
<script type="text/javascript">
function find_max(max,obj){
alert("Please Enter Only "+max+" QTY");	
obj.value="";
}

function find_pay(count){
var obj=document.getElementsByName("processed[]");
var len=obj.length;
var amounts=0;
for(var i=0;i<len;i++){	
	if(obj[i].checked==true){
		amounts+=Number(document.getElementById("amount_"+obj[i].value).innerHTML);
	}		
}
document.getElementById("payable_id").innerHTML=amounts;
document.getElementById("balance_id").innerHTML=document.getElementById("totalpayable_id").innerHTML-amounts;
}

function refresh(){
document.getElementById("report_form").submit();
}
function calculate_price(obj,count){
   var obj1=document.getElementById("price_"+count);
   document.getElementById("amount_"+count).innerHTML=obj1.innerHTML*obj.value;
}
function Admin_forward(admin){
  document.getElementById("admin_approve").value=admin;
}
function fillter_date()
{
	var form = document.getElementById("report_form");
	 $.ajax({url: "fgreport_fillter.php", type: 'POST',data: new FormData(form),contentType:false, processData: false, success: function(result){
	$("#fillter_id").html(result);
	//alert(result);
	}
}); 
}
function filter_receipt()
{
	var form = document.getElementById("receipt_list");
	 $.ajax({url: "receipt_ajax.php", type: 'POST',data: new FormData(form),contentType:false, processData: false, success: function(result){
	$("#table_append").html(result);
	}
}); 
}
function payment_update(obj,buffid){
	var check=0;
	if(obj.checked==true)
		var check=1;		
		$.ajax({url: "process_ajax.php?check="+check+"&buffid="+buffid+"", type: 'POST' ,contentType:false, processData: false, success: function(result){
			//$("#table_append").html(result);
		}
	}); 
}
function show_order_data(supp,from1){
	if(from1==''||typeof(from1)=='undefined'){
		$.ajax({url: "inward_view.php?process_id="+supp,contentType:false, processData: false, success: function(result){
		var ser = document.getElementById('iframe_show_vehicle_data');
		ser.contentDocument.write(result);
		}
		});
	}else{
		$.ajax({url: "inwardappr_view.php?process_id="+supp,contentType:false, processData: false, success: function(result){
		var ser = document.getElementById('iframe_show_vehicle_data');
		ser.contentDocument.write(result);
		}
		});	
	}
}
function cancel_data(){
	var iframe = document.getElementById("iframe_show_vehicle_data");
	var html = "";
	iframe.contentWindow.document.open();
	iframe.contentWindow.document.write(html);
	iframe.contentWindow.document.close();
}
function create_hidden(ids){
  document.getElementById("forward_id").value=ids;
}

</script>

