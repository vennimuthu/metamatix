<script> 

function choose_product(obj,count){
	var stocks=obj.options[obj.selectedIndex].getAttribute('stock');
	document.getElementById('qty_'+count).disabled=false;
	document.getElementById('qty_'+count).setAttribute("max", stocks);
	document.getElementById('qty_'+count).setAttribute("required", true);
	document.getElementById('stock_'+count).value=stocks;
	var selct_obj=document.getElementById('product_id_'+count);
	var tl_price=selct_obj.options[selct_obj.selectedIndex].getAttribute('price');
	document.getElementById('rate_'+count).value=tl_price;
	intializeSelect();	

}

function find_qty(val){
	$.ajax({url: "process_qty.php?val="+val, type: 'POST',contentType:false, processData: false, success: function(result){
	qtys=  JSON.parse(result);
	document.getElementById("total").innerHTML=qtys.total;
	if(qtys.completed==null)
		qtys.completed=0;
	document.getElementById("completed").innerHTML=qtys.completed;
	document.getElementById("inprogress").innerHTML=qtys.inprogress;
	}});
}

function hide_product(obj,count){
	
var obje=document.getElementsByName('product_id[]');
var select_ids=[];
for(var i=1;i<=obje.length;i++){

if('product_id_'+i!=obj.id){
	$(document.getElementById('product_id_'+i).options).each(function(index, option){ 
	if(option.selected==true){ //alert(option.value);
	select_ids+=option.value;
	}
	if(option.value!=''){
	option.style.display = 'none';
	if( option.value == obj.value ){
	option.style.display = 'none';
	}else{
	option.style.display = 'block';	
	}
}
	for(var cont=0;cont<select_ids.length;cont++){
		if(option.value==select_ids[cont]&&option.selected!=true)
			option.style.display = 'none';	
	}
});
}	
}
}

	function show_overtime(obj){
		if(obj.value==2||obj.value==3){
		document.getElementById("geneneral_tb").style.display="table";
		document.getElementById("contractor_tb").style.display="none";
		}else{
		document.getElementById("contractor_tb").style.display="table";
		document.getElementById("geneneral_tb").style.display="none";
		}
	}
	function set_role(obj){
		var role=$(obj).find('option:selected').attr('role_name');
		document.getElementById("now_id").innerHTML=role;
	}
	function form_submit(){
		document.getElementById("button_id").disabled=true;
		document.getElementById('inward_add').submit();
	}

</script>


<?php 
function main(){
$db_query=new db_query();  
global $db_helper_obj;
$db_helper_obj=new db_helper();
$supplier_list=$db_helper_obj->supplier_list($_GET["id"]);

$nature_list=$db_helper_obj->nature_list();
$deliverymo_list=$db_helper_obj->deliverymo_list();
if(count($deliverymo_list)>0)
	$inward_no="MMSPL-JW-OUT-".sprintf("%03s",count($deliverymo_list)+1);
else
$inward_no="MMSPL-JW-OUT-001";

 
if(!empty($_POST)){
	$process=$db_helper_obj->deliverymo_insert();	
	header('Location:  deliverymo_list.php');
}

$inward_appr_list=$db_helper_obj->inward_approve_admin();
$subproduct_list=$db_helper_obj->total_product_maching(); 
foreach($subproduct_list as $va=>$key){	
	$product_ids[]=$key["id"];	
	$po_qty=$db_helper_obj->openstock_edit1($key["id"]);	
    $open_stock[$key["id"]]=$po_qty[0]["processed_qty"];
	$subproduct_list[$va]["qty"]=$po_qty[0]["processed_qty"]; 	
	$product_name[$key["id"]]=$key["name"];
	$price[$key["id"]]=$key["price"];
	$product_price[$key["id"]]=$key["machining_price"];
}

foreach($inward_appr_list as $va=>$key){
	$products=convert_array($key["products"]);		
	$qtys=convert_array($key["qty_list"]);
	foreach($products as $va1=>$key1){
		if(in_array($key1["product_id"],$product_ids)){
			$total_qty[$key1["product_id"]]["qty_approve"]+=$key1["qty_approve"];
			$total_qty[$key1["product_id"]]["product_id"]=$key1["product_id"];
			$total_qty[$key1["product_id"]]["product_price"]=$key1["product_price"];
		}
	}
}
  
foreach($total_qty as $va1=>$key1){
	$total_qty[$va1]["stocks"]=$open_stock[$key1["product_id"]];
	$total_qty[$va1]["product_name"]=$product_name[$key1["product_id"]];
	$total_qty[$va1]["price"]=$price[$key1["product_id"]];
	$total_qty[$va1]["product_price"]=$product_price[$key1["product_id"]];
}

foreach($subproduct_list as $va=>$key){
	if(!isset($total_qty[$key["id"]]["product_id"])){
		if($key["qty"]!=0){
			$total_qty[$key["id"]]["product_id"]=$key["id"];
			$total_qty[$key["id"]]["stocks"]=$key["qty"];
			$total_qty[$key["id"]]["product_name"]=$key["name"];
			$total_qty[$key["id"]]["price"]=$key["price"];
			$total_qty[$key["id"]]["product_price"]=$key["product_price"];
		}	
	}
}
 
foreach($total_qty as $va2=>$key2){
$subproduct=$db_helper_obj->total_product_edit($key2["product_id"]);	
if($subproduct[0]["material"]==1||$subproduct[0]["material"]==2||$subproduct[0]["material"]==4){
	$total_qty1[$va2]=$key2;
	$id_machin[]=$key2["product_id"];
}
}

$buffing_list=$db_helper_obj->buffing_list();	
$return_list=$db_helper_obj->return_list();

foreach($return_list as $va2=>$key2){
$total_qty[$key2["product_id"]]["stocks"]+=	$key2["qty"];
$total_qty[$key2["product_id"]]["product_id"]=$key2["product_id"];
$total_qty[$key2["product_id"]]["product_name"]=$product_name[$key2["product_id"]];
$total_qty[$key2["product_id"]]["price"]=$price[$key2["product_id"]];
}

$employee_list=$db_helper_obj->employee_list();
$product_list=$db_helper_obj->total_product_list();
 
?>
<form name="deliverymo_add" id="deliverymo_add" method="post">
<h4 class='inline-heading'>JOB WORK OUT</h4>
<table class="table listhead table-bordered table-hover" align="center" cellspacing="0" cellpadding="0">
<tr>
<td colspan="8" align="center" valign="middle"><label>JOB WORK OUT (Material Out )</label></td>
</tr>

<tr>

<td colspan="2"><label>Date:</label>
<td  ><input type="date" name="dated" id="dated" value="<?php echo date('Y-m-d'); ?>" required class="form-control tbox_small">
</td>

<td ><label>Challan No:</label></td>
<td colspan="4">
<label><?php echo $inward_no; ?></label>
<input type="hidden" name="challan_no" id="challan_no" value="<?php echo $inward_no; ?>">
</td>
</tr>
<tr>
<td colspan="2" style="width: 20%;"><label>Supplier name & Adddress :</label></td>
<td style="padding: 4px;"  >
<select onchange="choose_product1(this,<?php echo $count; ?>);"  id="supplier" name="supplier" class="form-control tbox_small" required >
	<option value="" selected disabled>Select</option>
	<?php if(!empty($supplier_list)) foreach($supplier_list as $va=>$key1){
	?>
	<option <?php if($key1["id"]==1){ ?> selected <?php }?> supplier="<?php echo $key1["supplier_name"]; ?>" 
	gst_no="<?php echo $key1["gst_no"];?>"
	value="<?php echo $key1["id"];?>"><?php echo $key1["supplier_name"];?></option>
 <?php  } ?>
</select>
</td>

<td ><label>Supplier GST No :</label></td>
 <td colspan="4">
<font id="gst_no">
  <?php echo $supplier_list[0]["gst_no"]; ?></font>
  <input type="hidden" name="gst_number" id="gst_number" value="<?php echo $supplier_list[0]["gst_no"]; ?>"></b></td>
</tr>
<tr>

<td colspan="2"><label>Nature of Operation</label></td>
<td>
<select id="nature" style="" name="nature[]" class="form-control tbox_small" required>
	<option value="" selected disabled >Select</option>
	<?php if(!empty($nature_list)) foreach($nature_list as $va=>$key){
	?>
	<option value="<?php echo $key["id"];?>"><?php echo $key["nature"];?></option>
  <?php  } ?>
</select>
</td>
<td width="70px"><label>Duration of Process</label></td>
<td style="padding: 4px;" colspan="4">
<input required type="text" name="duration" id="duration" class="form-control tbox_small">
</td>
</tr>
</tr>
<tr>
<td  colspan="2"><label>Vehicle No</label></td>
<td ><input required type="text" name="vehicle_no" id="vehicle_no" class="form-control tbox_small">
</td>
<td colspan="1" width="70px"><label>Despatched through</label></td>
<td style="padding: 4px;" colspan="4">
<input required type="text" name="delivery_through" id="delivery_through" class="form-control tbox_small"> </td>
</tr>
<tr>
<td colspan="2"><label>Destination</label></td>
<td><input required type="text" name="destination" id="destination" class="form-control tbox_small" >
</td>
<td colspan="1" width="70px"><label>Mode/Terms of payment</label></td>
<td style="padding: 4px;" colspan="4">
	<input required type="text" name="payment_term" id="payment_term" value="30 Days" class="form-control tbox_small" >
</td>
</tr>
<tr>
<td align="center" ><label>S No</label></td>
<td align="center"><label>Description of goods</label></td>
<td align="center"><label>Stock In Hand</label></td>
<td align="center"><label>Quantity</label></td>
<td align="center"><label>Rate</label></td>
<td align="center"><label>Amount</label></td>
<td align="center" colspan="2" ></td>
</tr>

<?php $count=1; $num=0;
$total_prod=array("1","2","3","4","5");
foreach($total_qty as $va1=>$key1){
if($count<=7){
?>
 <tbody  id="order_item_body_1"  name="order_item_row_1_1">
<tr id="order_item_row_1_1" name="order_item_row_1_1">
<div id="product_select" style="display:none;">		
<option value="" selected disabled>Select</option>
<?php if(!empty($product_list)) foreach($product_list as $va=>$key){
	?>
<option value="<?php echo $key["id"];?>" ><?php echo $key["name"];?></option>
<?php } ?>
</div>

<tr <?php echo $style; ?> id="product_tr_<?php echo $num; ?>" >
<td align="center" ><?php echo $count; ?></td>
<td style="padding: 4px; width:15%;" align="center">

<select required style="width:100px;" id="product_id_<?php echo $count ?>" onchange="choose_product(this,<?php echo $count ?>)" name="product_id[]" class="form-control tbox_small removes" >
	<option value="" selected disabled >Select</option>
	<?php	 
	if(!empty($total_qty)) 
	foreach($total_qty as $va=>$key){
		
		if(in_array($key["product_id"],$id_machin)){
	if(in_array($key["product_id"],$product_ids)){
	?>
	<option value="<?php echo $key["product_id"];?>" 
	stock="<?php echo $key["stocks"];	?>" price="<?php 
		echo $product_price[$key["product_id"]];?>" series="<?php echo $key["product_name"];	?>" ><?php	 
	echo $key["product_name"]; 
  ?></option>
<?php  } } } ?>
</select>
</td>


<td align="center"  style="padding-right: 10px;"><input required type="text" min="0" name="stock[]" id="stock_<?php echo $count; ?>"  class="form-control tbox_small" value="" readonly="readonly" ></td>

<td align="center" style="padding: 4px;width:15%;">
<input style="width:150px;" type="number" required min=1 onchange="calc_price(this,<?php echo $count; ?>)"  name="qty[]" id="qty_<?php echo $count; ?>" class="form-control tbox_small">

</td>


<td  align="center" style="padding: 4px;width:15%;">
<input style="width:100px;" type="text" required name="rate[]" onblur="calc_price(this,<?php echo $count; ?>)" id="rate_<?php echo $count; ?>" class="form-control tbox_small">
</td>
<td  align="center" style="padding: 4px; width:15%;" colspan="2">
<input type="text" class="form-control tbox_small" required name="amount[]" id="amount_<?php echo $count; ?>"  >
<td><input <?php if($count==1){ ?> disabled <?php } ?> type="button" class="btn btn-danger" value="X" onclick="delete_order_item_row(this);"></td>
</td>
</tr>
<?php $count++; } } ?>
<tr>
<td></td>
<td></td>
<td class="border_cls3" align="CENTER" valign="MIDDLE" bgcolor="#FFFFFF" style="padding-top: 4px;padding-bottom: 4px;">
			   <label>Total</label>
			   </td>
<td><div align="CENTER" id="sub_totalqty"><?php echo sprintf("",$total_amount); ?>0 Nos</div>
			   <input type="hidden" id= "sub_totallqty" name="sub_totallqty" value="<?php echo $total_amount; ?>"></td>
			   <td></td>
<td class="border_cls3" align="CENTER" valign="MIDDLE" bgcolor="#FFFFFF" style="padding-top: 4px;padding-bottom: 4px;">
			   <div id="sub_total"><?php echo sprintf("%.2f",$total_amount); ?>0</div>
			   <input type="hidden" id= "sub_totall" name="sub_totall" value="<?php echo $total_amount; ?>">
			   </td>
			   <td colspan="2" ></td>
</tr>
 <tr>
  <td class="border_cls5" colspan="4" rowspan="4" align="LEFT" bgcolor="#FFFFFF">Amount Chargeable(in words)<br></br><?php ?>			 	   
			 
			 <b><div id="amount_words" name="amount_words" "convertnumbertowords()"></div></b>
			  <input type="hidden" name="words" id="words" value="">		   
			   
			  
               <td  colspan="4" rowspan="4" height="54" align="CENTER" bgcolor="#FFFFFF"><b><br></b>
			  <!-- <a href="#"  onclick="add_order_row(1)" class="btn btn-primary" ><b> ADD NEW</b></a>-->
				 <button type="submit" id="save_btn" class="btn btn-primary" ><b>Save</b></button>	
				<button type="button" onclick="window.location.href='deliverymo_list.php'" class="btn btn-primary"><b>Cancel</b></button>
			  			   </td> </td>
               
              
            </tr>
<?php //echo"<pre>"; print_r($key["qty"]); echo"</pre>"; exit; ?>
<!--
<tr>
<td align="center" colspan="5">
<a href="#" style="margin-right: 252px;"  onclick="add_order_row(1)" class="btn btn-primary" > ADD NEW</a>
<button type="button" style="margin-right: 252px;" onclick="window.location.href='deliverymo_list.php'" class="btn btn-primary">Cancel</button>
<button type="submit" id="button_id" class="btn btn-primary">Save</button></td>
</tr>-->
</table>
</form>
<?php } include("template.php"); ?>  
<script>

function intializeSelect() {
		
    // this "initializes the boxes"
	$('.removes').each(function(box) {
      var value = $('.removes')[box].value;
      if (value) {
        $('.removes').not(this).find('option[value="' + value + '"]').hide();
      }
    });
  };  

  // this is called every time a select box changes
  $('.removes').on('change', function(event) {
    $('.removes').find('option').show();
    intializeSelect();
  });
 
function add_order_row(order_row_id)
{	
	var order_item_index=$("#order_item_row_index_"+order_row_id).val();
	order_item_index++;
	var html_content;
	var options=document.getElementById('product_select').innerHTML;

	var slect='<td style="padding: 4px;" align="center"><select  required onchange="intializeSelect()" id="product_id_'+order_item_index+'" name="product_id['+order_item_index+']" class="form-control removes" >'+options+'</select>';
	html_content='<tr id="order_item_row_'+order_row_id+"_"+order_item_index+'" name="order_item_row_'+order_row_id+"_"+order_item_index+'">'
+'<td>'+slect+'</td></td>'
+'<td><input type="number" required class="form-control" name="qty['+order_item_index+']" id="qty_'+order_item_index+'" min=1 value=""></td>'
+'<td><input type="text" class="form-control" name="price['+order_item_index+']" id="price_'+order_item_index+'" value="" /></td>'+'<td><input type="text" onblur="calc_price(this,<?php echo $count; ?>)" class="form-control" name="rate['+order_item_index+']" id="rate_'+order_item_index+'" value="" /></td>'
+'<td><input type="button" class="btn btn-danger" value="X" onclick="delete_order_item_row(this);"></td></tr>';
	
	div = document.getElementById('order_item_body_'+order_row_id);	
	div.insertAdjacentHTML( 'beforeend', html_content);
	intializeSelect();
	$("#order_item_row_index_"+order_row_id).val(order_item_index);
}
function delete_order_item_row(element)  
{
    var row = element.parentNode.parentNode;
    row.parentNode.removeChild(row);
}

function convertnumbertowords(amount) {
   
    var words = new Array();
    words[0] = '';
    words[1] = 'One';
    words[2] = 'Two';
    words[3] = 'Three';
    words[4] = 'Four';
    words[5] = 'Five';
    words[6] = 'Six';
    words[7] = 'Seven';
    words[8] = 'Eight';
    words[9] = 'Nine';
    words[10] = 'Ten';
    words[11] = 'Eleven';
    words[12] = 'Twelve';
    words[13] = 'Thirteen';
    words[14] = 'Fourteen';
    words[15] = 'Fifteen';
    words[16] = 'Sixteen';
    words[17] = 'Seventeen';
    words[18] = 'Eighteen';
    words[19] = 'Nineteen';
    words[20] = 'Twenty';
    words[30] = 'Thirty';
    words[40] = 'Forty';
    words[50] = 'Fifty';
    words[60] = 'Sixty';
    words[70] = 'Seventy';
    words[80] = 'Eighty';
    words[90] = 'Ninety';
    amount = amount.toString();
    var atemp = amount.split(".");
	if(amount.toString().split(".")[1]!=null)
	var paisa=numbertopaisa(amount.toString().split(".")[1]);
    var number = atemp[0].split(",").join("");
	
    var n_length = number.length;
    var words_string = "";
    if (n_length <= 9) {
        var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
        var received_n_array = new Array();
        for (var i = 0; i < n_length; i++) {
            received_n_array[i] = number.substr(i, 1);
        }
        for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
            n_array[i] = received_n_array[j];
        }
        for (var i = 0, j = 1; i < 9; i++, j++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                if (n_array[i] == 1) {
                    n_array[j] = 10 + parseInt(n_array[j]);
                    n_array[i] = 0;
                }
            }
        }
        value = "";
        for (var i = 0; i < 9; i++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                value = n_array[i] * 10;
            } else {
                value = n_array[i];
            }
            if (value != 0) {
                words_string += words[value] + " ";
            }
            if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Crores ";
            }
            if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Lakhs ";
            }
            if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Thousand ";
            }
            if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
                words_string += "Hundred and ";
            } else if (i == 6 && value != 0) {
                words_string += "Hundred ";
            }
        }
        words_string = words_string.split("  ").join(" ");
    }
	if(paisa!=null&&paisa!="")
		words_string=words_string + "and " +paisa+ "Paise Only"
	else
		words_string=words_string + "only"
	
	document.getElementById('amount_words').innerHTML=words_string;
	document.getElementById("words").value=words_string
    return words_string;
}
function numbertopaisa(atemp){
    var words = new Array();
    words[0] = '';
    words[1] = 'One';
    words[2] = 'Two';
    words[3] = 'Three';
    words[4] = 'Four';
    words[5] = 'Five';
    words[6] = 'Six';
    words[7] = 'Seven';
    words[8] = 'Eight';
    words[9] = 'Nine';
    words[10] = 'Ten';
    words[11] = 'Eleven';
    words[12] = 'Twelve';
    words[13] = 'Thirteen';
    words[14] = 'Fourteen';
    words[15] = 'Fifteen';
    words[16] = 'Sixteen';
    words[17] = 'Seventeen';
    words[18] = 'Eighteen';
    words[19] = 'Nineteen';
    words[20] = 'Twenty';
    words[30] = 'Thirty';
    words[40] = 'Forty';
    words[50] = 'Fifty';
    words[60] = 'Sixty';
    words[70] = 'Seventy';
    words[80] = 'Eighty';
    words[90] = 'Ninety';  
    var number = atemp;
    var n_length = number.length;
    var words_string = "";
    if (n_length <= 9) {
        var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
        var received_n_array = new Array();
        for (var i = 0; i < n_length; i++) {
            received_n_array[i] = number.substr(i, 1);
        }
        for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
            n_array[i] = received_n_array[j];
        }
        for (var i = 0, j = 1; i < 9; i++, j++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                if (n_array[i] == 1) {
                    n_array[j] = 10 + parseInt(n_array[j]);
                    n_array[i] = 0;
                }
            }
        }
        value = "";
        for (var i = 0; i < 9; i++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                value = n_array[i] * 10;
            } else {
                value = n_array[i];
            }
            if (value != 0) {
                words_string += words[value] + " ";
            }
            
        }
        words_string = words_string.split("  ").join(" ");
    }	
    return words_string;	
}
function calc_price(obj,count){
var selct_obj=document.getElementById('product_id_'+count);
var tl_qty=selct_obj.options[selct_obj.selectedIndex].getAttribute('stock');
var qty_obj = obj.id.includes("rate_");
if(qty_obj!=true)
if(Number(obj.value)>Number(tl_qty)){
	alert('Only '+tl_qty+' quantitys are available');	
	obj.value='';
	event.preventDefault();
}

	var qty=document.getElementById("qty_"+count).value;
	var rate=document.getElementById("rate_"+count).value;
	document.getElementById("amount_"+count).value=qty*rate;
	var nameobj=document.getElementsByName("amount[]");
	var nameobj1=document.getElementsByName("qty[]");
	var total=0;
	var totalqty=0;
	
	
	for(var i=0;i<nameobj.length;i++){	
	total+=Number(document.getElementById(nameobj[i].id).value);

	}
	for(var i=0;i<nameobj1.length;i++){	
	totalqty+=Number(document.getElementById(nameobj1[i].id).value);

	}
	document.getElementById("sub_totalqty").innerHTML=totalqty;
	document.getElementById("sub_totallqty").value=totalqty;     
	var totalval=document.getElementById('sub_total').innerHTML=total;;
	document.getElementById("sub_totall").value=total;
	convertnumbertowords(Number(totalval));
	document.getElementById("sub_total").innerHTML=Number(totalval);
	}

function choose_product1(obj,count){
var supplier = $('option:selected', obj).attr('supplier');
var gst_no= $('option:selected', obj).attr('gst_no');
if(supplier!='')

		document.getElementById('gst_no').innerHTML=gst_no;
		document.getElementById('gst_number').value=gst_no;
	
}	
</script>