<table align="center" style="border-collapse:collapse;font-size:	15px;">
         <colgroup>
            <col width="10">
            <col width="10">
            <col width="133">
            <col width="119">
            <col width="111">
            <col width="244">
         </colgroup>
         <tbody>
            <tr>
			
               <td class="pur_head" colspan="6" align="center" valign="MIDDLE" >
			   <b>
			   PURCHASE ORDER</b></td>
            </tr>
			 <tr>
               <td class="company" style="-webkit-text-fill-color:black;padding-left: 0px;border-right:1px solid #ffffff;" valign="MIDDLE" align="LEFT" rowspan="4" >
				<img style="width:170px;max-height: 40px;" src="images/Metamatix_logo.png" alt="Team Metamatix" />
			   </td> 
				<td style="padding-left: 0px;padding-right:5px;" valign="bottom" align="LEFT" colspan="3">
				<b><font size="3">METAMATIX SOLUTIONS PRIVATE LIMITED</font></b>
			   </td>
               
               <td align="LEFT" valign="bottom" style="-webkit-text-fill-color:black;border-bottom: 1px solid #ffffff;border-left:1px solid #000000;" >
			 <font size="3">  <b>PO Number:</b></font>
			   </td>
               <td class="border_cls1" style="-webkit-text-fill-color:black;border-bottom: 1px solid #ffffff;" valign="bottom"  align="LEFT" >
			   <font size="3"><b>'.$_POST['pdfinfo']['pur_number'].'</b></font>
			   </td>			   
            </tr>
          
            <tr>
               <td class="company" colspan="3" style="-webkit-text-fill-color:black;padding-left: 0px;" valign="bottom" align="LEFT" >
			   <font size="3">SHED NO:27, SIDCO INDUSTRIAL ESTATE,</font>
			   </td>
               
               <td align="LEFT" valign="bottom" style="-webkit-text-fill-color:black;border-bottom: 1px solid #ffffff;padding-left:1px;">
			   <b><font size="3">PO Date:</font></b>
			   </td>
               <td class="border_cls1" style="-webkit-text-fill-color:black;border-bottom: 1px solid #ffffff;" align="LEFT" valign="bottom" >
			  
              <font size="3"><b>'.date('d-M-Y',strtotime($_POST["pdfinfo"]["dates"])).'</b></font>
               </td>
            </tr>
            <tr>
               <td class="company" style="-webkit-text-fill-color:black;padding-left: 2px;" colspan="3" align="LEFT" valign="bottom" >
			   <font size="3">KURICHI</font>
			   </td>
               
               <td align="LEFT" style="-webkit-text-fill-color:black;border-bottom: 1px solid #ffffff;border-left: 1px solid #ffffff"  valign="bottom" >
			  <font size="3"> <b>Vendor ID:</b></font>
			   </td>
               <td class="border_cls1" style="-webkit-text-fill-color:black;border-bottom: 1px solid #ffffff;" align="LEFT" valign="bottom"  >
			   <font size="3"><b>'.$_POST["pdfinfo"]["vendor_number"].'</b></font>
			   </td>
            </tr>
            <tr>
               <td class="company" style="-webkit-text-fill-color:black;padding-left: 2px;border-bottom: 1px solid #000000;" colspan="3" valign="bottom" align="LEFT" >
			   <p><font size="3">COIMBATORE</font></p>
			   <p><font size="3">Tamil Nadu - 641 021</font></p>
			   <p><font size="3">GSTIN/UIN: 33AAKCM7413N1Z5</font></p>
			   <p><font size="3">State Name: Tamil Nadu, Code: 33</font></p>
			   <p><font size="3">CIN: U28990TZ2016PTC028310</font></p>
			   <p><font size="3">E-Mail: prasanth@metamatixsolutions.com</font></p>
			   </td>
              
               <td align="LEFT" style="border-bottom: 1px solid #000000;border-left: 1px solid #ffffff;">
			   <br>
			   </td>
               <td class="border_cls1" align="LEFT" style="border-bottom: 1px solid #000000;" >
			   <br>
			   </td>
            </tr>
			<tr>
			 
            <tr>
               <td class="company" colspan="3"  align="LEFT"	style="padding-left: 4px;padding-right:1px; border-bottom: 1px solid #ffffff;border-left:1px solid #000000;border-top:1px solid #000000;border-right:1px solid #000000;" valign="middle">
			   <font size="3"> <b>GSTIN/UIN: </b>'.$_POST["pdfinfo"]["gst_number"].' </font>
			  
				<font size="3"><p></p><b>Supplier:</b></font>
			   </td>              
               <td align="LEFT" colspan="3" valign="middle" style="border-bottom: 1px solid #ffffff;border-left: 1px solid #ffffff;border-top:1px solid #000000;border-right:1px solid #000000;">
			   <font size="3"><b>Ship To:</b></font>
			   </td>
			 
            </tr>
            
           '.$ship_purchase.'
            
            <tr>
              <td colspan="2" class="company" align="CENTER" valign="MIDDLE" style="border-bottom: 1px solid #000000; border-top:1px solid #000000;border-left:1px solid #000000;"><font size="3"><b>Despatch Through</b></font><br></td>
               <td colspan="2" align="CENTER" style="border-top:1px solid #000000;border-right:1px solid #000000;border-left:1px solid #000000;"  valign="MIDDLE"  ><font size="3"><b>Payment Terms</b></font></td>
               <td class="border_cls1" colspan="2" style="border-top:1px solid #000000;" align="CENTER" valign="MIDDLE"  	><font size="3"><b>Required By Date</b></font></td>
            </tr>
			
            <tr>
             
                <td colspan="2" class="company" align="CENTER" valign="MIDDLE" style="border-bottom: 1px solid #000000; border-top:1px solid #000000;border-left:1px solid #000000;">By Road -Transport<br></td>
               <td class="border_cls" style="border-top:1px solid #000000;border-bottom: 1px solid #000000;" colspan="2" align="CENTER" valign="MIDDLE" bgcolor="#FFFFFF"><font size="3">'.$_POST["pdfinfo"]["payment_terms"].'</font></td>
               
			   <td class="border_cls" style="border-top:1px solid #000000;border-bottom: 1px solid #000000;" colspan="2" align="CENTER" valign="MIDDLE" bgcolor="#FFFFFF"><font size="3">'.date('d-M-Y',strtotime($_POST["pdfinfo"]["required_date"])).'</font></td>
            </tr>
            <tr>
               <td class="company" style="border-bottom: 1px solid #000000;" align ="CENTER" width="2%"  bgcolor="#C3D69B" valign="middle"><font size="3"><b>SL NO</b><font></td>
               <td align="CENTER" bgcolor="#C3D69B" style="border-right:1px solid #000000;border-bottom: 1px solid #000000;" valign="middle" ><font size="3"><b> Particular </b>'.strtoupper($list[0]["name"]).'</font></td>
               <td align="CENTER" width="20%" style="border-right:1px solid #000000;border-bottom: 1px solid #000000;" bgcolor="#C3D69B" valign="middle"><font size="3"><b>Dimensions</b></font></td>
               <td class="border_cls1" style="border-bottom: 1px solid #000000;" align="CENTER" bgcolor="#C3D69B" valign="middle"><font size="3">Qty</font></td>
               <td class="border_cls1" style="border-bottom: 1px solid #000000;" align="CENTER" bgcolor="#C3D69B" valign="middle"><font size="3"><b>Rate</b></font></td>
               <td class="border_cls1" style="border-bottom: 1px solid #000000;" align="CENTER" bgcolor="#C3D69B" valign="middle"><font size="3"><b>Amount</b></font></td>
            </tr>
            <tr>
               <td class="company" height="19" align="LEFT" bgcolor="#FFFFFF"><br></td>
               <td class="border_cls" align="LEFT" bgcolor="#FFFFFF"><br></td>
               <td class="border_cls" align="LEFT" bgcolor="#FFFFFF"><br></td>
               <td class="border_cls" align="LEFT" bgcolor="#FFFFFF"><br></td>
               <td class="border_cls1" align="LEFT" bgcolor="#FFFFFF"><br></td>
               <td class="border_cls1" align="LEFT" bgcolor="#FFFFFF"><br></td>
            </tr>
		      '.$arr.' 
			
            <tr>
               <td style="border-bottom: 1px solid #000000; border-left: 1px solid #000000" height="19" align="LEFT" bgcolor="#FFFFFF"><br></td>
               <td class="border_cls3" align="LEFT" bgcolor="#FFFFFF"><br></td>
               <td class="border_cls3" align="LEFT" bgcolor="#FFFFFF"><br></td>
               <td class="border_cls3" align="LEFT" bgcolor="#FFFFFF"><br></td>
               <td class="border_cls5" align="LEFT" bgcolor="#FFFFFF"><br></td>
               <td class="border_cls5" align="LEFT" bgcolor="#FFFFFF"><br></td>
            </tr>
            <tr>
               <td style="border-top: 1px solid #000000; border-left: 1px solid #000000" height="23" align="LEFT" bgcolor="#FFFFFF"><br></td>
               <td style="border-top: 1px solid #000000" align="LEFT" bgcolor="#FFFFFF"><br></td>
               <td style="border-top: 1px solid #000000; border-right: 1px solid #000000" colspan="2" rowspan="2" align="LEFT" bgcolor="#FFFFFF"><b><br></b></td>
               <td style="border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" align="RIGHT" bgcolor="#FFFFFF"><br></td>
               <td style="border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" align="LEFT" bgcolor="#FFFFFF"><br></td>
            </tr>
            <tr>
               <td class="company" height="16" colspan="2" align="LEFT" style= "border-right: 1px solid #ffffff;" bgcolor="#FFFFFF"><b>&nbsp;&nbsp;&nbsp;&nbsp;Approved By:</b></td>
               <td class="border_cls3" bgcolor="#FFFFFF" align="center" valign="middle">Subtotal</td>
               <td class="border_cls3" align="CENTER" valign="MIDDLE" bgcolor="#FFFFFF" style="padding-top: 4px;padding-bottom: 4px;"><font size="3">
			   <label id="sub_total">'.$_POST["pdfinfo"]["sub_totall"].'</label></font></td>
            </tr>
            '.$val.'            
            <tr>
               <td style="border-bottom: 1px solid #000000;border-right:0px solid #000000;border-left: 0px solid #ffffff;" colspan="2" rowspan="2" height="54" align="CENTER" bgcolor="#FFFFFF"><b><br></b></td>
               <td class="border_cls3" bgcolor="#FFFFFF" align="center" valign="middle"><b>Order Total</b></td>
               <td class="border_cls3" align="CENTER" valign="middle" bgcolor="#C3D69B" style="padding-top: 4px;padding-bottom: 4px;"><b>
			  <font size="3" id="order_total" >'.$_POST["pdfinfo"]["orders"].'</font></b></td>
			
            </tr>
            
			<input type="hidden" name="orders" id="orders" value='.$_POST["pdfinfo"]["orders"].'>
           </tbody>
		
      </table>