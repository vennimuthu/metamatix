<?php
$css = "
table tr td th{
	width:auto;
	height: 40px;

}";

//error_reporting(0);
// Include the main TCPDF library (search for installation path).
//require_once('tpdf/TCPDF-master/tcpdf_import.php');
include("config/config.php");
include("core/class/db_query.php");                             // Class where query generetion is written
include("core/class/db_helper.php");                            // Class where table and feilds 
include("core/function/common.php"); 
require_once('HtmlExcel.php');
$db_helper_obj=new db_helper();
$inward=$db_helper_obj->inward_approve_list();
foreach($inward as $va=>$key){
$products=convert_array($key["products"]);
$qty_list=convert_array($key["qty_list"]);
$master_list1=$db_helper_obj->inward_edit($key["inward_id"]);
$products1=convert_array($master_list1[0]["products"]);
foreach($products as $va2=>$key2){
$account_list[$key["inward_no"]][$key["inward_no"]."_".$key2["product_id"]]["product_id"]=$key2["product_id"];
$account_list[$key["inward_no"]][$key["inward_no"]."_".$key2["product_id"]]["date"]=date("d-m-Y",$key["dated"]);
$account_list[$key["inward_no"]][$key["inward_no"]."_".$key2["product_id"]]["grn_no"]=$key["inward_no"];
$account_list[$key["inward_no"]][$key["inward_no"]."_".$key2["product_id"]]["inv_no"]=$key["inv_no"];
$account_list[$key["inward_no"]][$key["inward_no"]."_".$key2["product_id"]]["supplier"]=$key["supplier"];
$account_list[$key["inward_no"]][$key["inward_no"]."_".$key2["product_id"]]["ordered"]=$products1[$va2]["order_qty"];
$account_list[$key["inward_no"]][$key["inward_no"]."_".$key2["product_id"]]["balance"]=$products1[$va2]["order_qty"]-$products1[$va2]["qty"];
$account_list[$key["inward_no"]][$key["inward_no"]."_".$key2["product_id"]]["received"]=$key2["qty_received"];
$account_list[$key["inward_no"]][$key["inward_no"]."_".$key2["product_id"]]["approve"]=$key2["qty_approve"];
$account_list[$key["inward_no"]][$key["inward_no"]."_".$key2["product_id"]]["rejected"]=$key2["qty_rejected"];

$account_list[$key["inward_no"]][$key["inward_no"]."_".$key2["product_id"]]["product_price"]=$key2["product_price"];

$account_list[$key["inward_no"]][$key["inward_no"]."_".$key2["product_id"]]["cgst"]=$key2["cgst"];
$account_list[$key["inward_no"]][$key["inward_no"]."_".$key2["product_id"]]["sgst"]=$key2["sgst"];
$account_list[$key["inward_no"]][$key["inward_no"]."_".$key2["product_id"]]["igst"]=$key2["igst"];
/*
$account_list[$key["inward_no"]][$key["inward_no"]."_".$key2["product_id"]]["rate"]=$key2["rate"];
$account_list[$key["inward_no"]][$key["inward_no"]."_".$key2["product_id"]]["cgst_percent"]=$qty_list["cgst_percent"];
$account_list[$key["inward_no"]][$key["inward_no"]."_".$key2["product_id"]]["sgst_percent"]=$qty_list["sgst_percent"];
$account_list[$key["inward_no"]][$key["inward_no"]."_".$key2["product_id"]]["igst_percent"]=$qty_list["igst_percent"];
$account_list[$key["inward_no"]][$key["inward_no"]."_".$key2["product_id"]]["id"]=$key["inward_id"];*/
}
}

ob_start();
include "accountreport_html.php";
$myvar = ob_get_clean();

$xls = new HtmlExcel();
$xls->setCss($css);
$xls->addSheet("ACCOUNT REPORT",$myvar);
$xls->headers("ACCOUNTReport_".date('d-M-y').".xls");

echo $xls->buildFile();
?>