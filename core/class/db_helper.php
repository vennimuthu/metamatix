<?php
class db_helper extends db_query{
	function login($arr){
		return $this->select_query("users","id,name,password,user_type","name=? and password=?",$arr);
	}
	
	function product_insert(){
		extract($_POST);
		$already=$this->fg_product_exist($title);
		 
		if(empty($already)){
			$insr_array["product_name"]=$title;
			$insr_array["series"]=$series;
			$insr_array["price"]=$product_price;
			$insr_array["open_qty"]=(int)$open_qty;		
			$insr_array["creation_time"]=time();
			$insr_array["created_by"]=$_SESSION["user_id"];		
			$insert_id=$this->insert_query("products",$insr_array);		
			foreach($name as $va=>$key){
				$insr_array1["name"]=$key;
				$insr_array1["qty"]=(int)$qty[$va];
				$insr_array1["product_id"]=$insert_id;
				$insert=$this->insert_query("sub_products",$insr_array1);
			}
		}
		return "success";
	}
	
	function finished_insert(){
		extract($_POST);	
			
		$insr_array["mov_no"]=$mov_no;
		$insr_array["mov_date"]=strtotime($mov_date);
		$insr_array["pass_by"]=$passed;
		$insr_array["creation_time"]=time();
		$insr_array["created_by"]=$_SESSION["user_id"];
		$insr_array["status"]=1;
		$product_list=$this->product_list();
		$subproduct_list=$this->subproduct_list();
		$openstock_list=$this->openstock_list();
		
		foreach($product_list as $va=>$key){
			foreach($subproduct_list as $va3=>$key3){
				if($key3["product_id"]==$key["id"])	
					$total_arr[$key["id"]]["subproducts"][]=$key3;
				}
		}
		//echo"<pre>";print_r($product_list);echo"</pre>";
		
		foreach($product_id as $va=>$key){	
		$produqty[$key]=$produced_qty[$va];		
		}
		
		//echo"<pre>";print_r($total_arr);echo"</pre>";
		foreach($total_arr as $va=>$key){
			foreach($key["subproducts"] as $va1=>$key1){
			//echo"<pre>";print_r($key1);echo"</pre>";
			$qty[$key1["name"]]+=$produqty[$va]*$key1["qty"];
			}
			
		}
		
		foreach($openstock_list as $va=>$key){
		
		$insr_array7["finished_qty"]=$key["finished_qty"]+$qty[$key["subproduct_id"]];
		$where="subproduct_id=:productid";
		$where_arr= array('productid'=>$key["subproduct_id"]);
		$update1=$this->update_query("openstock",$insr_array7,$where,$where_arr);	
		}
		
		foreach($product_id as $va=>$key)
		{
		if($approve[$va]==1)
		{
			$fin_arry[$va]["product_id"]=$key;
			$fin_arry[$va]["series"]=$series[$va];
			$fin_arry[$va]["stock_hand"]=$stock_hand[$va];
			$fin_arry[$va]["produced_qty"]=$produced_qty[$va];				
			$fin_arry[$va]["remarks"]=$remarks[$va];
		}	
		}
		$insr_array["products"]=convert_json($fin_arry);
		$insert_id=$this->insert_query("finished",$insr_array);
		
		return "success";
	}
	
	function finishedappr_insert()
	{
		extract($_POST);
		
		$finised=$this->finishedappr_list($_GET["id"]);
		foreach($finised as $va=>$key){
		$product=convert_array($key["products"]);	
		foreach($product as $va1=>$key1){
		$fin_array[$key1["product_id"]]["approve_qty"]=$approve_qty[$key1["product_id"]];
		$fin_array[$key1["product_id"]]["remarks"]=$remarks[$key1["product_id"]];	
		$fin_array[$key1["product_id"]]["product_id"]=$key1["product_id"];		
		}
		}
		 
		//$insr_array["products_approve"]=base64_encode(json_encode($_POST));
		$insr_array["products_approve"]=convert_json($fin_array);
		$insr_array["status"]=2;
		$insr_array["ins_date"]=time();
		$insr_array["ins_by"]=$ins_by;		
		$where="id=:productid";
		$where_arr= array('productid'=>$_GET["id"]);
		$update1=$this->update_query("finished",$insr_array,$where,$where_arr);
		return "success";
		
	}
	
	function finishedapprove_insert()
	{
		extract($_POST);
		echo"<pre>";print_r($_POST);echo"</pre>";
		exit;
		$finised=$this->finishedappr_list($_GET["id"]);
		$product_list=$this->product_list();
	    $subproduct_list=$this->subproduct_list();
		foreach($finised as $va=>$key){
		$product=convert_array($key["approve_remarks"]);	
			foreach($product as $va1=>$key1){
				$fin_array[$key1["product_id"]]["approve_qty"]=$approve_qty[$key1["product_id"]];
				$fin_array[$key1["product_id"]]["remarks"]=$remarks[$key1["product_id"]]; $qtys=$this->openstock_edit1($key1["product_id"]);		
				$insr_array7["processed_finished"]=$qtys[0]["processed_finished"]+$approve_qty[$key1["product_id"]];
				$insr_array7["finished_qty"]=$qtys[0]["finished_qty"]-$approve_qty[$key1["product_id"]];
				$where="subproduct_id=:productid";
				$where_arr= array('productid'=>$key1["product_id"]);				
				$update1=$this->update_query("openstock",$insr_array7,$where,$where_arr);
			}
		}
		$insr_array["status"]=3;
		$insr_array["approve_time"]=time();
		$insr_array["approve_by"]=$apr_by;	
		$insr_array["approve_remarks"]=convert_json($_POST);			
		$where="id=:productid";
		$where_arr= array('productid'=>$_GET["id"]);
		$update1=$this->update_query("finished",$insr_array,$where,$where_arr);
		return "success";
		
		
	}
	
	function total_product_insert(){
		 
			// total_products
			extract($_POST);			
			foreach($name as $va=>$key){
				
			$already=$this->product_exist($key);
			  
			if(empty($already)){
				$insr_array1["name"]=$key;
				$insr_array1["hsn"]=$hsn[$va];
				$insr_array1["material"]=$material[$va];
				$insr_array1["dimension"]=$dimension[$va];
				if(isset($price[$va]))
					$insr_array1["price"]=$price[$va];
				if(isset($machining_price[$va]))
					$insr_array1["machining_price"]=$machining_price[$va];
				if(isset($product_price[$va]))
					$insr_array1["product_price"]=$product_price[$va];
				if(isset($process[$va])&&$process[$va]==1)
					$insr_array1["process"]=1;
				else
					$insr_array1["process"]=0;
				if(isset($machining[$va])&&$machining[$va]==1)
					$insr_array1["machining"]=1;
				else
					$insr_array1["machining"]=0;
				
				$insr_array1["creation_time"]=time();
				$insr_array1["created_by"]=$_SESSION["user_id"];			
				$insert=$this->insert_query("total_products",$insr_array1);
						
			$insr_array2["subproduct_id"]=$insert;
			 
			$insr_array2["qty"]=0;	
			$insr_array2["buffed_qty"]=0;
			$insr_array2["processed_buff"]=0;		
			$insr_array2["processed_qty"]=0;			
			$insr_array2["creation_time"]=time();
			$insr_array2["created_by"]=$_SESSION["user_id"];			
			$insert=$this->insert_query("openstock",$insr_array2);
			}	
			
		}
		return "success";
			
	}

	function openstock_insert(){
		extract($_POST);
		
		foreach($name as $va=>$key){
			$insr_array1["subproduct_id"]=$key;
			$insr_array1["qty"]=$qty[$va];	
			$insr_array1["buffed_qty"]=$buffed_qty[$va];
			$insr_array1["processed_buff"]=$buffed_qty[$va];		
			$insr_array1["processed_qty"]=$qty[$va];				
			$insr_array1["creation_time"]=time();
			$insr_array1["created_by"]=$_SESSION["user_id"];			
			$insert=$this->insert_query("openstock",$insr_array1);
		}
		
		return "success";
	}
	
	function stock_adjust(){
		extract($_POST);
		$insr_array1["subproduct_id"]=$key;
		$insr_array1["qty"]=$qty[$va];
		$insr_array1["buffed_qty"]=$buffed_qty[$va];
		$insr_array1["processed_buff"]=$buffed_qty[$va];		
		$insr_array1["processed_qty"]=$qty[$va];		
		$insr_array1["creation_time"]=time();
		$insr_array1["created_by"]=$_SESSION["user_id"];
		$insr_array7["processed_maching"]=$qtys[0]["processed_maching"]+$qty_approve[$va];
		$where="subproduct_id=:productid";
		$where_arr= array('productid'=>$key);
		$update1=$this->update_query("openstock",$insr_array7,$where,$where_arr);
	}
	
	function report_generation(){
		extract($_POST);
		
		$insr_array["bpl_no"]=base64_encode(json_encode($_POST["processed"]));
		
		foreach($_POST["processed"] as $va=>$key){
		$insr_array7["report"]=1;
		$where="bpl_no=:inwardid";
		$where_arr= array('inwardid'=>$key);
		$update1=$this->update_query("buffing",$insr_array7,$where,$where_arr);	
		}
		
		
		$report=$this->report_list();
		if(count($report)>0)
		$report_no="MMSPL-REP-".sprintf("%03s",count($report)+1);
		else
		$report_no="MMSPL-REP-001";
		
		$insr_array["report_no"]=$report_no;
		$insr_array["pays"]=base64_encode(json_encode($_POST));
		$insr_array["creation_time"]=time();
		$insr_array["created_by"]=$_SESSION["user_id"];
		$insert_id=$this->insert_query("report",$insr_array);
		
		return "success";
	}
	function fgreport_generation($final_array){
		extract($_POST);			
		$insr_array["inv_no"]=$inst_no;
		$insr_array["inv_date"]=strtotime($inst_date);
		
		foreach($final_array as $va=>$key){
		$products["list"][$va]=$key;
		$payed+=$key["amount"];		
		}
		$products["total_pay"]=$totalpay;
		$products["payed"]=$payed;
		$products["balance"]=$totalpay-$payed;	
		
		$insr_array["products"]=convert_json($products);
		$insr_array["creation_time"]=time();
		$insr_array["created_by"]=$_SESSION["user_id"];
		$insert_id=$this->insert_query("fg_report",$insr_array);
		
		return "success";
	}	
	function report_list(){ 
			return $this->select_query("report","*","id!=''");
	}
	function scrap_history_list(){ 
			return $this->select_query("scrap_history","*","id!=''");
	}
	function order_insert(){
	
	extract($_POST);
	$already=$this->po_exist($pur_number);

	if(empty($already)){
		$insr_array["po_number"]=$pur_number;
		$insr_array["material_type"]=$material_type;
		$insr_array["po_date"]=($dates);
		$insr_array["creation_time"]=time();
		$insr_array["created_by"]=$_SESSION["user_id"];
		$insr_array["supplier_id"]=$supplier_id;
		$insr_array["gst_no"]=$gst_number;
		$insr_array["shipto_id"]=$shipto_id;
		$insr_array["payment_terms"]=$payment_terms;
		$insr_array["required_date"]=($required_date);
		$insr_array["sgst_percent"]=$sgst;
		$insr_array["cgst_percent"]=$cgst;
		$insr_array["igst_percent"]=$igst;
		$insr_array["sgst_total"]=$sgst_total;
		$insr_array["cgst_total"]=$cgst_total;
		$insr_array["igst_total"]=$igst_total;
		$insr_array["sub_totall"]=$sub_totall;
		$insr_array["orders"]=$orders;
		$insr_array["amount_words"]=$words;
		$insr_array["pdfinfo"]=base64_encode(json_encode($_POST));
		$insert_id=$this->insert_query("orders",$insr_array);
		
		foreach($name as $va=>$key){
			
			//$insr_array3["product_id"]=$key;	
			//$insr_array3["nonbuff_price"]=$price[$va];
			//$insr_array3["creation_date"]=time();			
			//$insert=$this->insert_query("price_history",$insr_array3);
			$insr_array1["product_name"]=$key;
			$insr_array1["hsn"]=$hsn[$va];
			$insr_array1["dimension"]=$dimension[$va];
			$insr_array1["qty"]=$qty[$va];
			$insr_array1["unit"]=$units[$va];
			$insr_array1["price"]=$price[$va];
			$insr_array1["amount"]=$qty[$va]*$price[$va];
			$insr_array1["order_id"]=$insert_id;
			$insr_array1["ordersub_pdfinfo"]= base64_encode(json_encode($_POST));
			$insert=$this->insert_query("order_sub",$insr_array1);
			
		}
	}
		
		return "success";
	}
	
	function inward_insert()
	{
		extract($_POST);
		
		$insr_array1["dated"]=strtotime($date);
		$insr_array1["inv_no"]=$inv_no;
		$insr_array1["supplier"]=$supplier;
		$insr_array1["po_list"]=$po_list;
		$insr_array1["inv_date"]=strtotime($inv_date);
		$final_arry=array();
		foreach($product_id as $va=>$key){
			$final_arry[$va]["product_id"]=$key;
			$final_arry[$va]["specification"]=$specification[$va];	
			$final_arry[$va]["order_qty"]=$order_qty[$va];	
			$final_arry[$va]["qty"]=$qty[$va];
			$final_arry[$va]["product_price"]=$product_price[$va];
			$final_arry[$va]["sgst"]=$sgst[$va];
			$final_arry[$va]["cgst"]=$cgst[$va];
			$final_arry[$va]["igst"]=$igst[$va]; 
		}
		
		$final_arry1=array();
		
		$final_arry1["total_order"]=$total_order;
		$final_arry1["total_received"]=$total_received;	
		$final_arry1["total_balance"]=$total_balance;	

		
		$insr_array1["products"]=base64_encode(json_encode($final_arry));
		$insr_array1["qty_list"]=base64_encode(json_encode($final_arry1));
		$insr_array1["comments"]=$comments;
		$insr_array1["received_by"]=$received_by;
		$insr_array1["created_by"]=$_SESSION["user_id"];
		$insr_array1["creation_time"]=time();
		$insr_array1["inward_no"]=$inward_no;
		
		$insr_array1["status"]=1;
		$insert8=$this->insert_query("inward",$insr_array1);
		return "success";
	}
	function inwardappr_insert(){
		extract($_POST);
		$insr_array1["dated"]=strtotime($grn_date);
		$insr_array1["inward_no"]=$grn_no;			
		$insr_array1["inv_no"]=$inv_no;
		$insr_array1["supplier"]=$supplier;
		$insr_array1["ins_date"]=strtotime($ins_date);
		$insr_array1["ins_by"]=$ins_by;
		$insr_array1["received_by"]=$recei_by;
		$insr_array1["inv_date"]=strtotime($inv_date);
		$final_arry=array();
		$total_received=0;
		$total_approve=0;
		$total_reject=0;
		foreach($product_id as $va=>$key){
			$final_arry[$va]["product_id"]=$key;
			$final_arry[$va]["qty_received"]=$qty_received[$va];
			$final_arry[$va]["qty_approve"]=$qty_approve[$va];
			$final_arry[$va]["qty_rejected"]=$qty_received[$va]-$qty_approve[$va];
			$final_arry[$va]["ins_remarks"]=$ins_remarks[$va];	
			$final_arry[$va]["product_price"]=$product_price[$va];	
			$final_arry[$va]["sgst"]=$sgst[$va];	
			$final_arry[$va]["cgst"]=$cgst[$va];	
			$final_arry[$va]["igst"]=$igst[$va];	
			
			$total_received+=$qty_received[$va];
			$total_approve+=$qty_approve[$va];
			$total_reject+=$qty_received[$va]-$qty_approve[$va];
			
		}
		$final_arry1["total_received"]=$total_received;
		$final_arry1["total_approve"]=$total_approve;
		$final_arry1["total_reject"]=$total_reject;
		$insr_array1["products"]=base64_encode(json_encode($final_arry));
		$insr_array1["qty_list"]=base64_encode(json_encode($final_arry1));
		$insr_array1["comments"]=$comments;
		$insr_array1["created_by"]=$_SESSION["user_id"];
		$insr_array1["creation_time"]=time();
		$insr_array1["inward_id"]=$inward_id;
		
		
		

		$insert1s=$this->insert_query("inward_approve",$insr_array1);
		$insr_array["status"]=2;
		$where="id=:inwardid";
		$where_arr= array('inwardid'=>$inward_id);
		$update=$this->update_query("inward",$insr_array,$where,$where_arr);
		
		
		
		return "success";
	}
	function employee_insert(){ 
		extract($_POST);		
		$insr_array["name"]=$employee_name;
		$insr_array["role"]=$employee_role;
		$insr_array["price"]=$price;
		$insr_array["creation_time"]=time();
		$insert_id=$this->insert_query("employee",$insr_array);
		return "success";
	}
	function inventory_update($product_price){
		
		extract($_POST);
		$approve_list=$this->inwardapprove_edit($_GET["id"]);
		$qty_list=convert_array($approve_list[0]["qty_list"]);
		$products=convert_array($approve_list[0]["products"]);
		 
		if(!empty($cgst_per))
			$qty_list["cgst_per"]=$cgst_per;
		if(!empty($sgst_per))
			$qty_list["sgst_per"]=$sgst_per;
		if(!empty($igst_per))
			$qty_list["igst_per"]=$igst_per;
		foreach($product_id as $va1=>$key1){
			$insr_array1["product_id"]=$key1;
			$nonbuff_price=$this->get_updated_price($key1);
			$insr_array1["nonbuffed_price"]=$product_price[$key1]["price"];
			$insr_array1["buffed_price"]=$nonbuff_price[0]["buffed_price"];
			$insr_array1["maching_price"]=$nonbuff_price[0]["maching_price"];
			$insr_array1["nonbuffed_qty"]=$qty_approve[$va1];
			$insr_array1["nonbuffed_process_qty"]=$qty_approve[$va1];
			$insr_array1["po_id"]=$product_price[$key1]["po_id"];
			$insr_array1["inward_id"]=$_GET["id"];			
			$insr_array1["creation_date"]=time();
			$insr_array1["status"]=0;
			$insert_ids=$this->insert_query("price_history",$insr_array1);	   
			$product[$va1][$key1]=$rate[$va1];
		}
		 
		foreach($product_id as $va=>$key){
			$final_arry[$va]["product_id"]=$key;
			$final_arry[$va]["qty_received"]=$qty_received[$va];
			$final_arry[$va]["qty_approve"]=$qty_approve[$va];
			$final_arry[$va]["qty_rejected"]=$qty_received[$va]-$qty_approve[$va];
			$final_arry[$va]["ins_remarks"]=$ins_remarks[$va];
			$final_arry[$va]["product_price"]=$product_price[$key]["price"];
						
			$total_received+=$qty_received[$va];
			$total_approve+=$qty_approve[$va];
			$total_reject+=$qty_received[$va]-$qty_approve[$va];			
		}
		
		$final_arry1["total_received"]=$total_received;
		$final_arry1["total_approve"]=$total_approve;
		$final_arry1["total_reject"]=$total_reject;
		$final_arry1["sgst_percent"]=$sgst_per;
		$final_arry1["cgst_percent"]=$cgst_per;	
		$final_arry1["igst_percent"]=$igst_per;	
		$final_arry1["sgst"]=$sgst;
		$final_arry1["cgst"]=$cgst;
		$final_arry1["igst"]=$igst;			
		//echo"<pre>";print_r($final_arry);echo"</pre>";
		//echo"<pre>";print_r($final_arry1);echo"</pre>";
		$insr_array["products"]=convert_json($final_arry);
		$insr_array["qty_list"]=convert_json($final_arry1);
		//foreach($products as $va=>$key){
		//	$products[$va]["rate"]=$product[$va][$key["product_id"]];
		//}
		
		//echo"<pre>";print_r($final_arry1);echo"</pre>";
		
		foreach($product_id as $va=>$key){
			$qtys=$this->openstock_edit1($key);		
			$updatearr["processed_qty"]=$qtys[0]["processed_qty"]+$qty_approve[$va];		
			$where2="subproduct_id=:product_id ";
			$where_arr2= array('product_id'=>$key);
			$insert_ids=$this->update_query("openstock",$updatearr,$where2,$where_arr2);		
		}
		 
		$insr_array["admin_approve"]=$approve;
		$insr_array["admin_comment"]=$comments;
		$insr_array["admin_processed"]=$processed;	
		$insr_array["admin_appr_time"]=time();
		//$insr_array["qty_list"]=convert_json($qty_list);
		//$insr_array["products"]=convert_json($products);
		 
		$where="id=:sub_product_id";
		$where_arr=array('sub_product_id'=>$_GET["id"]);
		$update=$this->update_query("inward_approve",$insr_array,$where,$where_arr);
		return "success";
	}
	/*function inwardstock_qty($inward_id=''){
		return $this->select_query("price_history","*","inward_id=".$inward_id."");
	}*/
	function inventory_edit_update($product_price){
		//$already_stock=$this->inwardstock_qty($_GET["inward_id"]);		 
		extract($_POST);
		$approve_list=$this->inwardapprove_edit($_GET["inward_id"]);
		$qty_list=convert_array($approve_list[0]["qty_list"]);
		$products=convert_array($approve_list[0]["products"]);
		
		if(!empty($cgst_per))
			$qty_list["cgst_per"]=$cgst_per;
		if(!empty($sgst_per))
			$qty_list["sgst_per"]=$sgst_per;
		if(!empty($igst_per))
			$qty_list["igst_per"]=$igst_per;
		foreach($product_id as $va1=>$key1){
			$insr_array1["product_id"]=$key1;
			$nonbuff_price=$this->get_updated_price($key1);
			$insr_array1["nonbuffed_price"]=$product_price[$key1]["price"];
			$insr_array1["buffed_price"]=$nonbuff_price[0]["buffed_price"];
			$insr_array1["maching_price"]=$nonbuff_price[0]["maching_price"];
			$insr_array1["nonbuffed_qty"]=$qty_approve[$va1];
			$insr_array1["nonbuffed_process_qty"]=$qty_approve[$va1];
			$insr_array1["po_id"]=$product_price[$key1]["po_id"];
			$insr_array1["creation_date"]=time();
			$insr_array1["status"]=0;
			//$where2="id=:sub_product_id ";
			//$where_arr2= array('sub_product_id'=>$key1);
			$where2="inward_id=:inward_id AND product_id=:product_id";
			$where_arr2= array('inward_id'=>$_GET["inward_id"],'product_id'=>$key1);
			$insert_ids=$this->update_query("price_history",$insr_array1,$where2,$where_arr2);	
			//$insert_ids=$this->insert_query("price_history",$insr_array1);	   
			$product[$va1][$key1]=$rate[$va1];
		}
		foreach($product_id as $va=>$key){
			$final_arry[$va]["product_id"]=$key;
			$final_arry[$va]["qty_received"]=$qty_received[$va];
			$final_arry[$va]["qty_approve"]=$qty_approve[$va];
			$final_arry[$va]["qty_rejected"]=$qty_received[$va]-$qty_approve[$va];
			$final_arry[$va]["ins_remarks"]=$ins_remarks[$va];	
			$final_arry[$va]["product_price"]=$product_price[$key]["price"];			
			$total_received+=$qty_received[$va];
			$total_approve+=$qty_approve[$va];
			$total_reject+=$qty_received[$va]-$qty_approve[$va];
		}
		
		$final_arry1["total_received"]=$total_received;
		$final_arry1["total_approve"]=$total_approve;
		$final_arry1["total_reject"]=$total_reject;
		$final_arry1["sgst_percent"]=$sgst_per;
		$final_arry1["cgst_percent"]=$cgst_per;
		$final_arry1["igst_percent"]=$igst_per;	
		$final_arry1["sgst"]=$sgst;
		$final_arry1["cgst"]=$cgst;
		$final_arry1["igst"]=$igst;
		$insr_array["products"]=convert_json($final_arry);
		$insr_array["qty_list"]=convert_json($final_arry1);
		//foreach($products as $va=>$key){
		//$products[$va]["rate"]=$product[$va][$key["product_id"]];
		//}
		
		foreach($product_id as $va=>$key){
			$qtys=$this->openstock_edit1($key);		
			$updatearr["processed_qty"]=$qtys[0]["processed_qty"]+$qty_approve[$va];		
			$where2="subproduct_id=:product_id ";
			$where_arr2= array('product_id'=>$key);
			$insert_ids=$this->update_query("openstock",$updatearr,$where2,$where_arr2);		
		}
		 
		$insr_array["admin_approve"]=$approve;
		$insr_array["admin_comment"]=$comments;
		$insr_array["admin_processed"]=$processed;	
		$insr_array["admin_appr_time"]=time();
		//$insr_array["qty_list"]=convert_json($qty_list);
		//$insr_array["products"]=convert_json($products);
		 
		$where="id=:sub_product_id";
		$where_arr= array('sub_product_id'=>$_GET["inward_id"]);
		$update=$this->update_query("inward_approve",$insr_array,$where,$where_arr);
		return "success";
	}
	
	
	
	function inwardappr_update($product_price){
		 
		extract($_POST);
		//$insr_array1["dated"]=strtotime($grn_date);
		//$insr_array1["inward_no"]=$grn_no;			
		//$insr_array1["inv_no"]=$inv_no;
		$insr_array1["supplier"]=$supplier;
		$insr_array1["ins_date"]=strtotime($ins_date);
		$insr_array1["ins_by"]=$ins_by;
		//$insr_array1["received_by"]=$recei_by;
		$insr_array1["inv_date"]=strtotime($inv_date);
		$final_arry=array();
		$total_received=0;
		$total_approve=0;
		$total_reject=0;
		foreach($product_id as $va=>$key){
			$final_arry[$va]["product_id"]=$key;
			$final_arry[$va]["qty_received"]=$qty_received[$va];
			$final_arry[$va]["qty_approve"]=$qty_approve[$va];
			$final_arry[$va]["qty_rejected"]=$qty_received[$va]-$qty_approve[$va];
			$final_arry[$va]["ins_remarks"]=$ins_remarks[$va];	
			$final_arry[$va]["product_price"]=$product_price[$va];	
			$final_arry[$va]["sgst"]=$sgst[$va];
			$final_arry[$va]["cgst"]=$cgst[$va];
			$final_arry[$va]["igst"]=$igst[$va];
			$total_received+=$qty_received[$va];
			$total_approve+=$qty_approve[$va];
			$total_reject+=$qty_received[$va]-$qty_approve[$va];
		}
		$final_arry1["total_received"]=$total_received;
		$final_arry1["total_approve"]=$total_approve;
		$final_arry1["total_reject"]=$total_reject;
		$insr_array1["products"]=base64_encode(json_encode($final_arry));
		$insr_array1["qty_list"]=base64_encode(json_encode($final_arry1));
		$insr_array1["comments"]=$comments;
		$insr_array1["modified_by"]=$_SESSION["user_id"];
		$insr_array1["modified_date"]=time();		
		 
		$where="inward_id=:inwardid";
		$where_arr= array('inwardid'=>$_GET["inward_id"]);
		 
		$update=$this->update_query("inward_approve",$insr_array1,$where,$where_arr);
		return "success";
	}
	
	function received_insert(){
		extract($_POST);		
		$insr_array["product_id"]=$product_id;
		$insr_array["qty"]=$qty;
		$insr_array["price"]=$price;
		$insr_array["supplier_id"]=$supplier_id;
		$insr_array["received_date"]=$received_date;
		$insr_array["creation_time"]=time();
		$insert_id=$this->insert_query("received_products",$insr_array);
		return "success";
	}
	function random(){
	$characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $result = '';
    for ($i = 0; $i < 5; $i++){
        $result .= $characters[mt_rand(0, 26)];
	}
	return $result;
	}
	function buffing_insert(){
	
		extract($_POST); 
		$insr_array["name"]=$name;
		$insr_array["supervisor_name"]=$supervisor_name; 
		$insr_array["buffing_date"]=strtotime($grn_date);
		$insr_array["bpl_no"]=$bpl_no;
		$insr_array["creation_time"]=time(); 
		$insr_array["emp_type"]=$emp_type;
		$insr_array["buffed_total"]=$buffed_total;
		$insr_array["payable_total"]=$payable_total;
				
		//$inward_appr_list=$this->inward_approve_admin();
		$maching_approve=$this->maching_approve_admin();
		$maching_appr_list=convert_array($maching_approve[0]["adminproduct_approve"]);
		
		foreach($maching_appr_list["product_id"] as $va=>$key){
			$maching_list[$va]["product_id"]=$key;
			$maching_list[$va]["qty_received"]=$maching_appr_list["qty_received"][$va];
			$maching_list[$va]["qty_approve"]=$maching_appr_list["qty_approve"][$va];
			$maching_list[$va]["qty_rejected"]=$maching_appr_list["qty_rejected"][$va];
		}
		//$subproduct_list=$db_helper_obj->subproduct_pinvolve();
		$subproduct_list=$this->totalpr_pinvolve();
		

		foreach($subproduct_list as $va=>$key){
			$product_ids[]=$key["id"];
			$po_qty=$this->openstock_edit1($key["id"]);
			$total_product_edit=$this->total_product_edit($key["id"]);
			//echo"<pre>";print_r($po_qty);echo"</pre>";
			if($total_product_edit[0]["machining"]==1){
				$open_stock[$key["id"]]=$po_qty[0]["processed_maching"];
				$subproduct_list[$va]["qty"]=$po_qty[0]["processed_maching"]; 
			}else{
				$open_stock[$key["id"]]=$po_qty[0]["processed_qty"];
				$subproduct_list[$va]["qty"]=$po_qty[0]["processed_qty"]; 	
			}
			
			$product_name[$key["id"]]=$key["name"];
			$price[$key["id"]]=$key["price"];
		}
 
		foreach($maching_list as $va1=>$key1){
			if(in_array($key1["product_id"],$product_ids)){
				$total_qty[$key1["product_id"]]["qty_approve"]+=$key1["qty_approve"];
				$total_qty[$key1["product_id"]]["product_id"]=$key1["product_id"];
			}
		 }
		 
		foreach($total_qty as $va1=>$key1){
		$total_qty[$va1]["stocks"]=$open_stock[$key1["product_id"]];
		$total_qty[$va1]["product_name"]=$product_name[$key1["product_id"]];
		$total_qty[$va1]["price"]=$price[$key1["product_id"]];
		}
		
		
		foreach($subproduct_list as $va=>$key){
			
		if(!isset($total_qty[$key["id"]]["product_id"])){
		if($key["qty"]!=0){
		$total_qty[$key["id"]]["product_id"]=$key["id"];
		$total_qty[$key["id"]]["stocks"]=$key["qty"];
		$total_qty[$key["id"]]["product_name"]=$key["name"];
		$total_qty[$key["id"]]["price"]=$key["price"];
		}
		}	
		}

		if($emp_type!=1){
		if(!empty($aprv_over_time))
			$insr_array["aprve_time_before"] =$aprv_over_time;
		if(!empty($approve))
			$insr_array["approved"] =$approve[0];
		$insr_array["comments"]=$comments_regular;	
		}else{
		$insr_array["comments"]=$comments;
		}
		
		 $final_arry=array();
		if($emp_type==1){		
		foreach($product_id as $va=>$key){			
		if($qty[$va]!=0)	{
		 $final_arry[$va]["product_id"]=$total_qty[$key]["product_id"];
		
		
		 $qtys=$this->openstock_edit1($total_qty[$key]["product_id"]);
		$total_product_edit=$this->total_product_edit($total_qty[$key]["product_id"]);
			
		if($total_product_edit[0]["machining"]==1){			
			if($qtys[0]["processed_maching"]>=$qty[$va])			 
				$updatearr[$va]["processed_maching"]=$qtys[0]["processed_maching"]-$qty[$va];
			else if($qtys[0]["processed_maching"]<$qty[$va])
				$updatearr[$va]["processed_maching"]=0;
		}else{
			if($qtys[0]["processed_qty"]>=$qty[$va])			 
				$updatearr[$va]["processed_qty"]=$qtys[0]["processed_qty"]-$qty[$va];
			else if($qtys[0]["processed_qty"]<$qty[$va])
				$updatearr[$va]["processed_qty"]=0;	
		}
		$where2="subproduct_id=:product_id ";
		$where_arr2=array('product_id'=>$total_qty[$key]["product_id"]);
		
		$insert_ids=$this->update_query("openstock",$updatearr[$va],$where2,$where_arr2);
		$final_arry[$va]["stocks"] =$total_qty[$key]["stocks"];
		$final_arry[$va]["product_name"] =$total_qty[$key]["product_name"];
		$final_arry[$va]["price"] =$price[$va];
		$final_arry[$va]["buffed_qty"] =$qty[$va];
		$final_arry[$va]["amount"] =$qty[$va]*$total_qty[$key]["price"];		
		}
		}
		}
		//exit;
		if($emp_type!=1){
		foreach($regular_price as $va=>$key){ 
			$final_arry[$va]["regular_time"] =$regular_time;
			$final_arry[$va]["regular_price"] =$key;
			$final_arry[$va]["regular_amount"] =$regular_time*$key;
		if(!empty($approve)){
			$final_arry[$va]["over_time"] =$over_time;
			$final_arry[$va]["overtime_price"] =$key;
			$final_arry[$va]["overtime_amount"] =$over_time*$key;	
		}
		}			
		}

		$insr_array["products"]=base64_encode(json_encode($final_arry));		
		$insr_array["created_by"]=$_SESSION["user_id"];
		$insr_array["status"]=1;
		$insert_id=$this->insert_query("buffing",$insr_array);
		
		return "success";
	}
	
	function machining_insert(){
		
		extract($_POST);
		//echo"<pre>";print_r($_POST);echo"</pre>";
		//exit;
		$insr_array["name"]=$name;
		$insr_array["supervisor_name"]=$supervisor_name;
		
		//$date = date("d-m-Y", strtotime($date));		
		$insr_array["machining_date"]=strtotime($grn_date);
		$insr_array["bpl_no"]=$bpl_no;
		$insr_array["creation_time"]=time();
				
		$insr_array["emp_type"]=$emp_type;
		$insr_array["buffed_total"]=$buffed_total;
		$insr_array["payable_total"]=$payable_total;
				
		$inward_appr_list=$this->inward_approve_admin();
		$subproduct_list=$this->totalpr_pinvolve();
		//echo"<pre>";print_r($subproduct_list);echo"</pre>";
		foreach($subproduct_list as $va=>$key){
			$product_ids[]=$key["id"];
			$po_qty=$this->openstock_edit1($key["id"]);
			$open_stock[$key["id"]]=$po_qty[0]["processed_qty"];
			$subproduct_list[$va]["qty"]=$po_qty[0]["processed_qty"];
			//$po_name=$this->product_edit($key["product_id"]);
			$product_name[$key["id"]]=$key["name"];
			$price[$key["id"]]=$key["price"];
		}

		foreach($inward_appr_list as $va=>$key){
			$products=convert_array($key["products"]);		
			$qtys=convert_array($key["qty_list"]);	
			foreach($products as $va1=>$key1){
				if(in_array($key1["product_id"],$product_ids)){
					$total_qty[$key1["product_id"]]["qty_approve"]+=$key1["qty_approve"];
					$total_qty[$key1["product_id"]]["product_id"]=$key1["product_id"];
				}
			}
		}
		//echo"<pre>";print_r($total_qty);echo"</pre>";
		//exit;
		foreach($total_qty as $va1=>$key1){
		$total_qty[$va1]["stocks"]=$open_stock[$key1["product_id"]];
		$total_qty[$va1]["product_name"]=$product_name[$key1["product_id"]];
		$total_qty[$va1]["price"]=$price[$key1["product_id"]];
		}

foreach($subproduct_list as $va=>$key){
if(!isset($total_qty[$key["id"]]["product_id"])){
if($key["qty"]!=0){
$total_qty[$key["id"]]["product_id"]=$key["id"];
$total_qty[$key["id"]]["stocks"]=$key["qty"];
$total_qty[$key["id"]]["product_name"]=$key["name"];
$total_qty[$key["id"]]["price"]=$key["price"];
}	
}	
}

		if($emp_type!=1){
		if(!empty($aprv_over_time))
			$insr_array["aprve_time_before"] =$aprv_over_time;
		if(!empty($approve))
			$insr_array["approved"] =$approve[0];
		$insr_array["comments"]=$comments_regular;	
		}else{
		$insr_array["comments"]=$comments;
		
		}
		
		 $final_arry=array();
		if($emp_type==1){	
		foreach($product_id as $va=>$key){
		if($qty[$va]!=0)	{
		 $final_arry[$va]["product_id"] =$total_qty[$key]["product_id"];
		 
		 $qtys=$this->openstock_edit1($total_qty[$key]["product_id"]);
		
		if($qtys[0]["processed_qty"]>=$qty[$va])			 
		 $updatearr["processed_qty"]=$qtys[0]["processed_qty"]-$qty[$va];
	     else if($qtys[0]["processed_qty"]<$qty[$va])
		  $updatearr["processed_qty"]=0;
			$where2="subproduct_id=:product_id ";
			$where_arr2= array('product_id'=>$total_qty[$key]["product_id"]);
	
		$insert_ids=$this->update_query("openstock",$updatearr,$where2,$where_arr2);
	  
	  
		 $final_arry[$va]["stocks"] =$total_qty[$key]["stocks"];
		 $final_arry[$va]["product_name"] =$total_qty[$key]["product_name"];
		 $final_arry[$va]["price"] =$total_qty[$key]["price"];
		 $final_arry[$va]["buffed_qty"] =$qty[$va];
		 $final_arry[$va]["amount"] =$qty[$va]*$total_qty[$key]["price"];
		
		}
		}
		}
		
		if($emp_type!=1){
		
		foreach($regular_price as $va=>$key){
		
		 $final_arry[$va]["regular_time"] =$regular_time;
		 $final_arry[$va]["regular_price"] =$key;
		 $final_arry[$va]["regular_amount"] =$regular_time*$key;
		 if(!empty($approve)){
		 $final_arry[$va]["over_time"] =$over_time;
		 $final_arry[$va]["overtime_price"] =$key;
		 $final_arry[$va]["overtime_amount"] =$over_time*$key;	
		 }
		}			
		}

		$insr_array["products"]=base64_encode(json_encode($final_arry));		
		$insr_array["created_by"]=$_SESSION["user_id"];
		$insr_array["status"]=1;
		$insert_id=$this->insert_query("machining",$insr_array);
		
		return "success";
	}

	function supplier_insert(){
		extract($_POST);
		$insr_array["supplier_name"]=$supplier_name;
		$insr_array["vendor_id"]=$vendor_id;
		$insr_array["address"]=$address;
		$insr_array["city"]=$city;
		$insr_array["state"]=$state;	
		$insr_array["gst_no"]=$gst_no;
		$insr_array["pincode"]=$pincode;
		$insr_array["payment_terms"]=$payment_terms;
		$insr_array["creation_time"]=time();
		$insr_array["created_by"]=$_SESSION["user_id"];
		$insert_id=$this->insert_query("suppliers",$insr_array);
		return "success";
	}
	function product_update(){ 
		extract($_POST);
		$insr_array["product_name"]=$title;
		$insr_array["series"]=$series;
		$insr_array["price"]=$product_price;
		$insr_array["open_qty"]=$open_qty;		
		//$insr_array["creation_time"]=time();
		//$insr_array["created_by"]=$_SESSION["user_id"];
		
		$where="id=:product_id ";
	 	$where_arr= array('product_id'=>$product_id);
	
		$insert_id=$this->update_query("products",$insr_array,$where,$where_arr);
		//$total_count=$order_item_row_index_1;
		foreach($sub_product_id as $va=>$key){
			$insr_array1["name"]=$name[$va];			 
			$insr_array1["qty"]=$qty[$va];			 
			$insr_array1["product_id"]=$product_id;	
 
			if(!empty($name[$va])){ 
				$where="id=:sub_product_id AND  product_id=:product_id";
				$where_arr= array('sub_product_id'=>$key,'product_id'=>$product_id);
				$update=$this->update_query("sub_products",$insr_array1,$where,$where_arr);
				
			}else{
				//$insert=$this->insert_query("sub_products",$insr_array1);
				$where1="id=:sub_product_id AND  product_id=:product_id";
				$where_arr1= array('sub_product_id'=>$key,'product_id'=>$product_id);
			   //echo 'deleteids'.$sub_product_id[$va].'<br/>';
				$delete=$this->delete_query("sub_products",$where1,$where_arr1);
			}
		}
		foreach($_POST["name_new"] as $va1=>$key1){
			$insr_array2["name"]=$key1;
			$insr_array2["qty"]=$_POST["qty_new"][$va1];
				
			$insr_array2["product_id"]=$product_id;	

			$insert=$this->insert_query("sub_products",$insr_array2);
		}

		return "success";
	}
	function total_product_update($id){
			extract($_POST);
		foreach($name as $va=>$key){
			$insr_array1["name"]=$name[$va];
			$insr_array1["hsn"]=$hsn[$va];
			$insr_array1["material"]=$material[$va];
			$insr_array1["dimension"]=$dimension[$va];
			$insr_array1["price"]=$price[$va];
			$insr_array1["machining_price"]=$machining_price[$va];
			$insr_array1["update_machinprice"]=$machining_price[$va];
			$insr_array1["product_price"]=$product_price[$va];	
			$insr_array1["creation_time"]=time();
			if(isset($process[$va]))
				$insr_array1["process"]=1;
			else
				$insr_array1["process"]=0;
			if(isset($machining[$va]))
				$insr_array1["machining"]=1;
			else
				$insr_array1["machining"]=0;
			$insr_array1["update_time"]=time();
			$insr_array1["updated_by"]=$_SESSION["user_id"];
			
			
		$totalproduct_list=$this->total_product_edit($id);

			if(isset($product_price[$va]))
				$insr_array2["nonbuffed_price"]=$product_price[$va];
			if(isset($price[$va]))
				$insr_array2["buffed_price"]=$price[$va];
			if(isset($machining_price[$va]))
				$insr_array2["maching_price"]=$machining_price[$va];			
				$insr_array2["creation_date"]=time();
			$already_stock=$this->openstock_qty($id);
			if(!empty($already_stock)){
				$where1="id=:history_id";
				$where_arr1= array('history_id'=>$already_stock[0]["id"]);
				$update=$this->update_query("price_history",$insr_array2,$where1,$where_arr1);
			}

		/* if($totalproduct_list[0]["price"]!=$price[$va]||$totalproduct_list[0]["product_price"]!=$product_price[$va]||$totalproduct_list[0]["machining_price"]!=$machining_price[$va]){ 
			$insr_array3["product_id"]=$name[$va];
			if($totalproduct_list[0]["price"]!=$price[$va])
				$insr_array3["nonbuff_price"]=$price[$va];
			if($totalproduct_list[0]["product_price"]!=$product_price[$va])
				$insr_array3["buff_price"]=$product_price[$va];
			if($totalproduct_list[0]["machining_price"]!=$machining_price[$va])
				$insr_array3["machin_price"]=$machining_price[$va];
			$insr_array3["creation_date"]=time();
			$insert=$this->insert_query("price_history",$insr_array3);
		} */
	
	
		$updated_price=array_merge(convert_array($totalproduct_list[0]["updated_price"]),array($price[$va]));
		$updated_product_price=array_merge(convert_array($totalproduct_list[0]["updated_product_price"]),array($product_price[$va]));
		$update_machinprice=array_merge(convert_array($totalproduct_list[0]["update_machinprice"]),array($machining_price[$va]));
		$insr_array1["updated_price"]=convert_json($updated_price);
		$insr_array1["updated_product_price"]=convert_json($updated_product_price);
		$insr_array1["update_machinprice"]=convert_json($update_machinprice);	
		$where="id=:sub_product_id";
		$where_arr= array('sub_product_id'=>$id);
		$update=$this->update_query("total_products",$insr_array1,$where,$where_arr);
				
		}
		return "success";
	}
	function openstock_qty($product_id){
			return $this->select_query("price_history","*","product_id=".$product_id." and status=1");
	}
	function openstock_update($id){
 
		$already_stock=$this->openstock_qty($_POST["name"][1]);
		 
		extract($_POST);		
		foreach($name as $va=>$key){
		
			$insr_array1["subproduct_id"]=$key;			
			$insr_array1["qty"]=(int)$qty[$va];	
			if(isset($qty[$va]))			
				$insr_array1["processed_qty"]=(int)$qty[$va];
			if(isset($general_qty[$va]))			
				$insr_array1["general_qty"]=(int)$general_qty[$va];
			if(isset($buffed_qty[$va]))
				$insr_array1["buffed_qty"]=(int)$buffed_qty[$va];
			if(isset($buffed_qty[$va]))
				$insr_array1["processed_buff"]=(int)$buffed_qty[$va];
			if(isset($maching_qty[$va]))
				$insr_array1["maching_qty"]=(int)$maching_qty[$va];
			if(isset($maching_qty[$va]))
				$insr_array1["processed_maching"]=(int)$maching_qty[$va];			
			if(isset($nonbuffed_price[$va]))
				$insr_array1["nonbuffed_price"]=(int)$nonbuffed_price[$va];
			if(isset($maching_price[$va]))
				$insr_array1["maching_price"]=(int)$maching_price[$va];
			if(isset($buffed_price[$va]))
				$insr_array1["buffed_price"]=(int)$buffed_price[$va];
			
			if(isset($rm_weight[$va]))
				$insr_array1["rm_weight"]=(int)$rm_weight[$va];
			if(isset($rm_wastage[$va]))
				$insr_array1["rm_wastage"]=(int)$rm_wastage[$va];
			
			$insr_array1["creation_time"]=time();
			$insr_array1["created_by"]=$_SESSION["user_id"];			 
			$insr_array2["product_id"]=$key;
			if(isset($nonbuffed_price[$va]))
			$insr_array2["nonbuffed_price"]=$nonbuffed_price[$va];
			if(isset($buffed_price[$va]))
				$insr_array2["buffed_price"]=$buffed_price[$va];
			if(isset($maching_price[$va]))
				$insr_array2["maching_price"]=$maching_price[$va];
			
			$insr_array2["nonbuffed_qty"]=(int)$qty[$va];
			if(isset($maching_qty[$va]))
				$insr_array2["maching_qty"]=(int)$maching_qty[$va];
			if(isset($buffed_qty[$va]))
				$insr_array2["buffed_qty"]=(int)$buffed_qty[$va];		
			
			$insr_array2["nonbuffed_process_qty"]=(int)$qty[$va];
			if(isset($maching_qty[$va]))
				$insr_array2["maching_process_qty"]=(int)$maching_qty[$va];
			if(isset($buffed_qty[$va]))
				$insr_array2["buffed_process_qty"]=(int)$buffed_qty[$va];
			
			$insr_array2["status"]=1;
			$insr_array2["creation_date"]=time();		
			if(!empty($already_stock)){
				$where1="id=:history_id";
				$where_arr1= array('history_id'=>$already_stock[0]["id"]);
				$update=$this->update_query("price_history",$insr_array2,$where1,$where_arr1);
			}else{
				$insert=$this->insert_query("price_history",$insr_array2);	
			}
			

			$where="id=:sub_product_id";
			$where_arr= array('sub_product_id'=>$id);
			$update=$this->update_query("openstock",$insr_array1,$where,$where_arr);
			if(isset($buffed_price[$va]))
				$insr_array3["price"]=$buffed_price[$va];
			if(isset($maching_price[$va]))
				$insr_array3["machining_price"]=$maching_price[$va];
			if(isset($nonbuffed_price[$va]))
				$insr_array3["product_price"]=$nonbuffed_price[$va];	
			$where="id=:sub_product_id";
			$where_arr= array('sub_product_id'=>$id);
			$update=$this->update_query("total_products",$insr_array3,$where,$where_arr);
		}
		return "success";
	}
	
	function product_update_new($pid){
		extract($_POST);
		foreach($sub_product_id as $va=>$key){
			$insr_array1["name"]=$name[$va];
			$insr_array1["qty"]=$qty[$va];
			$insr_array1["processed_qty"]=$qty[$va];			
			$insr_array1["price"]=$price[$va];	
			$insr_array1["product_id"]=$product_id;	
			if(isset($process[$va]))
				$insr_array1["process"]=1;
			else
				$insr_array1["process"]=0;
			if(!empty($name[$va])){ 
				$where="id=:sub_product_id AND  product_id=:product_id";
				$where_arr= array('sub_product_id'=>$key,'product_id'=>$product_id);
				$update=$this->update_query("sub_products",$insr_array1,$where,$where_arr);
				
			}
		}
		foreach($_POST["name_new"] as $va1=>$key1){
			$insr_array2["name"]=$key1;
			$insr_array2["qty"]=$_POST["qty_new"][$va1];
			$insr_array2["processed_qty"]=$_POST["qty_new"][$va1];			
			$insr_array2["price"]=$_POST["price_new"][$va1];	
			$insr_array2["product_id"]=$pid;
			if(isset($approve_new[$va1]))
				$insr_array2["approved"]=1;
			else
				$insr_array2["approved"]=0;			
			if(isset($process_new[$va1]))
				$insr_array2["process"]=1;
			else
				$insr_array2["process"]=0;
			$insert=$this->insert_query("sub_products",$insr_array2);
		}

		return "success";
	}
	
	/*function total_product_update($id){
			extract($_POST);
		foreach($name as $va=>$key){
			$insr_array1["name"]=$name[$va];
			$insr_array1["hsn"]=$hsn[$va];
			$insr_array1["material"]=$material[$va];
			$insr_array1["dimension"]=$dimension[$va];
			$insr_array1["price"]=$price[$va];
			$insr_array1["product_price"]=$product_price[$va];			
			if(isset($process[$va]))
				$insr_array1["process"]=1;
			else
				$insr_array1["process"]=0;
			if(isset($machining[$va]))
				$insr_array1["machining"]=1;
			else
				$insr_array1["machining"]=0;
			$insr_array1["update_time"]=time();
			$insr_array1["updated_by"]=$_SESSION["user_id"];			
			
			$where="id=:sub_product_id";
			$where_arr= array('sub_product_id'=>$id);
			$update=$this->update_query("total_products",$insr_array1,$where,$where_arr);
				
		}
		return "success";
	}*/
	function order_update($id){
		extract($_POST);
		 
		//$insr_array["product_name"]=$title;
		//$insr_array["series"]=$series;
		$insr_array["po_date"]=($dates);
		$insr_array["creation_time"]=time();
		$insr_array["created_by"]=$_SESSION["user_id"];
		$insr_array["supplier_id"]=$supplier_id;
		$insr_array["gst_no"]=$gst_number;
		$insr_array["shipto_id"]=$shipto_id;
		$insr_array["payment_terms"]=$payment_terms;
		$insr_array["required_date"]=($required_date);
		$insr_array["sgst_percent"]=$sgst;
		$insr_array["cgst_percent"]=$cgst;
		$insr_array["igst_percent"]=$igst;
		$insr_array["sgst_total"]=$sgst_total;
		$insr_array["cgst_total"]=$cgst_total;
		$insr_array["igst_total"]=$igst_total;
		$insr_array["sub_totall"]=$sub_totall;
		$insr_array["material_type"]=$material_type;
		$insr_array["orders"]=$orders;
		$insr_array["amount_words"]=$words;
		$insr_array["pdfinfo"]=base64_encode(json_encode($_POST));
		$where="id=:order_id ";
	 	$where_arr= array('order_id'=>$_GET["order_id"]);
		$insert_id=$this->update_query("orders",$insr_array,$where,$where_arr);
		//$total_count=$order_item_row_index_1;
		
		foreach($name as $va3=>$key3){
			$update_name["product_name"]=$name[$va3];
			$update_name["hsn"]=$hsn[$va3];
			$update_name["dimension"]=$dimension[$va3];
			$update_name["qty"]=$qty[$va3];
			$update_name["unit"]=$units[$va3];
			$update_name["price"]=$price[$va3];
			$update_name["amount"]=$qty[$va3]*$price[$va3];
			//$update_name["order_id"]=$insert_id;
			$update_name["ordersub_pdfinfo"]=base64_encode(json_encode($_POST));
			 
		 
			$where1="product_name=:sub_order_id AND order_id=:order_id";
			$where_arr1= array('sub_order_id'=>$key3,'order_id'=>$_GET["order_id"]);
			$update=$this->update_query("order_sub",$update_name,$where1,$where_arr1);		
		}		
		 

		/*foreach($name as $va4=>$key4)
		{
			$insert_name["product_name"]=$key4;
			$insert_name["hsn"]=$hsn[$va4];
			$insert_name["dimension"]=$dimension[$va4];
			$insert_name["qty"]=$qty[$va4];
			$insert_name["price"]=$price[$va4];
			$insert_name["amount"]=$qty[$va]*$price[$va4];
			$insert_name["order_id"]=$insert_id;
			$insert_name["ordersub_pdfinfo"]= base64_encode(json_encode($_POST));
		$where2="id=:sub_order_id AND  order_id=:order_id";
		$where_arr2= array('sub_order_id'=>$suborderedit_id[$va3],'order_id'=>$_GET["order_id"]);
		$insert=$this->insert_query("order_sub",$insert_name,$where2,$where_arr2);
		} 
		
		foreach($suborder_id as $va=>$key){
			
			$insr_array1["product_name"]=$name[$va];
			$insr_array1["hsn"]=$hsn[$va];
			$insr_array1["dimension"]=$dimension[$va];
			$insr_array1["qty"]=$qty[$va];
			$insr_array1["price"]=$price[$va];
			$insr_array1["amount"]=$qty[$va]*$price[$va];
			//$insr_array1["order_id"]=$insert_id;
			$insr_array1["order_id"]=$_GET["order_id"];
			if(!empty($name[$va])){
				$where1="id=:sub_order_id AND  order_id=:order_id";
				$where_arr1= array('sub_order_id'=>$key,'order_id'=>$_GET["order_id"]);
				$update=$this->update_query("order_sub",$insr_array1,$where1,$where_arr1);				
			}else{
				$where2="id=:sub_order_id AND  order_id=:order_id";
				$where_arr2= array('sub_order_id'=>$key,'order_id'=>$_GET["order_id"]);
				$delete=$this->delete_query("order_sub",$where2,$where_arr2);
			}
		}*/
			return "success";
	}
	
	
	
	function employee_update(){
		extract($_POST);
		$insr_array["name"]=$employee_name;
		$insr_array["role"]=$employee_role;
		$insr_array["price"]=$price;
		$insr_array["creation_time"]=time();
		
		$where="id=:emp_id ";
	 	$where_arr= array('emp_id'=>$emp_id);
	
		$insert_id=$this->update_query("employee",$insr_array,$where,$where_arr);
	}	
	function received_update(){
		extract($_POST);
		$insr_array["product_id"]=$product_id;
		$insr_array["qty"]=$qty;
		$insr_array["price"]=$price;
		$insr_array["supplier_id"]=$supplier_id;
		$insr_array["received_date"]=$received_date;
		$insr_array["creation_time"]=time();
		
		$where="id=:recei_id ";
	 	$where_arr= array('recei_id'=>$recei_id);
	
		$insert_id=$this->update_query("received_products",$insr_array,$where,$where_arr);
	}
	/* function payment_update($pay,$buffid){
		$insr_array["payment"]=$pay;
		$where="id=:buffid ";
		$where_arr= array('buffid'=>$buffid);	
		$insert_id=$this->update_query("buffing",$insr_array,$where,$where_arr);	
	} */
	
	function process_insert(){
		extract($_POST);
		$insr_array["process"]=$process;
		$insr_array["po_number"]=$po_number;
		//$insr_array["inward_id"]=$purchase;
		
		$insr_array["name"]=$name;
		$insr_array["supervisor_name"]=$supervisor_name;
		$insr_array["receipt_date"]=$date;
		$insr_array["pur_id"]=$purchase;
		$insr_array["approved"]=$approved;
		$insr_array["vf_supervisor"]=$vf_supervisor;
		$final_arry=array();
		$val=0;
		foreach($_POST["check_name"] as $va=>$key){
			
		$final_arry["list"][$va]["name"]=$_POST["subproduct"][$va];	
		$final_arry["list"][$va]["qty"]=$_POST["qty"][$va];
		$final_arry["list"][$va]["price"]=$_POST["price"][$va];
		$final_arry["list"][$va]["amount"]=$_POST["amount"][$va];
		$val++;
		}
		$insr_array["sub_products"]=base64_encode(json_encode($final_arry));		
		$insr_array["created_by"]=$_SESSION["user_id"];
		$insr_array["creation_time"]=time();
		$insert_id=$this->insert_query("inward_process",$insr_array);
		return "success";
	}
	
	function invoice_generation(){
		extract($_POST);
		$insr_array["invoice_no"]=$inv_no;
		$insr_array["invoice_date"]=strtotime($inv_date);
		$insr_array["inward_ids"]=$inward_id;
		//$insr_array["supplier_id"]=$name;
		$insr_array["created_by"]=$_SESSION["user_id"];
		$insr_array["creation_time"]=time();
		$insert_id=$this->insert_query("invoice",$insr_array);
		foreach(explode(",",$inward_id) as $va=>$key){
			$where2="inward_id=:id ";
			$where_arr2= array('id'=>$key);
			$updatearr["invoice_status"]=1;
			$insert_ids=$this->update_query("inward_approve",$updatearr,$where2,$where_arr2);
		}
		 
		return "success";
	}
	
	
	
	function deliverymo_insert()
	{
		extract($_POST);
		$insr_array["dated"]=$dated;
		$insr_array["challan_no"]=$challan_no;
		$insr_array["amount_words"]=$words;
		$insr_array["sub_totallqty"]=$sub_totallqty;
		$insr_array["supplier_id"]=$supplier;
		$insr_array["gst_no"]=$gst_number;
		$insr_array["duration"]=$duration;
		$insr_array["delivery_through"]=$delivery_through;
		$insr_array["payment_term"]=$payment_term;
		$insr_array["vehicle_no"]=$vehicle_no;
		$insr_array["destination"]=$destination;
		$insr_array["nature"]=implode(",",$nature);
		$insr_array["created_by"]=$_SESSION["user_id"];
		$insr_array["creation_time"]=time();
		$insr_array["products"]=base64_encode(json_encode($_POST));
		$insert_id=$this->insert_query("deliverymo",$insr_array);
		 
		 $inward_appr_list=$this->inward_approve_admin();

	$subproduct_list=$this->total_product_list();

 
	foreach($subproduct_list as $va=>$key){
		$product_ids[]=$key["id"];
		$po_qty=$this->openstock_edit1($key["id"]);	
		$open_stock[$key["id"]]=$po_qty[0]["processed_qty"];
		$subproduct_list[$va]["qty"]=$po_qty[0]["processed_qty"];
		$product_name[$key["id"]]=$key["name"];
		$price[$key["id"]]=$key["price"];
		$product_price[$key["id"]]=$key["product_price"];
	}


	foreach($inward_appr_list as $va=>$key){
		
		$products=convert_array($key["products"]);		
		$qtys=convert_array($key["qty_list"]);	
			
		foreach($products as $va1=>$key1){
			if(in_array($key1["product_id"],$product_ids)){
				$total_qty[$key1["product_id"]]["qty_approve"]+=$key1["qty_approve"];
				$total_qty[$key1["product_id"]]["product_id"]=$key1["product_id"];
				$total_qty[$key1["product_id"]]["product_price"]=$key1["product_price"];
			}
		}
	}

	foreach($total_qty as $va1=>$key1){
		$total_qty[$va1]["stocks"]=$open_stock[$key1["product_id"]];
		$total_qty[$va1]["product_name"]=$product_name[$key1["product_id"]];
		$total_qty[$va1]["price"]=$price[$key1["product_id"]];
		$total_qty[$va1]["product_price"]=$product_price[$key1["product_id"]];
	}
	foreach($subproduct_list as $va=>$key){
	if(!isset($total_qty[$key["id"]]["product_id"])){
	if($key["qty"]!=0){
	$total_qty[$key["id"]]["product_id"]=$key["id"];
	$total_qty[$key["id"]]["stocks"]=$key["qty"];
	$total_qty[$key["id"]]["product_name"]=$key["name"];
	$total_qty[$key["id"]]["price"]=$key["price"];
	$total_qty[$key["id"]]["product_price"]=$key["product_price"];
	}	
	}	
	}

	foreach($total_qty as $va2=>$key2){
	$subproduct=$this->total_product_edit($key2["product_id"]);	
	if($subproduct[0]["material"]==1||$subproduct[0]["material"]==4){
		$total_qty1[$va2]=$key2;
		$id_machin[]=$key2["product_id"];
	}
	}
		foreach($product_id as $va=>$key){
		if($qty[$va]!=0){		 
		$final_arry[$va]["product_id"] =$total_qty[$key]["product_id"];		 
		$qtys=$this->openstock_edit1($total_qty[$key]["product_id"]);
		$updatearr["processed_qty"]=$qtys[0]["processed_qty"]-$qty[$va];
		$where2="subproduct_id=:product_id ";
		$where_arr2= array('product_id'=>$total_qty[$key]["product_id"]);
		$insert_ids=$this->update_query("openstock",$updatearr,$where2,$where_arr2);
		 $final_arry[$va]["stocks"] =$total_qty[$key]["stocks"];
		 $final_arry[$va]["product_name"] =$total_qty[$key]["product_name"];
		 $final_arry[$va]["price"] =$total_qty[$key]["price"];
		 $final_arry[$va]["processed_qty"] =$qty[$va];
		 $final_arry[$va]["amount"] =$qty[$va]*$total_qty[$key]["price"];
		
		}
		}
		foreach($_POST["product_id"] as $va=>$key)
		{
			$insr_array1["dated"]=$dated;
			$insr_array1["product_id"]=$key;	
			$insr_array1["qty"]=$_POST["qty"][$va];
			$insr_array1["rate"]=$_POST["rate"][$va];
			$insr_array1["deliverymo_id"]=$insert_id;
			$insr_array1["products_sub"]=base64_encode(json_encode($_POST));
			$insert_id1=$this->insert_query("deliverymo_sub",$insr_array1);
		}
		return "success";
	}
	function deliverymi_insert()
	{
		extract($_POST);
		$insr_array["dated"]=$dated;
		$insr_array["challan_no"]=$challan_no;
		$insr_array["amount_words"]=$words;
		$insr_array["sub_totallqty"]=$sub_totallqty;
		$insr_array["supplier"]=$supplier;
		$insr_array["jout_list"]=$jout_list;
		$insr_array["gst_no"]=$gst_number;
		$insr_array["supp_bill_date"]=$supp_bill_date;
		$insr_array["supp_invoice_no"]=$supp_invoice_no;
		$insr_array["duration"]=$duration;
		$insr_array["delivery_through"]=$delivery_through;
		$insr_array["payment_term"]=$payment_term;
		$insr_array["vehicle_no"]=$vehicle_no;
		$insr_array["destination"]=$destination;
		$insr_array["status"]=1;
		$insr_array["qc_approve"]=0;
		$insr_array["admin_approve"]=0;
		$insr_array["nature"]=implode(",",$nature);
		$insr_array["created_by"]=$_SESSION["user_id"];
		$insr_array["creation_time"]=time();
		$insr_array["products"]=base64_encode(json_encode($_POST));
		$insert_id=$this->insert_query("deliverymi",$insr_array);
		 
		$inward_appr_list=$this->inward_approve_admin();

		$subproduct_list=$this->total_product_list();

 
			foreach($subproduct_list as $va=>$key){
				$product_ids[]=$key["id"];
				$po_qty=$this->openstock_edit1($key["id"]);
				$open_stock[$key["id"]]=$po_qty[0]["processed_qty"];
				$subproduct_list[$va]["qty"]=$po_qty[0]["processed_qty"];
				$product_name[$key["id"]]=$key["name"];
				$price[$key["id"]]=$key["price"];
				$product_price[$key["id"]]=$key["product_price"];
			}


			foreach($inward_appr_list as $va=>$key){
				
				$products=convert_array($key["products"]);		
				$qtys=convert_array($key["qty_list"]);
					
				foreach($products as $va1=>$key1){
					if(in_array($key1["product_id"],$product_ids)){
						$total_qty[$key1["product_id"]]["qty_approve"]+=$key1["qty_approve"];
						$total_qty[$key1["product_id"]]["product_id"]=$key1["product_id"];
						$total_qty[$key1["product_id"]]["product_price"]=$key1["product_price"];
					}
				}
			}

			foreach($total_qty as $va1=>$key1){
				$total_qty[$va1]["stocks"]=$open_stock[$key1["product_id"]];
				$total_qty[$va1]["product_name"]=$product_name[$key1["product_id"]];
				$total_qty[$va1]["price"]=$price[$key1["product_id"]];
				$total_qty[$va1]["product_price"]=$product_price[$key1["product_id"]];
			}
			foreach($subproduct_list as $va=>$key){
			if(!isset($total_qty[$key["id"]]["product_id"])){
			//if($key["qty"]!=0){
			$total_qty[$key["id"]]["product_id"]=$key["id"];
			$total_qty[$key["id"]]["stocks"]=$key["qty"];
			$total_qty[$key["id"]]["product_name"]=$key["name"];
			$total_qty[$key["id"]]["price"]=$key["price"];
			$total_qty[$key["id"]]["product_price"]=$key["product_price"];
			//}	
			}	
			}

foreach($total_qty as $va2=>$key2){
$subproduct=$this->total_product_edit($key2["product_id"]);	
if($subproduct[0]["material"]==1||$subproduct[0]["material"]==2|| $subproduct[0]["material"]==4){
	$total_qty1[$va2]=$key2;
	$id_machin[]=$key2["product_id"];
}
}
		foreach($product_id as $va=>$key){		
		$final_arry[$va]["product_id"] =$total_qty[$key]["product_id"];
		$qtys=$this->openstock_edit1($total_qty[$key]["product_id"]); 
		$updatearr["processed_qty"]=$qtys[0]["processed_qty"]+$qty[$va];
		$where2="subproduct_id=:product_id ";
		$where_arr2= array('product_id'=>$total_qty[$key]["product_id"]);
	
		 $insert_ids=$this->update_query("openstock",$updatearr,$where2,$where_arr2);
		 $final_arry[$va]["stocks"] =$total_qty[$key]["stocks"];
		 $final_arry[$va]["product_name"] =$total_qty[$key]["product_name"];
		 $final_arry[$va]["price"] =$total_qty[$key]["price"];
		 $final_arry[$va]["processed_qty"] =$qty[$va];
		 $final_arry[$va]["amount"] =$qty[$va]*$total_qty[$key]["price"];
		}
		
	
		foreach($product_id as $va=>$key)
		{
			$insr_array1["dated"]=$dated;
			$insr_array1["product_id"]=$key;	
			$insr_array1["qty"]=$_POST["qty"][$va];
			$insr_array1["rate"]=$_POST["rate"][$va];
			$insr_array1["deliverymi_id"]=$insert_id;
			$insr_array1["products_sub"]=base64_encode(json_encode($_POST));
			$insert_id1=$this->insert_query("deliverymi_sub",$insr_array1);
		}
		return "success";
	}
	function process_update($id){
		extract($_POST);
		$insr_array["process"]=$process;
		$insr_array["complete_qty"]=$qtys;
		$insr_array["complete_date"]=$date;
		$insr_array["po_number"]=$po_number;
		$insr_array["inward_id"]=$purchase;
		$insr_array["creation_time"]=time();
		$insr_array["created_by"]=$_SESSION["user_id"];
		$where="id=:process_id ";
	 	$where_arr= array('process_id'=>$id);
	
		$insert_id=$this->update_query("inward_process",$insr_array,$where,$where_arr);
		return "success";
	}
	
	
	function supplier_update(){ 
		extract($_POST);		
		$insr_array["supplier_name"]=$supplier_name;
		$insr_array["vendor_id"]=$vendor_id;
		$insr_array["address"]=$address;
		$insr_array["gst_no"]=$gst_no;
		$insr_array["pincode"]=$pincode;
		$insr_array["payment_terms"]=$payment_terms;
		$insr_array["creation_time"]=time();
		$insr_array["city"]=$city;
		$insr_array["state"]=$state;
		$insr_array["created_by"]=$_SESSION["user_id"];
		$where="id=:supplier_id";
	 	$where_arr= array('supplier_id'=>$_GET["supplier_id"]);
 
		$insert_id=$this->update_query("suppliers",$insr_array,$where,$where_arr);
		return "success";
	}
	
	function forwards($ids){ 
		extract($_POST);
		$insr_array["forward_approved"]=1;
		$where="id=:ids";
	 	$where_arr= array('ids'=>$ids);	
		$insert_id=$this->update_query("buffing",$insr_array,$where,$where_arr);	

		return "success";
	}
	function approve($ids){ 
		extract($_POST);
		
		
		$already=$this->buffing_edit($buffing_id);
		foreach($already as $va=>$key){
$products=convert_array($key["products"]);
//echo"<pre>";print_r($products);echo"</pre>";
}

		$products[0]["over_time"]=$aprv_over_time;
		$products[0]["overtime_amount"]=$aprv_over_time*$products[0]["overtime_price"];
		$insr_array["products"]=base64_encode(json_encode($products));
		$insr_array["admin_approved"]=1;
		
		$where="id=:ids";
	 	$where_arr= array('ids'=>$buffing_id);	
		$insert_id=$this->update_query("buffing",$insr_array,$where,$where_arr);	

		return "success";
	}
	function forwards_admin($ids){ 
		extract($_POST);
		$insr_array["admin_approved"]=1;
		$where="id=:ids";
	 	$where_arr= array('ids'=>$ids);	
		$insert_id=$this->update_query("buffing",$insr_array,$where,$where_arr);	

		return "success";
	}
	function receivedqty_approve(){ 
		extract($_POST);
		$insr_array["admin_approve"]=1;
		$insr_array["qty_approve"]=$qty_approve;
		$insr_array["reason"]=$reason_reject;
		$where="id=:ids";
	 	$where_arr= array('ids'=>$recei_id);	
		$insert_id=$this->update_query("received_products",$insr_array,$where,$where_arr);	

		return "success";
	}	
	function buffingappr_insert($id)
	{ 
		extract($_POST);
		$insr_array["qc_approved"]=1;
		foreach($product_id as $va=>$key){
		$finalarry[$va]["product_id"]=$key;
		$finalarry[$va]["approve"]=$approve[$va];
		$finalarry[$va]["reject"]=$reject[$va];
		}		
		$insr_array["products_approve"]=base64_encode(json_encode($finalarry));
		$insr_array["qc_comments"]=$comments;
		$insr_array["status"]=2;
		$insr_array["ins_by"]=$ins_by;	
		$insr_array["qtyapprove_total"]=$approve_total;
		$insr_array["qtyreject_total"]=$reject_total;		
		$where="id=:ids";
	 	$where_arr= array('ids'=>$id);	
		$insert_id=$this->update_query("buffing",$insr_array,$where,$where_arr);	
		//echo"<pre>final_array"; print_r($final_array); echo"</pre>";
		//echo"<pre>insr_array"; print_r($_insr_array); echo"</pre>";
		return "success";
	}	
	
	function payment_update($id)
	{ 
		extract($_POST);
		$insr_array["admin_approved"]=1;		
		$insr_array["admin_comments"]=$comments;
		$insr_array["admin_processed"]=$admin_processed;
		//$insr_array["products_approve"]=base64_encode(json_encode($insr_array));
		$where="id=:ids";
	 	$where_arr= array('ids'=>$id);	
		$insert_id=$this->update_query("buffing",$insr_array,$where,$where_arr);	

		return "success";
	}	
	
	function bufreject_approve($list){ 
		
		extract($_POST);
		$insr_array["buffing_id"]=$list["id"];
		$insr_array["product_id"]=$list["product_id"];
		$insr_array["return_by"]=$_SESSION["user_id"];
		$insr_array["qty"]=$list["reject"];		
		$insr_array["creation_time"]=time();
		$insert_id=$this->insert_query("return_qty",$insr_array);	
		return "success";
	}
	
		
	 /* function bufreject_approve($list){ 
	 
		foreach($product_id as $va=>$key){
		if($retun_qty[$va]!=0)	{
		// $final_arry[$va]["product_id"] =$total_qty[$key]["product_id"];
		 
		 $qtys=$this->openstock_edit1($list["product_id"]);
		
			 
		 $updatearr["retun_qty"]=$list["reject"];
	    $where2="subproduct_id=:product_id ";
		$where_arr2= array('product_id'=>$list["product_id"]);
	
		$insert_ids=$this->update_query("openstock",$updatearr,$where2,$where_arr2);	
		}
		}
	 } */
		
	function rd_product_insert(){
		extract($_POST);
		
		foreach($name as $va=>$key){
			
			$insr_array1["name"]=$key;
			$insr_array1["material"]=$material[$va];
			$insr_array1["price"]=$price[$va];
			$insr_array1["product_price"]=$product_price[$va];
			if(isset($process[$va])&&$process[$va]==1)
				$insr_array1["process"]=1;
			else
				$insr_array1["process"]=0;
			$insr_array1["creation_time"]=time();
			$insr_array1["created_by"]=$_SESSION["user_id"];
			$insr_array1["approved"]=0;		
			$insr_array1["rd_product"]=1;
			$insert=$this->insert_query("total_products",$insr_array1);
		
		}
		return "success";
		
	}
	function rd_product_update($id){
			extract($_POST);
		foreach($name as $va=>$key){
			$insr_array1["name"]=$name[$va];
			$insr_array1["material"]=$material[$va];
			$insr_array1["price"]=$price[$va];
			$insr_array1["product_price"]=$product_price[$va];			
			if(isset($process[$va]))
				$insr_array1["process"]=1;
			else
				$insr_array1["process"]=0;
			$insr_array1["update_time"]=time();
			$insr_array1["updated_by"]=$_SESSION["user_id"];

			if($_SESSION["user_type"]==1){
				$insr_array1["approved"]=1;
			}
			
			$where="id=:sub_product_id";
			$where_arr= array('sub_product_id'=>$id);
			$update=$this->update_query("total_products",$insr_array1,$where,$where_arr);
				
		}
		return "success";
	}
function finished_return($list){
		extract($_POST);
		$insr_array["finished_id"]=$list["id"];
		$insr_array["product_id"]=$list["product_id"];
		$insr_array["return_by"]=$_SESSION["user_id"];
		$insr_array["qty"]=$list["reject"];
		
		$product_list=$this->product_list();
		$subproduct_list=$this->subproduct_list();
		$openstock_list=$this->openstock_list();
		foreach($openstock_list as $va5=>$key5){
		$openstock[$key5["subproduct_id"]]["id"]=$key5["subproduct_id"];
		$openstock[$key5["subproduct_id"]]["processed_qty"]=$key5["processed_qty"];
		$openstock[$key5["subproduct_id"]]["finished_qty"]=$key5["finished_qty"];
		}

		foreach($product_list as $va=>$key){
		$total_arr[$key["id"]]["product_id"]=$key["id"];
		$total_arr[$key["id"]]["product_name"]=$key["product_name"];
		$total_arr[$key["id"]]["stocks"]=$key["open_qty"];
		$total_arr[$key["id"]]["series"]=$key["series"];
		foreach($subproduct_list as $va3=>$key3){
		if($list["product_id"]==$key["id"]&&$key3["product_id"]==$key["id"])	
		$total_arr[$list["product_id"]]["subproducts"][]=$key3;
		}
		}		
				
		foreach($total_arr as $va2=>$key2){
		foreach($key2["subproducts"] as $va3=>$key3){
		$qty[$key3["name"]]+=$list["reject"]*$key3["qty"];
		}			
		}
		
		foreach($openstock as $va6=>$key6){
		$insr_array7["finished_qty"]=$key6["finished_qty"]-$qty[$key6["id"]];
		$where="subproduct_id=:productid";
		$where_arr= array('productid'=>$key6["id"]);
		$update1=$this->update_query("openstock",$insr_array7,$where,$where_arr);	
		}
	
		$insr_array["creation_time"]=time();
		$insert_id=$this->insert_query("fg_return",$insr_array);	

		return "success";
	}	
	
	function pointent_insert(){
		extract($_POST);
		
		$insr_array["intent_no"]=$intent_no;
		$insr_array["intent_date"]=$intent_date;
		$insr_array["supplier"]=$supplier;
		$insr_array["requested_by"]=$requested_by[0];
		$insr_array["status"]=1;
		$insr_array["creation_time"]=time();
		$insr_array["created_by"]=$_SESSION["user_id"];
		$insr_array["puchase_intent"]=base64_encode(json_encode($_POST));		
		$insert_id=$this->insert_query("purchase_intent",$insr_array);
		
		foreach($product_id as $va=>$key){
		$insr_array1["subproduct_id"]=$key;	
		//$insr_array1["stock"]=$stock[$va];
		$insr_array1["fg_stock"]=$fg_stock[$va];
		$insr_array1["stock"]=$stock[$va];
		$insr_array1["qty"]=$qty[$va];
		$insr_array1["remarks"]=$remarks[$va];
		$insr_array1["intent_id"]=$insert_id;		
		$insr_array1["purchase_intentsub"]=base64_encode(json_encode($_POST));		
		$insert_id1=$this->insert_query("purchase_intentsub",$insr_array1);
		}
		
		return "success";
	}
	
	function pointentapprove()
	{	
		extract($_POST);
		
		$insr_array["intent_date"]=$intent_date;
		$insr_array["supplier"]=$supplier;
		$insr_array["approve_by"]=$approve_by[0];
		$insr_array["status"]=2;
		$where="id=:ids";
	 	$where_arr= array('ids'=>$intent_id);	
		$insert_id=$this->update_query("purchase_intent",$insr_array,$where,$where_arr);			
		
		foreach($approved_qty as $va=>$key)
		{	
		
			$insr_array1["qty_approved"]=$key;	
			$insr_array1["remarks_admin"]=$remarks[$va];
			$where1="id=:sub_intent_id AND  intent_id=:intent_id";
			$where_arr1= array('sub_intent_id'=>$key,'intent_id'=>$_GET["intent_id"]);
			//$where1="id=:ids1";
			//$where_arr1= array('ids1'=>$intent_id);	
			$insert_id1=$this->update_query("purchase_intentsub",$insr_array1,$where1,$where_arr1);
		}	
			return "success";
	}
	
	function reject_approve($list){ 
		extract($_POST);
		$insr_array["inward_approve"]=$list["id"];
		$insr_array["debit_no"]=$list["debit_no"];
		$insr_array["product_id"]=$list["product_id"];
		$insr_array["removed_by"]=$_SESSION["user_id"];
		$insr_array["removed_qty"]=$list["reject"];		
		$insr_array["removed_time"]=time();
		//$where="id=:ids";
	 	//$where_arr= array('ids'=>$list["id"]);	
		$insert_id=$this->insert_query("rejection",$insr_array);	

		return "success";
	}	

	function dashboard_list($fillter){
	 if($_GET["fillter"]=='today'){
	$date=strtotime('today');
	$where="`buffing_date`= '".$date."' ";
	 }else if($_GET["fillter"]=='week'){ 
		 
	$date=strtotime('-1 week');
	$where="  `buffing_date` >= '".$date."' ";
	 }else if($_GET["fillter"]=='month'){
		$date=strtotime('-1 month');
	$where=" `buffing_date` >= '".$date."'";
	 }else
		 $where="`id`!=''";
			return $this->select_query("buffing","id,name,supervisor_name,buffing_date,products,created_by,comments,creation_time,approved,payment,forward_approved,admin_approved",$where);
	}
	function report_filter(){
		extract($_POST);
		$where=" `buffing_date` >= '".strtotime($from_date)."' AND `buffing_date` <= '".strtotime($to_date)."' AND report=0 ";
		return $this->select_query("buffing","*",$where,array()," order by emp_type ASC");
	}
	
	function inward_filter(){
	extract($_POST);
		$where=" `dated` >= '".strtotime($from_date)."' AND `dated` <= '".strtotime($to_date)."' ";
	return $this->select_query("inward_approve","*",$where,array()," order by dated ASC");
	}
	function invoice_list(){ 
			return $this->select_query("invoice","*","id!=''");
	}
	function invoice_edit($id){ 
			return $this->select_query("invoice","*","id=".$id."");
	}
	function product_list(){ 
			return $this->select_query("products","*","id!=''");
	}
	function total_product_list($id=""){
		$where="id!='' and approved=1 and delete_status=0";
		if(isset($id)&&$id!=''){
			$where.=" and material=5 and delete_status=0";
		}		
		return $this->select_query("total_products","*",$where);			
	}
	function material_products($id){
	
	if($id==2)
		$where="material=5";
	else if($id==3)
		$where="material=6";
	else 
		$where="material!=5 and material!=6";
	
			return $this->select_query("total_products","*",$where);
	}
	
	function total_product_maching(){
			return $this->select_query("total_products","*","id!='' and machining=1",array(),"");			
	}
	
	function finished_list(){
			return $this->select_query("finished","*","id!=''");
	}
	function fgreport_list(){
			return $this->select_query("fg_report","*","id!=''");
	}
	function fgreport_edit($id){
			return $this->select_query("fg_report","*","id=".$id."");
	}
	function finished_view($id){
			return $this->select_query("finished","*","id=".$id."");
	}
	function pointent_list(){ 
			return $this->select_query("purchase_intent","*","id!=''");
	}
	
	function pointent_edit($id)
	{ 
			return $this->select_query("purchase_intent","*","id=".$id."");
	}
	
	function pointentsub_list(){ 
			return $this->select_query("purchase_intentsub","*","intent_id=".$_GET["id"]."");
	}
	function finishedappr_list($ids){
			return $this->select_query("finished","*","id=".$ids." and status=1");
	}
	function finishedapprove_list($ids){
			return $this->select_query("finished","*","id=".$ids." and status=2");
	}
	function finishedapproves_list($ids){
			return $this->select_query("finished","*","id=".$ids." and status=3");
	}
	function total_product_edit($id){
			return $this->select_query("total_products","*","id=".$id."");
	}
	function pages_list($id){
			return $this->select_query("permission","*","user_type=".$id."");
	}
	function product_exist($name){
		return $this->select_query("total_products","*","name='".$name."'");
	}
	function fg_product_exist($name){
		return $this->select_query("products","*","product_name='".$name."'");
	}
	function po_exist($po_number){
		return $this->select_query("orders","*","po_number='".$po_number."'");
	}
	
	 
	
	function openstock_list(){
			return $this->select_query("openstock","*","id!=''");
	}
	function openstock_edit($id){
			return $this->select_query("openstock","*","id=".$id."");
	}
	function openstock_edit1($id){
			return $this->select_query("openstock","*","subproduct_id=".$id."");
	}
	function subproduct_list($mate){ 
	if($mate=='')		
			return $this->select_query("sub_products","*","id!=''");
	else
			return $this->select_query("products a,sub_products b","*","a.id=b.product_id AND a.material=".$mate."");
	}
	function totalproduct_list($mate=''){
	if($mate==4||$mate==2||$mate==3)
		$where="material=1"	;
	else if($mate=='')
		$where="id!='' and material=1";
	else 
		$where="material=".$mate."";
	
		return $this->select_query("total_products","*",$where);
	

	}
	function totalproduct_list2($mate=''){
	
	if($mate=='')
		$where="id!='' and material=1";
	else 
		$where="material=".$mate."";
	
		return $this->select_query("total_products","*",$where);
	

	}
	function totalproducts(){
		return $this->select_query("total_products","*","id!=''");	
	}
	function totalpr_pinvolve(){ 
		return $this->select_query("total_products","*","id!='' and process=1");
	}
	function totalpr_minvolve(){ 
		return $this->select_query("total_products","*","id!='' and machining=1");
	}
	function category_pinvolve(){ 
		return $this->select_query("total_products","*","id!='' and process=1");
	}
	function inward_list(){
		return $this->select_query("inward","*","id!=''");
	}
	function inward_edit($inward){
		return $this->select_query("inward","*","id=".$inward."");
	}
	function inwardapprove_edit($inward){
		return $this->select_query("inward_approve","*","id=".$inward."");
	}
	function inwardappr_edit_inward($inward){
		return $this->select_query("inward_approve","*","inward_id=".$inward."");
	}
	function inward_approve_list(){
		return $this->select_query("inward_approve","*","id!=''");
	}
	function inward_approve_admin(){
		return $this->select_query("inward_approve","*","id!='' and admin_approve=1");
	}
	function maching_approve_admin(){
			return $this->select_query("deliverymi","*","id!='' and admin_approve=1");
	}
	function material_category(){ 
			return $this->select_query("material_category","*","id!=''");
	}
	function material_edit($id){ 
			return $this->select_query("material_category","*","id=".$id."");
	}
	function order_list(){ 
			return $this->select_query("orders","id,po_number,po_date,creation_time,supplier_id,pdfinfo","id!=''");
	}
	function return_list(){ 
			return $this->select_query("return_qty","*","id!=''");
	}
	function returnlist_edit($id){ 
			return $this->select_query("return_qty","*","id=".$id."");
	}
	function fgreturn_list(){ 
			return $this->select_query("fg_return","*","id!=''");
	}
	
	function order_edit($order_id)
	{
			return $this->select_query("orders a,order_sub b","DISTINCT a.id,a.pdfinfo,a.material_type,a.po_number,a.po_date,a.creation_time,a.supplier_id,(select suppliers.state from suppliers suppliers where suppliers.id=a.supplier_id) as state, a.gst_no, a.shipto_id, a.payment_terms,a.required_date,a.sgst_percent, a.cgst_percent,a.igst_percent,a.sgst_total, a.cgst_total, a.igst_total,a.sub_totall,a.orders,a.amount_words,b.id as sub_order_id,b.product_name as product_id, b.hsn,b.dimension,b.qty,b.unit,b.price,b.amount,b.order_id","a.id=b.order_id AND b.order_id=".$order_id."");
	}
	function get_po_number($order_id){
			return $this->select_query("orders","po_number","id=".$order_id."");
	}
	
	function order_qty(){
			return $this->select_query("order_sub","id,product_name as product_id,qty,price,ordersub_pdfinfo,order_id","id!=''");
	}
	
	function order_dates($order_id){ 
			return $this->select_query("orders","id,creation_time","id='".$order_id."'");
	}
	
	function employee_list(){ 
			return $this->select_query("employee","id,name,price,(select role_name from roles where employee.role=roles.id) as role,creation_time","id!=''");
	}
	
	function buffing_list(){ 
			return $this->select_query("buffing","*","id!=''",array()," order by id ASC");
	}
	
	function price_history($product_id){
		return $this->select_query("price_history","product_id,nonbuffed_qty,maching_qty,buffed_qty,nonbuffed_price,creation_date,buffed_price,maching_price,nonbuffed_process_qty,buffed_process_qty,maching_process_qty","product_id='".$product_id."'");
	}
	function price_historylist(){
		return $this->select_query("price_history","product_id,nonbuffed_qty,maching_qty,buffed_qty,nonbuffed_price,creation_date,buffed_price,maching_price","id!=''");
	}
	
	function report_history(){
		return $this->select_query("report","*","id!=''");
	}
	function steel_history_list($id=''){
		if($id!='')
			$where=" id=".$id." ";
		else
			$where=" id!='' ";
		return $this->select_query("scrap_history","*",$where);
	}
	
	/*function rd_product_list($user_type){
		$user_type=$_SESSION['user_type'];
		//print_r($user_type);                           
		if($user_type==1)
			return $this->select_query("total_products","*","id!=''and rd_product=1 and approved=1");
		else if($user_type==4)
			return $this->select_query("total_products","*","id!=''and rd_product=1 or approved=0");
		else	
			return $this->select_query("total_products","*","id!=''");
		}*/
		function rd_product_list(){
			return $this->select_query("total_products","*","id!='' and rd_product=1 ");
	}
	function report_edit($id){
			return $this->select_query("report","*","id=".$id."");
	}
	function buffing_list1(){ 
			return $this->select_query("buffing","*","id!='' and report=0",array()," order by buffing_date ASC");
	}
	
	function buffing_edit($id){ 
			return $this->select_query("buffing","*","id=".$id."");
	}
	
	function buffing_view($id){ 
			return $this->select_query("buffing","*","id=".$id."");
	}
	function inward_view($id){ 
			return $this->select_query("inward","*","id=".$id."");
	}
	function inwardappr_view($id){ 
			return $this->select_query("inward_approve","*","inward_id=".$id."");
	}
	function rejected($id,$product_id){ 
			return $this->select_query("rejection","*","inward_approve=".$id." and product_id=".$product_id."");
	}
	function received_list(){ 
			return $this->select_query("received_products","id,qty,price,received_date,(select supplier_name from suppliers where received_products.supplier_id=suppliers.id) as supplier_name,(select product_name from products where received_products.product_id=products.id) as product_name,creation_time,product_id,admin_approve,qty_approve","id!=''");
	}
	function employee_edit($id){ 
		return $this->select_query("employee","id,name,role,price","id=".$id."");
	}
	function received_edit($id){ 
		return $this->select_query("received_products","id,qty,price,received_date,product_id,supplier_id,admin_approve,qty_approve","id=".$id."");
	}
	function received_edit1($id){ 
		return $this->select_query("received_products","id,qty,price,received_date,product_id,supplier_id,admin_approve,qty_approve","product_id=".$id."");
	}
	function roles(){ 
			return $this->select_query("roles","id,role_name","id!=''");
	}
	function roles_edit($role_id){ 
			return $this->select_query("roles","id,role_name","id=".$role_id."");
	}	
	function emp_delete($emp_edit){
		extract($_POST);
		$insert_array["reason"]=$reason_delete;
		$insert_array["name"]=$emp_edit[0]["name"];
		$insert_array["role"]=$emp_edit[0]["role"];
		$insert_array["delete_time"]=time();
		$insert_array["delete_by"]=$_SESSION["user_id"];
		$insert_array["delete_date"]=($delete_date);
		$insert_id=$this->insert_query("employee_deletelog",$insert_array);
		$where1="id=:emp_id";
		$where_arr1= array('emp_id'=>$emp_edit[0]["id"]);	
		$delete=$this->delete_query("employee",$where1,$where_arr1);
	}		
	function supplier_list()
	{
			return $this->select_query("suppliers","id,supplier_name,vendor_id,address,city,state,gst_no,pincode,payment_terms,creation_time","id!=''");
	}

	function master_request(){ 
	
		$where="a.id=b.product_id";
		return $this->select_query("products a,sub_products b","a.id,a.product_name,a.series,a.open_qty,a.price as product_price,a.creation_time,b.name,b.qty,b.price,b.process,b.id as sub_product_id",$where);
	
	}
	function product_edit($product_id){
	    $where="a.id=b.product_id AND a.id=".$product_id;
		return $this->select_query("products a,sub_products b","a.id,b.approved,a.product_name,a.series,a.open_qty,a.price as product_price,a.creation_time,b.name,b.qty,b.id as sub_product_id",$where);
	}
	function sub_pro_name($product_id){
		$where="id=".$product_id."";
		return $this->select_query("sub_products","*",$where);	
	}

	function subproduct_edit($product_id){	
    	$where="a.id=".$product_id." AND a.id=b.product_id";
		return $this->select_query("products a,sub_products b","*",$where);	
	}
		function find_product($series){
			return $this->select_query("products","*","id!='' and series LIKE ('".$series."')");
	}
	function get_productid($product_id){	
    	$where="product_name LIKE ('".$product_id."')";
		return $this->select_query("products","*",$where);	
	}
	function product_delete($product_id){
		$insr_array["delete_status"]=1;
		$where="id=:product_id";
	 	$where_arr= array('product_id'=>$product_id);
		$insert_id=$this->update_query("total_products",$insr_array, $where,$where_arr);
		return 'deleted';
	}
	function supplier_delete($supplier_id){
		$where="id=:supplier_id";
		
		$where_arr=array("supplier_id"=>$supplier_id);

		$this->delete_query("suppliers",$where,$where_arr);

		return 'deleted';
	}
	
	function supplier_edit($supplier_id){ 
		$where1="id=".$supplier_id."";
		return $this->select_query("suppliers","id,supplier_name,gst_no,address,city,state, pincode,payment_terms,creation_time,vendor_id",$where1);
	}
	function nature_edit($id)
	{
		$where="id=".$id."";
		return $this->select_query("nature","id,nature",$where);
	
	}
	function nature_list(){
			return $this->select_query("nature","*");
	}
	function deliverymo_list()
	{
			return $this->select_query("deliverymo","*","id!=''");
	}
	function deliverymi_list(){
			return $this->select_query("deliverymi","*");
	}
	function deliverymisub_list(){
			return $this->select_query("deliverymi_sub","*");
	}
	function deliverymitot_list(){
			return $this->select_query("deliverymi a,deliverymi_sub b","*","a.id=b.deliverymi_id");
	}
	function deliverymotot_list(){
			return $this->select_query("deliverymo a,deliverymo_sub b","*","a.id=b.deliverymo_id");
	}
	/*function deliverymo_edit($id){
			return $this->select_query("deliverymo a,deliverymo_sub b","*","a.id=".$id."");
	}*/
	
	function deliverymo_edit($process_id){
			return $this->select_query("deliverymo a,deliverymo_sub b","*","a.id=".$process_id." AND a.id=b.deliverymo_id");
	}
	
	function deliverymi_edit($process_id){
		return $this->select_query("deliverymi a,deliverymi_sub b","*","a.id=".$process_id." AND a.id=b.deliverymi_id");
	}
	
	/* function deliverymi_edit(){
			return $this->select_query("deliverymi a,deliverymi_sub b","*","a.id=b.deliverymi_id");
	} */
	/* function deliverymi_edit($process_id)
	{
		$where="id=".$process_id."";
		return $this->select_query("deliverymi","*",$where);
	
	} */

	function shipto_list(){
		return $this->select_query("settings","id,ship_name,ship_address,ship_city,ship_state");
	}
	function shipto($id){
		$where="id=".$id."";
		return $this->select_query("settings","id,ship_name,ship_address,ship_city,ship_state",$where);
	}
	
	function buffing_update($buffing_id){
		extract($_POST);
		
		$insr_array["name"]=$name;
		$insr_array["supervisor_name"]=$supervisor_name;
		$insr_array["buffing_date"]=strtotime($grn_date);
		$insr_array["bpl_no"]=$bpl_no;
		$insr_array["creation_time"]=time();
		$insr_array["emp_type"]=$emp_type;
		$insr_array["buffed_total"]=$buffed_total;
		$insr_array["payable_total"]=$payable_total;		
		$inward_appr_list=$this->inward_approve_admin();
		$subproduct_list=$this->totalpr_pinvolve();
		
		foreach($subproduct_list as $va=>$key){
			$product_ids[]=$key["id"];
			$po_qty=$this->openstock_edit1($key["id"]);
			$open_stock[$key["id"]]=$po_qty[0]["processed_qty"];
			$subproduct_list[$va]["qty"]=$po_qty[0]["processed_qty"];
			$product_name[$key["id"]]=$key["name"];
			$price[$key["id"]]=$key["price"];
		}

		foreach($inward_appr_list as $va=>$key){
			$products=convert_array($key["products"]);		
			$qtys=convert_array($key["qty_list"]);	
			foreach($products as $va1=>$key1){
				if(in_array($key1["product_id"],$product_ids)){
					$total_qty[$key1["product_id"]]["qty_approve"]+=$key1["qty_approve"];
					$total_qty[$key1["product_id"]]["product_id"]=$key1["product_id"];
				}
			}
		}
		
		foreach($total_qty as $va1=>$key1){
		$total_qty[$va1]["stocks"]=$open_stock[$key1["product_id"]];
		$total_qty[$va1]["product_name"]=$product_name[$key1["product_id"]];
		$total_qty[$va1]["price"]=$price[$key1["product_id"]];
		$total_qty[$va1]["buffed_total"]=$buffed_total[$key1["buffed_total"]];
		$total_qty[$va1]["payable_total"]=$payable_total[$key1["payable_total"]];
		}

		foreach($subproduct_list as $va=>$key){
		if(!isset($total_qty[$key["id"]]["product_id"])){
		if($key["qty"]!=0){
		$total_qty[$key["id"]]["product_id"]=$key["id"];
		$total_qty[$key["id"]]["stocks"]=$key["qty"];
		$total_qty[$key["id"]]["product_name"]=$key["name"];
		$total_qty[$key["id"]]["price"]=$key["price"];
		
		}	
		}	
		}
		if($emp_type!=1){
		if(!empty($aprv_over_time))
			$insr_array["aprve_time_before"] =$aprv_over_time;
		if(!empty($approve))
			$insr_array["approved"] =$approve[0];
		$insr_array["comments"]=$comments_regular;	
		}else{
		$insr_array["comments"]=$comments;
		
		}
		
		 $final_arry=array();
		 //echo"<pre>";print_r($_POST);echo"</pre>";
		 	
		if($emp_type==1){	
		
			foreach($product_id as $va=>$key){
				if($qty[$va]!=0)
				{
					
					$final_arry[$va]["product_id"]=$total_qty[$key]["product_id"];
					$qtys=$this->openstock_edit1($total_qty[$key]["product_id"]);
					
					if($qtys[0]["processed_qty"]>=$qty[$va])	
						$updatearr["processed_qty"]=$qtys[0]["processed_qty"]-$qty[$va];	
					
					else if($qtys[0]["processed_qty"]<=$qty[$va])	
						$updatearr["processed_qty"]=$qtys[0]["processed_qty"]+$qty[$va];
					
					else 
						$updatearr["processed_qty"]=0;	
														 
					$where2="subproduct_id=:product_id ";
					$where_arr2= array('product_id'=>$total_qty[$key]["product_id"]);
					$insert_ids=$this->update_query("openstock",$updatearr,$where2,$where_arr2);
					$final_arry[$va]["stocks"]=$total_qty[$key]["stocks"];
					$final_arry[$va]["product_name"]=$total_qty[$key]["product_name"];
					$final_arry[$va]["price"]=$total_qty[$key]["price"];
					$final_arry[$va]["buffed_qty"] =$qty[$va];
					$final_arry[$va]["amount"]=$qty[$va]*$total_qty[$key]["price"];
					$final_arry[$va]["buffed_total"]=$total_qty[$key]["buffed_total"];
					$final_arry[$va]["payable_total"]=$total_qty[$key]["payable_total"];
				}
			}
		}
		
		if($emp_type!=1){	
		
		foreach($regular_price as $va=>$key)
		{		
		 $final_arry[$va]["regular_time"] =$regular_time;
		 $final_arry[$va]["regular_price"] =$regular_price;
		 $final_arry[$va]["regular_amount"] =$regular_time *$regular_price[$va];
		 if(!empty($approve))
		 {
		  $final_arry[$va]["over_time"] =$over_time;
		 $final_arry[$va]["overtime_price"] =$overtime_price;
		 $final_arry[$va]["overtime_amount"] =$over_time *$overtime_price[$va];	
		}
		}	
		}
		
		$insr_array["products"]=base64_encode(json_encode($final_arry));		
		$insr_array["created_by"]=$_SESSION["user_id"];
		$insr_array["status"]=1;
		$where="id=:buffing_id";
	 	$where_arr= array('buffing_id'=>$_GET["buffing_id"]);
		$insert_id=$this->update_query("buffing",$insr_array, $where,$where_arr);
		return "success";
	}

function inward_update($inward_id)
	{
		
		extract($_POST);
		
		$insr_array1["dated"]=strtotime($date);
		$insr_array1["inv_no"]=$inv_no;
		$insr_array1["supplier"]=$supplier;
		$insr_array1["inv_date"]=strtotime($inv_date);
		$final_arry=array();
		foreach($product_id as $va=>$key){
			$final_arry[$va]["product_id"]=$key;
			$final_arry[$va]["specification"]=$specification[$va];	
			$final_arry[$va]["order_qty"]=$order_qty[$va];	
			$final_arry[$va]["qty"]=$qty[$va];	
		}
		 
		$final_arry1=array();
		$final_arry1["total_order"]=$total_order;
		$final_arry1["total_received"]=$total_received;	
		$final_arry1["total_balance"]=$total_balance;	

		
		$insr_array1["products"]=base64_encode(json_encode($final_arry));
		$insr_array1["qty_list"]=base64_encode(json_encode($final_arry1));
		$insr_array1["comments"]=$comments;
		$insr_array1["received_by"]=$received_by;
		$insr_array1["created_by"]=$_SESSION["user_id"];
		$insr_array1["creation_time"]=time();
		$insr_array1["inward_no"]=$inward_no;
		
		$insr_array1["status"]=1;
		$where="id=:inward_id";
	 	$where_arr= array('inward_id'=>$_GET["inward_id"]);
		$insert_id=$this->update_query("inward",$insr_array1, $where,$where_arr);
		return "success";
	}	
	function buffingappr_update($buffing_id)
	{ 			
		extract($_POST);
		$insr_array["qc_approved"]=1;
		foreach($product_id as $va=>$key)
		{
		$finalarry[$va]["product_id"]=$key;
		$finalarry[$va]["approve"]=$approve[$va];
		$finalarry[$va]["reject"]=$reject[$va];
		
		}
		$insr_array["products_approve"]=base64_encode(json_encode($finalarry));
		$insr_array["qtyapprove_total"]=$approve_total;
		$insr_array["qtyreject_total"]=$reject_total;	
		$insr_array["qc_comments"]=$comments;
		$insr_array["ins_by"]=$ins_by;		
		$where="id=:ids";
	 	$where_arr= array('ids'=>$buffing_id);	
		$insert_id=$this->update_query("buffing",$insr_array,$where,$where_arr);	
		return "success";
	}
		
	function adminappr_update($buffing_id){
		extract($_POST);
		$insr_array["admin_approved"]=1; 
		$price_list=$this->find_buffing_price($_GET["id"]);		
		$get_price=convert_array($price_list[0]["products"]);
		foreach($get_price as $va=>$key){
			$product[$key["product_id"]]["price"]=$key["price"];
			$product[$key["product_id"]]["buffing_id"]=$_GET["buffing_id"];
		}
		  
		foreach($product_id as $va=>$key){
			$finalarry[$va]["product_id"]=$key;
			$finalarry[$va]["approve"]=$approve[$va];
			$finalarry[$va]["reject"]=$reject[$va];	
			$product_stock=$this->get_product_stock($key);
		
			$qty_remove=$approve[$va];
			foreach($product_stock as $va1=>$key1){
				$product_actual_qty[$key1["id"]]=$key1["maching_process_qty"];	
				if($key1["maching_process_qty"]>=$approve[$va]&&$qty_remove>0){
					$product_stock[$va1]["maching_process_qty"]=$key1["maching_process_qty"]-$approve[$va];
					$qty_remove=$qty_remove-$approve[$va];
				}else if($qty_remove>0){
					if($qty_remove>=$key1["maching_process_qty"]){
						$product_stock[$va1]["maching_process_qty"]=0;
						$qty_remove=$qty_remove-$key1["maching_process_qty"];
					}else{
						$product_stock[$va1]["maching_process_qty"]=$key1["maching_process_qty"]-$qty_remove;
						$qty_remove=0;
					}
				}
			}
			$this->update_product_stock1($product_stock,$product_actual_qty);		
			$qtys=$this->openstock_edit1($key);		
			$insr_array7["processed_buff"]=$qtys[0]["processed_maching"]+$approve[$va];
			$where="subproduct_id=:productid";
			$where_arr= array('productid'=>$key);
			$update1=$this->update_query("openstock",$insr_array7,$where,$where_arr);
		
			$insr_array1["product_id"]=$key;
			$nonbuff_price=$this->get_updated_price($key);
			$insr_array1["nonbuffed_price"]=$nonbuff_price[0]["nonbuffed_price"];
			$insr_array1["buffed_price"]=$product[$key]["price"];
			$insr_array1["maching_price"]=$nonbuff_price[0]["maching_price"];
			$insr_array1["buffed_price"]=$product[$key]["price"];
			$insr_array1["buffed_qty"]=$approve[$va];
			$insr_array1["buffed_process_qty"]=$approve[$va];
			$insr_array1["buffing_id"]=$_GET["id"];
			$insr_array1["creation_date"]=time();
			$insr_array1["status"]=0;		
			$insert_ids=$this->insert_query("price_history",$insr_array1);		 
		}
			$insr_array["products_approve"]=base64_encode(json_encode($finalarry));
			$insr_array["qtyapprove_total"]=$approve_total;
			$insr_array["qtyreject_total"]=$reject_total;
			$insr_array["admin_comments"]=$comments;
			$insr_array["admin_processed"]=$admin_processed;		
			$where="id=:ids";
			$where_arr= array('ids'=>$buffing_id);
			$insert_id=$this->update_query("buffing",$insr_array,$where,$where_arr);	
		return "success";
	}
	
		function pointent_update($id){
		extract($_POST);
		
		$insr_array["intent_no"]=$intent_no;
		$insr_array["intent_date"]=strtotime($intent_date);
		$insr_array["category"]=implode(",",$category);
		$insr_array["requested_by"]=$requested_by[0];
		$insr_array["creation_time"]=time();
		$insr_array["approve_by"]=$approve_by[0];
		$where="id=:po_id ";
	 	$where_arr= array('po_id'=>$_GET["po_id"]);
		$insert_id=$this->update_query("purchase_intent",$insr_array,$where,$where_arr);	
				
		foreach($product_id as $va=>$key){			
		$insr_array1[$va]["subproduct_id"]=$key;	
		$insr_array1[$va]["qty"]=$qty[$va];
		$insr_array1[$va]["remarks"]=$remarks[$va];
		$insr_array1[$va]["po_id"]=$insert_id;		
		$where1="id=:subproduct_id AND  po_id=:po_id";
		$where_arr1= array('subproduct_id'=>$key,'po_id'=>$_GET["po_id"]);
		$update=$this->update_query("purchase_intentsub",$insr_array1,$where1,$where_arr1);
		}
		
		return "success";
	}
	
	function finished_edit($id){
		return $this->select_query("finished","*","id=".$id."");
	}

	function finished_update($id){
	extract($_POST);		
	//$insr_array["mov_no"]=$mov_no;
	$insr_array["mov_date"]=strtotime($mov_date);
	$insr_array["pass_by"]=$passed;
	$insr_array["creation_time"]=time();
	//$insr_array["created_by"]=$_SESSION["user_id"];
	//$insr_array["status"]=1;
	$product_list=$this->product_list();
	$subproduct_list=$this->subproduct_list();
	$openstock_list=$this->openstock_list();
	
	foreach($product_list as $va=>$key){
		foreach($subproduct_list as $va3=>$key3){
			if($key3["product_id"]==$key["id"])	
				$total_arr[$key["id"]]["subproducts"][]=$key3;
			}
	}
	
	foreach($product_id as $va=>$key){	
		$produqty[$key]=$produced_qty[$va];		
	}
			
	foreach($total_arr as $va=>$key){
		foreach($key["subproducts"] as $va1=>$key1){
		$qty[$key1["name"]]+=$produqty[$va]*$key1["qty"];
		}		
	}
	
	foreach($openstock_list as $va=>$key){
		if($produced_qtyhidden<$produced_qty)
			$insr_array7["finished_qty"]=$key["finished_qty"]+$qty[$key["subproduct_id"]];
		else if($produced_qtyhidden>$produced_qty)
			$insr_array7["finished_qty"]=$key["finished_qty"]-$qty[$key["subproduct_id"]];
	
		$where="subproduct_id=:product_id";
		$where_arr= array('product_id'=>$key["subproduct_id"]);
		$update1=$this->update_query("openstock",$insr_array7,$where,$where_arr);
	}
	
	foreach($product_id as $va=>$key)
	{
	if($approve[$va]==1)
	{
		$fin_arry[$va]["product_id"]=$key;
		$fin_arry[$va]["series"]=$series[$va];
		$fin_arry[$va]["stock_hand"]=$stock_hand[$va];
		$fin_arry[$va]["produced_qty"]=$produced_qty[$va];				
		$fin_arry[$va]["remarks"]=$remarks[$va];
	}	
	}
	$insr_array["products"]=convert_json($fin_arry);
	$where="id=:process_id";
	$where_arr= array('process_id'=>$_GET["process_id"]);
	$insert_id=$this->update_query("finished",$insr_array,$where,$where_arr);		
	return "success";
}	
	function finishedappr_edit($ids){
			return $this->select_query("finished","*","id=".$ids."");
	}
	function finishedapprove_edit($ids){
			return $this->select_query("finished","*","id=".$ids." and status=3");
	}
	
	function finishedappr_update($id)
	{
		extract($_POST);
		
		$finised=$this->finishedappr_list($_GET["process_id"]);
		foreach($finised as $va=>$key){
		$product=convert_array($key["products"]);	
		foreach($product as $va1=>$key1){
		$fin_array[$key1["product_id"]]["approve_qty"]=$approve_qty[$key1["product_id"]];
		$fin_array[$key1["product_id"]]["remarks"]=$remarks[$key1["product_id"]];			
		}
		}		
		//$insr_array["products_approve"]=convert_json($fin_array);
		$insr_array["status"]=2;
		$insr_array["ins_date"]=time();
		$insr_array["ins_by"]=$ins_by;		
		$insr_array["products_approve"]=base64_encode(json_encode($_POST));
		$where="id=:process_id";
		$where_arr= array('process_id'=>$_GET["process_id"]);
		$update1=$this->update_query("finished",$insr_array,$where,$where_arr);
		return "success";
	}	

	/*	function finishedapprove_update($id)
		{		
		extract($_POST);		
		$finised=$this->finishedappr_list($_GET["id"]);
		$product_list=$this->product_list();
		$subproduct_list=$this->subproduct_list();
		$insr_array["status"]=3;
		$insr_array["approve_time"]=time();
		$insr_array["approve_by"]=$apr_by;	
		$insr_array["approve_remarks"]=convert_json($remarks);			
		$where="id=:productid";
		$where_arr= array('productid'=>$_GET["id"]);
		$update1=$this->update_query("finished",$insr_array,$where,$where_arr);
		return "success";
		
				
		}

	function finishedapprove_update1($id)
	{
	$finised=$this->finishedappr_edit($_GET["process_id"]);
	foreach($finished_list as $va1=>$key1){
	$products_arr=convert_array($key1["products"]);
	$productsappr_arr=convert_array($key1["products_approve"]);
	//echo"<pre>";print_r($productsappr_arr);echo"</pre>"; 
	foreach($productsappr_arr as $va1=>$key1){
	$totalqty+=$key1["approve_qty"];	
	}
	}
	$insr_array["status"]=3;
	$insr_array["approve_time"]=time();
	$insr_array["approve_by"]=$apr_by;	
	$insr_array["approve_remarks"]=base64_encode(json_encode($_POST));			
	$where="id=:process_id";
	$where_arr= array('process_tid'=>$_GET["process_id"]);
	$update1=$this->update_query("finished",$insr_array,$where,$where_arr);
	return "success";
	} */
	
	function finishedapprove_update($id){
	 extract($_POST);
		
		$finised=$this->finishedapproves_list($_GET["process_id"]);
		$product_list=$this->product_list();
	    $subproduct_list=$this->subproduct_list();	
		//echo"<pre>";print_r($approve_qty);echo"</pre>";		
		foreach($finised as $va=>$key){
		$product=convert_array($key["products_approve"]);	
		//echo"<pre>";print_r($product);echo"</pre>";
		foreach($product as $va1=>$key1){
			$fin_array[$key1["product_id"]]["approve_qty"]=$approve_qty[$key1["product_id"]];
			$fin_array[$key1["product_id"]]["rejected_qty"]=$reject_qty[$key1["product_id"]];
			$fin_array[$key1["product_id"]]["remarks"]=$remarks[$key1["product_id"]];	
			$fin_array[$key1["product_id"]]["product_id"]=$key1["product_id"];			
		}
		}
		
		$insr_array["status"]=3;
		$insr_array["approve_time"]=time();
		$insr_array["approve_by"]=$apr_by;	
		$insr_array["products_approve"]=convert_json($fin_array);
		$insr_array["approve_remarks"]=convert_json($fin_array);
		$where="id=:process_id";
		$where_arr= array('process_id'=>$_GET["process_id"]);		 
		$update1=$this->update_query("finished",$insr_array,$where,$where_arr);
		return "success";
				
	}
	
	function inwardsuppliername($supplier_id)
	{
	if($supplier_id!='')
		$where="supplier_id='".$supplier_id."'";
	else
		$where="id!=''";
	
	if($_REQUEST["from"]=='steel')
		$where.=" and material_type=2";
				
		return $this->select_query("orders", "*",$where);
	}

function poitem_name($id){
	if($id!='')
		$where="id='".$id."'";
	else
		$where="id!=''";
				
		return $this->select_query("orders", "*",$where);
	}	
	function inwarditemname($po_list)
	{
	if($po_list!='')
		  $where="po_list='".$po_list."'";
	else
		$where="po_list!=''";
				
		return $this->select_query("inward", "*",$where);
	}

	function steelitemname($po_list)
	{ 
	if($po_list!='')
		  $where="po_number=".$po_list."";
	else
		$where="po_number!=''";
				
		return $this->select_query("steel", "*",$where);
	}	
  
	function purchaseintent_edit($id)
	{
			return $this->select_query("purchase_intent a,purchase_intentsub b","DISTINCT a.id,a.intent_no,a.intent_date,a.supplier,a.requested_by,a.status, a.creation_time, b.id as sub_intent_id,b.subproduct_id as subproduct_id, b.stock,b.qty,b.remarks,b.intent_id,b.qty_approved,b.remarks_admin","a.id=b.intent_id AND b.intent_id=".$id."");
	}
	function ordertot_list(){
			return $this->select_query("orders a,order_sub b","*","a.id=b.order_id");
	}

	
function pointentappr_insert($id)
	{ 
		extract($_POST);
		$insr_array["po_number"]=$purchase_no;
		$insr_array["approve_by"]=$approve_by[0];
		$insr_array["status"]=2;
		foreach($product_id as $va=>$key){
		$finalarry[$va]["product_id"]=$key;
		$finalarry[$va]["qty_approved"]=$approved_qty[$va];
		$finalarry[$va]["remarks_admin"]=$remarks[$va];
		}		
		$insr_array["products_approve"]=base64_encode(json_encode($finalarry));
		$where="id=:ids";
	 	$where_arr= array('ids'=>$id);	
		$insert_id=$this->update_query("purchase_intent",$insr_array,$where,$where_arr);	
		return "success";
	}	
function employee_deletelog(){
			return $this->select_query("employee_deletelog","*","id!=''");
	}
	
	function rdproduct_delete($product_id){
		$where="id=:product_id";		
		$where_arr=array("product_id"=>$product_id);
		$this->delete_query("total_products",$where,$where_arr);
		return 'deleted';
	}
	
	function user_type($id){
		return $this->select_query("user_types","*","id=".$id."");
	}
	
	function consum_insert()
	{
		extract($_POST);
		$insr_array["consum_no"]=$consum_no;
		$insr_array["consum_date"]=$consum_date;
		$insr_array["status"]=1;
		$insr_array["creation_time"]=time();
		$final_arry=array();
		foreach($product_id as $va=>$key){
			$final_arry[$va]["product_id"]=$key;
			$final_arry[$va]["stock"]=$stock[$va];	
			$final_arry[$va]["qty"]=$qty[$va];
			$final_arry[$va]["rate"]=$rate[$va];
			$final_arry[$va]["remarks"]=$remarks[$va];
			}
		
		 
		$insr_array["products_pdf"]=base64_encode(json_encode($final_arry));
		
		$insert=$this->insert_query("consumable",$insr_array);
		
		foreach($product_id as $va=>$key){
			$insr_array1["product_id"]=$key;
			$insr_array1["stock"]=$stock[$va];
			$insr_array1["qty"]=$qty[$va];
			$insr_array1["remarks"]=$remarks[$va];
			$insr_array1["consum_id"]=$insert;
			$insr_array1["consum_sub"]= base64_encode(json_encode($_POST));
			$insert_id=$this->insert_query("consum_sub",$insr_array1);
		} 
		return "success";
	}
	
	
	
	function consumappr_insert($id)
	{
		
		extract($_POST);	
		 $insr_array["admin_approve"]=1;
		foreach($_POST["product_id"] as $va=>$key){
		$finalarry["product_id"]=$key;
		$finalarry["stock"]=$stock;
		$finalarry["qty"]=$qty;
		$finalarry["rate"]=$rate;
		$finalarry["remarks"]=$remarks;
		}		
		$insr_array["products_approve"]=base64_encode(json_encode($finalarry));
		$insr_array["status"]=2;
		$where="id=:ids";
	 	$where_arr= array('ids'=>$id);	
		$insert_id=$this->update_query("consumable",$insr_array,$where,$where_arr);
		$inward_appr_list=$this->inward_approve_admin();

$subproduct_list=$this->total_product_list();

 
foreach($subproduct_list as $va=>$key){
	
	$product_ids[]=$key["id"];
	$po_qty=$this->openstock_edit1($key["id"]);

    $open_stock[$key["id"]]=$po_qty[0]["processed_qty"];
	$subproduct_list[$va]["qty"]=$po_qty[0]["processed_qty"];
 	
	$product_name[$key["id"]]=$key["name"];
	$price[$key["id"]]=$key["price"];
	$product_price[$key["id"]]=$key["product_price"];
}


foreach($inward_appr_list as $va=>$key){
	
	$products=convert_array($key["products"]);		
	$qtys=convert_array($key["qty_list"]);	
		
	foreach($products as $va1=>$key1){
		if(in_array($key1["product_id"],$product_ids)){
			$total_qty[$key1["product_id"]]["qty_approve"]+=$key1["qty_approve"];
			$total_qty[$key1["product_id"]]["product_id"]=$key1["product_id"];
			$total_qty[$key1["product_id"]]["product_price"]=$key1["product_price"];
		}
	}
}

foreach($total_qty as $va1=>$key1){
$total_qty[$va1]["stocks"]=$open_stock[$key1["product_id"]];
$total_qty[$va1]["product_name"]=$product_name[$key1["product_id"]];
$total_qty[$va1]["price"]=$price[$key1["product_id"]];
$total_qty[$va1]["product_price"]=$product_price[$key1["product_id"]];
}
foreach($subproduct_list as $va=>$key){
if(!isset($total_qty[$key["id"]]["product_id"])){
if($key["qty"]!=0){
$total_qty[$key["id"]]["product_id"]=$key["id"];
$total_qty[$key["id"]]["stocks"]=$key["qty"];
$total_qty[$key["id"]]["product_name"]=$key["name"];
$total_qty[$key["id"]]["price"]=$key["price"];
$total_qty[$key["id"]]["product_price"]=$key["product_price"];
}	
}	
}

foreach($total_qty as $va2=>$key2){
$subproduct=$this->total_product_edit($key2["product_id"]);	
if($subproduct[0]["material"]==3){
	$total_qty1[$va2]=$key2;
	$id_machin[]=$key2["product_id"];
}
}
		 foreach($product_id as $va=>$key){
		if($qty[$va]!=0)	{
			//echo"<pre>"; print_r($product_id); echo"</pre>"; exit;
		 $final_arry[$va]["product_id"] =$total_qty[$key]["product_id"];
		 
		 $qtys=$this->openstock_edit1($total_qty[$key]["product_id"]);
		
		//if($qtys[0]["processed_qty"]>=$qty[$va])			 
		 $updatearr["processed_qty"]=$qtys[0]["processed_qty"]-$qty[$va];
	    // else if($qtys[0]["processed_qty"]<$qty[$va])
		  //$updatearr["processed_qty"]=0;
			$where2="subproduct_id=:product_id ";
			$where_arr2= array('product_id'=>$total_qty[$key]["product_id"]);
	
		$insert_ids=$this->update_query("openstock",$updatearr,$where2,$where_arr2);
	  
	  
		 $final_arry[$va]["stocks"] =$total_qty[$key]["stocks"];
		 $final_arry[$va]["product_name"] =$total_qty[$key]["product_name"];
		 $final_arry[$va]["price"] =$total_qty[$key]["price"];
		 $final_arry[$va]["processed_qty"] =$qty[$va];
		 $final_arry[$va]["amount"] =$qty[$va]*$total_qty[$key]["price"];
		
		}
		}
		
		
		return "success";
	}
	
	function consum_list(){ 
			return $this->select_query("consumable","*","id!=''",array()," order by consum_date ASC");
	}
	function consumsub_list(){
			return $this->select_query("consumable a,consum_sub b","*","a.id=b.consum_id");
	}
	 
	function find_price($POST){
			return $this->select_query("`inward` a,`orders` b","a.po_list,b.pdfinfo","a.id='".$POST["inward_id"]."' and a.po_list=b.id");
	}
	  
	
	function consumbale_view($id){ 
			return $this->select_query("consumable","*","id=".$id."");
	}
/* 	function consumable_edit($id){
			return $this->select_query("consumable a,consum_sub b","*","a.id=".$id." AND a.id=b.consum_id");
	} */
	function consumable_edit($id)
	{ 
			return $this->select_query("consumable","*","id=".$id."");
	}



function joutsupplier($supplier_id)
	{
	if($supplier_id!='')
		$where="	supplier_id='".$supplier_id."'";
	else
		$where="id!=''";
				
		return $this-> select_query("deliverymo", "*",$where);
	}
	
	function joutitem_name($id)
	{
	if($id!='')
		$where="id='".$id."'";
	else
		$where="id!=''";
				
		return $this-> select_query("deliverymo", "*",$where);
	}	
	
	
	function jinitemname($jout_list)
	{
	if($jout_list!='')
		  $where="jout_list='".$jout_list."'";
	else
		$where="jout_list!=''";
				
		return $this->select_query("deliverymi", "*",$where);
	}
	
	function deliverymo_qty(){ 
			return $this->select_query("deliverymo_sub","id,product_id as product_id,qty,rate,products_sub","id!=''");
	}
	
	
	function machinappr_insert($id)
	{
		//echo"<pre>"; print_r($_POST); echo"</pre>"; exit;
		extract($_POST);
		$insr_array["qc_approve"]=1;
		foreach($product_id as $va=>$key){
		$finalarry[$va]["product_id"]=$key;
		$finalarry[$va]["qty_approve"]=$qty_approve[$va];
		$finalarry[$va]["qty_rejected"]=$qty_rejected[$va];
		
		}	
			/* 	if(isset($supp_endproblem[$va])&&$supp_endproblem[$va]==1)
					$insr_array["supp_endproblem"]=1;
						else
							$insr_array["supp_endproblem"]=0;
					
				if(isset($rm_issue[$va])&&$rm_issue[$va]==1)
					$insr_array["rm_issue"]=1;
						else
							$insr_array["rm_issue"]=0;
		
							if(isset($draw_problem[$va])&&$draw_problem[$va]==1)
									$insr_array["draw_problem"]=1;
							
								else
									$insr_array["draw_problem"]=0;	 */	
		$insr_array["qcproduct_approve"]=base64_encode(json_encode($_POST));
		$insr_array["qc_comments"]=$comments;		
		$insr_array["status"]=2;		
		$where="id=:ids";
	 	$where_arr= array('ids'=>$id);	
		$insert_id=$this->update_query("deliverymi",$insr_array,$where,$where_arr);	
		return "success";
	}
	
	function machining_update($id)
	{
		extract($_POST);
	 
		$insr_array["admin_approve"]=1;
		 
		$price_list=$this->find_maching_price($_GET["id"]);		 
		$get_price=convert_array($price_list[0]["products"]);		 
		
		foreach($get_price["product_id"] as $va=>$key){
		  $product[$key]["price"]=$get_price["rate"][$va];
		  $product[$key]["jobin_id"]=$_GET["id"];
		}
		 
		foreach($product_id as $va=>$key){
		$product_stock=$this->get_product_stock($key);
		$qty_remove=$qty_approve[$va];
		foreach($product_stock as $va1=>$key1){
		 
		$product_actual_qty[$key1["id"]]=$key1["nonbuffed_process_qty"];
	
			if($key1["nonbuffed_process_qty"]>=$qty_approve[$va]&&$qty_remove>0){
				$product_stock[$va1]["nonbuffed_process_qty"]=$key1["nonbuffed_process_qty"]-$qty_approve[$va];
				$qty_remove=$qty_remove-$qty_approve[$va];
			}else if($qty_remove>0){
				if($qty_remove>=$key1["nonbuffed_process_qty"]){
					$product_stock[$va1]["nonbuffed_process_qty"]=0;
					$qty_remove=$qty_remove-$key1["nonbuffed_process_qty"];
				}else{
					$product_stock[$va1]["nonbuffed_process_qty"]=$key1["nonbuffed_process_qty"]-$qty_remove;
					$qty_remove=0;
				}
			}
		}
		 
		$this->update_product_stock($product_stock,$product_actual_qty);
		 
		$finalarry[$va]["product_id"]=$key;
		$finalarry[$va]["qty_approve"]=$qty_approve[$va];
		$finalarry[$va]["qty_rejected"]=$qty_rejected[$va];
		
		$qtys=$this->openstock_edit1($key);		
		$insr_array7["processed_maching"]=$qtys[0]["processed_maching"]+$qty_approve[$va];
		$where="subproduct_id=:productid";
		$where_arr= array('productid'=>$key);
		$update1=$this->update_query("openstock",$insr_array7,$where,$where_arr);
		$nonbuff_price=$this->get_updated_price($key);
		$insr_array1["product_id"]=$key;
		$insr_array1["nonbuffed_price"]=$nonbuff_price[0]["nonbuffed_price"];
		$insr_array1["buffed_price"]=$nonbuff_price[0]["buffed_price"];
		$insr_array1["maching_price"]=$product[$key]["price"];
		$insr_array1["maching_qty"]=$qty_approve[$va];
		$insr_array1["maching_process_qty"]=$qty_approve[$va];
		$insr_array1["jobin_id"]=$product[$key]["jobin_id"];
		$insr_array1["creation_date"]=time();
		$insr_array1["status"]=0;			 
		$insert_ids=$this->insert_query("price_history",$insr_array1);
		
		}
		$insr_array["adminproduct_approve"]=base64_encode(json_encode($_POST));
		$insr_array["admin_comments"]=$comments;		
		$insr_array["status"]=3;	
		$where="id=:ids";
	 	$where_arr= array('ids'=>$id);	
		$insert_id=$this->update_query("deliverymi",$insr_array,$where,$where_arr);	
		return "success";
	}
	function find_maching_price($id){
		return $this->select_query("`deliverymi` a,`deliverymo` b","a.jout_list,b.products","a.id='".$id."' and a.jout_list=b.id");			
	}
	function find_buffing_price($id){
	return $this->select_query("buffing","id,products","id='".$id."'");			
	}
	function update_product_stock($list,$actual_list){ 
		
		foreach($list as $va=>$key){
			if($key["nonbuffed_process_qty"]!=$actual_list[$key["id"]]){
				$insr_array["nonbuffed_process_qty"]=$key["nonbuffed_process_qty"];
				$where="id=:ids";
				$where_arr= array('ids'=>$key["id"]);	
				$insert_id=$this->update_query("price_history",$insr_array,$where,$where_arr);	
			}
			
		}
		return 'success';
	}
	function update_product_stock1($list,$actual_list){ 
		
		foreach($list as $va=>$key){
			if($key["maching_process_qty"]!=$actual_list[$key["id"]]){
				$insr_array["maching_process_qty"]=$key["maching_process_qty"];
				$where="id=:ids";
				$where_arr= array('ids'=>$key["id"]);	
				$insert_id=$this->update_query("price_history",$insr_array,$where,$where_arr);	
			}
		}
		return 'success';
	}
	function get_product_stock($id){
		return $this->select_query("price_history","id,product_id,nonbuffed_qty,maching_qty,buffed_qty,nonbuffed_process_qty,buffed_process_qty,maching_process_qty","product_id='".$id."' and (nonbuffed_process_qty!=0 or buffed_process_qty!=0 or maching_process_qty!=0)  ",array(),"order by id asc");
	}
	function get_updated_price($id){
		return $this->select_query("price_history","id,product_id,nonbuffed_price,maching_price,buffed_price","product_id='".$id."'",array(),"order by id desc limit 1");
	}
	function unit_list(){
		return $this->select_query("units","id,unit_name");
	}
	
	function steel_view($id){
		return $this->select_query("steel as st,steel_sub_1 as sts1,steel_sub_2 as sts2","st.id as steel_id,st.converted_from,st.supplier,st.qc_approve,st.po_number,st.steel_number,st.created_date,sts1.order_product_id,sts1.qty_received,sts1.unit ,sts2.sub_product_id,sts2.qty_produced,sts2.qty_approved,sts2.rm_weight,sts2.rm_wastage,sts2.total_weight,sts2.storage_location,sts2.rm_weight_apr,sts2.rm_wastage_apr,sts2.total_weight_apr","st.id=sts1.steel_id and sts1.id=sts2.steel_sub_id and st.id=".$id."");
	}
	 
	function steel_history($from){
		$where="";
		if($from=='history')
			$where=" and report=0";
		
		return $this->select_query("steel as st,steel_sub_1 as sts1,steel_sub_2 as sts2","st.id as steel_id,st.converted_from,st.supplier,st.qc_approve,st.po_number,st.steel_number,st.created_date,sts1.order_product_id,sts1.qty_received,sts1.unit ,sts2.sub_product_id,sts2.qty_produced,sts2.qty_approved,sts2.rm_weight,sts2.rm_wastage,sts2.total_weight,sts2.storage_location,sts2.rm_weight_apr,sts2.rm_wastage_apr,sts2.total_weight_apr","st.id=sts1.steel_id and sts1.id=sts2.steel_sub_id $where");
	}
	function steel_fillter($date){
		 
		return $this->select_query("steel as st,steel_sub_1 as sts1,steel_sub_2 as sts2","st.id as steel_id,st.converted_from,st.supplier,st.qc_approve,st.po_number,st.steel_number,st.created_date,sts1.order_product_id,sts1.qty_received,sts1.unit ,sts2.sub_product_id,sts2.qty_produced,sts2.qty_approved,sts2.rm_weight,sts2.rm_wastage,sts2.total_weight,sts2.storage_location,sts2.rm_weight_apr,sts2.rm_wastage_apr,sts2.total_weight_apr","st.id=sts1.steel_id and sts1.id=sts2.steel_sub_id and  st.`created_date` >= '".strtotime($date["from_date"])."' AND st.`created_date` <= '".strtotime($date["to_date"])."' AND st.report=0");
	}
	
	function steel_list($id=''){
		$where="id!=''";
		if(isset($id)&&$id!='')
			$where="po_number=".$id."";
		return $this->select_query("steel","id,converted_from,supplier,po_number,steel_number,created_date,qc_approve",$where);
	}
	function steel_edit($id){
		return $this->select_query("steel","id,converted_from,supplier,po_number,steel_number,created_date,qc_approve","id=".$id."");
	}
	
	
	function steel_sub_list($id){
	$where="";
	if($id!='')
		$where="steel_id=".$id."";
		return $this->select_query("steel_sub_1","id,order_product_id,qty_received,unit,steel_id",$where);
	}
	
	function steel_sub_list_1($id){
	$where="";
		if($id!='')
			$where="steel_sub_id=".$id."";
		return $this->select_query("steel_sub_2","id,sub_product_id,qty_produced,qty_approved,qty_rejected,steel_id,steel_sub_id,rm_weight,rm_wastage,total_weight,storage_location",$where);
	}
	
	function unit_edit($id){
		return $this->select_query("units","id,unit_name","id='".$id."'");
	}
	function steel_insert()
	{
		extract($_POST);
		 
		$insr_array["converted_from"]=$converted_from[0];
		if(isset($supplier))
			$insr_array["supplier"]=$supplier;
		if(isset($po_list))
			$insr_array["po_number"]=$po_list;
		if(isset($qc_approved))
			$insr_array["qc_approve"]=$qc_approved;
		$insr_array["steel_number"]=$steel_number;
		$insr_array["created_date"]=time();
		 
		$steel_id=$this->insert_query("steel",$insr_array);
		 
		$final_arry=array();
		
		foreach($product_id as $va=>$key){
			$insr_array1["order_product_id"]=$key;
			$insr_array1["qty_received"]=$qty_received[$va];
			$insr_array1["unit"]=$units[$va];
			$insr_array1["steel_id"]=$steel_id;
			$steel_sub_id=$this->insert_query("steel_sub_1",$insr_array1);
			
			$products_subproduct=$_POST["sub_product_id_".$key];				
			foreach($products_subproduct as $va1=>$key1){					
				$insr_array2["sub_product_id"]=$key1;
				$insr_array2["qty_produced"]=$_POST["qty_produced_".$key][$va1];
				if($qc_approved==0)
					$insr_array2["qty_approved"]=$_POST["qty_produced_".$key][$va1];
				//$insr_array["qty_rejected"]=$_POST["qty_produced_".$key1];
				$insr_array2["steel_id"]=$steel_id;
				$insr_array2["steel_sub_id"]=$steel_sub_id;
				
				$insr_array2["rm_weight"]=$_POST["rm_weight_".$key][$va1];
				$insr_array2["rm_wastage"]=$_POST["rm_wastage_".$key][$va1];
				$insr_array2["total_weight"]=$_POST["total_weight_kg_".$key][$va1];
				
				if($qc_approved==0){
					 
					$insr_array2["rm_weight_apr"]=$_POST["rm_weight_".$key][$va1];
					$insr_array2["rm_wastage_apr"]=$_POST["rm_wastage_".$key][$va1];
					$insr_array2["total_weight_apr"]=$_POST["total_weight_kg_".$key][$va1];
				}
				
				$insr_array2["storage_location"]=$_POST["storage_location_".$key][$va1];
				$steel_sub_id_1=$this->insert_query("steel_sub_2",$insr_array2);
				
				if($converted_from[0]==2){
					$qtys_st=$this->openstock_edit1($key);				 
					$insr_array4["processed_qty"]=bcsub($qtys_st[0]["processed_qty"],$insr_array2["total_weight"],1);
					 
					$where1="subproduct_id=:productid";
					$where_arr1= array('productid'=>$key);
				$update1=$this->update_query("openstock",$insr_array4,$where1,$where_arr1);	
				}
				
				
				if($qc_approved==0){
					$qtys=$this->openstock_edit1($key1);					
					if($_POST["storage_location_".$key][$va1]==1){
						$insr_array3["qty"]=$qtys[0]["qty"]+$_POST["qty_produced_".$key][$va1];			
						$insr_array3["processed_qty"]=$qtys[0]["processed_qty"]+$_POST["qty_produced_".$key][$va1];
					}else if($_POST["storage_location_".$key][$va1]==3){
						$insr_array3["buffed_qty"]=$qtys[0]["buffed_qty"]+$_POST["qty_produced_".$key][$va1];
						$insr_array3["processed_buff"]=$qtys[0]["processed_buff"]+$_POST["qty_produced_".$key][$va1];	
					}else if($_POST["storage_location_".$key][$va1]==2){
						$insr_array3["maching_qty"]=$qtys[0]["maching_qty"]+$_POST["qty_produced_".$key][$va1];
						$insr_array3["processed_maching"]=$qtys[0]["processed_maching"]+$_POST["qty_produced_".$key][$va1];	
					}					 
					$where="subproduct_id=:productid";
					$where_arr= array('productid'=>$key1);
					$update1=$this->update_query("openstock",$insr_array3,$where,$where_arr);
				}
			}
		}
		return "success";
	}
	
	function steel_approve()
	{
		extract($_POST);

			$insr_array["qc_approve"]=0;
			$where="id=:productid";
			$where_arr= array('productid'=>$_GET["id"]);			
			$steel_id=$this->update_query("steel",$insr_array,$where,$where_arr);
		 
		foreach($product_id as $va=>$key){
			$products_subproduct=$_POST["sub_product_id_".$key];
			 
			foreach($products_subproduct as $va1=>$key1){
				
				//$insr_array2["qty_produced"]=$_POST["qty_produced_".$key][$va1];
				$insr_array2["qty_approved"]=$_POST["qty_approved_".$key][$va1];
				//$insr_array["qty_rejected"]=$_POST["qty_produced_".$key1];
				//$insr_array2["rm_weight"]=$_POST["rm_weight_".$key][$va1];
				//$insr_array2["rm_wastage"]=$_POST["rm_wastage_".$key][$va1];
				//$insr_array2["total_weight"]=$_POST["total_weight_kg_".$key][$va1];
				
				$insr_array2["rm_weight_apr"]=$_POST["rm_weight_".$key][$va1];
				$insr_array2["rm_wastage_apr"]=$_POST["rm_wastage_".$key][$va1];
				$insr_array2["total_weight_apr"]=$_POST["total_weight_kg_".$key][$va1];
				
				
				
				$insr_array2["storage_location"]=$_POST["storage_location_".$key][$va1];				 	
				$where="id=:productid";
				
				$where_arr= array('productid'=>$_POST["steel_sub_id_".$key][$va1]);
				$update1=$this->update_query("steel_sub_2",$insr_array2,$where,$where_arr);
				
				 
					$qtys=$this->openstock_edit1($key);
					
					if($_POST["storage_location_".$key][$va1]==1){
						$insr_array3["qty"]=$qtys[0]["qty"]+$_POST["qty_approved_".$key][$va1];			
						$insr_array3["processed_qty"]=$qtys[0]["processed_qty"]+$_POST["qty_approved_".$key][$va1];
					}else if($_POST["storage_location_".$key][$va1]==3){
						$insr_array3["buffed_qty"]=$qtys[0]["buffed_qty"]+$_POST["qty_approved_".$key][$va1];
						$insr_array3["processed_buff"]=$qtys[0]["processed_buff"]+$_POST["qty_approved_".$key][$va1];	
					}else if($_POST["storage_location_".$key][$va1]==2){
						$insr_array3["maching_qty"]=$qtys[0]["maching_qty"]+$_POST["qty_approved_".$key][$va1];
						$insr_array3["processed_maching"]=$qtys[0]["processed_maching"]+$_POST["qty_approved_".$key][$va1];	
					}
				 
					$where="subproduct_id=:productid";
					$where_arr= array('productid'=>$key);
					$update1=$this->update_query("openstock",$insr_array3,$where,$where_arr);
				 
					if($converted_from[0]==2){
						$qtys_st=$this->openstock_edit1($key);
						$insr_array4["processed_qty"]=$qtys_st[0]["processed_qty"]-$insr_array2["total_weight_apr"];
						$where1="subproduct_id=:productid";
						$where_arr1= array('productid'=>$key);
						$update1=$this->update_query("openstock",$insr_array4,$where1,$where_arr1);
					}
				
				
			}	
			
		}
	  
		return "success";
	}
	function scrap_generation(){
		extract($_POST);
		
		$insr_array["stl_no"]=base64_encode(json_encode($_POST["approve"]));
		
		foreach($_POST["approve"] as $va=>$key){
		$insr_array7["report"]=1;
		$where="steel_number=:stl_no";
		$where_arr= array('stl_no'=>$key);
		$update1=$this->update_query("steel",$insr_array7,$where,$where_arr);	
		}
		
		
		$report=$this->scrap_history_list();
		if(count($report)>0)
		$report_no="MMSPL-REP-".sprintf("%03s",count($report)+1);
		else
		$report_no="MMSPL-REP-001";
		
		$insr_array["report_no"]=$report_no;
		$insr_array["pays"]=base64_encode(json_encode($_POST));
		$insr_array["creation_time"]=time();
		$insr_array["created_by"]=$_SESSION["user_id"];
		$insert_id=$this->insert_query("scrap_history",$insr_array);
		
		return "success";
	}
	
}
?>
