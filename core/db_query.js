/* var sql = require('mssql');
function db_query(){
	this.select_query= function (queryValue,callback,dd){
		//console.log(queryValue);
		global.connection.acquire(function(connection_error, con){
			var query_exec =con.query(queryValue,function(err, data)
			{
				if (err) 
				{
					self.genrate_log(err,queryValue);
					sql.close();
					callback(1, null);
				}
				else
				{
					sql.close();
					callback(0, data.recordset);
				}
			});
			if(dd==1)
			console.log(query_exec.sql);
		});
	}

	this.insert_query=function (insert_data,callback,dd){
		//console.log(insert_data);
		global.connection.acquire(function(connection_error, con) 
		{
			var query_exec =con.query(insert_data, function(err, data) 
			{
				if (err)
				{
					self.genrate_log(err,' '+insert_data);
					sql.close();
					callback(1, null);
				}
				else
				{
					var str = "SELECT @@IDENTITY AS 'identity'";
					con1 = new sql.Request();
					con1.query(str, function(suberr, subdata)
					{
						sql.close();
						callback(0, subdata.recordset[0]);
					});				
				}
			});
			if(dd==1)
			console.log(query_exec.sql);
		});
	}

	this.update_query= function (update_data,callback,dd){
		//console.log(update_data);
		global.connection.acquire(function(connection_error, con) 
		{
			var query_exec =con.query(update_data, function(err, data) 
			{
				if (err)
				{
					self.genrate_log(err,' '+update_data);
					sql.close();
					callback(1, null);
				}
				else
				{
					sql.close();
					callback(0, data.recordset);
				}
			});
			if(dd==1)
			console.log(query_exec.sql);
		});
	}

	 this.genrate_log=function (err,qry){
		var d = new Date();
		var error_message='\r\n\r\n LOG: '+Date();
		error_message +='\r\n ERROR TYPE: SELECT QUERY ';
		error_message +='\r\n ERROR: '+err;
		error_message +='\r\n query: '+qry;
		var month=d.getMonth()+1;
		global.fs.appendFile("error_log/query_error_log_"+d.getDate()+"_"+month+"_"+d.getFullYear()+".txt", error_message, function(err) 
		{
			if(err) 
			{
				console.log(err);
			}
		});
	} 
}
var self =module.exports = new db_query(); */
function db_query(){
this.select_query= function (table_name,select_data,where,where_arr,sort_by,callback,dd){	
	global.connection.acquire(function(connection_error, con){
		var select_query='SELECT '+select_data+' from '+table_name+' where '+where+' '+sort_by+' ';
		console.log(select_query);
		var query_exec =con.query(select_query,where_arr,function(err, data) 
		{
			con.release();
				
			if (err) 
	 		{
				self.genrate_log(err,select_query);
				callback(1, null);
			}
			else
			{
				callback(0, data);
	 		}
			
		});
		if(dd==1)
			console.log(query_exec.sql);
    });
}


this.select_query_1= function (queryValue,callback,dd){	
	global.connection.acquire(function(connection_error, con){
		var query_exec =con.query(queryValue,function(err, data) 
		{
			con.release();
				
			if (err) 
	 		{
				self.genrate_log(err,queryValue);
				callback(1, null);
			}
			else
			{
		
				callback(0, data);
	 		}
			
		});
		if(dd==1)
			console.log(query_exec.sql);
    });
}

this.insert_query=function (table_name,insert_data,callback,dd){
	
	global.connection.acquire(function(connection_error, con) 
	{		
		var query_exec=con.query('INSERT INTO '+table_name+' SET ? ', [insert_data], function(err, data) 
		{		
			con.release();
			if (err){
				self.genrate_log(err,'INSERT INTO '+table_name);
				callback(1, null);
			}
			else
			{
				callback(0, data);
				con.commit();				 
	 		}
			
			
		});
		if(dd==1)
				console.log(query_exec.sql);
		
    });
}

this.insert_query_1=function (insert_data,callback,dd){	
	global.connection.acquire(function(connection_error, con) 
	{   con.release();
		var query_exec =con.query(insert_data, function(err, data) 
		{		
			
			if (err){
				console.log(err);
				self.genrate_log(err,' '+insert_data);
				callback(1, null);
			}
			else
			{
				callback(0, data);
				con.commit();								
	 		}
			
		});
		
		if(dd==1)
			console.log(query_exec.sql);
		
    });
}

this.update_query= function (table_name,where,where_arr,update_data,callback,dd){	
	global.connection.acquire(function(connection_error, con) 
	{
		var qry="UPDATE "+table_name+" SET ";
		var updt_arr=[];
		for (var i in update_data){
			qry+= i+" = ? ,";
			updt_arr.push(update_data[i]);
		}
		for (var i in where_arr){
			//updt_arr[]=where_arr[i];
			updt_arr.push(where_arr[i]);
		}
		qry = qry.replace(/(^[,\s]+)|([,\s]+$)/g, '');
		qry = qry+" where "+where;
		var query_exec =con.query(qry, updt_arr, function(err, data) 
		{		
			
			if (err) 
	 		{
				self.genrate_log(err,qry);
				callback(1, null);
			}
			else
			{
				callback(0, data);con.release();
	 		}
			
		});
		if(dd==1)
		console.log(query_exec.sql);
    });
}
this.delete_query= function (table_name,where,callback,dd){	
	global.connection.acquire(function(connection_error, con) 
	{
		var delete_query='DELETE from '+table_name+' where '+where+' ';
		con.query(delete_query, function(err, data) 
		{		
			con.release();
			if (err) 
	 		{
				var d = new Date();
				var error_message='\r\n\r\n LOG: '+Date();
				error_message +='\r\n ERROR TYPE: DELETE QUERY ';
				error_message +='\r\n ERROR: '+err;
				global.fs.appendFile("error_log/query_error_log"+d.getDate()+"_"+d.getMonth()+"_"+d.getFullYear()+".txt", error_message, function(err) 
				{
					if(err) 
					{
						console.log(err);
					}
				});
				callback(1, null);
			}
			else
			{
				callback(0, data);
	 		}
		});
    });
}
this.genrate_log=function (err,qry){
	var d = new Date();
	var error_message='\r\n\r\n LOG: '+Date();
	error_message +='\r\n ERROR TYPE: SELECT QUERY ';
	error_message +='\r\n ERROR: '+err;
	error_message +='\r\n query: '+qry;
	var month=d.getMonth()+1;
	global.fs.appendFile("error_log/query_error_log_"+d.getDate()+"_"+month+"_"+d.getFullYear()+".txt", error_message, function(err) 
	{
		if(err) 
		{
			console.log(err);
		}
	});
} 
}
var self =module.exports = new db_query();