/* var sql = require('mssql');
function Connection() {
	this.init = function() {
		var config = {
			user: 'sa',
			password: 'Mcpl@2014',
			server: '106.51.48.168',
			//driver: 'tedious',
			database: 'MobileTracking',
			port:1433
			
		};

		this.acquire = function(callback) {
			new sql.connect(config).then(function(){
				var connection = new sql.Request();
				callback(null,connection);
			}).catch(function(err){
				console.log(err);
			})
		};
	};
}
module.exports = new Connection(); */
var mysql = require('mysql');

function Connection() {
  this.pool = null;

  this.init = function() {
    this.pool = mysql.createPool({
      connectionLimit: 10,
      host: '13.126.66.230',
      user: 'supreme',
      password: 'appscomp',
      database: 'trackingapp'
    });
  };

  this.acquire = function(callback) {
    this.pool.getConnection(function(err, connection) {
      callback(err, connection);
    });
  };
}

module.exports = new Connection();
