<?php
global $db_helper_obj;
$dept_arr=array_merge($data1,$data2);
$sale_data=$db_helper_obj->get_graph_data_by_dept($year,$mnth,"'".implode("','",$dept_arr)."'");
$data=array();
$tot=array();
foreach($sale_data as $va=>$key){
	if($mnth=="")
		$dd=$key["date_time"].",".($key["mnth_no"]-1); 
	else
		$dd=$key["date_time"].",".($key["mnth_no"]-1).",".$key["date_no"]; 
	if(in_array($key["dept_type_id"],$data1)){
		if(!isset($data[$key["unit"]]["data1"][$dd]))
			$data[$key["unit"]]["data1"][$dd]=0;
		$data[$key["unit"]]["data1"][$dd]+=$key["sum_val"];
		if(!isset($data["tot"]["data1"][$dd]))
			$data["tot"]["data1"][$dd]=0;
		$data["tot"]["data1"][$dd]+=$key["sum_val"];
	}
	if(in_array($key["dept_type_id"],$data2)){
		if(!isset($data[$key["unit"]]["data2"][$dd]))
			$data[$key["unit"]]["data2"][$dd]=0;
		$data[$key["unit"]]["data2"][$dd]+=$key["sum_val"];
		if(!isset($data["tot"]["data2"][$dd]))
			$data["tot"]["data2"][$dd]=0;
		$data["tot"]["data2"][$dd]+=$key["sum_val"];
	}
}
$graph_data=array();
for($i=1;$i<=6;$i++){
	$str[$i]="";
}
$str["tot"]="";
foreach($data as $va_data=>$key_data){
	if(isset($key_data["data1"]) && isset($key_data["data2"])){
		foreach($key_data["data1"] as $va=>$key){
			if(isset($key_data["data2"][$va]) && $key_data["data2"][$va] && $key){
				$round_val=round(($key_data["data2"][$va]/$key)*100,2);
				$str[$va_data].="{ x: new Date(".$va."), y: ".$round_val." },";
			}
			
			//$graph_data[$va_data][$va]=($key_data["data2"][$va]/$key)*100;
	//$graph_data[$va_data][]=array("x"=>$va,"y"=>($key_data["data2"][$va]/$key)*100);
		}
	}
}
$str["tot"]=trim($str["tot"],",");
//print_r($data);
//exit(); 
for($i=1;$i<=6;$i++){
	$str[$i]=trim($str[$i],",");
}

?>
			var chart_salary = new CanvasJS.Chart("chartContainer_main", {
				animationEnabled: true,
				toolTip: {
					shared: true,
				},
				theme: "theme2",
				axisX: {
					valueFormatString: "<?php if($mnth!="")
						echo 'DD'; ?> MMM",
					interval: 1,
					<?php if($mnth=="")
						echo 'intervalType: "month",'; ?>
					gridColor: "Silver",
					tickColor: "silver",

				},
				axisY: {
					gridColor: "Silver",
					tickColor: "silver"
				},
				legend: {
					verticalAlign: "center",
					horizontalAlign: "right"
				},
				data: [
				{
					type: "line",
					showInLegend: true,
					lineThickness: 2,
					name: "Plant-I",
					toolTipContent: "<span style='color:#F08080;'>Plant-I: {y} %</span>",
					markerType: "square",
					color: "#F08080",
					dataPoints: [<?php echo $str[1];?>]
				},
				{
					type: "line",
					showInLegend: true,
					toolTipContent: "<span style='color:#20B2AA;'>Plant-II: {y} %</span>",
					name: "Plant-II",
					color: "#20B2AA",
					lineThickness: 2,
					dataPoints: [<?php echo $str[2];?>]
				},
				{
					type: "line",
					showInLegend: true,
					name: "ISAS",
					toolTipContent: "<span style='color:#0168b1;'>ISAS: {y} %</span>",
					color: "#0168b1",
					lineThickness: 2,
					dataPoints: [<?php echo $str[3];?>]
				},
				{
					type: "line",
					showInLegend: true,
					name: "NF",
					toolTipContent: "<span style='color:#f3c350;'>NF: {y} %</span>",
					color: "#f3c350",
					lineThickness: 2,
					dataPoints: [<?php echo $str[4];?>]
				},
				{
					type: "line",
					showInLegend: true,
					name: "Plant-III",
					color: "#1e1402",
					toolTipContent: "<span style='color:#1e1402;'>Plant III: {y} %</span>",
					lineThickness: 1,
					dataPoints: [<?php echo $str[5];?>]
				},
				{
					type: "line",
					showInLegend: true,
					name: "Plant-VI",
					color: "#01b11f",
					toolTipContent: "<span style='color:#01b11f;'>Plant VI: {y} %</span>",
					lineThickness: 2,
					dataPoints: [<?php echo $str[6];?>]
				},
				{
					type: "line",
					showInLegend: true,
					name: "Over All",
					color: "#f00808",
					toolTipContent: "<span style='color:#f00808;'>Over All: {y} %</span>",
					lineThickness: 2,
					dataPoints: [<?php echo $str["tot"];?>]
				}
				],
				legend: {
					cursor: "pointer",
					itemclick: function (e) {
						if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
							e.dataSeries.visible = false;
						}
						else {
							e.dataSeries.visible = true;
						}
						chart_salary.render();
					}
				}
			});

			chart_salary.render();
