<?php
global $db_helper_obj;
$dept_arr=array_merge($data1,$data2);
$sale_data=$db_helper_obj->get_bar_graph_data($year,$mnth,"'".implode("','",$dept_arr)."'","1");
$data=array();
foreach($sale_data as $va=>$key){
	if($mnth=="")
		$dd=$key["date_time"].",".($key["mnth_no"]-1); 
	else
		$dd=$key["date_time"].",".($key["mnth_no"]-1).",".$key["date_no"]; 
	if(in_array($key["Department"],$data1)){
		if(!isset($data["data1"][$dd]))
			$data["data1"][$dd]=0;
		$data["data1"][$dd]+=$key["sum_val"];
	}
	if(in_array($key["Department"],$data2)){
		if(!isset($data["data2"][$dd]))
			$data["data2"][$dd]=0;
		$data["data2"][$dd]+=$key["sum_val"];
	}
}
$graph_data=array();
for($i=1;$i<=6;$i++){
	$str[$i]="";
}

		foreach($data["data1"] as $va=>$key){
			if(isset($data["data2"][$va]) && $data["data2"][$va] && $key){
				$str[1].="{label: '$va', y: $key},";
				$str[2].="{label: '$va', y: ".$data["data2"][$va]."},";
			}
			//$graph_data[$va_data][$va]=($data["data2"][$va]/$key)*100;
	//$graph_data[$va_data][]=array("x"=>$va,"y"=>($data["data2"][$va]/$key)*100);
		}
//echo "<pre>"; print_r($str); echo "</pre>";
//exit(); 
for($i=1;$i<=6;$i++){
	$str[$i]=trim($str[$i],",");
}
?>var chart = new CanvasJS.Chart("chartContainer_main", {
				
				data: [{
					type: "column",
					dataPoints: [<?php echo $str[1]; ?>]
				}, {
					type: "column",
					dataPoints: [<?php echo $str[2]; ?>]
				}]
				
				
			});
			chart.render();