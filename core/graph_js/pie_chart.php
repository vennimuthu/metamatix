<?php 			
$over_all=$db_helper_obj->get_overall_pie($year,$mnth);
$hr=array("Staff salary","Permanent workers salary","Contract man rate salary","Contract piece rate salary","General salary","Genral salary","Contract man/piece rate salary","Company Contract salary","Packing man rate salary","CNC Salary","QC Salary(Casting Inspection and Packing)","Others Contract salary","Man Rate & Piece Rate","Man Rate","Piece Rate");
$other_expense=array("Others expense - Plant Actual","Others expense - on Prod capacity basis","Others expense");
$p_cons=array("Raw material consumption","Raw materials Consumption","Raw materials(Aluminium Alloys) Consumption","Centrifugal Sleeve Consumption","Other Production materials Consumption","Assets Consumption","Packing Consumption in P3 & P6","P1 Machining Expenses","P2 Machining Expenses","ISAS Machining Expenses","P3 Machining Expenses");
$power=array("Purchase EB","TNEB","Purchase EB Cost","TNEB Cost");
$transport=array("Freight outward","Freight inward","Freight inward (Purchase)","Freight inward (Sales Return)","Freight inward(Purchase)","Freight inward(Sales Return)","Internal Transport");
$sub_contract=array("Sub-contracting (proof-machining)","Sub-contracting (CNC-machining)","Sub-contracting (Heat treatment)","Sub-contracting (machining)","Sub-contracting (CNC machining)-plant 3","Sub-contracting (fettling)");
$rejection=array("Rejection value on production","Sales rejection - Domestic","Sales rejection - Export","Rejection value","Inhouse Rejection value","Vendor Rejection Value","Casting & Vendor Machining Rejection of P2 in P3","P2 Rejection value on production");
$deduction=array("Deduction in payment advice","Deduction for price difference","Debit in payment advice");
$fuel=array("Diesel","Diesel (Pyrolitic Oil)","LPG");
$data=array();
$clr=array('Production Consumption'=>'#f79647','HR'=>'#33558b','Power'=>'#8064a1','Transport'=>'#4aacc5','Rejection'=>'#23bfaa','Other Expense'=>'#bf2323','Sub Contract'=>'#9bbb58','Deduction'=>'#813b16','Fuel'=>'#10728b');
for($i=1; $i<=6; $i++){
	$data[$i]["HR"]=0;
	$data[$i]["Power"]=0;
	$data[$i]["Transport"]=0;
	$data[$i]["Rejection"]=0;
	$data[$i]["Other Expense"]=0;
	$data[$i]["Sub Contract"]=0;
	$data[$i]["Production Consumption"]=0;
	$data[$i]["Deduction"]=0;
	$data[$i]["Fuel"]=0;
}
foreach($over_all as $va=>$key){
	if(in_array($key["Department"],$hr))
		$data[$key["unit"]]["HR"]+=$key["sum_val"];
	else if(in_array($key["Department"],$power))
		$data[$key["unit"]]["Power"]+=$key["sum_val"];
	else if(in_array($key["Department"],$transport))
		$data[$key["unit"]]["Transport"]+=$key["sum_val"];
	else if(in_array($key["Department"],$sub_contract))
		$data[$key["unit"]]["Sub Contract"]+=$key["sum_val"];
	else if(in_array($key["Department"],$rejection))
		$data[$key["unit"]]["Rejection"]+=$key["sum_val"];
	else if(in_array($key["Department"],$p_cons))
		$data[$key["unit"]]["Production Consumption"]+=$key["sum_val"];
	else if(in_array($key["Department"],$deduction))
		$data[$key["unit"]]["Deduction"]+=$key["sum_val"];
	else if(in_array($key["Department"],$fuel))
		$data[$key["unit"]]["Fuel"]+=$key["sum_val"];
	else 
		$data[$key["unit"]]["Other Expense"]+=$key["sum_val"];
	//$data2[$key["unit_name"]]=$key["Budget"];
}
$data["total"]["HR"]=0;
$data["total"]["Power"]=0;
$data["total"]["Transport"]=0;
$data["total"]["Rejection"]=0;
$data["total"]["Other Expense"]=0;
$data["total"]["Sub Contract"]=0;
$data["total"]["Production Consumption"]=0;
$data["total"]["Deduction"]=0;
$data["total"]["Fuel"]=0;
$min_data=array();
for($i=1; $i<=6; $i++){
	$data["total"]["HR"]+=$data[$i]["HR"];
	$data["total"]["Power"]+=$data[$i]["Power"];
	$data["total"]["Transport"]+=$data[$i]["Transport"];
	$data["total"]["Rejection"]+=$data[$i]["Rejection"];
	$data["total"]["Other Expense"]+=$data[$i]["Other Expense"];
	$data["total"]["Sub Contract"]+=$data[$i]["Sub Contract"];
	$data["total"]["Production Consumption"]+=$data[$i]["Production Consumption"];
	$data["total"]["Deduction"]+=$data[$i]["Deduction"];
	$data["total"]["Fuel"]+=$data[$i]["Fuel"];
	$data[$i]=array_filter($data[$i]);
	$min_val=min($data[$i]);
	$min_val_key=array_keys($data[$i],$min_val);
	$min_data[$i][$min_val_key[0]]=$min_val;
}
//print_r($min_data);
$graph_data=array();
foreach($data as $va=>$key){
	foreach($key as $va_d=>$key_d){
		$graph_data[$va][]=array('y'=>$key_d,'indexLabel'=>$va_d,'color'=>$clr[$va_d]);
	}
}

?>


	<?php foreach($graph_data as $va=>$key){?>
	var chart<?php echo $va; ?> = new CanvasJS.Chart("chartContainer<?php echo $va; ?>",
	{
		theme: "theme2",
		
		data: [
		{
			type: "pie",
			toolTipContent: "{y} - #percent % {indexLabel}",
			yValueFormatString: "#.0#,,. Lakhs",
			legendText: "{indexLabel}",
			dataPoints: <?php echo json_encode($key);?>
		}
		]
	});
	chart<?php echo $va; ?>.render();
	<?php } ?>

