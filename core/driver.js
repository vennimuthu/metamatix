function driver() {

this.login = function(req,res){
	var data_to_return={'status':0,'msg':'Invalid Data','data':{}};
	
	// setup validation
	req.assert('email_id', 'Invalid value').notEmpty();
	req.assert('password', 'Invalid value').notEmpty();
	
	// validating errors
	var errors = req.validationErrors();
	if (errors) {
		data_to_return['data']['error_msg']=errors[0]['msg'];
		// returning the error
		res.send(data_to_return);
		return;
	}
	// data sanitisation  
	var email_id=req.sanitize('email_id').escape().trim();
	var password=req.sanitize('password').escape().trim();

	var crypto=require('crypto');	
	var hashed_password=crypto.createHash('md5').update(password).digest("hex");
	
	var user_select_qry_data=function(error, result){
	if(error==1){
		res.send({status: 500, msg: 'Query Failed'});
		return;
	}else{
		if (result==''){
			res.send({status: 400, msg: 'empty result'});
			return;
		}else{			
			data_to_return['status']=200;
			data_to_return['msg']="success";
			data_to_return['data']=result;
			res.send(data_to_return);
		}
	}
	}
	global.db_query.select_query("users","id as user_id","email_id=? and password=?",[email_id,hashed_password],"",user_select_qry_data);
}
	

this.driver_otp_send=function(req,res){
	
	var data_to_return={'status':0,'msg':'Invalid Data','data':{}};
	
	// validating the data
	req.assert('mobile_number', 'Invalid value').notEmpty();
	
	// validating the errors
	var errors = req.validationErrors();
	if (errors) {
		data_to_return['data']['error_msg']=errors[0]['msg'];
		res.send(data_to_return);
		return;
	}
	
	//Sanitize the data
	var number=req.sanitize('mobile_number').escape().trim();

	var async=require('async');
	var moment=require('moment');
	var urlencode=require('urlencode');
	var request = require("request");
	var randomize = require('randomatic');
	var date=moment(new Date(Date.now())).format('YYYY-MM-DD HH:mm:ss');
	var data_send={};
	
	var otp=randomize('0', 4);
	var user_name=urlencode("carol.dalmeida@idelivery.in");
	var hash="6e2a31220b7cfd3b759e8e911be628d42872d7ba";
	var sender="IDLVRY";
	var test=false;
	var date1=moment(date).add(1,'minutes');
	data_send['otp_send_datetime']=date;
	data_send['otp_expiry_datetime']=moment(date1).format('YYYY-MM-DD HH:mm:ss');
	data_send['otp']=otp;
	var message="Your OTP is "+otp;
	var encode_message =urlencode(message);
	
	async.parallel([
	function(callback){
		//SEND SMS
		request("http://api.textlocal.in/send/?username="+user_name+"&hash="+hash+"&message="+encode_message+"&sender="+sender+"&numbers="+number+"&test="+test, function(error, response, body) {
			msg_response = JSON.parse(body);
			var msg_status = msg_response["status"];
			msg_response=JSON.stringify(msg_response);
			if(msg_status == 'success'){
				global.db_query.sendSmsSuccessLog(number,message,msg_response);
				callback(null,1);
			}else if(msg_status == 'failure'){
				global.db_query.sendSmsFailureLog(number,message,msg_response);
				callback(null,1);
			}
		});
	},
	function(callback){
		var driver_otp_send_data=function(error,result) {
		if(error==1){
			res.send({status: 500, msg: 'Query failed'});
		}else{
			if(result!=''){
				callback(null,1);
			}
		}
		}
		global.db_query.update_query("driver","mobile_number=?",[number],data_send,driver_otp_send_data);
	}
	],function(errors,results){
		data_to_return['status']=200;
		data_to_return['msg']="success";
		data_to_return['data']['mobile_number']=number;
		res.send(data_to_return);
	});		
}

/* 
this.driver_otp_received=function(req,res){
	var data_to_return={'status':0,'msg':'Invalid Data','data':{}};

	//setup validation
	
	req.assert('otp','Invalid value').notEmpty();
	req.assert('mobile_number','Invalid value').notEmpty();
	console.log(req.body);
	// validating errors
	var errors = req.validationErrors();
	if (errors) {
		data_to_return['data']['error_msg']=errors[0]['msg'];
		// returning the error
		res.send(data_to_return);
		return;
	}
	//data sanitisation	
	var otp=req.sanitize('otp').escape().trim();
	var mobile_number=req.sanitize('mobile_number').escape().trim();
	var moment=require('moment');

	var driver_otp_data_rtn=function(err, result) {
	if (err==1){
		res.send({status: 500, msg: 'Query failed'});
		return;
	}else{
		if(result==""){
			res.send({status: 400, msg: 'empty result'});
		}else{
//		var expiry_time=(result[0].otp_expiry_datetime).getTime();
		var current_time=new Date(Date.now());
//		var diff=(current_time-expiry_time);
		var expiry_time=new Date(result[0].otp_expiry_datetime);
		var diff=moment(current_time).diff(expiry_time);
		console.log("expiry_time--->"+expiry_time);
		console.log("current_time--->"+current_time);
		
		console.log(diff);
		if(diff<0){			
			var mobile_number=result[0].mobile_number;
			var driver_otp_data1_rtn=function(err1, result1){
			if (result1==''){
				res.send({status: 400, msg: 'driver does not exist'});
				return;
			}else{
				data_to_return['status']=200;
				data_to_return['msg']="success";
				data_to_return['data']['driver_id']=result1[0].driver_id;
				data_to_return['data']['company_id']=result1[0].company_id;
				data_to_return['data']['mobile_number']=mobile_number;				
				res.send(data_to_return);	
			}
			}		
			global.db_query.select_query("driver","id as driver_id,company_id","mobile_number=? ",[mobile_number],"",driver_otp_data1_rtn);			
		}else{
			res.send({status: 400, msg: 'otp expired'});
		}
		}
	}
	}
	global.db_query.select_query("driver_otp","mobile_number,otp_expiry_datetime","mobile_number=? and otp=?",[mobile_number,otp],"order by id DESC",driver_otp_data_rtn);
}
 */

this.driver_otp_received=function(req,res){
	var data_to_return={'status':0,'msg':'Invalid Data','data':{}};

	//setup validation
	
	req.assert('otp','Invalid value').notEmpty();
	req.assert('mobile_number','Invalid value').notEmpty();
	
	// validating errors
	var errors = req.validationErrors();
	if (errors) {
		data_to_return['data']['error_msg']=errors[0]['msg'];
		// returning the error
		res.send(data_to_return);
		return;
	}
	
	//data sanitisation	
	var otp=req.sanitize('otp').escape().trim();
	var mobile_number=req.sanitize('mobile_number').escape().trim();

	var moment=require('moment');
	var async=require('async');
	var jwt = require('jsonwebtoken');
	var uuidv4 = require('uuid/v4');
	var date=moment(new Date(Date.now())).format('YYYY-MM-DD HH:mm:ss');

	async.waterfall([
	function(callback){
		var driver_otp_received_data=function(error, result) {
		if (error==1){
			res.send({status: 500, msg: 'Query failed'});
			return;
		}else{
			if (result==''){
				res.send({status: 400, msg: 'empty result'});
			}else{
				data_to_return['data']['driver_id']=result[0].driver_id;	
				data_to_return['data']['company_id']=result[0].company_id;	
				data_to_return['data']['mobile_number']=result[0].mobile_number;
				callback(null,result);
			}
		}
		}
		global.db_query.select_query("driver","id as driver_id,mobile_number,company_id,otp_expiry_datetime,otp,token","mobile_number=?",[mobile_number],"",driver_otp_received_data);	
	},
	function(arg,callback){
		var otp_val=arg[0].otp;
		var expiry_time=(arg[0].otp_expiry_datetime).getTime();
		var current_time=new Date().getTime();
		var diff=(current_time-expiry_time);
		if(diff<0 &&otp_val==otp){
			if(arg[0].token!='' && typeof(arg[0].token)!=undefined){
				data_to_return['data']['token']=arg[0].token;
				callback(null,1);
			}else{
				var token = jwt.sign({'driver_id':arg[0].driver_id,'mobile_number':arg[0].mobile_number,'company_id':arg[0].company_id,'date':date},global.config.secret,{'issuer':arg[0].driver_id.toString(),'jwtid':uuidv4()});
				var driver_otp_received_data1=function(error1,result1){
				if(error1==1){
					res.send({status: 500, msg: 'query failed 1'});
				}else{
					if(result1!=''){
						data_to_return['data']['token']=token;
						callback(null,1);
					}
				}
				}		
				global.db_query.update_query("driver","mobile_number=?",[mobile_number],{'token':token},driver_otp_received_data1);
			}
		}else{
			res.send({status: 400, msg: 'otp expired'});
			return;
		}
	}
	],function(errors,results){
		data_to_return['status']=200;
		data_to_return['msg']="success";
		res.send(data_to_return);
	});	
}


this.driver_profile=function(req,res){
	var data_to_return={'status':0,'msg':'Invalid data','data':{}};
	
	//validating the data
	req.assert('driver_id','Invalid value').notEmpty();
	
	//validating the errors
	var errors=req.validationErrors();
	if(errors){	
		data_to_return['data']['error_msg']=errors[0]['msg'];
		res.send(data_to_return);
		return;
	}
	
	//sanitizating the data
	var driver_id=req.sanitize('driver_id').escape().trim();
	var async=require('async');
	var moment=require('moment');
	var fs = require('fs');
	
	async.waterfall([
	function(callback){
		var query="SELECT dr.id,comp.company_name as transporter_name,dr.name,dr.mobile_number,dr.address,dr.o_contact_number,dr.driver_vehicle_type,dr.driver_type,dr.aadhar_number,dr.pan_number,dr.license_number,dr.license_start_date,dr.license_validity_till_date ,dr.pan_number_upath,dr.aadhar_number_upath,dr.address_proof_upath,dr.driver_image,dr.driver_license_image1,dr.driver_license_image2 FROM driver dr join company comp on comp.id=dr.company_id where dr.id="+driver_id+" ";
		var driver_profile_data=function(error,result){
		if(error==1){
			res.send({status:500,msg:'Query failed'});
			return;
		}else{
			if (result==''){
				res.send({status: 400, msg: 'empty result'});
			}else{
				callback(null,result);
			}
		}
		}
		global.db_query.select_query_1(query,driver_profile_data);
	}
	],function(errors,results){
		async.parallel([
		function(callback){
			results[0].license_start_date=moment(results[0].license_start_date).format('YYYY-MM-DD HH:mm:ss');
			results[0].license_validity_till_date=moment(results[0].license_validity_till_date).format('YYYY-MM-DD HH:mm:ss');
			callback(null,1);
		},
		function(callback){
			//pan_number_upath
			if(results[0].pan_number_upath=="" ||results[0].pan_number_upath=='null'){
				results[0].pan_number_upath=default_image;	
				callback(null,1);				
			}else{
				var path=file_base_path;
				path+=results[0].pan_number_upath;			
				fs.exists(path,(exists)=>{		
				if (exists) {
					results[0].pan_number_upath=results[0].pan_number_upath;
					callback(null,1);
				}else{
					results[0].pan_number_upath=default_image;	
					callback(null,1);						
				}
				});
			}
		},		
		function(callback){
			//aadhar_number_upath
			if(results[0].aadhar_number_upath=="" ||results[0].aadhar_number_upath=='null'){
				results[0].aadhar_number_upath=default_image;	
				callback(null,1);				
			}else{
				var path=file_base_path;
				path+=results[0].aadhar_number_upath;
				fs.exists(path,(exists)=>{		
				if (exists) {
					results[0].aadhar_number_upath=results[0].aadhar_number_upath;
					callback(null,1);
				}else{
					results[0].aadhar_number_upath=default_image;	
					callback(null,1);						
				}
				});
			}
		},	
		function(callback){
			//address_proof_upath
			if(results[0].address_proof_upath=="" ||results[0].address_proof_upath=='null'){
				results[0].address_proof_upath=default_image;	
				callback(null,1);				
			}else{
				var path=file_base_path;
				path+=results[0].address_proof_upath;
				fs.exists(path,(exists)=>{		
				if (exists) {
					results[0].address_proof_upath=results[0].address_proof_upath;
					callback(null,1);
				}else{
					results[0].address_proof_upath=default_image;	
					callback(null,1);						
				}
				});
			}
		},	
		function(callback){
			//driver_image
			if(results[0].driver_image=="" ||results[0].driver_image=='null'){
				results[0].driver_image=default_image;	
				callback(null,1);				
			}else{
				var path=file_base_path;
				path+=results[0].driver_image;
				fs.exists(path,(exists)=>{		
				if (exists) {
					results[0].driver_image=results[0].driver_image;
					callback(null,1);
				}else{
					results[0].driver_image=default_image;	
					callback(null,1);						
				}
				});
			}
		},	
		function(callback){
			//driver_license_image1
			if(results[0].driver_license_image1=="" ||results[0].driver_license_image1=='null'){
				results[0].driver_license_image1=default_image;	
				callback(null,1);				
			}else{
				var path=file_base_path;
				path+=results[0].driver_license_image1;
				fs.exists(path,(exists)=>{		
				if (exists) {
					results[0].driver_license_image1=results[0].driver_license_image1;
					callback(null,1);
				}else{
					results[0].driver_license_image1=default_image;	
					callback(null,1);						
				}
				});
			}
		},
		function(callback){
			//driver_license_image2
			if(results[0].driver_license_image2=="" || results[0].driver_license_image2=='null'){
				results[0].driver_license_image2=default_image;	
				callback(null,1);				
			}else{
				var path=file_base_path;
				path+=results[0].driver_license_image2;
				fs.exists(path,(exists)=>{		
				if (exists) {
					results[0].driver_license_image2=results[0].driver_license_image2;
					callback(null,1);
				}else{
					results[0].driver_license_image2=default_image;	
					callback(null,1);						
				}
				});
			}
		}
		],function(errors1,results1){
			data_to_return['status']=200;
			data_to_return['msg']='success';
			data_to_return['data']=results[0];
			res.send(data_to_return);
		});
	});		
}


this.location=function(req,res){
	
	var data_to_return={'status':0,'msg':'Invalid Data','data':{}};
	
	var driver_id=req.sanitize('driver_id').escape().trim();

	var Promise=require("promise");
	var promises = [];
	var async=require('async');
	var moment=require('moment');
	var date=moment(new Date(Date.now())).format('YYYY-MM-DD HH:mm:ss');
	for(var key in req.body){
		promises.push(checkValue(req.body[key],key));
	}		 

Promise.all([promises])    
.then(function(data){
	data_to_return['status']=200;
	data_to_return['msg']="success";
	data_to_return['data']['driver_id']=driver_id;
	res.send(data_to_return); 
}).catch(function(err){ 
	console.log(err);  
});

var temp="";
function checkValue(value,key_val){
return new Promise(function(resolve, reject){
    console.log(value);
    console.log("driver_id--->"+driver_id);
	var provider="";
	var speed="";
	var altitude="";
	var bearing="";
	var latitude="";
	var longitude="";

	if(value.provider==undefined ||value.provider==""){
		value.provider="-";
	}
	if(value.speed==undefined ||value.speed==""){
		value.speed="-";
	}
	if(value.altitude==undefined ||value.altitude==""){
		value.altitude="-";
	}
	if(value.bearing==undefined ||value.bearing==""){
		value.bearing="-";
	}
	
	async.waterfall([
	function(callback){
		var query="SELECT ti.driver_id,ti.trip_status,'multi' as status FROM trip_info ti join order_aggregate_info oai on oai.trip_id=ti.trip_id where ti.driver_id='"+driver_id+"' and ti.is_deleted=0 and ti.trip_status=3 and oai.device_type=2 UNION SELECT moai.driver_id,moai.trip_status,'milk' as status FROM milk_order_aggregate_info moai where moai.driver_id='"+driver_id+"' and moai.is_deleted=0 and moai.trip_status=2 and moai.device_type=2";
		var location_data=function(error,result){
		if (error==1){
			res.send({status: 500, msg: 'Query failed'});
			return;
		}else{
		if(result==""){
			callback('error',1);
		}else{
			callback(null,result);
		}
		}
		}
		global.db_query.select_query_1(query,location_data)
	},
	function(arg,callback){
		var location_data1=function(error1,result1){
		if (error1==1){
			res.send({status: 500, msg: 'Query failed 1'});
			return;
		}else{
		if(result1==""){
			callback(null,value.latitude,value.longitude,0,0,0);
		}else{	
			var prev_lat=result1[0].latitude;
			var prev_lon=result1[0].longitude;
			var data_insertion_time=new Date(result1[0].data_insertion_time);
			var diff=moment(value.time).diff(data_insertion_time);
//			diff=moment.utc(diff.asMilliseconds()).format(":mm:ss");
//			console.log("diff--->"+diff);
			diff=Math.floor(diff/1000);
			console.log("diff--->"+diff);
//			var today=value.time;
//			today=today-(data_insertion_time);
//			today=today/1000;

//			today=Math.floor(today);
			//AT the speed of 65km/hr can travel 0.018055 km in 1sec
//							console.log("today-->"+today);
			dist_travel=0.010055*diff;
			console.log("prev_lat-->"+prev_lat);
			console.log("prev_lon-->"+prev_lon);
			console.log("latitude-->"+value.latitude);
			console.log("longitude-->"+value.longitude);
			
			var dist=calc_distance(prev_lat,prev_lon,value.latitude,value.longitude);
			console.log("Maximum distance that can  be covered--->"+dist_travel);
			console.log("total distance-->"+dist);
//			today<40&&dist>0.7222
			if(dist_travel<=dist){
				callback(null,value.latitude,value.longitude,dist,diff,1);
			}else{
			if(dist>0.15){
				callback(null,value.latitude,value.longitude,dist,diff,0);
			}else{
				callback(null,prev_lat,prev_lon,dist,diff,0);
			}
			}
		}
		}
		}
		global.db_query.select_query("mob_location_history","latitude,longitude,data_insertion_time","user_id=? and status="+0+" ",[driver_id],"order by data_insertion_time desc LIMIT 1",location_data1);
	},
	function(arg2,arg3,arg4,arg5,arg6,callback){
		console.log("arg6---->"+arg6);
		if(arg6==1){
		var location_insert_qry_data3=function(err3,result3){
		if (err3==1){
			res.send({status: 500, msg: 'Query failed 3'});
			return;
		}else{
		if(result3==""){
			res.send({status: 400, msg: 'No Result found 3'});
		}else{
			var count=0;
			for(let val in result3){
				var dist=calc_distance(result3[val].latitude,result3[val].longitude,value.latitude,value.longitude);
				var data_insertion_time=new Date(result3[val].data_insertion_time);
				console.log("data_insertion_time--->"+moment(data_insertion_time).format('YYYY-MM-DD HH:mm:ss'));
				console.log("time--->"+moment(value.time).format('YYYY-MM-DD HH:mm:ss'));
				var diff=moment(value.time).diff(data_insertion_time);
				diff=Math.floor(diff/1000);
				dist_travel=0.018055*diff;
				console.log("diff--->"+diff);
				console.log("dist--->"+dist);
				console.log("dist_travel--->"+dist_travel);
				if(dist_travel>dist){
					console.log(count);
					count++;
					if(count>=3){
						console.log("jtech");
						callback(null,arg2,arg3,arg4,arg5,0);
						return;
					}else if(val==result3.length-1){
						console.log("jtech1");
						callback(null,arg2,arg3,arg4,arg5,1);
						return;
					}
				}
			}
		}
		}
		}
		global.db_query.select_query("mob_location_history","latitude,longitude,data_insertion_time","user_id=? and status="+0+" ",[driver_id],"order by data_insertion_time desc LIMIT 5",location_insert_qry_data3,1);
		}else{
			callback(null,arg2,arg3,arg4,arg5,arg6);
		}
	},
	function(arg2,arg3,arg4,arg5,arg6,callback){
		console.log("arg2--->"+arg2);
		console.log("arg3--->"+arg3);
		console.log("arg4--->"+arg4);
		console.log("arg6--->"+arg6);
		console.log("distance---->"+arg5);
		latitude=arg2;
		longitude=arg3;
		var location_insert_qry_data3=function(err3,result3){
		if (err3==1){
			res.send({status: 500, msg: 'Query failed 3'});
			return;
		}else{
		if(result3==""){
			res.send({status: 400, msg: 'No Result found 3'});
		}else{
			console.log("inserted_id--->"+result3.insertId);
			if(arg6==1){
				callback('ERROR',result3.insertId);
			}else{
				
				callback(null,arg2,arg3);
			}
		}
		}
		}
		global.db_query.insert_query("mob_location_history",{'user_id':driver_id,'latitude':arg2,'actual_latitude':value.latitude,'longitude':arg3,'actual_longitude':value.longitude,'battery_percentage':value.batteryLevel,'data_insertion_time':moment(value.time).format('YYYY-MM-DD HH:mm:ss'),'current_time_now':date,'accuracy':value.accuracy,'distance':arg4,'time_interval':arg5,'provider':value.provider,"altitude":value.altitude,"speed":value.speed,"bearing":value.bearing,"status":arg6},location_insert_qry_data3,1);
	},
	 function(args1,args2,callback){
	 console.log("args1--->"+args1);
		var location_insert_qry_data=function(err,result){
		if (err==1){
			res.send({status: 500, msg: 'Query failed'});
			return;
		}else{
		if(result==""){
			callback(null,0,args1,args2);
		}else{
			callback(null,1,args1,args2)
		}
		}
		}
		global.db_query.select_query("device_port","driver_id,lat_message,lon_message","driver_id=?",[driver_id],"",location_insert_qry_data)
	 },
	 function(args3,args4,args5,callback){
		 console.log("args3-->"+args3);
		var location_insert_qry_data1=function(err1,result1){
		if (err1==1){
			res.send({status: 500, msg: 'Query failed 1'});
			return;
		}else{
		if(result1=="")
		{
			res.send({status: 400, msg: 'No Result found 1'});
		}else{
			callback(null,10)
		}
		}
		}
		if(args3==0){
			global.db_query.insert_query("device_port",{'lat_message':args4,'pre_lat_message':args4,'lon_message':args5,'pre_lon_message':args5,'battery_percentage':value.batteryLevel,'device_type':2,'driver_id':driver_id,'created_date':date,'pre_created_date':date},location_insert_qry_data1);
		}else if(args3==1){
			global.db_query.update_query("device_port","driver_id=?",[driver_id],{'lat_message':args4,'pre_lat_message':args4,'lon_message':args5,'pre_lon_message':args5,'battery_percentage':value.batteryLevel,'modified_date':date},location_insert_qry_data1);
		}		 
	 }
	],function(errors,results){
		console.log("errors--->"+errors);
		console.log("results");
		console.log(results);
	});			
	});	 
 }
}


function calc_distance(from_lat,from_lng,to_lat,to_lng){
	var theta = from_lng- to_lng;
	var dist = Math.sin(from_lat*(Math.PI/180)) * Math.sin(to_lat*(Math.PI/180)) +  Math.cos(from_lat*(Math.PI/180)) * Math.cos(to_lat*(Math.PI/180)) * Math.cos(theta*(Math.PI/180));
	dist = Math.acos(dist);
	dist = dist*(180/Math.PI);
	var miles = dist * 60 * 1.1515;
	miles=(miles * 1.609344).toFixed(2);
	return miles;
}


this.location_find = function(req,res){
	var data_to_return={'status':0,'msg':'Invalid Data','data':{}};

	req.assert('user_id', 'Invalid value').notEmpty();
	req.assert('device_id', 'Invalid value').notEmpty();
	
	// validating errors
	var errors = req.validationErrors();
	if (errors) {
		data_to_return['data']['error_msg']=errors[0]['msg'];
		// returning the error
		res.send(data_to_return);
		return;
	}
	// data sanitisation  
	var user_id=req.sanitize('user_id').escape().trim();
	var device_id=req.sanitize('device_id').escape().trim();
	
	var user_select_qry_data=function(err , result){
	if(err==1){
		res.send({status:500,msg:'Query failed'});
	}else{
	if (result==''){
		res.send({status: 400, msg: 'empty result'});
		return;
	}else{
		data_to_return['data']=result;
		data_to_return['status']=200;
		data_to_return['msg']="success";				
		res.send(data_to_return);
	}
	}
	}
	global.db_query.select_query("mob_location_history","id","user_id=? and device_id=?",[user_id,device_id],"order by id DESC LIMIT 5",user_select_qry_data);
}


this.location_insert = function(req,res){
//		console.log("jtech0");
		var data_to_return={'status':0,'msg':'Invalid Data','data':{}};

				console.log("jtech");
		//validating the data
		req.assert('driver_id', 'Invalid value').notEmpty();
		req.assert('latitude', 'Invalid value').notEmpty();
		req.assert('longitude', 'Invalid value').notEmpty();
				
		// validating errors
		var errors = req.validationErrors();
		if (errors) {
			data_to_return['data']['error_msg']=errors[0]['msg'];
			// returning the error
			res.send(data_to_return);
			return;
		}
//		console.log("jtech1");

		// data sanitisation
		var data_send={};
		var data1_send={};
		var driver_id=req.sanitize('driver_id').escape().trim();
		data_send['user_id']=driver_id;
		var latitude=req.sanitize('latitude').escape().trim();
		data_send['latitude']=latitude;
		var longitude=req.sanitize('longitude').escape().trim();
		data_send['longitude']=longitude;
		var date=new Date(Date.now());
		data_send['data_insertion_time']=date.toLocaleString();

		data1_send['device_type']='2';
		
		//insert the location details
		var location_insert_qry_data=function(err , result){
		if(err==1){
			res.send({status:500,msg:'Query Failed'});		
			}else{
		if (result==''){
			res.send({status: 400, msg: 'empty result'});
		return;}else{
			var location_insert_qry_data1=function(err1 , result1){
			if(err1==1){
				res.send({status:500,msg:'Query Failed 1'});
			}
			else{
			if (result1==''){
				data1_send['lat_message']=latitude;
				data1_send['pre_lat_message']=latitude;
				data1_send['lon_message']=longitude;
				data1_send['pre_lon_message']=longitude;

				data1_send['driver_id']=driver_id;
				data1_send['created_date']=date.toLocaleString();
				data1_send['pre_created_date']=date.toLocaleString();
				var location_insert_qry_data2=function(err2 , result2){
					if(err2==1){
						res.send({status:500,msg:'Query Failed 2'});
					}
					else{
				if (result2==''){
					res.send({status: 400, msg: 'empty result 2'});
				return;}else{
							data_to_return['data']['driver_id']=driver_id;
							data_to_return['status']=200;
							data_to_return['msg']="success";				
							res.send(data_to_return);
				}
				}			
				}
				global.db_query.insert_query("device_port",data1_send,location_insert_qry_data2);				
			return;}else{
			data1_send['modified_date']=date.toLocaleString();
			var pre_lat=result1[0].lat_message;
			var pre_lon=result1[0].lon_message;
			var distance=require('geo-distance');

			var pre_latlng={lon:pre_lon,lat:pre_lat};
			var latlng={lon:longitude,lat:latitude};

			var totaldistance=distance.between(pre_latlng,latlng);
			var dist=totaldistance.human_readable().distance;
			dist/=1000;
	//		console.log("jtech");
			if(dist>0.15){
				data1_send['lat_message']=latitude;
				data1_send['pre_lat_message']=latitude;
				data1_send['lon_message']=longitude;
				data1_send['pre_lon_message']=longitude;

			var location_insert_qry_data3=function(err3 , result3){
				if(err3==1){
					res.send({status:500,msg:'Query Failed 3'});
				}
				else{
				if (result3==''){
					res.send({status: 400, msg: 'empty result3'});
				return;}else{
							data_to_return['data']['driver_id']=driver_id;
							data_to_return['status']=200;
							data_to_return['msg']="success";				
							res.send(data_to_return);
				}
				}		
				}
				global.db_query.update_query("device_port","driver_id=?",[driver_id],data1_send,location_insert_qry_data3);
			}
			else{
				data1_send['lat_message']=pre_lat;
				data1_send['pre_lat_message']=pre_lat;
				data1_send['lon_message']=pre_lon;
				data1_send['pre_lon_message']=pre_lon;
				
				var location_insert_qry_data3=function(err3 , result3){
				if(err3==1){
					res.send({status:500,msg:'Query Failed 3'});
				}
				else{
				if (result3==''){
					res.send({status: 400, msg: 'empty result3'});
				return;}else{
							data_to_return['data']['driver_id']=driver_id;
							data_to_return['status']=200;
							data_to_return['msg']="success";				
							res.send(data_to_return);
				}
				}		
				}
				global.db_query.update_query("device_port","driver_id=?",[driver_id],data1_send,location_insert_qry_data3);
			}
			}
			}		
			}
			global.db_query.select_query("device_port","driver_id,lat_message,lon_message","driver_id=?",[driver_id],"",location_insert_qry_data1);
		}
		}		
		}
		global.db_query.insert_query("mob_location_history",data_send,location_insert_qry_data);
}


this.trip_status=function(req,res){
	var data_to_return={'status':0,'msg':'Invalid Data','data':{}};

	//setup validation
	
	req.assert('driver_id','Invalid value').notEmpty();
	
	// validating errors
	var errors = req.validationErrors();
	if (errors) {
		data_to_return['data']['error_msg']=errors[0]['msg'];
		// returning the error
		res.send(data_to_return);
		return;
	}

	//data sanitisation	
	var driver_id=req.sanitize('driver_id').escape().trim();
	var temp=[];
	
	var query="SELECT ti.driver_id,ti.trip_status as trip_status_code,'multi' as status FROM trip_info ti join order_aggregate_info oai on oai.trip_id=ti.trip_id where ti.driver_id="+driver_id+" and ti.is_deleted=0 and ti.trip_status=3 and oai.device_type=2 UNION SELECT moai.driver_id,moai.trip_status as trip_status_code,'milk' as status FROM milk_order_aggregate_info moai where moai.driver_id="+driver_id+" and moai.is_deleted=0 and moai.trip_status=2 and moai.device_type=2 ";
	var trip_status_data=function(error,result){
	if (error==1){
		res.send({status: 500, msg: 'Query failed'});
		return;
	}else{
		if(result==""){
			temp.push({'driver_id':driver_id,'trip_status_code':4,'trip_status':'Trip is ended','status':''});
			data_to_return['status']=200;
			data_to_return['msg']="success";
			data_to_return['data']=temp;
			res.send(data_to_return);
		}else{
			for(let val in result){
				if((result[val].trip_status_code==3 && result[val].status=='multi') || (result[val].trip_status_code==2 && result[val].status=='milk')){
					result[val].trip_status='Trip is started';
				}
				if(val==result.length-1){
					data_to_return['status']=200;
					data_to_return['msg']="success";
					data_to_return['data']=result;
					res.send(data_to_return);
				}
			}						
		}
	}
	}
	global.db_query.select_query_1(query,trip_status_data);
}


this.trip_start=function(req,res){
	var data_to_return={'status':0,'msg':'Invalid Data','data':{}};

	//Data validation
	req.assert('driver_id','Invalid value').notEmpty();

	var errors = req.validationErrors();
	if (errors) {
		data_to_return['data']['error_msg']=errors[0]['msg'];
		// returning the error
		res.send(data_to_return);
		return;
	}

	//Data Sanitization
	var driver_id=req.sanitize('driver_id').escape().trim();
	data_send={};
	
	var trip_start_data_rtn=function(err, result) {
	if (err==1){
		res.send({status: 500, msg: 'Query failed'});
		return;
	}else{
		if(result==""){
			res.send({status: 400, msg: 'No trip available'});
		}else{
		var trip_id=[];
		for(i=0;i<result.length;i++){
			trip_id.push(result[i].trip_id);
		}

		var date=new Date(Date.now());
		var str="";
		str+=date.getFullYear()+"-";
		if((date.getMonth())<9){
			str+="0"+(date.getMonth()+1)+"-";
		}
		else{
		str+=(date.getMonth()+1)+"-";
		}
		if((date.getDate())<10){
			str+="0"+date.getDate();
		}
		else{
		str+=date.getDate();
		}
		
		//Display the oa_id
		
		var trip_start_data1_rtn=function(err1, result1) {
			if (err1==1){
				res.send({status: 500, msg: 'Query failed 1'});
				return;
			}else{
				if(result1=="")
				{
					res.send({status: 400, msg: 'No trip available 1'});
				}
				else
				{
				var oa_id=[];
					for(i=0;i<result1.length;i++)
					{
						oa_id.push(result1[i].oa_id);
					}
//					console.log(oa_id);
				var trip_start_data2_rtn=function(err2, result2) {					
				if (err2==1){
					res.send({status: 500, msg: 'Query failed 2'});
				return;
				}else{
				if (result2==''){
					res.send({status: 400, msg: 'empty result 2'});
				return;
				}else{
					var oa1_id=result2[0].oa_id;
//						console.log(result2);
				var trip_start_data3_rtn=function(err3, result3) {					
				if (err3==1){
					res.send({status: 500, msg: 'Query failed 3'});
				return;
				}else{
					if (result3==''){
						res.send({status: 400, msg: 'empty result 3'});
						return;
					}else{
					var trip1_id=result3[0].trip_id;
					data_send['trip_status']='3';
					data_send['trip_start_datetime']=new Date(Date.now()).toLocaleString();

					var trip_start_data4_rtn=function(err4, result4) {
					if (err4==1){
						res.send({status: 500, msg: 'Query failed 4'});
					return;
					}else{
						if (result4==''){
						res.send({status: 400, msg: 'empty result 4'});
							return;
						}else{
						data_to_return['status']=200;
						data_to_return['msg']="success";
						data_to_return['data']['trip_id']=trip1_id;
						res.send(data_to_return);
					}
					}
					}					
					db_query.update_query("trip_info","trip_id=?",[trip1_id],data_send,trip_start_data4_rtn);
				}
				}
			}					
		global.db_query.select_query("order_aggregate_info","trip_id","oa_id=?",[oa1_id],"",trip_start_data3_rtn);

		}
		}
		}					
	global.db_query.select_query("order_info","oa_id","oa_id IN ("+oa_id+") and exp_pickup_datetime LIKE ?",[str+"%"],"",trip_start_data2_rtn);
	}
	}
	}
	global.db_query.select_query("order_aggregate_info","oa_id","trip_id IN ("+trip_id+") and vehicle_accepted_datetime LIKE ?",[str+"%"],"",trip_start_data1_rtn);
	}
	}
	}
	global.db_query.select_query("trip_info","trip_id","driver_id=? and is_deleted="+0+"",[driver_id],"",trip_start_data_rtn);					
}


this.dashboard=function(req,res){
	var data_to_return={'status':0,'msg':'Invalid value','data':{}};
	
	//validating the data
		req.assert('driver_id','Invalid value').notEmpty();
	
	// validating errors
	var errors = req.validationErrors();
	if (errors) {
		data_to_return['data']['error_msg']=errors[0]['msg'];
		// returning the error
		res.send(data_to_return);
		return;
	}
	
	//data sanitisation	
	var driver_id=req.sanitize('driver_id').escape().trim();
	var async=require('async');
	
	async.parallel([
	function(callback){
		//Active order count
		var query="SELECT count(*) as active_orders FROM order_aggregate_info oai join trip_info ti on ti.trip_id=oai.trip_id where ti.driver_id="+driver_id+" and ti.trip_status=3 and oai.trip_status=3 and ti.is_deleted=0 and oai.is_deleted=0 and ti.route_id!='0' and ti.route_id!='null' ";
		var active_order_data=function(error,result){
		if (error==1){
			res.send({status: 500, msg: 'Query failed..'});
			return;
		}else{
			if(result==""){
				res.send({status: 400, msg: 'No result found..'});
			}else{
				callback(null,result);
			}
		}
		}
		global.db_query.select_query_1(query,active_order_data);
	},
	function(callback){
		//complete order count
		var query1="SELECT count(*) as completed_orders FROM order_aggregate_info oai join trip_info ti on ti.trip_id=oai.trip_id where ti.driver_id="+driver_id+" and (ti.trip_status=3 || ti.trip_status=4) and oai.trip_status=4 and ti.is_deleted=0 and oai.is_deleted=0 and ti.route_id!='0' and ti.route_id!='null' ";
		var completed_order_data=function(error1,result1){
		if (error1==1){
			res.send({status: 500, msg: 'Query failed.. 1'});
			return;
		}else{
			if(result1==""){
				res.send({status: 400, msg: 'No result found..1'});
			}else{
				callback(null,result1);
			}
		}
		}
		global.db_query.select_query_1(query1,completed_order_data);
	},
	function(callback){
		//active milk order count
		var active_milk_order_data=function(error2,result2){
		if (error2==1){
			res.send({status: 500, msg: 'Query failed..2'});
			return;
		}else{
			if(result2==""){
				res.send({status: 400, msg: 'No result found..2'});
			}else{
				callback(null,result2);
			}
		}
		}
		global.db_query.select_query("milk_order_aggregate_info","count(*) as active_milk_orders","driver_id=? and trip_status=2 and is_deleted=0",[driver_id],"",active_milk_order_data);
	},
	function(callback){
		//completed milk order count
		var completed_milk_order_data=function(error3,result3){
		if (error3==1){
			res.send({status: 500, msg: 'Query failed..3'});
			return;
		}else{
			if(result3==""){
				res.send({status: 400, msg: 'No result found..3'});
			}else{
				callback(null,result3);
			}
		}
		}
		global.db_query.select_query("milk_order_aggregate_info","count(*) as completed_milk_orders","driver_id=? and trip_status=3 and is_deleted=0",[driver_id],"",completed_milk_order_data);
	}	
	],function(errors,results){
		data_to_return['status']=200;
		data_to_return['msg']="success";
		data_to_return['data']['active_order']=results[0][0].active_orders;
		data_to_return['data']['completed_order']=results[1][0].completed_orders;
		data_to_return['data']['active_milk_order']=results[2][0].active_milk_orders;
		data_to_return['data']['completed_milk_order']=results[3][0].completed_milk_orders;
		res.send(data_to_return);
	});
}


this.trip=function(req,res){
	var data_to_return={'status':0,'msg':'Invalid value','data':{}};
	
	//validating the data
		req.assert('driver_id','Invalid value').notEmpty();
	
	// validating errors
	var errors = req.validationErrors();
	if (errors) {
		data_to_return['data']['error_msg']=errors[0]['msg'];
		// returning the error
		res.send(data_to_return);
		return;
	}
	
	//data sanitisation	
	var driver_id=req.sanitize('driver_id').escape().trim();
	var moment=require('moment');
	
	var query="select oai.oa_id,oai.oa_number,oai.tonnage,mri.route_name,vd.vehicle_number,oai.device_type,ti.device_id,dp.running_no as device_num,dr.name as driver,oai.created_datetime,oai.trip_status as trip_status_code,oai.load_status from order_aggregate_info oai join mst_route_info mri on mri.route_id=oai.route_id join trip_info ti on ti.trip_id=oai.trip_id join vehicle_details vd on vd.id=ti.vehicle_id left join driver dr on dr.id=ti.driver_id left join device_port dp on dp.device_id=ti.device_id where ti.driver_id="+driver_id+" and oai.trip_status=1 and oai.is_deleted=0 and ti.is_deleted=0 group by oai.oa_id order by oai.oa_id desc ";
	var driver_assigned_order_list_data=function(error,result) {
	if (error==1){
		res.send({status: 500, msg: 'Query failed'});
		return;
	}else{
	if(result==""){
		res.send({status: 400, msg: 'No result found'});
	}else{
		for(let val in result){
			result[val].trip_status="trip is assigned";
			result[val].created_datetime=moment(result[val].created_datetime).format('YYYY-MM-DD HH:mm:ss');
			var device_type=result[val].device_type;
			var ti_device_id=result[val].device_id;
			
			if(result[val].load_status==0){
				result[val].load_name='Part load';
			}else if(result[val].load_status==1){
				result[val].load_name='Full load';
			}
					
			if(((device_type=="" ||device_type==undefined) && (ti_device_id!=''&&ti_device_id!=undefined && ti_device_id!=null) && ti_device_id!='0')|| (device_type==1 && ti_device_id!='0')){
				result[val].driver="";
			}else{
				result[val].device_num="";
			}			
			if(val==result.length-1){
				data_to_return['status']=200;
				data_to_return['msg']='success';
				data_to_return['data']=result;
				res.send(data_to_return);
			}
		}
	}
	}
	}
	global.db_query.select_query_1(query,driver_assigned_order_list_data);
}


this.driver_assigned_orders=function(req,res){
	var data_to_return={'status':0,'msg':'Invalid value','data':{}};
	
	//validating the data
		req.assert('oa_id','Invalid value').notEmpty();
	
	// validating errors
	var errors = req.validationErrors();
	if (errors) {
		data_to_return['data']['error_msg']=errors[0]['msg'];
		// returning the error
		res.send(data_to_return);
		return;
	}
	
	//data sanitisation	
	var oa_id=req.sanitize('oa_id').escape().trim();
	var moment=require('moment');
	
	var query="select oi.order_id,oi.order_number,oi.trip_status as order_status_code,oai.trip_status as trip_status_code,oi.created_datetime,oi.exp_pickup_datetime,oi.exp_delivery_datetime,(select city from company_plant_detail where id=oi.source_id)as loading_point,(select city from company_plant_detail where id=oi.destination_id)as unloading_point,oi.source_address_lat,oi.source_address_lng,oi.destination_address_lat,oi.destination_address_lng,oi.pod_status,ti.vehicle_assigned_datetime,oai.vehicle_accepted_datetime,oi.source_in_datetime,oi.source_out_datetime,oi.pod_signed_datetime from order_info oi join order_aggregate_info oai on oai.oa_id=oi.oa_id left join trip_info ti on ti.trip_id=oai.trip_id where oai.oa_id="+oa_id+" and oi.is_deleted=0 and oai.is_deleted=0 and oai.trip_status=1";
	var driver_assigned_orders_data=function(error,result){
	if (error==1){
		res.send({status: 500, msg: 'Query failed'});
		return;
	}else{
	if(result==""){
		res.send({status: 400, msg: 'No result found'});
	}else{	
		for(let val in result){
			result[val].Total_distance=calc_distance(result[val].source_address_lat,result[val].source_address_lng,result[val].destination_address_lat,result[val].destination_address_lng);
			result[val].created_datetime=moment(result[val].created_datetime).format('YYYY-MM-DD HH:mm:ss');
			result[val].exp_pickup_datetime=moment(result[val].exp_pickup_datetime).format('YYYY-MM-DD HH:mm:ss');
			result[val].exp_delivery_datetime=moment(result[val].exp_delivery_datetime).format('YYYY-MM-DD HH:mm:ss');
			result[val].created_date=moment(result[val].created_datetime).format('YYYY-MM-DD');
			result[val].vehicle_assigned_datetime=result[val].vehicle_assigned_datetime==null?"":moment(result[val].vehicle_assigned_datetime).format('YYYY-MM-DD');
			result[val].vehicle_accepted_datetime=result[val].vehicle_accepted_datetime==null?"":moment(result[val].vehicle_accepted_datetime).format('YYYY-MM-DD');
			result[val].source_in_datetime=result[val].source_in_datetime==null?"":moment(result[val].source_in_datetime).format('YYYY-MM-DD');
			result[val].source_out_datetime=result[val].source_out_datetime==null?"":moment(result[val].source_out_datetime).format('YYYY-MM-DD');
			result[val].pod_signed_datetime=result[val].pod_signed_datetime==null?"":moment(result[val].pod_signed_datetime).format('YYYY-MM-DD');
			if(val==result.length-1){
				data_to_return['status']=200;
				data_to_return['msg']='success';
				data_to_return['data']=result;
				res.send(data_to_return);	
			}
		}
	}
	}
	}
	global.db_query.select_query_1(query,driver_assigned_orders_data);
}


this.driver_active_order_list=function(req,res){
	var data_to_return={'status':0,'msg':'Invalid value','data':{}};
	
	//validating the data
	req.assert('driver_id','Invalid value').notEmpty();
	
	// validating errors
	var errors = req.validationErrors();
	if (errors) {
		data_to_return['data']['error_msg']=errors[0]['msg'];
		// returning the error
		res.send(data_to_return);
		return;
	}
	
	//data sanitisation	
	var driver_id=req.sanitize('driver_id').escape().trim();
	var moment=require('moment');
	
	var query="select oai.oa_id,oai.oa_number,oai.tonnage,mri.route_name,vd.vehicle_number,oai.device_type,ti.device_id,dp.running_no as device_num,(select battery_percentage from device_port where (modified_date > oai.vehicle_accepted_datetime and oai.device_type=1 and ti.device_id!=0 and running_no=dp.running_no)or (modified_date > oai.vehicle_accepted_datetime and driver_id=ti.driver_id and oai.device_type=2))as battery_percentage,dr.name as driver,oai.created_datetime,oai.trip_status as trip_status_code,oai.load_status from order_aggregate_info oai join mst_route_info mri on mri.route_id=oai.route_id join trip_info ti on ti.trip_id=oai.trip_id join vehicle_details vd on vd.id=ti.vehicle_id left join driver dr on dr.id=ti.driver_id left join device_port dp on dp.device_id=ti.device_id where ti.driver_id="+driver_id+" and oai.trip_status=3 and oai.is_deleted=0 and ti.is_deleted=0 group by oai.oa_id order by oai.oa_id desc ";
	var driver_active_order_list_data=function(error,result) {
	if (error==1){
		res.send({status: 500, msg: 'Query failed'});
		return;
	}else{
	if(result==""){
		res.send({status: 400, msg: 'No result found'});
	}else{
		for(let val in result){
			result[val].trip_status="trip is active";
			result[val].created_datetime=moment(result[val].created_datetime).format('YYYY-MM-DD HH:mm:ss');
			var device_type=result[val].device_type;
			var ti_device_id=result[val].device_id;
			var battery_percentage='';
			
			if(result[val].load_status==0){
				result[val].load_name='Part load';
			}else if(result[val].load_status==1){
				result[val].load_name='Full load';
			}
			
			if(result[val].battery_percentage!='null' && result[val].battery_percentage!='' && result[val].battery_percentage!=undefined){
				battery_percentage=' | '+result[val].battery_percentage +'%';
			}
			if(((device_type=="" ||device_type==undefined) && (ti_device_id!=''&&ti_device_id!=undefined && ti_device_id!=null) && ti_device_id!='0')|| (device_type==1 && ti_device_id!='0')){
				result[val].device_num+=battery_percentage;
				result[val].driver="";
			}else{
				result[val].driver+=battery_percentage;
				result[val].device_num="";
			}
			
			if(val==result.length-1){
				data_to_return['status']=200;
				data_to_return['msg']='success';
				data_to_return['data']=result;
				res.send(data_to_return);
			}
		}
	}
	}
	}
	global.db_query.select_query_1(query,driver_active_order_list_data);
}


this.driver_active_orders=function(req,res){
	var data_to_return={'status':0,'msg':'Invalid value','data':{}};
	
	//validating the data
		req.assert('oa_id','Invalid value').notEmpty();
	
	// validating errors
	var errors = req.validationErrors();
	if (errors) {
		data_to_return['data']['error_msg']=errors[0]['msg'];
		// returning the error
		res.send(data_to_return);
		return;
	}
	
	//data sanitisation	
	var oa_id=req.sanitize('oa_id').escape().trim();
	var moment=require('moment');
	
	var query="select oi.order_id,oi.order_number,oi.trip_status as order_status_code,oai.trip_status as trip_status_code,oi.created_datetime,oi.exp_pickup_datetime,oi.exp_delivery_datetime,(select city from company_plant_detail where id=oi.source_id)as loading_point,(select city from company_plant_detail where id=oi.destination_id)as unloading_point,oi.source_address_lat,oi.source_address_lng,oi.destination_address_lat,oi.destination_address_lng,oi.pod_status,ti.vehicle_assigned_datetime,oai.vehicle_accepted_datetime,oi.source_in_datetime,oi.source_out_datetime,oi.pod_signed_datetime,oi.destination_in_datetime,oi.destination_out_datetime from order_info oi join order_aggregate_info oai on oai.oa_id=oi.oa_id left join trip_info ti on ti.trip_id=oai.trip_id where oai.oa_id="+oa_id+" and oi.is_deleted=0 and oai.is_deleted=0 and oai.trip_status=3";
	var driver_active_orders_data=function(error,result){
	if (error==1){
		res.send({status: 500, msg: 'Query failed'});
		return;
	}else{
	if(result==""){
		res.send({status: 400, msg: 'No result found'});
	}else{	
		for(let val in result){
			result[val].Total_distance=calc_distance(result[val].source_address_lat,result[val].source_address_lng,result[val].destination_address_lat,result[val].destination_address_lng);
			result[val].created_datetime=moment(result[val].created_datetime).format('YYYY-MM-DD HH:mm:ss');
			result[val].exp_pickup_datetime=moment(result[val].exp_pickup_datetime).format('YYYY-MM-DD HH:mm:ss');
			result[val].exp_delivery_datetime=moment(result[val].exp_delivery_datetime).format('YYYY-MM-DD HH:mm:ss');
			result[val].created_date=moment(result[val].created_datetime).format('YYYY-MM-DD');
			result[val].vehicle_assigned_datetime=result[val].vehicle_assigned_datetime==null?"":moment(result[val].vehicle_assigned_datetime).format('YYYY-MM-DD');
			result[val].vehicle_accepted_datetime=result[val].vehicle_accepted_datetime==null?"":moment(result[val].vehicle_accepted_datetime).format('YYYY-MM-DD');
			if(result[val].order_status_code==4){
				var start = moment.utc(result[val].source_in_datetime, "YYYY-MM-DD HH:mm:ss");
				var end = moment.utc(result[val].source_out_datetime, "YYYY-MM-DD HH:mm:ss");
				var d = moment.duration(end.diff(start));
				result[val].loading_time=moment.utc(+d).format('HH:mm:ss');
			}
			result[val].source_in_datetime=result[val].source_in_datetime==null?"":moment(result[val].source_in_datetime).format('YYYY-MM-DD');
			result[val].source_out_datetime=result[val].source_out_datetime==null?"":moment(result[val].source_out_datetime).format('YYYY-MM-DD');
			result[val].pod_signed_datetime=result[val].pod_signed_datetime==null?"":moment(result[val].pod_signed_datetime).format('YYYY-MM-DD');
			if(val==result.length-1){
				data_to_return['status']=200;
				data_to_return['msg']='success';
				data_to_return['data']=result;
				res.send(data_to_return);	
			}
		}
	}
	}
	}
	global.db_query.select_query_1(query,driver_active_orders_data);
}


this.driver_completed_order_list=function(req,res){
	var data_to_return={'status':0,'msg':'Invalid value','data':{}};
	
	//validating the data
		req.assert('driver_id','Invalid value').notEmpty();
	
	// validating errors
	var errors = req.validationErrors();
	if (errors) {
		data_to_return['data']['error_msg']=errors[0]['msg'];
		// returning the error
		res.send(data_to_return);
		return;
	}
	
	//data sanitisation	
	var driver_id=req.sanitize('driver_id').escape().trim();
	var moment=require('moment');
	
	var query="select oai.oa_id,oai.oa_number,oai.tonnage,mri.route_name,vd.vehicle_number,oai.device_type,ti.device_id,dp.running_no as device_num,dr.name as driver,oai.created_datetime,oai.trip_status as trip_status_code,oai.load_status from order_aggregate_info oai join mst_route_info mri on mri.route_id=oai.route_id join trip_info ti on ti.trip_id=oai.trip_id join vehicle_details vd on vd.id=ti.vehicle_id left join driver dr on dr.id=ti.driver_id left join device_port dp on dp.device_id=ti.device_id where ti.driver_id="+driver_id+" and oai.trip_status=4 and oai.is_deleted=0 and ti.is_deleted=0 group by oai.oa_id order by oai.oa_id desc ";
	var driver_completed_order_list_data=function(error,result) {
	if (error==1){
		res.send({status: 500, msg: 'Query failed'});
		return;
	}else{
	if(result==""){
		res.send({status: 400, msg: 'No result found'});
	}else{
		for(let val in result){
			result[val].trip_status="trip is completed";
			result[val].created_datetime=moment(result[val].created_datetime).format('YYYY-MM-DD HH:mm:ss');
			var device_type=result[val].device_type;
			var ti_device_id=result[val].device_id;
			
			if(result[val].load_status==0){
				result[val].load_name='Part load';
			}else if(result[val].load_status==1){
				result[val].load_name='Full load';
			}
					
			if(((device_type=="" ||device_type==undefined) && (ti_device_id!=''&&ti_device_id!=undefined && ti_device_id!=null) && ti_device_id!='0')|| (device_type==1 && ti_device_id!='0')){
				result[val].driver="";
			}else{
				result[val].device_num="";
			}			
			if(val==result.length-1){
				data_to_return['status']=200;
				data_to_return['msg']='success';
				data_to_return['data']=result;
				res.send(data_to_return);
			}
		}
	}
	}
	}
	global.db_query.select_query_1(query,driver_completed_order_list_data);
}


this.driver_completed_orders=function(req,res){
	var data_to_return={'status':0,'msg':'Invalid value','data':{}};
	
	//validating the data
		req.assert('oa_id','Invalid value').notEmpty();
	
	// validating errors
	var errors = req.validationErrors();
	if (errors) {
		data_to_return['data']['error_msg']=errors[0]['msg'];
		// returning the error
		res.send(data_to_return);
		return;
	}
	
	//data sanitisation	
	var oa_id=req.sanitize('oa_id').escape().trim();
	var moment=require('moment');
	
	var query="select oi.order_id,oi.order_number,oi.trip_status as order_status_code,oai.trip_status as trip_status_code,oi.created_datetime,oi.exp_pickup_datetime,oi.exp_delivery_datetime,(select city from company_plant_detail where id=oi.source_id)as loading_point,(select city from company_plant_detail where id=oi.destination_id)as unloading_point,oi.source_address_lat,oi.source_address_lng,oi.destination_address_lat,oi.destination_address_lng,oi.pod_status,ti.vehicle_assigned_datetime,oai.vehicle_accepted_datetime,oi.source_in_datetime,oi.source_out_datetime,oi.pod_signed_datetime,oi.destination_in_datetime,oi.destination_out_datetime from order_info oi join order_aggregate_info oai on oai.oa_id=oi.oa_id left join trip_info ti on ti.trip_id=oai.trip_id where oai.oa_id="+oa_id+" and oi.is_deleted=0 and oai.is_deleted=0 and oai.trip_status=4";
	var driver_completed_orders_data=function(error,result){
	if (error==1){
		res.send({status: 500, msg: 'Query failed'});
		return;
	}else{
	if(result==""){
		res.send({status: 400, msg: 'No result found'});
	}else{	
		for(let val in result){
			result[val].Total_distance=calc_distance(result[val].source_address_lat,result[val].source_address_lng,result[val].destination_address_lat,result[val].destination_address_lng);
			result[val].created_datetime=moment(result[val].created_datetime).format('YYYY-MM-DD HH:mm:ss');
			result[val].exp_pickup_datetime=moment(result[val].exp_pickup_datetime).format('YYYY-MM-DD HH:mm:ss');
			result[val].exp_delivery_datetime=moment(result[val].exp_delivery_datetime).format('YYYY-MM-DD HH:mm:ss');
			result[val].created_date=moment(result[val].created_datetime).format('YYYY-MM-DD');
			result[val].vehicle_assigned_datetime=result[val].vehicle_assigned_datetime==null?"":moment(result[val].vehicle_assigned_datetime).format('YYYY-MM-DD');
			result[val].vehicle_accepted_datetime=result[val].vehicle_accepted_datetime==null?"":moment(result[val].vehicle_accepted_datetime).format('YYYY-MM-DD');
			if(result[val].order_status_code>=5){
				var start = moment.utc(result[val].source_in_datetime, "YYYY-MM-DD HH:mm:ss");
				var end = moment.utc(result[val].source_out_datetime, "YYYY-MM-DD HH:mm:ss");
				var d = moment.duration(end.diff(start));
				result[val].loading_time=moment.utc(+d).format('HH:mm:ss');
			}
			if(result[val].order_status_code==6){
				var start1 = moment(result[val].destination_in_datetime, "YYYY-MM-DD HH:mm:ss");
				var end1 = moment(result[val].destination_out_datetime, "YYYY-MM-DD HH:mm:ss");
				var d1 = moment.duration(end1.diff(start1));
				result[val].delivery_time=moment.utc(+d1).format('HH:mm:ss');
			}
			result[val].source_in_datetime=result[val].source_in_datetime==null?"":moment(result[val].source_in_datetime).format('YYYY-MM-DD');
			result[val].source_out_datetime=result[val].source_out_datetime==null?"":moment(result[val].source_out_datetime).format('YYYY-MM-DD');
			result[val].destination_in_datetime=result[val].destination_in_datetime==null?"":moment(result[val].destination_in_datetime).format('YYYY-MM-DD');
			result[val].destination_out_datetime=result[val].destination_out_datetime==null?"":moment(result[val].destination_out_datetime).format('YYYY-MM-DD');
			result[val].pod_signed_datetime=result[val].pod_signed_datetime==null?"":moment(result[val].pod_signed_datetime).format('YYYY-MM-DD');
			if(val==result.length-1){
				data_to_return['status']=200;
				data_to_return['msg']='success';
				data_to_return['data']=result;
				res.send(data_to_return);	
			}
		}
	}
	}
	}
	global.db_query.select_query_1(query,driver_completed_orders_data);
}


this.order_edit=function(req,res){
	var data_to_return={'status':0,'msg':'Invalid Data','data':{}};
	
	//validating the data
	req.assert('order_id','Invalid value').notEmpty();
	
	//Validating the errors
	var errors=req.validationErrors();
	if(errors){
		data_to_return['data']['error_msg']=errors[0]['msg'];
		res.send(data_to_return);
	}
	
	//Sanitizing the data
	var order_id=req.sanitize('order_id').escape().trim();
	var async=require('async');
	var moment=require('moment');
	
	async.parallel([
	function(callback){
		var order_edit_data_rtn=function(error,result){ 
		if(error==1){
			res.send({'status':500,'msg':'Query Failed'});
		}else{
			if(result==""){
				res.send({'status':400,'msg':'Empty Result'});
			}else{
				callback(null,result);
			}
			}
		}
		global.db_query.select_query("order_info","order_id,oa_id,order_number,source_id,destination_id,exp_pickup_datetime,exp_delivery_datetime,tonnage,delivery_type,trip_status,(select load_status from order_aggregate_info oa_info where oa_info.oa_id=order_info.oa_id) as load_status","order_id=?",[order_id],"",order_edit_data_rtn);
	},
	function(callback){
		var order_edit_data1_rtn=function(error1,result1){
		if(error1==1){
			res.send({'status':500,'msg':'Query Failed1'});
		}else{
			if(result1==""){
				callback(null,[]);
			}else{
				callback(null,result1);
			}
		}
		}
		global.db_query.select_query("order_items","id as order_item_id,material,quantity,weight,llength,lwidth,lheight,package_count,is_stackable,lwh_unit","order_id=?",[order_id],"",order_edit_data1_rtn);
	}
	],function(errors,results){
		data_to_return['status']=200;
		data_to_return['msg']='success';
		data_to_return['data']['order_id']=results[0][0].order_id;
		data_to_return['data']['oa_id']=results[0][0].oa_id;
		data_to_return['data']['order_number']=results[0][0].order_number;
		data_to_return['data']['exp_pickup_datetime']=moment(results[0][0].exp_pickup_datetime).format('YYYY-MM-DD HH:mm:ss');
		data_to_return['data']['exp_delivery_datetime']=moment(results[0][0].exp_delivery_datetime).format('YYYY-MM-DD HH:mm:ss');
		data_to_return['data']['tonnage']=results[0][0].tonnage;
		data_to_return['data']['delivery_type']=results[0][0].delivery_type;
		data_to_return['data']['order_status_code']=results[0][0].trip_status;
		data_to_return['data']['load_status']=results[0][0].load_status;
		if(results[0][0].load_status==0){
			data_to_return['data']['load_name']='Part load';
		}else if(results[0][0].load_status==1){
			data_to_return['data']['load_name']='Full load';
		}
		var source_id=results[0][0].source_id;
		var destination_id=results[0][0].destination_id;
		
		async.parallel([
		function(callback){
			var query="select cpd.company_id as source_id,comp.company_name as source,cpd.id as source_plant_id,cpd.name as source_plant,cpd.geo_location_address as source_address,cpd.mobile_number as source_phone_number from company_plant_detail cpd join company comp on comp.id=cpd.company_id where cpd.id="+source_id+"";
			var order_edit_data2_rtn=function(error2,result2){
			if(error2==1){
				res.send({'status':500,'msg':'Query Failed2'});
			}else{
				if(result2==""){
					res.send({'status':400,'msg':'Empty Result2'});
				}else{
					callback(null,result2);
				}
			}
			}
			global.db_query.select_query_1(query,order_edit_data2_rtn);
		},
		function(callback){
			var query="select cpd.company_id as destination_id,comp.company_name as destination,cpd.id as destination_plant_id,cpd.name as destination_plant,cpd.geo_location_address as destination_address,cpd.mobile_number as destination_phone_number from company_plant_detail cpd join company comp on comp.id=cpd.company_id where cpd.id="+destination_id+"";
			var order_edit_data3_rtn=function(error3,result3){
			if(error3==1){
				res.send({'status':500,'msg':'Query Failed3'});
			}else{
				if(result3==""){
					res.send({'status':400,'msg':'Empty Result3'});
				}else{
					callback(null,result3);
				}
			}
			}
			global.db_query.select_query_1(query,order_edit_data3_rtn);
		},
		],function(errors1,results1){
			data_to_return['data']['source_id']=results1[0][0].source_id;
			data_to_return['data']['source']=results1[0][0].source;
			data_to_return['data']['source_plant_id']=results1[0][0].source_plant_id;
			data_to_return['data']['source_plant']=results1[0][0].source_plant;
			data_to_return['data']['source_address']=results1[0][0].source_address;
			data_to_return['data']['source_phone_number']=results1[0][0].source_phone_number;
			if(results1[0][0].source_plant==null || results1[0][0].source_plant==""){
				data_to_return['data']['source_geo_status']=0;
			}else{
				data_to_return['data']['source_geo_status']=1;
			}
			data_to_return['data']['destination_id']=results1[1][0].destination_id;
			data_to_return['data']['destination']=results1[1][0].destination;
			data_to_return['data']['destination_plant_id']=results1[1][0].destination_plant_id;
			data_to_return['data']['destination_plant']=results1[1][0].destination_plant;
			data_to_return['data']['destination_address']=results1[1][0].destination_address;
			data_to_return['data']['destination_phone_number']=results1[1][0].destination_phone_number;
			if(results1[1][0].destination_plant==null || results1[1][0].destination_plant==""){
				data_to_return['data']['destination_geo_status']=0;
			}else{
				data_to_return['data']['destination_geo_status']=1;
			}		
			data_to_return['data']['order_items']=results[1];
			res.send(data_to_return);
		});
	});	
}


this.update_lr=function(req,res){
	var data_to_return={'status':0,'msg':'Invalid Data','data':{}};

	//Data validation
	req.assert('order_id','Invalid order_id').notEmpty();
	req.assert('invoice_no','Invalid invoice_no').notEmpty();
	req.assert('invoice_value','Invalid invoice_value').notEmpty();
	req.assert('invoice_date','Invalid invoice_date').notEmpty();
	req.assert('insurance_value','Invalid insurance_value').notEmpty();
	
	//validating the error
	var errors = req.validationErrors();
	if (errors) {
		data_to_return['data']['error_msg']=errors[0]['msg'];
		// returning the error
		res.send(data_to_return);
		return;
	}
	
	var async=require('async');
	var moment=require('moment');
	var inr=0;
	var	item_tonnage=0;
	var data_send={};

   //Data Sanitization
	var order_id=req.sanitize('order_id').escape().trim();
	data_send['invoice_no']=req.sanitize('invoice_no').escape().trim();
	data_send['invoice_value']=req.sanitize('invoice_value').escape().trim();
	data_send['invoice_date']=req.sanitize('invoice_date').escape().trim();
	data_send['insurance_value']=req.sanitize('insurance_value').escape().trim();
	
	if(req.body.order_items!=undefined && req.body.order_items!=''){
	var total_orders_items=req.body.order_items.length;
		for(var j = 0;j<total_orders_items;j++){
			item_tonnage+=Number(req.body.order_items[j].weight);
		}
	}
	
	async.parallel([
	function(callback){
		var update_data_rtn2=function(error2, result2) {
		if (error2==1){
			res.send({status: 500, msg: 'Query failed 2'});
			return;
		}else{
			if(result2==""){
				res.send({status: 400, msg: 'No Result found 2'});	
			}else{
				
				callback(null,result2[0].tonnage);
			}
		}
		}	
		global.db_query.select_query("order_info","tonnage","order_id=?",[order_id],"",update_data_rtn2);
	}
	],function(errors1,results1){
		var order_tonnage=results1[0];
		if(item_tonnage>order_tonnage){
			res.send({status: 500, msg: '\n Order Tonnage Mismatch With Order Item Tonne'});
			return;
		}else{
		
		async.parallel([
		function(callback){
			var update_data_rtn=function(error, result) {
			if (error==1){
				res.send({status: 500, msg: 'Query failed'});
				return;
			}else{
				if(result!=""){
					callback(null,1);	
				}			
				}
			}					
			db_query.update_query("order_info","order_id=? ",[order_id],data_send,update_data_rtn);
		},
		function(callback){
			if(req.body.order_items!=undefined && req.body.order_items.length>0 ){
			for(var val in req.body.order_items){
				var update_data_rtn_1=function(error1, result1) {
				if (error1==1){
					res.send({status: 500, msg: 'Query failed 1'});
					return;
				}else{
					if(result1!=""){
							inr++;
						if(val==req.body.order_items.length-1){
							callback(null,1);
						}
					}			
					}
				}		
				db_query.update_query("order_items","id=?",[req.body.order_items[val].order_items_id],{'material':req.body.order_items[val].material,'weight':req.body.order_items[val].weight,'quantity':req.body.order_items[val].quantity,'package_count':req.body.order_items[val].package_count},update_data_rtn_1);
			}
			}else{
				callback(null,1);
			}
		},
		],function(errors,results){		
			data_to_return['status']=200;
			data_to_return['msg']="success";
			data_to_return['data']['order_id']=order_id;
			res.send(data_to_return);
		});					
		}
	});	
}


this.update_status=function(req,res){
	var data_to_return={'status':0,'msg':'Invalid Data','data':{}};

	//Data validation
	req.assert('oa_id','Invalid value').notEmpty();
	req.assert('reporting_time','Invalid value').notEmpty();
	req.assert('accept_order','Invalid value').notEmpty();
	
	//validating the error
	var errors = req.validationErrors();
	if (errors){
		data_to_return['data']['error_msg']=errors[0]['msg'];
		// returning the error
		res.send(data_to_return);
		return;
	}

	//Data Sanitization
	var oa_id=req.sanitize('oa_id').escape().trim();
	var reporting_time=req.sanitize('reporting_time').escape().trim();
	var accept_order=req.sanitize('accept_order').escape().trim();
	
	var async=require('async');
	var moment=require('moment');
	var data_send={};
	var date=moment(new Date(Date.now())).format("YYYY-MM-DD HH:mm:ss");
	
	async.waterfall([
	function(callback){
		var query="select ti.trip_id,ti.trip_status as ti_trip_status,ti.available_tonnage,oai.tonnage as oa_tonnage,oai.created_by,ti.vehicle_id,vd.tonnage as vehicle_tonnage,ti.driver_id FROM order_aggregate_info oai LEFT JOIN trip_info ti on oai.trip_id=ti.trip_id LEFT JOIN vehicle_details vd on vd.id=ti.vehicle_id where oai.oa_id="+oa_id+" ";
		var update_status_data=function(error,result){
		if(error==1){
			res.send({'status':500,'msg':'Query Failed'});
		}else{
			if(result==""){
				res.send({'status':400,'msg':'No result found'});
			}else{
				callback(null,result);
			}
		}
		}
		global.db_query.select_query_1(query,update_status_data);
	}
	],function(errors,results){
		var trip_id=results[0].trip_id;
		var trip_status=results[0].ti_trip_status;
//		var available_tonnage=results[0].available_tonnage-results[0].available_tonnage;
		var vehicle_id=results[0].vehicle_id;
		var driver_id=results[0].driver_id;
		var created_by=results[0].created_by;
	
	if(accept_order==1){
		async.parallel([
		function(callback){
			var update_status_data1=function(error1,result1){
			if(error1==1){
				res.send({'status':500,'msg':'Query Failed 1'});
			}else{
				callback(null,result1);
			}
			}
			global.db_query.select_query("vehicle_details","device_id,device_type,otrip_status","id=?",[vehicle_id],"",update_status_data1);
		},
		function(callback){
			var query1="select ((select count(*) from trip_info ti where ti.driver_id="+driver_id+" and ti.trip_status='3' and ti.is_deleted='0' and ti.trip_id!="+trip_id+" )+(select count(*) from milk_order_aggregate_info moai where moai.driver_id="+driver_id+" and moai.trip_status='2' and moai.is_deleted='0')) as active_order";
			var update_status_data2=function(error2,result2){
			if(error2==1){
				res.send({'status':500,'msg':'Query Failed 2'});
			}else{
				callback(null,result2);
			}	
			}
			global.db_query.select_query_1(query1,update_status_data2);
		}
		],function(errors1,results1){
			if(results1[0][0].otrip_status==1 && trip_status!=3){
				res.send({'status':400,'msg':'Please assign another vehicle, Assigned vehicle has been accepted by other order.'});
				return;
			}else if(results1[1][0].active_order>0){
				res.send({'status':400,'msg':'Please assign another Driver, Assigned Driver has been accepted by other order.'});
				return;
			}else if( results1[1][0].active_order==0 && (results1[0][0].otrip_status==0 || trip_status==3)){
				async.parallel([
				function(callback){
					var update_status_data3=function(error3,result3){
					if(error3==1){
						res.send({'status':500,'msg':'Query Failed 3'});
					}else{
						if(result3==""){
							res.send({'status':400,'msg':'No result found 3'});
						}else{
							callback(null,result3);
						}
					}	
					}
					global.db_query.update_query("trip_info","trip_id=? and trip_status!=4",[trip_id],{"device_id":results1[0][0].device_id,"trip_status":3,"trip_start_datetime":date},update_status_data3);
				},
				function(callback){
					data_send['otrip_status']=1;
					data_send['oid']=trip_id;
					if(results1[0][0].device_type==2){
						data_send['driver_id']=driver_id;
					}
					var update_status_data4=function(error4,result4){
					if(error4==1){
						res.send({'status':500,'msg':'Query Failed 4'});
					}else{
						if(result4==""){
							res.send({'status':400,'msg':'No result found 4'});
						}else{
							callback(null,result4);
						}
					}	
					}
					global.db_query.update_query("vehicle_details","id=? and otrip_status="+0+" ",[vehicle_id],data_send,update_status_data4);
				},
				function(callback){
					var update_status_data5=function(error5,result5){
					if(error5==1){
						res.send({'status':500,'msg':'Query Failed 5'});
					}else{
						if(result5==""){
							res.send({'status':400,'msg':'No result found 5'});
						}else{
							callback(null,result5);
						}
					}	
					}
					global.db_query.update_query("order_aggregate_info","oa_id=? ",[oa_id],{"reporting_time":reporting_time,"trip_status":3,"vehicle_accepted_datetime":date,'driver_acceptance_status':1,'device_type':results1[0][0].device_type},update_status_data5);
				},
				function(callback){
					var update_status_data6=function(error6,result6){
					if(error6==1){
						res.send({'status':500,'msg':'Query Failed 6'});
					}else{
						if(result6==""){
							res.send({'status':400,'msg':'No result found 6'});
						}else{
							callback(null,result6);
						}
					}	
					}
					global.db_query.update_query("order_info","oa_id=? ",[oa_id],{"trip_status":2},update_status_data6)
				},
				],function(errors2,results2){
					data_to_return['status']=200;
					data_to_return['msg']='success';
					data_to_return['data']['oa_id']=oa_id;
					res.send(data_to_return);
				});
			}else{
				res.send({'status':400,'msg':'Please assign another vehicle, Assigned vehicle has been accepted by other order.'});
				return;
			}
		});
	}else{
		var available_tonnage=results[0].available_tonnage+results[0].oa_tonnage;
		async.parallel([
		function(callback){
			var update_status_function4=function(err4,result4){
			if(err4==1){
				res.send({'status':500,'msg':'Query Failed4'});
			}else{
				if(result4==""){
					res.send({'status':400,'msg':'No Result found 4'});
				}else{
					callback(null,result4);
				}
			}
			}
			global.db_query.update_query("order_aggregate_info","oa_id=? ",[oa_id],{"trip_id":0,"trip_status":0},update_status_function4);
		},
		function(callback){
			var update_status_function5=function(err5,result5){
			if(err5==5){
				res.send({'status':500,'msg':'Query Failed5'});
			}else{
				if(result5==""){
					res.send({'status':400,'msg':'No Result found 5'});
				}else{
					callback(null,result5);
				}
			}
			}
			global.db_query.update_query("order_info","oa_id=? ",[oa_id],{"trip_status":0},update_status_function5)
		},function(callback){
			var update_status_function6=function(error6,result6){
			if(error6==1){
				res.send({'status':500,'msg':'Query Failed 6'});
			}else{
				if(result6==""){
					res.send({'status':400,'msg':'No Result found 6'});
				}else{
					callback(null,result6);
				}
			}
			}
			global.db_query.update_query("trip_info","trip_id=? ",[trip_id],{"available_tonnage":available_tonnage},update_status_function6)
		},
		function(callback){
			var update_status_function7=function(error7,result7){
			if(error7==1){
				res.send({'status':500,'msg':'Query Failed 7'});
			}else{
				if(result7==""){
					res.send({'status':400,'msg':'No Result found 7'});
				}else{
					callback(null,result7);
				}
			}
			}
			global.db_query.select_query("order_aggregate_info","count(*) as order_count","trip_id=? ",[trip_id],"",update_status_function7);
		}
		],function(error,results){
			var order_count=results[3][0].order_count;
			async.parallel([
			function(callback){
			if(!order_count>0){
				var update_status_function8=function(error8,result8){
				if(error8==1){
					res.send({'status':500,'msg':'Query Failed 8'});
				}else{
					if(result8==""){
						res.send({'status':400,'msg':'No Result found 8'});
					}else{
						callback(null,result8);
					}
				}
				}
				global.db_query.update_query("vehicle_details","id=? ",[vehicle_id],{"otrip_status":0,"oid":0},update_status_function8)
			}else{
				callback(null,1);
			}
			},
			function(callback){
			if(!order_count>0){
				var update_status_function9=function(error9,result9){
				if(error9==1){
					res.send({'status':500,'msg':'Query Failed 9'});
				}else{
					if(result9==""){
						res.send({'status':400,'msg':'No Result found 9'});
					}else{
						callback(null,result9);
					}
				}
				}
				global.db_query.update_query("trip_info","trip_id=? ",[trip_id],{"is_deleted":1},update_status_function9)
			}else{
				callback(null,1);
			}
			},
			],function(errors,results){
				data_to_return['status']=200;
				data_to_return['msg']='success';
				data_to_return['data']['oa_id']=oa_id;
				res.send(data_to_return);
			});	
		});
	}
	});
}


this.truck_details=function(req,res){
	var data_to_return={'status':0,'msg':'Invalid data','data':{}};
	
	//validating the data
	req.assert('oa_id','Invalid value').notEmpty();
	
	//validating the errors
	var errors=req.validationErrors();
	if(errors){
		data_to_return['data']['error_msg']=errors[0]['msg'];
		res.send(data_to_return);
		return;
	}
	
	//Sanitize the data
	var oa_id=req.sanitize('oa_id').escape().trim();
	var async=require('async');
	var fs = require('fs');
	
	async.parallel([
	function(callback){
		var query="SELECT vd.vehicle_number,veh.vehicle_type ,vd.rcbook_upath,vd.insurance_upath from vehicle_details vd join trip_info ti on ti.vehicle_id=vd.id join order_aggregate_info oai on oai.trip_id=ti.trip_id join vehicle_type veh on veh.id=vd.vehicle_type_id where oai.oa_id="+oa_id+"";
		var truck_details_data=function(error,result){
		if(error==1){
			res.send({'status':500,'msg':'Query Failed '});
		}else{
			if(result==""){
				res.send({'status':400,'msg':'No Result found '});
			}else{
				callback(null,result);
			}
		}	
		}
		global.db_query.select_query_1(query,truck_details_data);
	},
	function(callback){
		var query1="SELECT dr.name,dr.mobile_number,dr.driver_license_image1 from driver dr join trip_info ti on ti.driver_id=dr.id join order_aggregate_info oai on oai.trip_id=ti.trip_id where oai.oa_id="+oa_id+"";
		var truck_details_data1=function(error1,result1){
		if(error1==1){
			res.send({'status':500,'msg':'Query Failed 1'});
		}else{
			if(result1==""){
				res.send({'status':400,'msg':'No Result found 1'});
			}else{
				callback(null,result1);
			}
		}	
		}
		global.db_query.select_query_1(query1,truck_details_data1,1);
	}
	],function(errors,results){
		async.parallel([
		function(callback){
			//insurance_upath
			if(results[0][0].insurance_upath=="" ||results[0][0].insurance_upath=='null'){
				results[0][0].insurance_upath=default_image;	
				callback(null,1);				
			}else{
				var path=file_base_path;
				path+=results[0][0].insurance_upath;				
				fs.exists(path,(exists)=>{					
				if(exists) {
					results[0][0].insurance_upath=results[0][0].insurance_upath;
					callback(null,1);
				}else{
					results[0][0].insurance_upath=default_image;	
					callback(null,1);						
				}
				});
			}


		},
		function(callback){	
			//rcbook_upath
			if(results[0][0].rcbook_upath=="" ||results[0][0].rcbook_upath=='null'){
				results[0][0].rcbook_upath=default_image;	
				callback(null,1);
			}else{
				var path=file_base_path;
				path+=results[0][0].rcbook_upath;
				fs.exists(path,(exists)=>{			
				if(exists) {
					results[0][0].rcbook_upath=results[0][0].rcbook_upath;
					callback(null,1);
				}else{
					results[0][0].rcbook_upath=default_image;	
					callback(null,1);
				}
				});
			}
		},
		function(callback){	
			//driver_license_image1
			if(results[1][0].driver_license_image1=="" ||results[1][0].driver_license_image1=='null'){
				results[1][0].driver_license_image1=default_image;	
				callback(null,1);
			}else{
				var path=file_base_path;
				path+=results[1][0].driver_license_image1;
				fs.exists(path,(exists)=>{
				if(exists) {
					results[1][0].driver_license_image1=results[1][0].driver_license_image1;
					callback(null,1);
				}else{
					results[1][0].driver_license_image1=default_image;	
					callback(null,1);
				}
				});
			}
		}
		],function(errors1,results1){
			data_to_return['status']=200;
			data_to_return['msg']='success';
			data_to_return['data']['vehicle_number']=results[0][0].vehicle_number;
			data_to_return['data']['vehicle_type']=results[0][0].vehicle_type;
			data_to_return['data']['insurance_upath']=results[0][0].insurance_upath;
			data_to_return['data']['rcbook_upath']=results[0][0].rcbook_upath;
			data_to_return['data']['driver_name']=results[1][0].name;
			data_to_return['data']['mobile_number']=results[1][0].mobile_number;
			data_to_return['data']['driver_license_image1']=results[1][0].driver_license_image1;
			res.send(data_to_return);
		});
	});	
}


this.lr_details=function(req,res){
	var data_to_return={'status':0,'msg':'Invalid Data','data':{}};

	//setup validation
	
	req.assert('order_id','Invalid value').notEmpty();
	
	// validating errors
	var errors = req.validationErrors();
	if (errors) {
		data_to_return['data']['error_msg']=errors[0]['msg'];
		// returning the error
		res.send(data_to_return);
		return;
	}

	//data sanitisation	
	var order_id=req.sanitize('order_id').escape().trim();
	var moment=require('moment');
	var async=require('async');
	var fs = require('fs');
	var date=moment(new Date(Date.now())).format('YYYY-MM-DD');
	
	async.parallel([
	function(callback){
		var query="select oi.oa_id,oi.order_id,oi.order_number,oi.source_id,oi.destination_id,oi.tonnage,oi.invoice_value,oi.invoice_no,oi.invoice_date,oi.insurance_value,oi.source_in_datetime,oi.lr_number,oai.trip_id from order_info oi join order_aggregate_info oai on oai.oa_id=oi.oa_id where oi.order_id="+order_id+" and oi.is_deleted=0";
		var lr_details_data=function(error, result) {
		if (error==1){
			res.send({status: 500, msg: 'Query failed '});
			return;
		}else{
			if(result==""){
				res.send({status: 400, msg: 'No Result found '});
			}else{
				callback(null,result);
			}
		}
		}
		global.db_query.select_query_1(query,lr_details_data);
	},
	function(callback){
		var lr_details_data1=function(error1,result1) {
		if (error1==1){
			res.send({status: 500, msg: 'Query failed 1'});
			return;
		}else{
			if(result1==""){
				callback(null,result1);
			}else{
				callback(null,result1);
			}
		}
		}
		global.db_query.select_query("order_items","id as order_items_id,material,quantity,weight,package_count","order_id=?",[order_id],"",lr_details_data1);	
	}
	],function(errors,results){
		data_to_return['data']['oa_id']=results[0][0].oa_id;
		data_to_return['data']['order_id']=results[0][0].order_id;
		data_to_return['data']['order_number']=results[0][0].order_number;
		data_to_return['data']['tonnage']=results[0][0].tonnage;
		data_to_return['data']['invoice_value']=results[0][0].invoice_value;
		data_to_return['data']['insurance_value']=results[0][0].insurance_value;
		data_to_return['data']['invoice_no']=(results[0][0].invoice_no==null)?"0":results[0][0].invoice_no;
		data_to_return['data']['invoice_date']=(results[0][0].invoice_date==null)?date:moment(results[0][0].invoice_date).format('YYYY-MM-DD');
		data_to_return['data']['source_in_datetime']=(results[0][0].source_in_datetime==null)?null:moment(results[0][0].source_in_datetime).format('YYYY-MM-DD HH:mm:ss');	
		data_to_return['data']['lr_number']=results[0][0].lr_number;
		data_to_return['data']['order_items']=results[1];
		var trip_id=results[0][0].trip_id;
		var source_id=results[0][0].source_id;
		var destination_id=results[0][0].destination_id;
		
		async.parallel([
		function(callback){
			var query1="select comp.company_name,cpd.name,cpd.geo_location_address,cpd.city,cpd.post_code from company_plant_detail cpd join company comp on comp.id=cpd.company_id  where cpd.id="+source_id+"";
			var lr_details_data2=function(error2,result2){
			if(error2==1){
				res.send({status:500,msg:'Query failed 2'});
			}else{
				if(result2==''){
					res.send({status:400,msg:'No result found 2'});
				}else{
					callback(null,result2);
				}
			}
			}
			global.db_query.select_query_1(query1,lr_details_data2);
		},
		function(callback){
			var query2="select comp.company_name,cpd.name,cpd.geo_location_address,cpd.city,cpd.post_code from company_plant_detail cpd join company comp on comp.id=cpd.company_id  where cpd.id="+destination_id+"";
			var lr_details_data3=function(error3,result3){
			if(error3==1){
				res.send({status:500,msg:'Query failed 3'});
			}else{
				if(result3==''){
					res.send({status:400,msg:'No result found 3'});
				}else{
					callback(null,result3);
				}
			}
			}
			global.db_query.select_query_1(query2,lr_details_data3);
		},
		function(callback){
			var query2="select vd.id,vd.vehicle_number from vehicle_details vd join trip_info ti on ti.vehicle_id=vd.id where ti.trip_id="+trip_id+"";
			var lr_details_data4=function(error4,result4){
			if(error4==1){
				res.send({status:500,msg:'Query failed 4'});
			}else{
				if(result4==''){
					data_to_return['data']['vehicle_id']='';
					data_to_return['data']['vehicle_number']='';
					callback(null,1);
				}else{
					data_to_return['data']['vehicle_id']=result4[0].id;
					data_to_return['data']['vehicle_number']=result4[0].vehicle_number;
					callback(null,1);
				}
			}
			}
			global.db_query.select_query_1(query2,lr_details_data4);
		},
		function(callback){
			var query3="select comp.profile_img_upath from order_info oi join users us on us.id=oi.created_by join company comp on comp.id=us.company_id where oi.order_id="+order_id+" and (comp.company_type='1' or comp.company_type='2')";
			var lr_details_data5=function(error5,result5){
			if(error5==1){
				res.send({status:500,msg:'Query failed 5'});
			}else{
			if(result5==''){
				data_to_return['data']['profile_img_upath']=idelivery_logo;	
				callback(null,1);
			}else{
				//profile_img_upath
				if(result5[0].profile_img_upath==''||result5[0].profile_img_upath=='null'){
					data_to_return['data']['profile_img_upath']=default_image;	
					callback(null,1);
				}else{
					var path=file_base_path;
					path+=result5[0].profile_img_upath;
					fs.exists(path,(exists)=>{			
					if (exists) {
						data_to_return['data']['profile_img_upath']=result5[0].profile_img_upath;
						callback(null,1);
					}else{
						data_to_return['data']['profile_img_upath']=default_image;	
						callback(null,1);
					}
					});
				}
			}
			}
			}
			global.db_query.select_query_1(query3,lr_details_data5);
		}
		],function(error1,results1){
			data_to_return['status']=200;		
			data_to_return['msg']="success";
			data_to_return['data']['source_company']=results1[0][0].company_name;
			data_to_return['data']['sender_name']=results1[0][0].name;
			data_to_return['data']['sender_address']=results1[0][0].geo_location_address;	
			data_to_return['data']['sender_city']=results1[0][0].city;
			data_to_return['data']['sender_post_code']=results1[0][0].post_code;
			data_to_return['data']['destination_company']=results1[1][0].company_name;
			data_to_return['data']['Receiver_name']=results1[1][0].name;
			data_to_return['data']['Receiver_address']=results1[1][0].geo_location_address;
			data_to_return['data']['Receiver_city']=results1[1][0].city;
			data_to_return['data']['Receiver_post_code']=results1[1][0].post_code;
			res.send(data_to_return);
		});
	});
}


this.pod_details=function(req,res){
	var data_to_return={'status':0,'msg':'Invalid Data','data':{}};
	
	//setup validation
	req.assert('order_id','Invalid value').notEmpty();
	
	// validating errors
	var errors = req.validationErrors();
	if (errors) {
		data_to_return['data']['error_msg']=errors[0]['msg'];
		res.send(data_to_return);
		return;
	}

	//data sanitisation	
	var order_id=req.sanitize('order_id').escape().trim();
	var moment=require('moment');
	var async=require('async');
	var fs = require('fs');

	var query="select oi.oa_id,oi.order_id,oi.order_number,oi.source_id,oi.destination_id,oi.created_by,oi.tonnage,oi.pod_signed_datetime,oi.invoice_value,oi.invoice_no,oi.invoice_date,oi.insurance_value,oi.pod_remark,oi.pod_rep_name,oi.pod_rep_mobile_number,oi.pod_sign_upath,oi.source_in_datetime,oi.lr_number,oai.trip_id from order_info oi join order_aggregate_info oai on oai.oa_id=oi.oa_id where oi.order_id="+order_id+" and oi.is_deleted=0";
	var pod_details_data_rtn=function(error, result) {
	if (error==1){
		res.send({status: 500, msg: 'Query failed'});
		return;
	}else{
	if(result==""){
		res.send({status: 400, msg: 'No Result found'});
	}else{
		var oa_id=result[0].oa_id;
		var source_id=result[0].source_id;
		var destination_id=result[0].destination_id;
		var trip_id=result[0].trip_id;
		data_to_return['data']['oa_id']=result[0].oa_id;
		data_to_return['data']['order_id']=result[0].order_id;
		data_to_return['data']['order_number']=result[0].order_number;
		data_to_return['data']['tonnage']=result[0].tonnage;
		data_to_return['data']['invoice_value']=result[0].invoice_value;
		data_to_return['data']['insurance_value']=result[0].insurance_value;data_to_return['data']['invoice_no']=result[0].invoice_no;
		data_to_return['data']['invoice_date']=(result[0].invoice_date==null)?null:moment(result[0].invoice_date).format('YYYY-MM-DD HH:mm:ss');
		data_to_return['data']['source_in_datetime']=moment((result[0].source_in_datetime==null)?null:(result[0].source_in_datetime)).format('YYYY-MM-DD HH:mm:ss');
		data_to_return['data']['lr_number']=result[0].lr_number;
		data_to_return['data']['pod_signed_datetime']=result[0].pod_signed_datetime==null?null:moment(result[0].pod_signed_datetime).format('YYYY-MM-DD HH:mm:ss');		
		data_to_return['data']['pod_remark']=result[0].pod_remark;
		data_to_return['data']['pod_rep_name']=result[0].pod_rep_name;
		data_to_return['data']['pod_rep_mobile_number']=result[0].pod_rep_mobile_number;
				
		async.parallel([
		function(callback){
			var query1="select vd.id,vd.vehicle_number from vehicle_details vd join trip_info ti on ti.vehicle_id=vd.id where ti.trip_id="+trip_id+"";
			var pod_details_data_rtn1=function(error1,result1){
			if (error1==1){
				res.send({status: 500, msg: 'Query failed 3'});
				return;
			}else{
				if(result1==""){
					data_to_return['data']['vehicle_id']="";	
					data_to_return['data']['vehicle_number']="";
					callback(null,1);
				}else{
					data_to_return['data']['vehicle_id']=result1[0].id;	
					data_to_return['data']['vehicle_number']=result1[0].vehicle_number;	
					callback(null,1);
				}
			}
			}
			global.db_query.select_query_1(query1,pod_details_data_rtn1);
			
		},
		function(callback){
			var pod_details_data_rtn2=function(error2, result2){
			if (error2==1){
				res.send({status: 500, msg: 'Query failed 2'});
				return;
			}else{
				if(result2==""){
					data_to_return['data']['order_items']=result2;
					callback(null,1);
				}else{
					data_to_return['data']['order_items']=result2;
					callback(null,1);
				}
			}
			}
			global.db_query.select_query("order_items","id as order_items_id,material,quantity,weight,package_count","order_id=?",[order_id],"",pod_details_data_rtn2);
		},
		function(callback){
			var query2="select comp.company_name,cpd.name,cpd.geo_location_address,cpd.city,cpd.post_code from company_plant_detail cpd join company comp on comp.id=cpd.company_id  where cpd.id="+source_id+"";
			var pod_details_data_rtn3=function(error3,result3){
			if(error3==1){
				res.send({status:500,msg:'Query failed 3'});
			}else{
				if(result3==''){
					res.send({status:400,msg:'No result found 3'});
				}else{
					data_to_return['data']['source_company']=result3[0].company_name;
					data_to_return['data']['sender_name']=result3[0].name;
					data_to_return['data']['sender_address']=result3[0].geo_location_address;	
					data_to_return['data']['sender_city']=result3[0].city;
					data_to_return['data']['sender_post_code']=result3[0].post_code;
					callback(null,1);
				}
			}
			}
			global.db_query.select_query_1(query2,pod_details_data_rtn3);
		},
		function(callback){
			var query3="select comp.company_name,cpd.name,cpd.geo_location_address,cpd.city,cpd.post_code from company_plant_detail cpd join company comp on comp.id=cpd.company_id  where cpd.id="+destination_id+"";
			var pod_details_data_rtn4=function(error4,result4){
			if(error4==1){
				res.send({status:500,msg:'Query failed 4'});
			}else{
				if(result4==''){
					res.send({status:400,msg:'No result found 4'});
				}else{
					data_to_return['data']['destination_company']=result4[0].company_name;
					data_to_return['data']['Receiver_name']=result4[0].name;
					data_to_return['data']['Receiver_address']=result4[0].geo_location_address;
					data_to_return['data']['Receiver_city']=result4[0].city;
					data_to_return['data']['Receiver_post_code']=result4[0].post_code;
					callback(null,1);
				}
			}
			}
			global.db_query.select_query_1(query3,pod_details_data_rtn4);
		},
		function(callback){
			if(result[0].pod_sign_upath=="" ||result[0].pod_sign_upath=='null'){
				data_to_return['data']['pod_sign_upath']=default_image;
				callback(null,1);
			}else{
				var path=file_base_path;
				path+=result[0].pod_sign_upath;
				fs.exists(path,(exists)=>{
				if (exists) {
					data_to_return['data']['pod_sign_upath']=result[0].pod_sign_upath;
					callback(null,1);
				}
				else{
					data_to_return['data']['pod_sign_upath']=default_image;	callback(null,1);					
				}
				});
		}
		},
		function(callback){
			var query4="select comp.profile_img_upath from order_info oi join users us on us.id=oi.created_by join company comp on comp.id=us.company_id where oi.order_id="+order_id+" and  (comp.company_type='1' or comp.company_type='2')";
			var pod_details_data_rtn5=function(error5,result5){
			if(error5==1){
				res.send({status:500,msg:'Query failed 5'});
			}else{
			if(result5==''){
				data_to_return['data']['profile_img_upath']=idelivery_logo;	
				callback(null,1);
			}else{
				//profile_img_upath
				if(result5[0].profile_img_upath=='null' ||result5[0].profile_img_upath==''){
				 data_to_return['data']['profile_img_upath']=default_image;	
					callback(null,1);
				}else{
					var path=file_base_path;
					path+=result5[0].profile_img_upath;
					fs.exists(path,(exists)=>{			
					if (exists) {
						data_to_return['data']['profile_img_upath']=result5[0].profile_img_upath;
						callback(null,1);
					}else{
						data_to_return['data']['profile_img_upath']=default_image;	
						callback(null,1);
					}
					});
				}
			}
			}
			}
			global.db_query.select_query_1(query4,pod_details_data_rtn5);
		}
		],function(errors,results){
			data_to_return['status']=200;		
			data_to_return['msg']="success";
			res.send(data_to_return);
		});
	}
	}
	}
	global.db_query.select_query_1(query,pod_details_data_rtn);		
}


this.send_pod_sms=function(req,res){
		var data_to_return={'status':0,'msg':'Invalid Data','data':{}};
	
		//Data validation
		req.assert('driver_id','Invalid value').notEmpty();
	
		var errors = req.validationErrors();
		if (errors) {
			data_to_return['data']['error_msg']=errors[0]['msg'];
			// returning the error
			res.send(data_to_return);
			return;
		}
	
		//Data Sanitization
		var driver_id=req.sanitize('driver_id').escape().trim();
		data_send={};
		
		var send_pod_sms_rtn=function(err, result) {
			if (err==1){
				res.send({status: 500, msg: 'Query failed'});
				return;
			}else{
				if(result=="")
				{
					res.send({status: 400, msg: 'Empty result'});
				}
				else
				{
				var trip_id=[];
				for(i=0;i<result.length;i++)
				{
					trip_id.push(result[i].trip_id);
				}
//				console.log(trip_id);

				//Getting the current date
				var date=new Date(Date.now());
				var str="";
				str+=date.getFullYear()+"-";
				if((date.getMonth())<9){
					str+="0"+(date.getMonth()+1)+"-";
				}
				else{
				str+=(date.getMonth()+1)+"-";
				}
				if((date.getDate())<10){
					str+="0"+date.getDate();
				}
				else{
				str+=date.getDate();
				}
				
				//Display the oa_id
				
				var send_pod_sms1_rtn=function(err1, result1) {
					if (err1==1){
						res.send({status: 500, msg: 'Query failed 1'});
						return;
					}else{
						if(result1=="")
						{
							res.send({status: 400, msg: 'empty result 1'});
						}
						else
						{
//							console.log(result1);
							var oa_id=[];
							for(i=0;i<result1.length;i++)
							{
								oa_id.push(result1[i].oa_id);
							}
	//						console.log(oa_id);
						var send_pod_sms2_rtn=function(err2, result2) {					
						if (err2==1){
							res.send({status: 500, msg: 'Query failed 2'});
						return;
						}else{
						if (result2==''){
							res.send({status: 400, msg: 'empty result 2'});
						return;}else{
							var order_id=result2[0].order_id;
							var destination_id=result2[0].destination_id;
							console.log(order_id);
							var send_pod_sms3_rtn=function(err3, result3) {					
							if (err3==1){
								res.send({status: 500, msg: 'Query failed 2'});
							return;
							}else{
							if (result3==''){
								res.send({status: 400, msg: 'empty result 2'});
							return;}else{
								var number=result3[0].mobile_number;
								var temp='1234567890';	  
								var shuffle=temp.split('').sort(function(){return 0.5-Math.random()}).join('');
								var otp=shuffle.substring(0,4);
								data_send['otp']=otp;
								var urlencode=require('urlencode');
								var msg_response;
								var msg_status;
								var user_name=urlencode("carol.dalmeida@idelivery.in");
								var hash="6e2a31220b7cfd3b759e8e911be628d42872d7ba";
								var sender="IDLVRY";
						//		otp="1234";
								var message =urlencode("Your OTP is "+otp); 
								var test=false;
								request = require("request");
								//SEND SMS
								request("http://api.textlocal.in/send/?username="+user_name+"&hash="+hash+"&message="+message+"&sender="+sender+"&numbers="+number+"&test="+test, function(error, response, body) {
								msg_response = JSON.parse(body);
								var msg_status = msg_response["status"];
								console.log(msg_status);
								});

							
						}
						}
						}					
					global.db_query.select_query("company_plant_detail","mobile_number","id=?",[destination_id],"",send_pod_sms3_rtn);
				}
				}
				}					
			global.db_query.select_query("order_info","order_id,destination_id","oa_id IN ("+oa_id+") and exp_delivery_datetime LIKE ? and trip_status="+5+"",[str+"%"],"",send_pod_sms2_rtn);
			}
			}
			}
			global.db_query.select_query("order_aggregate_info","oa_id","trip_id IN ("+trip_id+")","","",send_pod_sms1_rtn);
			}
			}
			}
			global.db_query.select_query("trip_info","trip_id","driver_id=? and is_deleted="+0+"",[driver_id],"",send_pod_sms_rtn);					
}


this.map_view=function(req,res){
	var data_to_return={'status':0,'msg':'Invalid data','data':{}};
	
	//validating the data
	req.assert('oa_id','Invalid value').notEmpty();
	
	//validating the errors
	var errors=req.validationErrors();
	if(errors){
		data_to_return['data']['error_msg']=errors[0]['msg'];
		res.send(data_to_return);
		return;
	}
	
	//Sanitize the data
	var oa_id=req.sanitize('oa_id').escape().trim();
	
	var async=require('async');
	var moment=require('moment');
	var arraySort = require('array-sort');
	var inr=0;
	var temp=[];
	var origins=[];
	var date=moment(new Date(Date.now())).format('YYYY-MM-DD HH:mm:ss');
	async.waterfall([
	function(callback){
		var query="SELECT oi.trip_status as oi_trip_status,oai.trip_status as oa_trip_status,oai.device_type,oi.source_in_datetime,oi.destination_in_datetime,oai.vehicle_accepted_datetime,ti.device_id,ti.driver_id from order_aggregate_info oai join order_info oi on oi.oa_id=oai.oa_id LEFT join trip_info ti on ti.trip_id=oai.trip_id WHERE oai.oa_id="+oa_id+" and oi.is_deleted=0 and oai.is_deleted=0"
		var map_view_data=function(error,result){
		if(error==1){
			res.send({'status':500,'msg':'Query Failed'});
		}else{
			if(result==""){
				res.send({'status':400,'msg':'No Result found'});
			}else{
				callback(null,result);
			}
		}	
		}
		global.db_query.select_query_1(query,map_view_data);
	},
	],function(errors,results){
		var device_type=results[0].device_type;
		var oa_trip_status=results[0].oa_trip_status;
		async.parallel([
		function(callback){
			if(oa_trip_status>1){
			for(let value in results){
				var data_from_date="";
				var data_to_date="";
				if(inr==0){
					inr++;
					data_from_date=moment(results[value].source_in_datetime).format('YYYY-MM-DD HH:mm:ss');
				}else{
					inr++;
					data_from_date=moment(results[value].vehicle_accepted_datetime).format('YYYY-MM-DD HH:mm:ss');
				}
				if(results[value].oi_trip_status<5){
					data_to_date=date;
				}else{
					data_to_date=results[value].destination_in_datetime;
					data_to_date=moment(data_to_date).add(10,'minutes');
					data_to_date=moment(data_to_date).format('YYYY-MM-DD HH:mm:ss');
				}
	
				if(device_type==1||device_type==0){
					var query1="SELECT lh.running_no,lh.created_date as datetime,lh.lat_message AS latitude,lh.lon_message AS longitude,lh.battery_percentage from location_history_7 lh join device_port dp on dp.running_no=lh.running_no where dp.device_id='"+results[value].device_id+"'  AND lh.created_date>='"+data_from_date+"' AND lh.created_date<='"+data_to_date+"' order by lh.created_date asc";
				}else if(device_type==2){
					var query1="SELECT user_id as driver_id,data_insertion_time as datetime,latitude,longitude,battery_percentage from mob_location_history where user_id="+results[value].driver_id+"  AND data_insertion_time>='"+data_from_date+"' AND data_insertion_time<='"+data_to_date+"' AND status=0 order by data_insertion_time asc"; 
				}
				var map_view_data1=function(error1,result1){
				if(error1==1){
					res.send({'status':500,'msg':'Query Failed 1'});
				}else{
				if(result1==""){
					if(value==results.length-1){
						arraySort(temp, 'timestamp',{reverse: false});
						temp.sort((a, b) => a.timestamp - b.timestamp);
						callback(null,temp);
					}
				}else{
				for(let value1 in result1){			
					if(device_type==1 || device_type==0){
						temp.push({'running_no':result1[value1]['running_no'],'datetime':moment(result1[value1]['datetime']).format('YYYY-MM-DD HH:mm:ss'),'latitude':result1[value1]['latitude'],'longitude':result1[value1]['longitude'],'timestamp':new Date(result1[value1]['datetime']).getTime()});
					}else if(device_type==2){
						temp.push({'driver_id':result1[value1]['driver_id'],'datetime':moment(result1[value1]['datetime']).format('YYYY-MM-DD HH:mm:ss'),'latitude':result1[value1]['latitude'],'longitude':result1[value1]['longitude'],'timestamp':new Date(result1[value1]['datetime']).getTime()});
					}	
					if((value1==result1.length-1)&&(value==results.length-1)){
						arraySort(temp, 'timestamp',{reverse: false});
						temp.sort((a, b) => a.timestamp - b.timestamp);
						callback(null,temp);
					}				
				}
				}
				}
				}
				global.db_query.select_query_1(query1,map_view_data1);
			}
		}else{
			callback(null,[]);
		}
		},
		function(callback){
			var vehicle_accepted_datetime=moment(results[0].vehicle_accepted_datetime).format('YYYY-MM-DD HH:mm:ss');
			if(oa_trip_status==3){
			var map_view_data2=function(error2,result2){
			if(error2==1){
				res.send({'status':500,'msg':'Query Failed 2'});
			}else{
				if(result2==""){
					callback(null,[]);
				}else{
					callback(null,result2[0]);
				}
			}	
			}
			if(device_type==1 || device_type==0){
				global.db_query.select_query("device_port","lat_message as latitude,lon_message as longitude,battery_percentage","device_id=? and modified_date > '"+vehicle_accepted_datetime+"' ",[results[0].device_id],"",map_view_data2);
			}else if(device_type==2){
				global.db_query.select_query("device_port","lat_message as latitude,lon_message as longitude,battery_percentage","driver_id=? and modified_date > '"+vehicle_accepted_datetime+"' ",[results[0].driver_id],"",map_view_data2);
			}
		}else{
			callback(null,[]);
		}
		},
		function(callback){
			var query2="select oi.source_address_lat as source_latitude, oi.source_address_lng as source_longitude,oi.destination_address_lat as destination_latitude,oi.destination_address_lng as destination_longitude, (select geo_location_address from company_plant_detail where id=oi.source_id)as source_plant,(select geo_location_address from company_plant_detail where id=oi.destination_id)as destination_plant, oi.trip_status as oi_trip_status from order_info oi left join order_aggregate_info oai on oi.oa_id=oai.oa_id WHERE 1=1 AND oi.is_deleted=0 AND oai.is_deleted=0 AND oai.oa_id="+oa_id+" group by oi.order_id";
			var map_view_data3=function(error3,result3){
			if(error3==1){
				res.send({'status':500,'msg':'Query Failed 3'});
			}else{
				if(result3==""){
					callback(null,[]);
				}else{
					for(let val in result3){
						origins.push({'source_plant':result3[val].source_plant,'source_latitude':result3[val].source_latitude,'source_longitude':result3[val].source_longitude,'oi_trip_status':result3[val].oi_trip_status},{'destination_plant':result3[val].destination_plant,'destination_latitude':result3[val].destination_latitude,'destination_longitude':result3[val].destination_longitude,'oi_trip_status':result3[val].oi_trip_status});
						if(val==result3.length-1){
							callback(null,origins);
						}
					}
				}
			}	
			}
			global.db_query.select_query_1(query2,map_view_data3)
		},
		],function(errors1,results1){
			data_to_return['status']=200;
			data_to_return['msg']="success";
			data_to_return['data']['origins']=results1[2];
			data_to_return['data']['vehicle_latlng']=results1[1];
			data_to_return['data']['latlng']=results1[0];
			res.send(data_to_return);
		});
	});	
}


this.driver_assigned_milk_order_list=function(req,res){
	var data_to_return={'status':0,'msg':'Invalid data','data':{}};
	
	//validating the data
		var driver_id=req.assert('driver_id','Invalid value').notEmpty();
		
	//validating the erros
	var errors=req.validationErrors();
		if (errors) {
			data_to_return['data']['error_msg']=errors[0]['msg'];
			// returning the error
			res.send(data_to_return);
			return;
		}
	//Sanitizating the data
	var driver_id=req.sanitize('driver_id').escape().trim();
	var moment=require('moment');
	
	var query="select moai.oa_id,moai.oa_number,moai.route_id,mri.route_name,moai.company_id,(select company_name from company where id=moai.company_id)as company_name,vd. vehicle_number,moai.device_type,moai.device_id,dp.running_no as device_num,(select battery_percentage from device_port where modified_date > moai.vehicle_accepted_datetime and moai.device_type=1 and moai.device_id!=0 and running_no=dp.running_no)as battery_percentage,moai.order_datetime,moai.trip_status as trip_status_code,moai.trip_type FROM milk_order_aggregate_info moai join milk_route_info mri on mri.route_id=moai.route_id left join vehicle_details vd on vd.id=moai.vehicle_id left join driver dr on dr.id=moai.driver_id left join device_port dp on dp.device_id=moai.device_id where moai.driver_id="+driver_id+" and moai.trip_status='1' ORDER BY moai.created_datetime DESC";
	var driver_assigned_milk_order_list_data=function(error,result){
	if(error==1){
		res.send({'status':500,'msg':'Query Failed'});
	}else{
		if(result==""){
			res.send({'status':400,'msg':'No Result found'});
		}else{
			
		for(let val in result){
			result[val].order_datetime=moment(result[val].order_datetime).format('YYYY-MM-DD HH:mm:ss');
			
			var battery_percentage='';
			var device_type=result[val].device_type;
			var device_id=result[val].device_id;
			
			if(result[val].trip_status_code>0){
			if(((device_type=="" || device_type==undefined) && (device_id!=''&& device_id!=undefined && device_id!=null) && device_id!='0')|| (device_type==1 && device_id!='0')){
				result[val].device_num+=battery_percentage;
			}else{
				result[val].device_num="";
			}
			}
	
			if(result[val].trip_type==1){
				result[val].trip_type='DROP';
			}else if(result[val].trip_type==2){
				result[val].trip_type='PICKUP';
			}
			if(val==result.length-1){
				data_to_return['status']=200;
				data_to_return['msg']='success';
				data_to_return['data']=result;
				res.send(data_to_return);
			}
		}
		}
	}	
	}
	global.db_query.select_query_1(query,driver_assigned_milk_order_list_data);
}


this.driver_assigned_milk_order_details=function(req,res){
	var data_to_return={'status':0,'msg':'Invalid data','data':{}};
	
	//validating the data
	req.assert('oa_id','Invalid value').notEmpty();
	
	//validating the errors
	var errors=req.validationErrors();
	if(errors){
		data_to_return['data']['error_msg']=errors[0]['msg'];
		res.send(data_to_return);
		return;
	}
	
	//Sanitize the data
	var oa_id=req.sanitize('oa_id').escape().trim();
	
	var async=require('async');
	var moment=require('moment');
	var temp=[];
	
	async.parallel([
	function(callback){
		var query="SELECT moai.oa_id,moai.oa_number,moai.company_id,(select company_name from company where id=moai.company_id)as company_name,moai.order_datetime,moai.trip_status AS oa_trip_status,moai.route_id,mri.route_name,mri.start_point_name,mri.start_point_geo_address,moai.trip_type AS oa_trip_type  FROM milk_order_aggregate_info moai join milk_route_info mri on mri.route_id=moai.route_id WHERE moai.oa_id="+oa_id+" and moai.trip_status='1' ";
		var driver_assigned_milk_order_details_data=function(error,result){
		if(error==1){
			res.send({'status':500,'msg':'Query Failed'});
		}else{
			if(result==""){
				res.send({'status':400,'msg':'No Result found'});
			}else{
				if(result[0].oa_trip_type==1){
					result[0].oa_trip='DROP';
				}else if(result[0].oa_trip_type==2){
					result[0].oa_trip='PICKUP';
				}
				callback(null,result);
			}
		}	
		}
		global.db_query.select_query_1(query,driver_assigned_milk_order_details_data);	
	},
	function(callback){
		var query1="SELECT moai.oa_id,moai.route_id,moi.order_id,moi.hub_id,(select name from milk_route_hub_info where hub_id=moi.hub_id)as hub_name,moi.geo_address as point_address,moi.tonnage,moi.product,moi.hub_order,moai.trip_status AS oa_trip_status,moi.trip_status AS oi_trip_status,moai.trip_type AS oa_trip_type FROM milk_order_aggregate_info moai INNER JOIN milk_order_info moi ON moi.oa_id=moai.oa_id AND moi.is_deleted=0 WHERE moai.oa_id="+oa_id+"  and moai.trip_status='1' ORDER BY moi.hub_order ASC"
		var driver_assigned_milk_order_details_data1=function(error1,result1){
		if(error1==1){
			res.send({'status':500,'msg':'Query Failed 1'});
		}else{
			if(result1==""){
				res.send({'status':400,'msg':'No Result found 1'});
			}else{
				callback(null,result1);
			}
		}	
		}
		global.db_query.select_query_1(query1,driver_assigned_milk_order_details_data1);
	},
	],function(errors,results){
		temp.push({'oa_id':results[0][0].oa_id,'oa_number':results[0][0].oa_number,'company_id':results[0][0].company_id,'company_name':results[0][0].company_name,'order_datetime':moment(results[0][0].order_datetime).format('YYYY-MM-DD HH:mm:ss'),'oa_trip_status':results[0][0].oa_trip_status,'route_id':results[0][0].route_id,'route_name':results[0][0].route_name,'start_point_name':results[0][0].start_point_name,'start_point_geo_address':results[0][0].start_point_geo_address,'oa_trip_type':results[0][0].oa_trip_type,'oa_trip':results[0][0].oa_trip,'orders':results[1]});
		data_to_return['status']=200;
		data_to_return['msg']='success';
		data_to_return['data']=temp;
		res.send(data_to_return);		
	});
}


this.driver_active_milk_order_list=function(req,res){
	var data_to_return={'status':0,'msg':'Invalid data','data':{}};
	
	//validating the data
		var driver_id=req.assert('driver_id','Invalid value').notEmpty();
		
	//validating the erros
	var errors=req.validationErrors();
		if (errors) {
			data_to_return['data']['error_msg']=errors[0]['msg'];
			// returning the error
			res.send(data_to_return);
			return;
		}
	//Sanitizating the data
	var driver_id=req.sanitize('driver_id').escape().trim();
	var moment=require('moment');
	
	var query="select moai.oa_id,moai.oa_number,moai.route_id,mri.route_name,moai.company_id,(select company_name from company where id=moai.company_id)as company_name,vd. vehicle_number,moai.device_type,moai.device_id,dp.running_no as device_num,(select battery_percentage from device_port where (modified_date > moai.vechicle_accept_datetime and moai.device_type=1 and moai.device_id!=0 and running_no=dp.running_no)or(modified_date > moai.vechicle_accept_datetime and driver_id=moai.driver_id and moai.device_type=2))as battery_percentage,moai.order_datetime,moai.trip_status as trip_status_code,moai.trip_type FROM milk_order_aggregate_info moai join milk_route_info mri on mri.route_id=moai.route_id left join vehicle_details vd on vd.id=moai.vehicle_id left join driver dr on dr.id=moai.driver_id left join device_port dp on dp.device_id=moai.device_id where moai.driver_id="+driver_id+" and moai.trip_status='2' ORDER BY moai.created_datetime DESC";
	var driver_active_milk_order_list_data=function(error,result){
	if(error==1){
		res.send({'status':500,'msg':'Query Failed'});
	}else{
		if(result==""){
			res.send({'status':400,'msg':'No Result found'});
		}else{
			
		for(let val in result){
			result[val].order_datetime=moment(result[val].order_datetime).format('YYYY-MM-DD HH:mm:ss');
			
			var battery_percentage='';
			var device_type=result[val].device_type;
			var device_id=result[val].device_id;
			
			if(result[val].trip_status_code==2){
				if(result[val].battery_percentage!='null' && result[val].battery_percentage!='' && result[val].battery_percentage!=undefined){
					battery_percentage=' | '+result[val].battery_percentage+'%';
				}
			}
			if(result[val].trip_status_code>0){
			if(((device_type=="" || device_type==undefined) && (device_id!=''&& device_id!=undefined && device_id!=null) && device_id!='0')|| (device_type==1 && device_id!='0')){
				result[val].device_num+=battery_percentage;
			}else{
				result[val].device_num="";
			}
			}
	
			if(result[val].trip_type==1){
				result[val].trip_type='DROP';
			}else if(result[val].trip_type==2){
				result[val].trip_type='PICKUP';
			}
			if(val==result.length-1){
				data_to_return['status']=200;
				data_to_return['msg']='success';
				data_to_return['data']=result;
				res.send(data_to_return);
			}
		}
		}
	}	
	}
	global.db_query.select_query_1(query,driver_active_milk_order_list_data);
}


this.driver_active_milk_order_details=function(req,res){
	var data_to_return={'status':0,'msg':'Invalid data','data':{}};
	
	//validating the data
	req.assert('oa_id','Invalid value').notEmpty();
	
	//validating the errors
	var errors=req.validationErrors();
	if(errors){
		data_to_return['data']['error_msg']=errors[0]['msg'];
		res.send(data_to_return);
		return;
	}
	
	//Sanitize the data
	var oa_id=req.sanitize('oa_id').escape().trim();
	
	var async=require('async');
	var moment=require('moment');
	var temp=[];
	
	async.parallel([
	function(callback){
		var query="SELECT moai.oa_id,moai.oa_number,moai.company_id,(select company_name from company where id=moai.company_id)as company_name,moai.order_datetime,moai.trip_status AS oa_trip_status,moai.route_id,mri.route_name,mri.start_point_name,mri.start_point_geo_address,moai.trip_type AS oa_trip_type  FROM milk_order_aggregate_info moai join milk_route_info mri on mri.route_id=moai.route_id WHERE moai.oa_id="+oa_id+" and moai.trip_status='2' ";
		var driver_active_milk_order_details_data=function(error,result){
		if(error==1){
			res.send({'status':500,'msg':'Query Failed'});
		}else{
			if(result==""){
				res.send({'status':400,'msg':'No Result found'});
			}else{
				if(result[0].oa_trip_type==1){
					result[0].oa_trip='DROP';
				}else if(result[0].oa_trip_type==2){
					result[0].oa_trip='PICKUP';
				}
				callback(null,result);
			}
		}	
		}
		global.db_query.select_query_1(query,driver_active_milk_order_details_data);	
	},
	function(callback){
		var query1="SELECT moai.oa_id,moai.route_id,moi.order_id,moi.hub_id,(select name from milk_route_hub_info where hub_id=moi.hub_id)as hub_name,moi.geo_address as point_address,moi.tonnage,moi.product,moi.hub_order,moai.trip_status AS oa_trip_status,moi.trip_status AS oi_trip_status,moai.trip_type AS oa_trip_type FROM milk_order_aggregate_info moai INNER JOIN milk_order_info moi ON moi.oa_id=moai.oa_id AND moi.is_deleted=0 WHERE moai.oa_id="+oa_id+"  and moai.trip_status='2' ORDER BY moi.hub_order ASC"
		var driver_active_milk_order_details_data1=function(error1,result1){
		if(error1==1){
			res.send({'status':500,'msg':'Query Failed 1'});
		}else{
			if(result1==""){
				res.send({'status':400,'msg':'No Result found 1'});
			}else{
				callback(null,result1);
			}
		}	
		}
		global.db_query.select_query_1(query1,driver_active_milk_order_details_data1);
	},
	],function(errors,results){
		temp.push({'oa_id':results[0][0].oa_id,'oa_number':results[0][0].oa_number,'company_id':results[0][0].company_id,'company_name':results[0][0].company_name,'order_datetime':moment(results[0][0].order_datetime).format('YYYY-MM-DD HH:mm:ss'),'oa_trip_status':results[0][0].oa_trip_status,'route_id':results[0][0].route_id,'route_name':results[0][0].route_name,'start_point_name':results[0][0].start_point_name,'start_point_geo_address':results[0][0].start_point_geo_address,'oa_trip_type':results[0][0].oa_trip_type,'oa_trip':results[0][0].oa_trip,'orders':results[1]});
		data_to_return['status']=200;
		data_to_return['msg']='success';
		data_to_return['data']=temp;
		res.send(data_to_return);		
	});
}


this.driver_completed_milk_order_list=function(req,res){
	var data_to_return={'status':0,'msg':'Invalid data','data':{}};
	
	//validating the data
		var driver_id=req.assert('driver_id','Invalid value').notEmpty();
		
	//validating the erros
	var errors=req.validationErrors();
		if (errors) {
			data_to_return['data']['error_msg']=errors[0]['msg'];
			// returning the error
			res.send(data_to_return);
			return;
		}
	//Sanitizating the data
	var driver_id=req.sanitize('driver_id').escape().trim();
	var moment=require('moment');
	
	var query="select moai.oa_id,moai.oa_number,moai.route_id,mri.route_name,moai.company_id,(select company_name from company where id=moai.company_id)as company_name,vd. vehicle_number,moai.device_type,moai.device_id,dp.running_no as device_num,moai.order_datetime,moai.trip_status as trip_status_code,moai.trip_type FROM milk_order_aggregate_info moai join milk_route_info mri on mri.route_id=moai.route_id left join vehicle_details vd on vd.id=moai.vehicle_id left join driver dr on dr.id=moai.driver_id left join device_port dp on dp.device_id=moai.device_id where moai.driver_id="+driver_id+" and moai.trip_status='3' ORDER BY moai.created_datetime DESC";
	var driver_completed_milk_order_list_data=function(error,result){
	if(error==1){
		res.send({'status':500,'msg':'Query Failed'});
	}else{
		if(result==""){
			res.send({'status':400,'msg':'No Result found'});
		}else{
			
		for(let val in result){
			result[val].order_datetime=moment(result[val].order_datetime).format('YYYY-MM-DD HH:mm:ss');
			
			var battery_percentage='';
			var device_type=result[val].device_type;
			var device_id=result[val].device_id;
			
			if(result[val].trip_status_code>0){
			if(((device_type=="" || device_type==undefined) && (device_id!=''&& device_id!=undefined && device_id!=null) && device_id!='0')|| (device_type==1 && device_id!='0')){
				result[val].device_num+=battery_percentage;
			}else{
				result[val].device_num="";
			}
			}
	
			if(result[val].trip_type==1){
				result[val].trip_type='DROP';
			}else if(result[val].trip_type==2){
				result[val].trip_type='PICKUP';
			}
			if(val==result.length-1){
				data_to_return['status']=200;
				data_to_return['msg']='success';
				data_to_return['data']=result;
				res.send(data_to_return);
			}
		}
		}
	}	
	}
	global.db_query.select_query_1(query,driver_completed_milk_order_list_data);
}


this.driver_completed_milk_order_details=function(req,res){
	var data_to_return={'status':0,'msg':'Invalid data','data':{}};
	
	//validating the data
	req.assert('oa_id','Invalid value').notEmpty();
	
	//validating the errors
	var errors=req.validationErrors();
	if(errors){
		data_to_return['data']['error_msg']=errors[0]['msg'];
		res.send(data_to_return);
		return;
	}
	
	//Sanitize the data
	var oa_id=req.sanitize('oa_id').escape().trim();
	
	var async=require('async');
	var moment=require('moment');
	var temp=[];
	
	async.parallel([
	function(callback){
		var query="SELECT moai.oa_id,moai.oa_number,moai.company_id,(select company_name from company where id=moai.company_id)as company_name,moai.order_datetime,moai.trip_status AS oa_trip_status,moai.route_id,mri.route_name,mri.start_point_name,mri.start_point_geo_address,moai.trip_type AS oa_trip_type  FROM milk_order_aggregate_info moai join milk_route_info mri on mri.route_id=moai.route_id WHERE moai.oa_id="+oa_id+" and moai.trip_status='3'";
		var driver_completed_milk_order_details_data=function(error,result){
		if(error==1){
			res.send({'status':500,'msg':'Query Failed'});
		}else{
			if(result==""){
				res.send({'status':400,'msg':'No Result found'});
				return;
			}else{
				if(result[0].oa_trip_type==1){
					result[0].oa_trip='DROP';
				}else if(result[0].oa_trip_type==2){
					result[0].oa_trip='PICKUP';
				}
				callback(null,result);
			}
		}	
		}
		global.db_query.select_query_1(query,driver_completed_milk_order_details_data);	
	},
	function(callback){
		var query1="SELECT moai.oa_id,moai.route_id,moi.order_id,moi.hub_id,(select name from milk_route_hub_info where hub_id=moi.hub_id)as hub_name,moi.geo_address as point_address,moi.tonnage,moi.product,moi.hub_order,moai.trip_status AS oa_trip_status,moi.trip_status AS oi_trip_status,moai.trip_type AS oa_trip_type FROM milk_order_aggregate_info moai INNER JOIN milk_order_info moi ON moi.oa_id=moai.oa_id AND moi.is_deleted=0 WHERE moai.oa_id="+oa_id+" and moai.trip_status='3' ORDER BY moi.hub_order ASC"
		var driver_completed_milk_order_details_data1=function(error1,result1){
		if(error1==1){
			res.send({'status':500,'msg':'Query Failed 1'});
		}else{
			if(result1==""){
				res.send({'status':400,'msg':'No Result found 1'});
			}else{
				callback(null,result1);
			}
		}	
		}
		global.db_query.select_query_1(query1,driver_completed_milk_order_details_data1);
	},
	],function(errors,results){
		temp.push({'oa_id':results[0][0].oa_id,'oa_number':results[0][0].oa_number,'company_id':results[0][0].company_id,'company_name':results[0][0].company_name,'order_datetime':moment(results[0][0].order_datetime).format('YYYY-MM-DD HH:mm:ss'),'oa_trip_status':results[0][0].oa_trip_status,'route_id':results[0][0].route_id,'route_name':results[0][0].route_name,'start_point_name':results[0][0].start_point_name,'start_point_geo_address':results[0][0].start_point_geo_address,'oa_trip_type':results[0][0].oa_trip_type,'oa_trip':results[0][0].oa_trip,'orders':results[1]});
		data_to_return['status']=200;
		data_to_return['msg']='success';
		data_to_return['data']=temp;
		res.send(data_to_return);		
	});
}


this.milk_order_driver_acceptance_status=function(req,res){
	var data_to_return={'status':0,'msg':'Invalid data','data':{}};
	
	//validating the data
	req.assert('oa_id','Invalid value').notEmpty();
	req.assert('reporting_time','Invalid value').notEmpty();
	req.assert('accept_order','Invalid value').notEmpty();
	
	//validating the errors
	var errors=req.validationErrors();
	if(errors){
		data_to_return['data']['error_msg']=errors[0]['msg'];
		res.send(data_to_return);
		return;
	}
	
	//Sanitize the data
	var oa_id=req.sanitize('oa_id').escape().trim();
	var reporting_time=req.sanitize('reporting_time').escape().trim();
	var accept_order=req.sanitize('accept_order').escape().trim();
	
	var async=require('async');
	var moment=require('moment');
	var date=moment(new Date(Date.now())).format('YYYY-MM-DD HH:mm:ss');
	var vehicle_id="";
	var data_send={};

	async.waterfall([
	function(callback){
		var driver_acceptance_status_data=function(error,result){
		if(error==1){
			res.send({'status':500,'msg':'Query Failed'});
		}else{
		if(result==""){
			res.send({'status':400,'msg':'No Result found'});
		}else{
			if(result[0].trip_status==1){
				callback(null,1);
			}else{
				res.send({'status':400,'msg':'Refresh & Check The Order Status'});
			}
		}
		}	
		}
		global.db_query.select_query("milk_order_aggregate_info","trip_status","oa_id=?",[oa_id],"",driver_acceptance_status_data);
	}
	],function(errors,results){
		
	if(accept_order==1){		
		async.waterfall([
		function(callback){
			var query="SELECT moai.vehicle_id,moai.driver_id,vd.device_type,vd.device_id from milk_order_aggregate_info moai join vehicle_details vd on vd.id=moai.vehicle_id where moai.oa_id="+oa_id+" and vd.otrip_status=0 and moai.is_deleted=0 ";
			var driver_acceptance_status_data1=function(error1,result1){
			if(error1==1){
				res.send({'status':500,'msg':'Query Failed 1'});
			}else{
				if(result1==""){
					res.send({'status':400,'msg':'Vehicle Already Accepted'});
					return;
				}else{
					callback(null,result1);
				}
			}	
			}
			global.db_query.select_query_1(query,driver_acceptance_status_data1);
		},
		function(arg,callback){
			var query1="SELECT ti.driver_id,ti.trip_status,'multi' as status FROM trip_info ti join order_aggregate_info oai on oai.trip_id=ti.trip_id where ti.driver_id='"+arg[0].driver_id+"' and ti.is_deleted=0 and ti.trip_status=3 UNION SELECT moai.driver_id,moai.trip_status,'milk' as status FROM milk_order_aggregate_info moai where moai.driver_id='"+arg[0].driver_id+"' and moai.is_deleted=0 and moai.trip_status=2 ";
			var driver_acceptance_status_data2=function(error2,result2){
			if(error2==1){
				res.send({'status':500,'msg':'Query Failed 2'});
			}else{
				if(result2!=""){
					res.send({'status':400,'msg':'Driver already accepted'});
					return;
				}else{
					callback(null,arg);
				}
			}	
			}
			global.db_query.select_query_1(query1,driver_acceptance_status_data2);
		}
		],function(errors1,results1){
			async.parallel([
			function(callback){
				data_send['otrip_status']=1;
				data_send['oid']=oa_id;
				if(results1[0].device_type==2){				
					data_send['driver_id']=results1[0].driver_id;
				}
				var driver_acceptance_status_data3=function(error3,result3){
				if(error3==1){
					res.send({'status':500,'msg':'Query Failed 3'});
				}else{
					if(result3==""){
						res.send({'status':400,'msg':'No Result found 3'});
					}else{
						callback(null,1);
					}
				}	
				}
				global.db_query.update_query("vehicle_details","id=? and otrip_status=0",[results1[0].vehicle_id],data_send,driver_acceptance_status_data3);
			},
			function(callback){
				var driver_acceptance_status_data4=function(error4,result4){
				if(error4==1){
					res.send({'status':500,'msg':'Query Failed 4'});
				}else{
					if(result4==""){
						res.send({'status':400,'msg':'No Result found 4'});
					}else{
						callback(null,result4);
					}
				}	
				}
				global.db_query.update_query("milk_order_aggregate_info","oa_id=? and trip_status=1",[oa_id],{'device_type':results1[0].device_type,'device_id':results1[0].device_id,'trip_status':2,'vechicle_accept_datetime':date},driver_acceptance_status_data4);
			}
			],function(errors2,results2){
				data_to_return['status']=200;
				data_to_return['msg']='success';
				data_to_return['data']['oa_id']=oa_id;
				res.send(data_to_return);
			});
		});
	}else if(accept_order==0){
		var driver_acceptance_status_data6=function(error6,result6){
		if(error6==1){
			res.send({'status':500,'msg':'Query Failed 6'});
		}else{
			if(result6==""){
				res.send({'status':400,'msg':'No Result found 6'});
			}else{
				data_to_return['status']=200;
				data_to_return['msg']='success';
				data_to_return['data']['oa_id']=oa_id;
				res.send(data_to_return);
			}
		}	
		}
		global.db_query.update_query("milk_order_aggregate_info","oa_id=? and trip_status=1",[oa_id],{'device_id':0,'vehicle_id':0,'driver_id':0,'trip_status':0},driver_acceptance_status_data6)
	}
	});	
}


this.milk_order_truck_details=function(req,res){
	var data_to_return={'status':0,'msg':'Invalid data','data':{}};
	
	//validating the data
	req.assert('oa_id','Invalid value').notEmpty();
	
	//validating the errors
	var errors=req.validationErrors();
	if(errors){
		data_to_return['data']['error_msg']=errors[0]['msg'];
		res.send(data_to_return);
		return;
	}
	
	//Sanitize the data
	var oa_id=req.sanitize('oa_id').escape().trim();
	var async=require('async');
	var fs = require('fs');
	
	async.parallel([
	function(callback){
		var query="SELECT vd.vehicle_number,veh.vehicle_type ,vd.rcbook_upath,vd.insurance_upath from vehicle_details vd join milk_order_aggregate_info moai on moai.vehicle_id=vd.id join vehicle_type veh on veh.id=vd.vehicle_type_id where moai.oa_id="+oa_id+"";
		var milk_order_truck_details_data=function(error,result){
		if(error==1){
			res.send({'status':500,'msg':'Query Failed '});
		}else{
			if(result==""){
				res.send({'status':400,'msg':'No Result found '});
			}else{
				callback(null,result);
			}
		}	
		}
		global.db_query.select_query_1(query,milk_order_truck_details_data);
	},
	function(callback){
		var query1="SELECT dr.name,dr.mobile_number,dr.driver_license_image1 from driver dr join milk_order_aggregate_info moai on moai.driver_id=dr.id where moai.oa_id="+oa_id+"";
		var milk_order_truck_details_data1=function(error1,result1){
		if(error1==1){
			res.send({'status':500,'msg':'Query Failed 1'});
		}else{
			if(result1==""){
				res.send({'status':400,'msg':'No Result found 1'});
			}else{
				callback(null,result1);
			}
		}	
		}
		global.db_query.select_query_1(query1,milk_order_truck_details_data1);
	}
	],function(errors,results){
		async.parallel([
		function(callback){
			//insurance_upath
			if(results[0][0].insurance_upath=="" ||results[0][0].insurance_upath=='null'){
				results[0][0].insurance_upath=default_image;	
				callback(null,1);				
			}else{
				var path=file_base_path;
				path+=results[0][0].insurance_upath;
				fs.exists(path,(exists)=>{
					if(exists){
						results[0][0].insurance_upath=results[0][0].insurance_upath;
						callback(null,1);
					}else{
						results[0][0].insurance_upath=default_image;	
						callback(null,1);						
					}
				});
			}
		},
		function(callback){	
			//rcbook_upath
			if(results[0][0].rcbook_upath=="" ||results[0][0].rcbook_upath=='null'){
				results[0][0].rcbook_upath=default_image;	
				callback(null,1);
			}else{
				var path=file_base_path;
				path+=results[0][0].rcbook_upath;
				fs.exists(path,(exists)=>{
					if(exists){
						results[0][0].rcbook_upath=results[0][0].rcbook_upath;
						callback(null,1);
					}else{
						results[0][0].rcbook_upath=default_image;	
						callback(null,1);
					}
				});
			}
		},
		function(callback){	
			//driver_license_image1
			if(results[1][0].driver_license_image1=="" ||results[1][0].driver_license_image1=='null'){
				results[1][0].driver_license_image1=default_image;	
				callback(null,1);
			}else{
				var path=file_base_path;
				path+=results[1][0].driver_license_image1;
				fs.exists(path,(exists)=>{
					if(exists){
						results[1][0].driver_license_image1=results[1][0].driver_license_image1;
						callback(null,1);
					}else{
						results[1][0].driver_license_image1=default_image;	
						callback(null,1);
					}
				});
			}
		}
		],function(errors1,results1){
			data_to_return['status']=200;
			data_to_return['msg']='success';
			data_to_return['data']['vehicle_number']=results[0][0].vehicle_number;
			data_to_return['data']['vehicle_type']=results[0][0].vehicle_type;
			data_to_return['data']['insurance_upath']=results[0][0].insurance_upath;
			data_to_return['data']['rcbook_upath']=results[0][0].rcbook_upath;
			data_to_return['data']['driver_name']=results[1][0].name;
			data_to_return['data']['mobile_number']=results[1][0].mobile_number;
			data_to_return['data']['driver_license_image1']=results[1][0].driver_license_image1;
			res.send(data_to_return);
		});
	});	
}


this.milk_order_lr_details=function(req,res){
	var data_to_return={'status':0,'msg':'Invalid Data','data':{}};

	//validating the data
	req.assert('order_id','Invalid value').notEmpty();
	
	// validating errors
	var errors = req.validationErrors();
	if (errors) {
		data_to_return['data']['error_msg']=errors[0]['msg'];
		// returning the error
		res.send(data_to_return);
		return;
	}

	//data sanitisation	
	var order_id=req.sanitize('order_id').escape().trim();

	var moment=require('moment');
	var async=require('async');
	var fs = require('fs');
	var date=moment(new Date(Date.now())).format('YYYY-MM-DD');

	async.parallel([
	function(callback){
		var query="select moi.oa_id,moi.order_id,moi.order_number,moi.tonnage,moi.source_in_datetime,moi.lr_number,moai.vehicle_id,(select vehicle_number from vehicle_details where id=moai.vehicle_id)as vehicle_number,(select company_name from company where id=moai.company_id) as company_name from milk_order_info moi join milk_order_aggregate_info moai on moai.oa_id=moi.oa_id where moi.order_id="+order_id+" and moi.is_deleted=0";
		var lr_details_data=function(error, result) {
		if (error==1){
			res.send({status: 500, msg: 'Query failed '});
			return;
		}else{
			if(result==""){
				res.send({status: 400, msg: 'No Result found '});
			}else{
				callback(null,result);
			}
		}
		}
		global.db_query.select_query_1(query,lr_details_data);
	},
	function(callback){
		var query1="select comp.company_name,comp.address,comp.city,comp.post_code from company comp join milk_order_aggregate_info moai on moai.company_id=comp.id join milk_order_info moi on moi.oa_id=moai.oa_id where moi.order_id="+order_id+" ";
		var lr_details_data1=function(error1,result1) {
		if (error1==1){
			res.send({status: 500, msg: 'Query failed 1'});
			return;
		}else{
			if(result1==""){
				callback(null,result1);
			}else{
				callback(null,result1);
			}
		}
		}
		global.db_query.select_query_1(query1,lr_details_data1);	
	},
	function(callback){
		var query1="select hi.name,hi.geo_address,hi.city,hi.post_code from milk_route_hub_info hi join milk_order_info oi on oi.hub_id=hi.hub_id where oi.order_id="+order_id+" ";
		var lr_details_data2=function(error2,result2) {
		if (error2==1){
			res.send({status: 500, msg: 'Query failed 2'});
			return;
		}else{
			if(result2==""){
				callback(null,result2);
			}else{
				callback(null,result2);
			}
		}
		}
		global.db_query.select_query_1(query1,lr_details_data2);	
	},
	function(callback){
		var query3="select comp.profile_img_upath from milk_order_info moi join users us on us.id=moi.created_by join company comp on comp.id=us.company_id where moi.order_id="+order_id+" and  (comp.company_type='1' or comp.company_type='2')";
		var lr_details_data3=function(error3,result3){
		if(error3==1){
			res.send({status:500,msg:'Query failed 3'});
		}else{
			if(result3==''){
				data_to_return['data']['profile_img_upath']=idelivery_logo;	
				callback(null,1);
			}else{
				//profile_img_upath
				if(result3[0].profile_img_upath==''||result3[0].profile_img_upath=='null'){
					data_to_return['data']['profile_img_upath']=default_image;	
					callback(null,1);
				}else{
					var path=file_base_path;
					path+=result3[0].profile_img_upath;
					fs.exists(path,(exists)=>{			
					if(exists){
						data_to_return['data']['profile_img_upath']=result3[0].profile_img_upath;
						callback(null,1);
					}else{
						data_to_return['data']['profile_img_upath']=default_image;	
						callback(null,1);
					}
					});
				}	
			}
		}
		}
		global.db_query.select_query_1(query3,lr_details_data3);
	},
	function(callback){
		var lr_details_data4=function(error4,result4) {
		if(error4==1){
			res.send({status: 500, msg: 'Query failed 4'});
			return;
		}else{
			if(result4==""){			
				callback(null,result4);
			}else{
				callback(null,result4);
			}
		}
		}
		global.db_query.select_query("milk_order_info","product,tonnage","order_id=?",[order_id],"",lr_details_data4);	
	}
	],function(errors,results){
		data_to_return['status']=200;
		data_to_return['msg']='success';
		data_to_return['data']['oa_id']=results[0][0].oa_id;
		data_to_return['data']['order_id']=results[0][0].order_id;
		data_to_return['data']['order_number']=results[0][0].order_number;
		data_to_return['data']['tonnage']=results[0][0].tonnage;
		data_to_return['data']['source_in_datetime']=(results[0][0].source_in_datetime==null)?null:moment(results[0][0].source_in_datetime).format('YYYY-MM-DD HH:mm:ss');	
		data_to_return['data']['lr_number']=results[0][0].lr_number;
		data_to_return['data']['vehicle_id']=results[0][0].vehicle_id;
		data_to_return['data']['vehicle_number']=results[0][0].vehicle_number;
		data_to_return['data']['sender_name']=results[1][0].company_name;
		data_to_return['data']['sender_address']=results[1][0].address;	
		data_to_return['data']['sender_city']=results[1][0].city;
		data_to_return['data']['sender_post_code']=results[1][0].post_code;
		data_to_return['data']['Receiver_name']=results[2][0].name;
		data_to_return['data']['Receiver_address']=results[2][0].geo_address;	
		data_to_return['data']['Receiver_city']=results[2][0].city;
		data_to_return['data']['Receiver_post_code']=results[2][0].post_code;
		data_to_return['data']['order_items']=results[4];
		res.send(data_to_return);
	});
} 


this.milk_order_pod_details=function(req,res){
	var data_to_return={'status':0,'msg':'Invalid Data','data':{}};

	//setup validation
	req.assert('order_id','Invalid value').notEmpty();
	
	// validating errors
	var errors = req.validationErrors();
	if (errors) {
		data_to_return['data']['error_msg']=errors[0]['msg'];
		// returning the error
		res.send(data_to_return);
		return;
	}

	//data sanitisation	
	var order_id=req.sanitize('order_id').escape().trim();

	var moment=require('moment');
	var async=require('async');
	var fs = require('fs');
	var date=moment(new Date(Date.now())).format('YYYY-MM-DD');
	async.parallel([
	function(callback){
		var query="select moi.oa_id,moi.order_id,moi.order_number,moi.tonnage,moi.source_in_datetime,moi.lr_number,moai.vehicle_id,(select vehicle_number from vehicle_details where id=moai.vehicle_id)as vehicle_number,moi.pod_remark,moi.pod_rep_name,moi.pod_rep_mobile_number,moi.pod_sign_upath from milk_order_info moi join milk_order_aggregate_info moai on moai.oa_id=moi.oa_id where moi.order_id="+order_id+" and moi.is_deleted=0";
		var pod_details_data=function(error, result) {
		if (error==1){
			res.send({status: 500, msg: 'Query failed '});
			return;
		}else{
			if(result==""){
				res.send({status: 400, msg: 'No Result found '});
			}else{
				callback(null,result);
			}
		}
		}
		global.db_query.select_query_1(query,pod_details_data);
	},
	function(callback){
		var query1="select comp.company_name,comp.address,comp.city,comp.post_code from company comp join milk_order_aggregate_info moai on moai.company_id=comp.id join milk_order_info moi on moi.oa_id=moai.oa_id where moi.order_id="+order_id+" ";
		var pod_details_data1=function(error1,result1) {
		if (error1==1){
			res.send({status: 500, msg: 'Query failed 1'});
			return;
		}else{
			if(result1==""){
				callback(null,result1);
			}else{
				callback(null,result1);
			}
		}
		}
		global.db_query.select_query_1(query1,pod_details_data1);	
	},
	function(callback){
		var query1="select hi.name,hi.geo_address,hi.city,hi.post_code from milk_route_hub_info hi join milk_order_info oi on oi.hub_id=hi.hub_id where oi.order_id="+order_id+" ";
		var pod_details_data2=function(error2,result2) {
		if (error2==1){
			res.send({status: 500, msg: 'Query failed 2'});
			return;
		}else{
			if(result2==""){
				callback(null,result2);
			}else{
				callback(null,result2);
			}
		}
		}
		global.db_query.select_query_1(query1,pod_details_data2);	
	},
	function(callback){
		var query3="select comp.profile_img_upath from milk_order_info moi join users us on us.id=moi.created_by join company comp on comp.id=us.company_id where moi.order_id="+order_id+" and  (comp.company_type='1' or comp.company_type='2')";
		var pod_details_data3=function(error3,result3){
		if(error3==1){
			res.send({status:500,msg:'Query failed 3'});
		}else{
		if(result3==''){
			data_to_return['data']['profile_img_upath']=idelivery_logo;	
			callback(null,1);
		}else{
			//profile_img_upath
			if(result3[0].profile_img_upath==''||result3[0].profile_img_upath=='null'){
				data_to_return['data']['profile_img_upath']=default_image;	
				callback(null,1);
			}else{
				var path=file_base_path;
				path+=result3[0].profile_img_upath;
				fs.exists(path,(exists)=>{			
				if (exists) {
					data_to_return['data']['profile_img_upath']=result3[0].profile_img_upath;
					callback(null,1);
				}else{
					data_to_return['data']['profile_img_upath']=default_image;	
					callback(null,1);
				}
				});
			}		
		}
		}
		}
		global.db_query.select_query_1(query3,pod_details_data3);
	},
	function(callback){
		var pod_details_data4=function(error4,result4) {
		if (error4==1){
			res.send({status: 500, msg: 'Query failed 4'});
			return;
		}else{
			if(result4==""){			
				callback(null,result4);
			}else{
				callback(null,result4);
			}
		}
		}
		global.db_query.select_query("milk_order_info","product,tonnage","order_id=?",[order_id],"",pod_details_data4);	
	},
	function(callback){
		var pod_details_data5=function(error5,result5) {
		if (error5==1){
			res.send({status: 500, msg: 'Query failed 5'});
			return;
		}else{
		if(result5[0].pod_sign_upath=="" ||result5[0].pod_sign_upath=='null'){
			data_to_return['data']['pod_sign_upath']=default_image;
			callback(null,1);
		}else{
			var path=file_base_path;
			path+=result5[0].pod_sign_upath;
			fs.exists(path,(exists)=>{
			if (exists) {
				data_to_return['data']['pod_sign_upath']=result5[0].pod_sign_upath;
				callback(null,1);
			}else{
				data_to_return['data']['pod_sign_upath']=default_image;
				callback(null,1);					
			}
			});
		}
		}
		}
		global.db_query.select_query("milk_order_info","pod_sign_upath","order_id=?",[order_id],"",pod_details_data5);	
	}
	],function(errors,results){
		data_to_return['status']=200;
		data_to_return['msg']='success';
		data_to_return['data']['oa_id']=results[0][0].oa_id;
		data_to_return['data']['order_id']=results[0][0].order_id;
		data_to_return['data']['order_number']=results[0][0].order_number;
		data_to_return['data']['tonnage']=results[0][0].tonnage;
		data_to_return['data']['source_in_datetime']=(results[0][0].source_in_datetime==null)?null:moment(results[0][0].source_in_datetime).format('YYYY-MM-DD HH:mm:ss');	
		data_to_return['data']['lr_number']=results[0][0].lr_number;
		data_to_return['data']['pod_signed_datetime']=results[0][0].pod_signed_datetime==null?null:moment(results[0][0].pod_signed_datetime).format('YYYY-MM-DD HH:mm:ss');		
		data_to_return['data']['pod_remark']=results[0][0].pod_remark;
		data_to_return['data']['pod_rep_name']=results[0][0].pod_rep_name;
		data_to_return['data']['pod_rep_mobile_number']=results[0][0].pod_rep_mobile_number;
		data_to_return['data']['vehicle_id']=results[0][0].vehicle_id;
		data_to_return['data']['vehicle_number']=results[0][0].vehicle_number;
		data_to_return['data']['sender_name']=results[1][0].company_name;
		data_to_return['data']['sender_address']=results[1][0].address;	
		data_to_return['data']['sender_city']=results[1][0].city;
		data_to_return['data']['sender_post_code']=results[1][0].post_code;
		data_to_return['data']['Receiver_name']=results[2][0].name;
		data_to_return['data']['Receiver_address']=results[2][0].geo_address;	
		data_to_return['data']['Receiver_city']=results[2][0].city;
		data_to_return['data']['Receiver_post_code']=results[2][0].post_code;
		data_to_return['data']['order_items']=results[4];
		res.send(data_to_return);
	});
}


this.milk_order_map_view=function(req,res){
	var data_to_return={'status':0,'msg':'Invalid Data','data':{}};

	//Data validation
	req.assert('oa_id','Invalid value').notEmpty();

	var errors = req.validationErrors();
	if (errors) {
		data_to_return['data']['error_msg']=errors[0]['msg'];
		// returning the error
		res.send(data_to_return);
		return;
	}

	//Data Sanitization
	var oa_id=req.sanitize('oa_id').escape().trim();
	var async=require('async');
	var moment=require('moment');
	var date=moment(new Date(Date.now())).format('YYYY-MM-DD HH:mm:ss');
	
	async.waterfall([
	function(callback){
		var query="SELECT moai.device_id,moai.driver_id,moai.device_type,moai.trip_status,moai.vechicle_accept_datetime,moai.trip_completed_datetime FROM milk_order_aggregate_info moai where moai.oa_id='"+oa_id+"' ";
		var order_travel_path=function(err,result){
		if(err==1){
			res.send({'status':500,'msg':'Query Failed'});
		}else{
			if(result==""){
				res.send({'status':400,'msg':'No result found'});
			}else{
				callback(null,result);
			}
		}
		}
		global.db_query.select_query_1(query,order_travel_path);
	}
	],function(errors,results){
 		var device_id=results[0].device_id;
		var driver_id=results[0].driver_id;
		var trip_status=results[0].trip_status;
		var device_type=results[0].device_type;
		var vechicle_accept_datetime=moment(results[0].vechicle_accept_datetime).format('YYYY-MM-DD HH:mm:ss');
		var trip_completed_datetime=moment(results[0].trip_completed_datetime).format('YYYY-MM-DD HH:mm:ss');
		async.parallel([	
		function(callback){
			var query3="SELECT geo_address,geo_address_lat,geo_address_lng,trip_status FROM milk_order_info where oa_id='"+oa_id+"' and is_deleted=0 UNION ALL SELECT geo_address,geo_address_lat,geo_address_lng,trip_status FROM milk_order_aggregate_info where oa_id='"+oa_id+"' and is_deleted=0";
			var order_travel_path1=function(err1,result1){
			if(err1==1){
				res.send({'status':500,'msg':'Query Failed 1'});
			}else{
				if(result1==""){
					res.send({'status':400,'msg':'No result found 1'});
				}else{
					callback(null,result1);
				}
			}
			}
			global.db_query.select_query_1(query3,order_travel_path1);	
		},
		function(callback){
			if(trip_status>1 && (device_type==1 || device_type==2)){
			if(trip_status==2 && device_type==1){
				var query2="lh.created_date between '"+vechicle_accept_datetime+"' and '"+date+"' order by lh.created_date asc";
			}else if(trip_status==3 && device_type==1){
				var query2="lh.created_date between '"+vechicle_accept_datetime+"' and '"+trip_completed_datetime+"' order by lh.created_date asc";
			}
			
			if(trip_status==2 && device_type==2){
				var query2="data_insertion_time between '"+vechicle_accept_datetime+"' and '"+date+"' order by data_insertion_time asc";
			}else if(trip_status==3 && device_type==2){
				var query2="data_insertion_time between '"+vechicle_accept_datetime+"' and '"+trip_completed_datetime+"' order by data_insertion_time asc";
			}
			
			if(device_type==2){			
				var query1="select user_id as driver_id,data_insertion_time as datetime,latitude,longitude,battery_percentage from mob_location_history where user_id="+driver_id+" and status=0 and "+query2+"";
			}else{
				var query1="select lh.running_no,lh.created_date as datetime,lh.lat_message AS latitude,lh.lon_message AS longitude from location_history_7 lh join device_port dp on dp.running_no=lh.running_no where dp.device_id="+device_id+" and "+query2+"";
			}
			var order_travel_path2=function(err2,result2){
			if(err2==1){
				res.send({'status':500,'msg':'Query Failed 2'});
			}else{
				if(result2==""){
					callback(null,result2);
				}else{
					callback(null,result2);
				}
			}
			}
			global.db_query.select_query_1(query1,order_travel_path2);
			}else{
				callback(null,[]);
			}
		},
		function(callback){
			if(trip_status==2 && (device_type==1 || device_type==2)){
			var order_travel_path3=function(err3,result3){
			if(err3==1){
				res.send({'status':500,'msg':'Query Failed 3'});
			}else{
				if(result3==""){
					callback(null,result3);
				}else{
					callback(null,{'lat_message':result3[0].lat_message,'lon_message':result3[0].lon_message,'battery_percentage':result3[0].battery_percentage});
				}
			}
			}
			if(device_type==2){
				global.db_query.select_query("device_port","lat_message,lon_message,battery_percentage","driver_id=? and modified_date > '"+vechicle_accept_datetime+"'",[driver_id],"",order_travel_path3);
			}else{
				global.db_query.select_query("device_port","lat_message,lon_message,battery_percentage","device_id=? and modified_date > '"+vechicle_accept_datetime+"'",[device_id],"",order_travel_path3);
			}
			}else{
				callback(null,[]);
			}
		},
		],function(errors1,results1){
			data_to_return['status']=200;
			data_to_return['msg']='success'; 
			data_to_return['data']['origins']=results1[0];
			data_to_return['data']['latlng']=results1[1];
			data_to_return['data']['vehicle_latlng']=results1[2];
			res.send(data_to_return);
		});
	});
// moai join device_port dp on dp.device_id=moai.device_id
}


this.helpline=function(req,res){
	var data_to_return={'status':0,'msg':'Invalid data','data':{}};
	
	//helpline number 
	var helpline_number=function(error,result){
	if(error==1){
		res.send({'status':500,'msg':'Query Failed'});
		return;
	}else{
		if(result==""){
			res.send({status: 400, msg: 'No Result found'});
			
		}else{
			data_to_return['status']=200;
			data_to_return['msg']='success';
			data_to_return['data']=result;
			res.send(data_to_return);
		}
	}
	}
	global.db_query.select_query("helpline","mobile_no","is_available="+0+"","","LIMIT 1",helpline_number);
}


this.helpline_call_history=function(req,res){
	var data_to_return={'status':0,'msg':'Invalid data','data':{}};
	
	//validating the data
	var mobile_no=req.assert('mobile_no','Invalid value').notEmpty();
		
	//validating the erros
	var errors=req.validationErrors();
	if (errors) {
		data_to_return['data']['error_msg']=errors[0]['msg'];
		// returning the error
		res.send(data_to_return);
		return;
	}
	
	//Sanitizating the data
	var mobile_no=req.sanitize('mobile_no').escape().trim();	
	var moment=require('moment');
	var date=moment(new Date(Date.now())).format('YYYY-MM-DD HH:mm:ss');
	
	var helpline_call_history_data=function(error,result){
	if(error==1){
		res.send({'status':500,'msg':'Query failed'});
	}else{
		if(result==""){
			res.send({'status':400,'msg':'No result found'});
		}else{
			data_to_return['status']=200;
			data_to_return['msg']='success';
			data_to_return['data']=result.insertId;
			res.send(data_to_return);
		}
	}
	}
	global.db_query.insert_query("helpline_history",{'mobile_no':mobile_no,'call_history_datetime':date},helpline_call_history_data);
}


}
var self =module.exports = new driver();
