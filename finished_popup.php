<?php
include("config/config.php");
include("core/class/db_query.php");                            // Class where query generetion is written
include("core/class/db_helper.php");                            // Class where table and feilds 
include("core/function/common.php");
                                                                // are mentioned to generate query
ob_start();                                                     // to clear the internal output
global $db_helper_obj;
global $global_website_url;
$db_helper_obj=new db_helper();

$finished_list=$db_helper_obj->finished_list();
$product_list=$db_helper_obj->product_list();

foreach($finished_list as $va=>$key){
	$products_arr=convert_array($key["products"]);
	foreach($products_arr as $va1=>$key1){
	$already_processed[$key1["product_id"]]["produced_qty"]+=$key1["produced_qty"];
	}
}

$subproduct_list=$db_helper_obj->subproduct_list();
$openstock_list=$db_helper_obj->openstock_list();
$fgreturn_list=$db_helper_obj->fgreturn_list();

foreach($fgreturn_list as $va5=>$key5){
	$fgreturn[$key5["product_id"]]["return_qty"]+=$key5["qty"];
}

foreach($openstock_list as $va5=>$key5){
	$openstock[$key5["subproduct_id"]]["id"]=$key5["subproduct_id"];
	$total_product_edit=$db_helper_obj->total_product_edit($key5["subproduct_id"]);
	if($total_product_edit[0]["process"]==0&&$total_product_edit[0]["machining"]==0){
		$openstock[$key5["subproduct_id"]]["processed_qty"]=$key5["processed_qty"];
	}
	if($total_product_edit[0]["process"]==0&&$total_product_edit[0]["machining"]==1){
		$openstock[$key5["subproduct_id"]]["processed_maching"]=$key5["processed_maching"];
	}
	if($total_product_edit[0]["process"]==1&&$total_product_edit[0]["machining"]==0){
		$openstock[$key5["subproduct_id"]]["processed_buff"]=$key5["processed_buff"];
	}
	if($total_product_edit[0]["process"]==1&&$total_product_edit[0]["machining"]==1){
		$openstock[$key5["subproduct_id"]]["processed_buff"]=$key5["processed_buff"];
	}
		$openstock[$key5["subproduct_id"]]["finished_qty"]=$key5["finished_qty"];
		$openstock[$key5["subproduct_id"]]["processed_finished"]=$key5["processed_finished"];
	//$openstock[$key5["subproduct_id"]]["processed_buff"]=$key5["processed_buff"];
	//$openstock[$key5["subproduct_id"]]["processed_maching"]=$key5["processed_maching"];
}
//echo"<pre>";print_r($openstock);echo"</pre>";
$total_product_list=$db_helper_obj->total_product_list();
$buffing_list=$db_helper_obj->buffing_list();
 
foreach($buffing_list as $va=>$key){
	$products=convert_array($key["products_approve"]);
	if($key["admin_approved"]==1){
		foreach($products as $va1=>$key1){
			$buffed[$key1["product_id"]]["product_id"]=$key1["product_id"];
			$buffed[$key1["product_id"]]["approve"]+=$key1["approve"];
			$buffed[$key1["product_id"]]["reject"]+=$key1["reject"];	
		}
	}
}
//echo"<pre>";print_r($_GET["from"]);echo"</pre>";
//echo"<pre>";print_r($openstock);echo"</pre>";
	foreach($openstock as $va2=>$key2){
		$total_buffed[$key2["id"]]["product_id"]=$key2["id"];
		if($_GET["from"]=='edit')
			$key2["finished"]=$key2["processed_finished"];
		else
			$key2["finished"]=$key2["finished_qty"];
		
		if(($key2["processed_qty"]+$buffed[$key2["id"]]["approve"]+$key2["processed_buff"]+$key2["processed_maching"])>=$key2["finished"])
			$total_buffed[$key2["id"]]["approve"]=($key2["processed_qty"]+$buffed[$key2["id"]]["approve"]+$key2["processed_buff"]+$key2["processed_maching"])-$key2["finished"];	
	}
foreach($_POST["product_id"] as $va7=>$key7){
	$products_choose[$key7]=$key7;
	$producevalue=$_POST["produced_qty"][$va7];
}

foreach($product_list as $va=>$key){
	$total_arr[$key["id"]]["product_id"]=$key["id"];
	$total_arr[$key["id"]]["product_name"]=$key["product_name"];
	$total_arr[$key["id"]]["stocks"]=$key["open_qty"];
	$total_arr[$key["id"]]["series"]=$key["series"];
	foreach($subproduct_list as $va3=>$key3){
		if($key["id"]==$key3["product_id"]){
			$total_arr[$key["id"]]["subproducts"][]=$key3;
			if($key["id"]==$products_choose[$key["id"]]){	
				$subproducts[]=$key3["name"];	
			}
		}
	}
}

foreach(array_filter($_POST["produced_qty"]) as $va5=>$key5){
	$produced_qty+=$key5;
}

foreach($total_arr as $va5=>$key5){
	foreach($key5["subproducts"] as $va6=>$key6){
		if((($total_buffed[$key6["name"]]["approve"])>($_GET["maxi"]*$key6["qty"]))){
			$product[$key6["name"]]=$total_buffed[$key6["name"]]["approve"]-($_GET["maxi"]*$key6["qty"]);
		}
		else 
			$product[$key6["name"]]=$total_buffed[$key6["name"]]["approve"];
	}
}

foreach($total_arr as $va5=>$key5){
	foreach($key5["subproducts"] as $va6=>$key6){
		$posible[$va5][$key6["name"]]["id"]=$key6["name"];
		$posible[$va5][$key6["name"]]["qty"]=$key6["qty"]*$producevalue;
		$posible[$va5][$key6["name"]]["posible_qty"]=$total_buffed[$key6["name"]]["approve"];
	}
}
//echo"<pre>";print_r($posible);echo"</pre>";
?>
	<link rel="icon" type="images/png" href="images/favicon.png">
	<link rel='stylesheet' href='css/font-awesome.css'>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/jquery.mCustomScrollbar.min.css">
	<link rel="stylesheet" href="css/responsive-menu.css">
	<link rel="stylesheet" href="css/style.css">
	<script src="js/jquery.js"></script>
<div class="listwrapper" id="height_id">
<div class="table-responsive"> 
<table class="table listhead table-bordered table-hover" align="center" width="50%" border="1" cellspacing="0" cellpadding="0">
<tr>
	<td align="center"><label>S No</label></td>
	<td align="center"><label>Product Name</label></td>
	<td align="center"><label>Actual Qty</label></td>
	<td align="center"><label>Buffed Qty</label></td>
	<td align="center"><label>Needed Qty</label></td>
</tr>
<?php 
$count=1;
foreach($posible as $va=>$key){
	if($_GET["product_id"]==$va){
	foreach($key as $va1=>$key1){	
?>
<tr>
<td align="center"><?php echo $count; ?></td>
<td style="padding: 4px;">
<label><?php $prod=$db_helper_obj->total_product_edit($va1);
 echo $prod[0]["name"]; ?></label>
</td>
<td  align="center"style="padding: 4px;">
<label><?php echo $key1["qty"]; ?></label>
</td>
<td  align="center"style="padding: 4px;">
<label><?php echo $key1["posible_qty"]; ?></label>
</td>
<td  align="center"style="padding: 4px;">
<label>
	<?php
	if($key1["qty"]>$key1["posible_qty"])
		echo $key1["qty"]-$key1["posible_qty"];
	?>
</label>
</td>
</tr>
<?php $tl_qty+=$key["produced_qty"];$count++; }}} ?>
</table>
</div>
</div>		
<script type="text/javascript">
	setTimeout(function(){ parent.$("#iframe_show_vehicle_data").attr("height",$("#height_id").height()+30+"px"); }, 100);
</script>