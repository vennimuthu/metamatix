<?php
include("config/config.php");

if(!isset($_SESSION["user_id"])){ // checking whether the usertype is logged in
	header('Location: logout.php');        						//REdirection to logout.php
	exit;
}
include("core/class/db_query.php");                             // Class where query generetion is written
include("core/class/db_helper.php");                            // Class where table and feilds 
include("core/function/common.php");   	
                                                                // are mentioned to generate query
ob_start();                                                     // to clear the internal output
global $db_helper_obj;
global $global_website_url;
$db_helper_obj=new db_helper();  

$_GET["id"]=$_GET["po_id"];
$pointent=$db_helper_obj->pointent_edit($_GET["po_id"]);
$purchase_intent=convert_array($pointent[0]["puchase_intent"]);
$products_approve=convert_array($pointent[0]["products_approve"]);

$pointentsub=$db_helper_obj->pointentsub_list($pointent[0]["id"]);
$final_array=array();

foreach($pointent as $va=>$key){
	$receipt=convert_array($key["puchase_intent"]);
	$receipt_approve=convert_array($key["products_approve"]);

	foreach($receipt as $va1=>$key1){
		$final_array3[$key1["subproduct_id"]]["subproduct_id"]=$key1["subproduct_id"];
		$final_array3[$key1["subproduct_id"]]["product_name"]=$key1["product_name"];
		$final_array3[$key1["subproduct_id"]]["qty"]=$key1["qty"];
		$final_array3[$key1["subproduct_id"]]["qty_approved"]=$receipt_approve[$va1]["qty_approved"];
		$final_array3[$key1["subproduct_id"]]["reject"]=$key1["qty"]-$receipt_approve[$va1]["qty_approved"];
	}
}
//echo"<pre>"; print_r($receipt_approve); echo"</pre>"; 
foreach($pointentsub as $va=>$key){
	$final_array[$key["subproduct_id"]]["subproduct_id"]=$key["subproduct_id"];
	$final_array[$key["subproduct_id"]]["product_name"]=$key["product_name"];
	$final_array[$key["subproduct_id"]]["fg_stock"]=$key["fg_stock"];
	$final_array[$key["subproduct_id"]]["qty"]=$key["qty"];
	$final_array[$key["subproduct_id"]]["remarks"]=$key["remarks"];
	$final_array[$key["subproduct_id"]]["stock"]=$key["stock"];
	//$final_array[$key["subproduct_id"]]["reject"]=$key["qty"]-$key["qty_approved"];
}
foreach(convert_array($pointent[0]["products_approve"]) as $va1 =>$key1)
{
	$final_array[$key["subproduct_id"]]["subproduct_id"]=$key["subproduct_id"];
	$final_array[$key["subproduct_id"]]["qty_approved"]=$key["qty_approved"];
}
$lan2=explode(",", $pointent[0]['category']);

?>
<html>
<head>
<link rel="icon" type="images/png" href="">
  <link rel='stylesheet' href='css/font-awesome.css'>
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
   <link rel="stylesheet" href="css/responsive-menu.css">
  <link rel="stylesheet" href="css/style.css">
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap.js"></script>
  <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
  <link href="css/select2.min.css" rel="stylesheet" />
  <script src="js/select2.min.js"></script>
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" />
  <script src="js/modernizr-custom.js"></script>
  <script src="js/responsive-menu.js"></script>
</head>


<div>
<input type="submit" name="report_ids" id="report_ids" class="btn btn-primary pull-right"   onclick="printDiv('printableArea')" value="Print"></div>

<div id="printableArea">

<table class="table listhead table-bordered table-hover" align="center" width="50%" border="1" cellspacing="0" cellpadding="0"> 

<div class="listwrapper" id="height_id">


<div class="table-responsive"> 
<tr> <td colspan="8" align="center"><b> PURCHASE INTENT</b></td></tr>
<tr>
<td width="70px" ><label>PI No:</label></td>
<td>
<label><?php 
echo $pointent[0]["intent_no"];
 ?></label>
</td>
<td><label>PI Date:</label></td>
<td  colspan="3"><label><?php 
echo date('d-M-Y',strtotime($pointent[0]["intent_date"]));
 ?></label></td>
  <td><label>Supplier</label></td>
<td  colspan="2">
<label><?php 
$prod=$db_helper_obj->supplier_edit($pointent[0]["supplier"]);
echo $prod[0]["supplier_name"]; ?></label>
</label>
 </td> 

<!--<td width="70px"><label>Type of Employee</label></td>
<td    colspan="2">
<label><?php 
/*if($buffing_view[0]["emp_type"]==1)
	echo "Contractor";
else if($buffing_view[0]["emp_type"]==2)
	echo "Regular";
else if($buffing_view[0]["emp_type"]==3)
	echo "General";
 */?></label>
 </td> -->
</tr>
<!--<tr>
<td width="70px" ><label>PI Date:</label></td>
<td>
<label><?php 
echo date('d-M-Y',strtotime($pointent[0]["intent_date"]));
 ?></label>
</td>
<td></td>
<td></td>
<td></td>-->
<!-- <tr>
 <td><label>Supplier</label></td>
<td  colspan="2">
<label><?php 
//$prod=$db_helper_obj->supplier_edit($pointent[0]["supplier"]);
//echo $prod[0]["supplier_name"]; ?></label>
</label>
 </td> 
 </tr>-->
</tr>
<tr>
<!--
<td width="70px" colspan="2"><label>Category:</label></td>
<td><label>
 <?php 
/*$lan=explode(",",$pointent[0]['category']);
$lan1=array();
foreach ($lan as $key => $value1)
{
if($value1==1)
$lan1[$key]= "ROTULES ";
elseif($value1==2)
$lan1[$key]= "SPIDERS";
elseif($value1==3)
$lan1[$key]=  "CONNECTORS";	
elseif($value1==3)	
$lan1[$key]=  "OTHERS";	
	}
	
 echo implode(",",$lan1);*/?></label>
</td>-->
<!--
<td width="70px" colspan="2"><label>Name of Employee</label></td>
<td  >
<label> <?php /*$prod=$db_helper_obj->employee_edit($buffing_view[0]["name"]); echo $prod[0]["name"];
 ?></label>
</td> 
<td width="70px"><label>Shift Supervisor</label></td>
<td    colspan="2">
<label><?php $prod=$db_helper_obj->employee_edit($buffing_view[0]["supervisor_name"]); echo $prod[0]["name"];
*/ ?></label>
 </td>
</tr> -->
<tr>
<td align="center"><label>S No</label></td>
<td align="center" colspan="2" ><label>SUB COMPONENT NAME</label></td>
<td align="center" style="width:90px;"><label>Finished&nbsp;Stocks</label></td>
<td align="center" colspan="2"><label>Stock in Hand</label></td>
<td align="center"><label>Needed Qty</label></td>

<td align="center"><label>Remarks</label></td>
</tr>
<?php $count=1;
 foreach($final_array as $va=>$key){
	 $total_qtyneeded+=$key["qty"];
	 //$total_qtyapproved+=$key["qty_approved"];
	 //$total_qtyreject= $total_qtyneeded-$total_qtyapproved;
	 
	if($key["qty"]!=0) {
 ?>
<tr >
<td align="center"><?php echo $count; ?></td>
<td style="padding: 4px;" colspan="2" >
<label><?php $prod=$db_helper_obj->total_product_edit($key["subproduct_id"]);
 echo $prod[0]["name"]; ?></label>
</td>
<td align="center" style="padding: 4px;">
<label><?php echo $key["fg_stock"]; ?></label>
</td>
<td align="center" style="padding: 4px;" colspan="2" >
<label><?php echo $key["stock"]; ?></label>
</td>
<td align="center" style="padding: 4px;">
<label><?php echo $key["qty"]; ?></label>
</td>
<td align="center" style="padding: 4px;">
<label><?php echo $key["remarks"]; ?></label>
</td>
</tr>


<!--
<td align="center" style="padding: 4px;">
<label><?php //echo $key["reject"]; ?></label>
</td>
<td align="center" style="padding: 4px;">
<label><?php  ?></label>
</td>-->

	<?php $count++; } }?>

<tr>
<!--
<td rowspan="3" colspan="2"> <label>Remarks:</label><br/><label><?php //echo $buffing_view[0]["qc_comments"]; ?></label></td> -->
<td align="right" colspan="6" ><label>Total Qty Needed</label></td>
<td align="left" style="padding-left:50px;"colspan="2" ><?php echo $total_qtyneeded; ?></td>
</tr>


<!--
<tr>
<td align="center"><label>Total Qty Approved</label></td>
<td align="right"  colspan="2"> <label><?php echo $total_qtyapproved; ?></label></td>
</tr>
<tr>
<td align="center"><label>Total qty Rejected</label></td>
<td align="right"  colspan="2"> <label><?php echo $total_qtyreject; ?></label></td>
</tr>
<tr>
<td align="center"><label>SL No</label></td>
<td align="center"><label>Regular Time/Over Time</label></td>
<td align="center" style="width:90px;"><label>Work Hour</label></td>
<td align="center"><label>Rate Per Hour</label></td>
<td align="center"><label>Total Amount</label></td>
</tr>

<tr >
<td align="center">1</td>
<td style="padding: 4px;">
<label>Regular Time</label>
</td>
<td  align="center" style="padding: 4px;">
<label><?php /*echo $buffing_view[0]["regular_time"]; ?></label>
 </td>
<td  align="center" style="padding: 4px;">
<label><?php echo $buffing_view[0]["regular_price"]; ?></label>
</td>
<td  align="center" style="padding: 4px;">
<label><?php echo $buffing_view[0]["regular_amount"]; ?></label>
</td>
</tr>
<tr >
<td align="center">2</td>
<td style="padding: 4px;">
<label>Over Time</label>
</td>
<td  align="center" style="padding: 4px;">
<label><?php echo $buffing_view[0]["over_time"]; ?></label>
 </td>
<td  align="center" style="padding: 4px;">
<label><?php echo $buffing_view[0]["overtime_price"]; ?></label>
</td>
<td  align="center" style="padding: 4px;">
<label><?php echo $buffing_view[0]["overtime_amount"]; ?></label>
</td>
</tr>
<tr><td align="center"><?php echo $count; ?></td><td></td><td></td><td></td><td></td></tr>
<tr>
<td rowspan="2" colspan="3"> <label>Purpose Of OverTime</label><br/><label><?php echo $buffing_view[0]["comments"]; ?></label></td>
<td align="center"><label>Hours Required</label></td>
<td align="right" colspan="2"> <label><?php echo $buffing_view[0]["aprve_time_before"]; ?></label></td>

</tr>
<tr>
<td align="center"><label>Total Payable</label></td>
<td colspan="5" align="right" ><?php echo $total_amount; */?></td>
</tr>
-->
<tr>
	<td  colspan="3" align="center" ><label>Requested By</label><br/><?php echo $pointent[0]["requested_by"]; ?></td>
	<td colspan="3" align="center"><label>Po Number</label><br/><?php echo $pointent[0]["po_number"]; ?></td>
	<td colspan="2" align="center"><label>Verified By</label><br/><?php echo $pointent[0]["approve_by"]; ?></td>
</tr>
</div>

</table>

</div>
</div>
<script type="text/javascript">
parent.$("#iframe_show_vehicle_data").attr("height",$("#height_id").height()+500+"px");

function printDiv(printableArea) {
    var printContents = document.getElementById(printableArea).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
}

</script>
