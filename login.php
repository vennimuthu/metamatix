<?php 
include("config/config.php");
if(isset($_SESSION["user_id"])&&isset($_SESSION["paswrd"])){ // checking whether the usertype is veh owner
    
	if($_SESSION["user_type"]==1)
		header('Location:inward_register.php');
	else if($_SESSION["user_type"]==2)
		header('Location: inward_register.php');
	else if($_SESSION["user_type"]==3)
		header('Location: inwardappr_list.php');
	else if($_SESSION["user_type"]==4)
		header('Location: inward_list.php');
	exit;
}
include("core/class/db_query.php");       // Class where query generetion is written
include("core/class/db_helper.php");      // Class where table and feilds 
include("core/function/common.php"); 
ob_start();                               // to clear the internal output
global $db_helper_obj;
global $global_website_url;
$db_helper_obj=new db_helper(); 
if(isset($_POST["submit"]) && $_POST["submit"]=="login"){

	if(isset($_POST["u_name"]) && isset($_POST["p_word"]) && $_POST["u_name"] && $_POST["p_word"]){
		$log_data=$db_helper_obj->login(array($_POST["u_name"],password_hashing($_POST["p_word"])));
		if(count($log_data)==1){
			$_SESSION["user_id"]=$log_data[0]["id"];
			$_SESSION["user_name"]=$log_data[0]["name"];
			$_SESSION["paswrd"]=$log_data[0]["password"];
			$_SESSION["user_type"]=$log_data[0]["user_type"];
			if($_SESSION["user_type"]==1)
				header('Location: dashboard.php');
			else if($_SESSION["user_type"]==2)
				header('Location: inward_register.php');
			else if($_SESSION["user_type"]==3)
				header('Location: inwardappr_list.php');
			else if($_SESSION["user_type"]==4)
				header('Location: inward_list.php');
			}else
				$error="Invalid Login";
	}else
		$error="Enter Valid User Name and Password";
}?>

<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>metamatix Login</title>

	<link rel="icon" type="images/png" href="">
	<link rel='stylesheet' href='css/font-awesome.css'>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/jquery.mCustomScrollbar.min.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/style1.css">  
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.js"></script>
  <style>
  body {
	  background: #76b852; /* fallback for old browsers */
	  background: -webkit-linear-gradient(right, #76b852, #8DC26F);
	  background: -moz-linear-gradient(right, #76b852, #8DC26F);
	  background: -o-linear-gradient(right, #76b852, #8DC26F);
	  background: linear-gradient(to left, #ffffff, #ffffff);
	  font-family: "Roboto", sans-serif;
	  -webkit-font-smoothing: antialiased;
	  -moz-osx-font-smoothing: grayscale;      
   }
  </style>

  
</head>

<body>
	<div class="login-page">
      <div class="form">
      	<div class="login_logo" align="center">
        	<img src="images/Metamatix_logo.png" class="img-responsive" alt="Indoshell Cast">
		</div>
        <form class="login-form" method="post">
		<?php if(isset($error) && $error!=""){?>
		<div class="alert alert-danger">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<?php echo $error; ?>
		</div>
		<?php } ?>
		<input type="text" name="u_name" placeholder="username"/>
		<input type="password" name="p_word" placeholder="password"/>
		<!--<button>login</button> -->
		<input type="submit" name="submit" value="login" class="login_btn">
        <?php /*<p class="message"><a href="#">Forgot Password</a></p> */ ?>
        </form>
      </div>
    </div>
</body>

</html>
