<?php
include("config/config.php");

if(!isset($_SESSION["user_id"])){ // checking whether the usertype is logged in
	header('Location: logout.php');        						//REdirection to logout.php
	exit;
}

include("core/class/db_query.php");                             // Class where query generetion is written
include("core/class/db_helper.php");                            // Class where table and feilds 
include("core/function/common.php");
                                                               // are mentioned to generate query
ob_start();														// to clear the internal output
global $db_helper_obj;
global $global_website_url;
$db_helper_obj=new db_helper();  

$inward_view=$db_helper_obj->inward_view($_GET["process_id"]);
//$inward_view=$inward_view1[0];

foreach($inward_view as $va=>$key){
	$receipt=convert_array($key["products"]);
	$qtylist=convert_array($key["qty_list"]);
}

$total=0;
foreach($receipt as $va2=>$key2){	
$total+=$key2["amount"];
}

?>
<html>
<head>
<link rel="icon" type="images/png" href="">
  <link rel='stylesheet' href='css/font-awesome.css'>
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
   <link rel="stylesheet" href="css/responsive-menu.css">
  <link rel="stylesheet" href="css/style.css">
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap.js"></script>
  <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
  <link href="css/select2.min.css" rel="stylesheet" />
  <script src="js/select2.min.js"></script>
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" />
  <script src="js/modernizr-custom.js"></script>
  <script src="js/responsive-menu.js"></script>
</head>
<div class="listwrapper" id="height_id">

<div class="table-responsive"> 
<table class="table listhead table-bordered table-hover" align="center" width="50%" border="1" cellspacing="0" cellpadding="0">
<tr>	
<td width="70px"><label>Date:</label></td>
<td  >
<label><?php 
echo date('d-m-Y',$inward_view[0]["dated"]);
 ?></label>
</td>
<td width="70px"><label>GRN NO:</label></td>
<td colspan="2">
<label><?php echo $inward_view[0]["inward_no"]; ?></label>
 </td>
</tr>
<tr>
<td><label>DC/INV NO</label></td>

<td  >
<label><?php 

echo $inward_view[0]["inv_no"];
 ?></label>
</td>
<td width="70px"><label>Supplier Name:</label></td>
<td colspan="2">
<label><?php 
$prod=$db_helper_obj->supplier_edit($inward_view[0]["supplier"]);
echo $prod[0]["supplier_name"]; ?></label>
</td>
</tr>

<tr>
<td width="70px"><label>Inv Date</label></td>
<td  >
<label><?php 

echo date('d-m-Y',$inward_view[0]["inv_date"]);
 ?></label>
</td>
<td width="70px"><label>Received By</label></td>
<td colspan="2">
<label><?php echo $inward_view[0]["received_by"]; ?></label>
</td>
</tr>
<td align="center"><label>S No</label></td>
<td align="center"><label>Item Name</label></td>
<td align="center"><label>Specification</label></td>
<td align="center" style="width:90px;"><label>Qty Received</label></td>
<td align="center"><label>Qty Ordered</label></td>

</tr>
<?php
$count=1;
 foreach($receipt as $va=>$key){
	if($key["qty"]!=0) {
 ?>
<tr >
<td align="center"><?php echo $count; ?></td>
<td align="center" style="padding: 4px;">
<label><?php $prod2=$db_helper_obj->total_product_edit($key["product_id"]);
 echo $prod2[0]["name"]; ?></label>
</td>
<td  align="center" style="padding: 4px;">
<label><?php echo $key["specification"]; ?></label>
</td>
<td  align="center" style="padding: 4px;">
<label><?php $total_rece+=$key["qty"]; echo $key["qty"]; ?></label>
 </td>
<td  align="center" style="padding: 4px;">
<label><?php echo $key["order_qty"]; ?></label>
</td>

</tr>
	<?php $count++; } }?>
 
<tr>
<td rowspan="3" colspan="3"> <label>Remarks:</label><br/><label><?php echo $inward_view[0]["comments"]; ?></label></td>
<td align="center"><label>Total Qty Ordered</label></td>
<td align="right"> <label><?php echo $qtylist["total_order"]; ?></label></td>
</tr>
<tr>

<td align="center"><label>Total Qty Received</label></td>
<td align="right"> <label><?php echo $total_rece;//$qtylist["total_received"]; ?></label></td>
</tr>
<tr>
<td align="center"><label>Balance Qty</label></td>
<td align="right"> <label><?php echo $qtylist["total_order"]-$total_rece;//$qtylist["total_balance"]; ?></label></td>
</tr>
</table>
</div>
</div>
<script type="text/javascript">
setTimeout(function(){ parent.$("#iframe_show_vehicle_data").attr("height",$("#height_id").height()+30+"px"); }, 100);
</script>
