<?php
include("config/config.php");

if(!isset($_SESSION["user_id"])){ // checking whether the usertype is logged in
	header('Location: logout.php');        						//REdirection to logout.php
	exit;
}

include("core/class/db_query.php");                          // Class where query generetion is written
include("core/class/db_helper.php");                            // Class where table and feilds 
include("core/function/common.php");   	
                                                                // are mentioned to generate query
ob_start();                                                     // to clear the internal output
$db_helper_obj=new db_helper();  
$buffing_view=$db_helper_obj->buffing_view($_GET["process_id"]);

$final_array=array();
foreach($buffing_view as $va=>$key){
	$receipt=convert_array($key["products"]);
	$receipt_approve=convert_array($key["products_approve"]);
	//echo"<pre>";print_r($receipt);echo"</pre>";
	foreach($receipt as $va1=>$key1){
		$final_array[$key1["product_id"]]["product_id"]=$key1["product_id"];
		$final_array[$key1["product_id"]]["product_name"]=$key1["product_name"];
		$final_array[$key1["product_id"]]["buffed_qty"]=$key1["buffed_qty"];
		$final_array[$key1["product_id"]]["price"]=$key1["price"];
		$final_array[$key1["product_id"]]["approve"]=$receipt_approve[$va1]["approve"];
		$final_array[$key1["product_id"]]["reject"]=$key1["buffed_qty"]-$receipt_approve[$va1]["approve"];
	}
}

$total=0;
if($buffing_view[0]["emp_type"]==1){
foreach($final_array as $va2=>$key2){	
$total_buffed+=$key2["buffed_qty"];
$total_approve+=$key2["approve"];
$total_reject+=$key2["buffed_qty"]-$key2["approve"];
}
}
else{
foreach($receipt as $va2=>$key2){
$buffing_view[0]["regular_time"]=$key2["regular_time"];
$buffing_view[0]["regular_price"]=$key2["regular_price"];
$buffing_view[0]["regular_amount"]=$key2["regular_time"]*$key2["regular_price"];
$buffing_view[0]["over_time"]=$key2["over_time"];
$buffing_view[0]["overtime_price"]=$key2["overtime_price"];
$buffing_view[0]["overtime_amount"]=$key2["over_time"]*$key2["overtime_price"];

$total_amount=$buffing_view[0]["regular_amount"]+$buffing_view[0]["overtime_amount"];
}
}

?>
<html>
<head>
<link rel="icon" type="images/png" href="">
  <link rel='stylesheet' href='css/font-awesome.css'>
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
   <link rel="stylesheet" href="css/responsive-menu.css">
  <link rel="stylesheet" href="css/style.css">
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap.js"></script>
  <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
  <link href="css/select2.min.css" rel="stylesheet" />
  <script src="js/select2.min.js"></script>
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" />
  <script src="js/modernizr-custom.js"></script>
  <script src="js/responsive-menu.js"></script>
</head>
<div class="listwrapper" id="height_id">

<div class="table-responsive"> 
<table class="table listhead table-bordered table-hover" align="center" width="50%" border="1" cellspacing="0" cellpadding="0">
<tr>
<td width="70px" colspan="2"><label>Bpl No : </label></td>
<td  >
<label><?php 
echo $buffing_view[0]["bpl_no"];
 ?></label>
</td>
<td width="70px" colspan="2"> <label>Type of Employee</label></td>
<td   colspan="3">
<label><?php 
if($buffing_view[0]["emp_type"]==1)
	echo "Contractor";
else if($buffing_view[0]["emp_type"]==2)
	echo "Regular";
else if($buffing_view[0]["emp_type"]==3)
	echo "General";
 ?></label>
 </td>
</tr>
<tr>
<td width="70px" colspan="2"><label>Bpl Date :</label></td>
<td>
<label><?php 
echo date("d-m-Y",$buffing_view[0]["buffing_date"]);
 ?></label>
</td>
<td colspan="2"><label>Nature of Work</label></td>
<td  colspan="3">
<label><?php 
$prod=$db_helper_obj->employee_edit($buffing_view[0]["name"]);
$prod1=$db_helper_obj->roles_edit($prod[0]["role"]);
echo $prod1[0]["role_name"];
 ?></label>
 </td>
</tr>
<tr>
<td width="70px" colspan="2"><label>Name of Employee</label></td>
<td  >
<label><?php 
$prod=$db_helper_obj->employee_edit($buffing_view[0]["name"]);
echo $prod[0]["name"];
 ?></label>
</td>
<td width="70px" colspan="2"><label>Shift Supervisor</label></td>
<td colspan="3">
<label><?php 
$prod=$db_helper_obj->employee_edit($buffing_view[0]["supervisor_name"]);
echo $prod[0]["name"];
 ?></label>
 </td>
</tr>
<?php if($buffing_view[0]["emp_type"]==1){ ?>
<tr>
<td align="center"><label>S No</label></td>
<td align="center"><label>Sub Component Name</label></td>
<td align="center" style="width:90px;"><label>Series</label></td>
<td align="center"><label>Buffed Qty</label></td>
<td align="center"><label>Approved Qty</label></td>
<td align="center"><label>Piece Rate</label></td>
<td align="center"><label>Amount</label></td>
<td align="center"><label>Rejected Qty</label></td>
</tr>
<?php $count=1;

 foreach($final_array as $va=>$key){
	if($key["buffed_qty"]!=0) {
 ?>
<tr >
<td align="center"><?php echo $count; ?></td>
<td  align="center" style="padding: 4px;">
<label><?php $prod=$db_helper_obj->total_product_edit($key["product_id"]);
 echo $prod[0]["name"]; ?></label>
</td>
<td align="center" style="padding: 4px;">
<label><?php echo $key["product_name"]; ?></label>
</td>
<td align="center" style="padding: 4px;">
<label><?php echo $key["buffed_qty"]; ?></label>
</td>
<td align="center" style="padding: 4px;">
<label><?php echo $key["approve"]; ?></label>
</td>
<td align="center" style="padding: 4px;">
<label><?php echo $key["price"]; ?></label>
</td>
<td align="center" style="padding: 4px;">
<label><?php echo $key["approve"]*$key["price"]; ?></label>
</td>
<td align="center" style="padding: 4px;">
<label><?php echo $key["reject"]; ?></label>
</td>
</tr>
<?php $count++; } } ?>
<tr>
<td rowspan="3" colspan="5"> <label>Remarks:</label><br/><label><?php echo $buffing_view[0]["admin_comments"]; ?></label></td>
<td align="center"><label>Total Qty buffed</label></td>
<td align="right"  colspan="2"> <label><?php echo $total_buffed; ?></label></td>
</tr>
<tr>
<td align="center"><label>Total Qty Approved</label></td>
<td align="right"  colspan="2"> <label><?php echo $total_approve; ?></label></td>
</tr>
<tr>
<td align="center"><label>Total Qty Rejected</label></td>
<td align="right"  colspan="2"> <label><?php echo $total_reject; ?></label></td>
</tr>
<?php }else{ ?>
<tr>
<td align="center"><label>S No</label></td>
<td align="center"><label>Regular Time/Over Time</label></td>
<td align="center" style="width:90px;"><label>Work Hour</label></td>
<td align="center"><label>Rate Per Hour</label></td>
<td align="center" colspan="2" ><label>Total Amount</label></td>
</tr>

<tr>
<td align="center">1</td>
<td style="padding: 4px;">
<label>Regular Time</label>
</td>
<td  align="center" style="padding: 4px;">
<label><?php echo $buffing_view[0]["regular_time"]; ?></label>
 </td>
<td  align="center" style="padding: 4px;">
<label><?php echo $buffing_view[0]["regular_price"]; ?></label>
</td>
<td  align="center" colspan="2" style="padding: 4px;">
<label><?php echo $buffing_view[0]["regular_amount"]; ?></label>
</td>
</tr>
<tr>
<td align="center">2</td>
<td style="padding: 4px;">
<label>Over Time</label>
</td>
<td  align="center"style="padding: 4px;">
<label><?php echo $buffing_view[0]["over_time"]; ?></label>
 </td>
<td  align="center"style="padding: 4px;">
<label><?php echo $buffing_view[0]["overtime_price"]; ?></label>
</td>
<td  align="center" colspan="2" style="padding: 4px;">
<label><?php echo $buffing_view[0]["overtime_amount"]; ?></label>
</td>
</tr>
<tr><td align="center"><?php echo $count; ?></td>
<td colspan="7"></td></tr>
<tr>
<td rowspan="2" colspan="3"> <label>Purpose Of OverTime</label><br/><label><?php echo $buffing_view[0]["comments"]; ?></label></td>
<td align="center"><label>Hours Required</label></td>
<td align="right" colspan="2"> <label><?php echo $buffing_view[0]["aprve_time_before"]; ?></label></td>

</tr>
<tr>
<td align="center"><label>Total Payable</label></td>
<td colspan="5" align="right" ><?php echo $total_amount; ?></td>
</tr>
<?php } ?>
<tr>
<td align="center" colspan="2"><label>Processed By</label></td>
<td colspan="6" ><?php echo $buffing_view[0]["admin_processed"]; ?></td>
</tr>
</table>
</div>
</div>
<script type="text/javascript">
	setTimeout(function(){ parent.$("#iframe_show_vehicle_data").attr("height",$("#height_id").height()+30+"px"); }, 100);
</script>
