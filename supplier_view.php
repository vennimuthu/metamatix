<?php 
include("config/config.php");

if(!isset($_SESSION["user_id"])){ // checking whether the usertype is logged in
	header('Location: logout.php');        						//REdirection to logout.php
	exit;
}

include("core/class/db_query.php");                             // Class where query generetion is written
include("core/class/db_helper.php");                            // Class where table and feilds 
                                                                // are mentioned to generate query
ob_start();                                                     // to clear the internal output
global $db_helper_obj;
global $global_website_url;
$db_helper_obj=new db_helper();                                 // Creating object to get data from table
$current_page = basename($_SERVER['PHP_SELF']); // to get the page name from url
global $month_name_arr;
$month_name_arr=array('1'=>'Jan','2'=>'Feb','3'=>'Mar','4'=>'Apr','5'=>'May','6'=>'Jun','7'=>'Jul','8'=>'Aug','9'=>'Sep','10'=>'Oct','11'=>'Nov','12'=>'Dec');

?>
	<link rel="icon" type="images/png" href="images/favicon.png">
	<link rel='stylesheet' href='css/font-awesome.css'>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/jquery.mCustomScrollbar.min.css">
	<link rel="stylesheet" href="css/responsive-menu.css">
	<link rel="stylesheet" href="css/style.css">
	<script src="js/jquery.js"></script>

<?php 

	$db_query=new db_query();  
	global $db_helper_obj;
	$db_helper_obj=new db_helper();

	if(!empty($_REQUEST)){
	
	$master=$db_helper_obj->supplier_edit($_REQUEST["supplier_id"]); 
	
	}
?>
<style>
	.cost_form { }
	.cost_form input.form-control { margin-bottom:10px; }
</style>

							  
                        <div class="table-responsive" id="supplier_viewid"> 
                            <table class="table listhead table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Supplier Name</th>
										<th>Supplier Id</th>
                                        <th>Address</th>
										<th>City</th>
										<th>State</th>
										<th>GST NO</th>
										<th>Payment Terms</th>
										<th>Pincode</th>
                                    </tr>
                                </thead>
                                <tbody>
								 <?php $count=1;
									if(!empty($master)){	
									foreach($master as $va=>$key){ ?>   
									<tr>
                                        <td><?php echo $key["supplier_name"] ?></td>
										<td><?php echo $key["vendor_id"] ?></td>
										<td><?php echo $key["address"] ?></td>
										<td><?php echo $key["city"] ?></td>
										<td><?php echo $key["state"] ?></td>
										<td><?php echo $key["gst_no"] ?></td>
										<td><?php echo $key["payment_terms"] ?></td>
										<td><?php echo $key["pincode"] ?></td>										
                                    </tr>
                                  <?php $count++; }} ?>  
                                </tbody>
                            </table>
                        </div>        
<script type="text/javascript">						
setTimeout(function(){ parent.$("#iframe_show_vehicle_data").attr("height",$("#supplier_viewid").height()+40+"px"); }, 100);	
</script>					