<?php
error_reporting(0);
include ("MPDF57/mpdf.php");
$html = '
<div class="container-fluid">
 <form name="subproduct_add" action="order_addnew.php" id="subproduct_add" method="post" target="_blank">
<div class="col-sm-13">
</div><br />
<br />
<input type="hidden" name="total_pro" value="<?php echo base64_encode(json_encode($final_array)); ?>">
<div class="listwrapper">
<div class="table-responsive"> 
<div id="print_div">
<table align="center" class="table listhead table-bordered table-hover" cellspacing="0" cols="6" border="0">
         <colgroup>
            <col width="60">
            <col width="301">
            <col width="133">
            <col width="119">
            <col width="111">
            <col width="244">
         </colgroup>
         <tbody>
            <tr>
               <td class="pur_head" colspan="6" width="945" height="95" align="CENTER" valign="MIDDLE">
			   <img src="images/Metamatix_logo.png" style="width:200px; height:60px; alt="metamatix" />
			   <p></p>
			   <b><font>PURCHASE ORDER</font></b></td>
            </tr>
            <tr>
               <td class="company" height="20" align="LEFT" colspan="2" >
				<b><font><?php echo $shipto[0]["ship_name"]; ?></font></b>
			   </td>
               <td align="LEFT"  >
			   <b><br></b>
			   </td>
               <td align="RIGHT"  colspan="2">
			   <b><font>PO Number:</font></b>
			   </td>
               <td  align="LEFT" valign="MIDDLE" >
			   <b><font ><?php echo $vall; ?></font>
			   <input type="hidden" name="pur_number" value="<?php echo $vall; ?>">
			   </b>
			   </td>
            </tr>
            <tr>
               <td class="company" colspan="2" align="LEFT">
			   <b><font><?php echo $add1; ?></font></b>
			   </td>
			   <td align="RIGHT" >
			   <b><br></b>
			   </td>
               <td  align="LEFT" valign="MIDDLE" >
			   <b><br></b>
			   </td>
               <td align="RIGHT" >
			   <b><font>PO Date:</font></b>
			   </td>
               <td class="border_cls1" align="LEFT" valign="MIDDLE" >
			  
                  <input type="date" class="form-control" name="dates" id="dates" value="<?php echo date("Y-m-d"); ?>">
               </td>
            </tr>
            <tr>
               <td class="company" height="20" colspan="2" align="LEFT">
			   <b><font><?php echo $add2; ?></font></b>
			   </td>
               <td align="RIGHT" >
			   <b><br></b>
			   </td>
               <td  align="LEFT" valign="MIDDLE" >
			   <b><br></b>
			   </td>
               <td align="RIGHT" >
			   <b><font>Vendor ID:</font></b>
			   </td>
               <td align="LEFT" valign="MIDDLE" >
			   <b><font id="vendor_id">
			   <?php echo $supplier_list[0]["vendor_id"]; ?></font>
			   <input type="hidden" name="vendor_number" id="vendor_number" value="<?php echo $supplier_list[0]["vendor_id"]; ?>"></b>
			   </td>
            </tr>
            <tr>
               <td class="company" height="20"  colspan="2" align="LEFT">
			   <b><font><?php echo $shipto[0]["ship_city"]; ?></font></b>
			   </td>
               <td align="LEFT" >
			  <br>
			   </td>
               <td align="LEFT" >
			   <br>
			   </td>
               <td align="LEFT" >
			   <br>
			   </td>
               <td class="border_cls1" align="LEFT" >
			   <br>
			   </td>
            </tr>
            <tr>
               <td class="company" colspan="3" height="19" align="LEFT" >
			   <b>Supplier: </b>
			   
			   <p></p>
			   <b>GSTIN: </b> 
			   </td>
               <td align="LEFT" >
			   <b><br></b>
			   </td>
               <td align="LEFT" >
			   <b>Ship To:</b>
			   </td>
               <td class="border_cls1" align="LEFT" >
			   <b><br></b>
			   </td>
            </tr>
            <tr>
               <td class="company" height="19" colspan="2"  align="LEFT" bgcolor="#FFFFFF">
			   <select required class="form-control tbox_small" name="supplier_id" id="supplier_id"  onchange="puradd_fix(this,"purfrom")">
					<option  disabled value="">Select</option>
					<?php if(!empty($supplier_list)){foreach($supplier_list as $va1=>$key1){ ?>
					<option <?php if($key1["id"]==1){ ?>selected<?php } ?> pur_addr1="<?php echo $key1["supplier_name"]; ?>"
					pur_addr2="<?php echo $key1["addr1"]; ?>"
					pur_addr3="<?php  if(isset($key1["addr2"])) echo $key1["addr2"]; ?>"
					pur_addr4="<?php echo $key1["city"]; ?>"
					vendor="<?php echo $key1["vendor_id"]; ?>"
					state="<?php echo $key1["state"]; ?>"
					value="<?php echo $key1["id"]; ?>"><?php echo $key1["supplier_name"] ?></option>
					<?php }} ?>
				</select>
				
			   </td>
               <td align="LEFT" bgcolor="#FFFFFF" colspan="2">
			   <br>
			   </td>
               <td class="border_cls1"   colspan="3" align="LEFT" bgcolor="#FFFFFF">
						<select required class="form-control tbox_small" name="shipto_id"id="shipto_id"  onchange="puradd_fix(this,"shipto")">
							<option  disabled value="">Select	</option>
							<?php $count=1;if(!empty($shipto_list)){foreach($shipto_list as $va1=>$key1){
            
			     ?>
							<option <?php if($count==1){ ?> selected <?php } ?>  ship_addr1="<?php echo $key1["ship_name"]; ?>"
						ship_addr2="<?php echo $key1["addr1"]; ?>"
						ship_addr3="<?php if(isset($key1["addr2"])) echo $key1["addr2"]; ?>"
						ship_addr4="<?php echo $key1["ship_city"]; ?>"
	value="<?php echo $key1["id"]; ?>" ><?php echo $key1["ship_name"] ?></option>
							<?php $count++;}} ?>
						</select>  		
			   </td>
               
            </tr>
            <tr>
               <td class="company" style="padding-left: 5px;" height="19" colspan="2" align="LEFT" bgcolor="#FFFFFF">
			   <label id="pur_add1"><?php echo strtoupper($supplier_list[0]["supplier_name"]); ?></label>
			   </td>
               <td align="LEFT" bgcolor="#FFFFFF" colspan="2" >
			   <br>
			   </td>
               <td class="border_cls1" style="padding-left: 5px;" align="LEFT" bgcolor="#FFFFFF" colspan="3" >
			   <label id="supp_addr1"><?php echo strtoupper($shipto_list[0]["ship_name"]); ?></label>
			   </td>
			    
            </tr>
			<tr>
               <td class="company" style="padding-left: 5px;" height="19" colspan="2" align="LEFT" bgcolor="#FFFFFF">
			  <label id="pur_add2"><?php if(isset($supplier_list[0]["addr1"])) echo strtoupper($supplier_list[0]["addr1"]); ?></label>
			   </td>
               <td align="LEFT" bgcolor="#FFFFFF" colspan="2" >
			  <br>
			   </td>
               <td class="border_cls1"  style="padding-left: 5px;" align="LEFT" bgcolor="#FFFFFF" colspan="2" >
			   <label id="supp_addr2"><?php if(isset($shipto_list[0]["addr1"])) echo strtoupper($shipto_list[0]["addr1"]); ?></label>
			   </td>
			    
            </tr>
            <tr>
               <td class="company" style="padding-left: 5px;" height="19" colspan="2" align="LEFT" bgcolor="#FFFFFF">
			   <label id="pur_add3"><?php if(isset($supplier_list[0]["addr2"]))  echo strtoupper($supplier_list[0]["addr2"]); ?></label></td>
               <td align="LEFT" bgcolor="#FFFFFF" colspan="2" ><br></td>
			    <td align="LEFT"  bgcolor="#FFFFFF" colspan="3">
				<label  id="supp_addr3"><?php if(isset($shipto_list[0]["addr2"])) echo strtoupper($shipto_list[0]["addr2"]); ?></label></td>
			   
            </tr>
            <tr>
               <td class="company" style="padding-left: 5px;" height="19" colspan="2" align="LEFT" bgcolor="#FFFFFF">
			   <label id="pur_add4"><?php echo strtoupper($supplier_list[0]["city"]); ?></label></td>
             <td align="LEFT" bgcolor="#FFFFFF" colspan="2" ><br></td>
			    <td align="LEFT" bgcolor="#FFFFFF" colspan="3">
				<label id="supp_addr4"><?php echo strtoupper($shipto_list[0]["ship_city"]); ?></label></td>
            </tr>
            <tr>
               <td class="company" style="padding-left: 5px;" height="19" align="LEFT" bgcolor="#FFFFFF"><br></td>
               <td align="LEFT" bgcolor="#FFFFFF"><br></td>
               <td align="LEFT" bgcolor="#FFFFFF"><br></td>
               <td align="LEFT" bgcolor="#FFFFFF"><br></td>
               <td align="LEFT" bgcolor="#FFFFFF"><br></td>
               <td class="border_cls1" align="LEFT" bgcolor="#FFFFFF"><br></td>
            </tr>
            <tr>
               <td class="company" height="19" align="CENTER" valign="MIDDLE" ><b><br></b></td>
               <td align="CENTER" valign="MIDDLE" ><b>Dispatch Through</b></td>
               <td colspan="2" align="CENTER" valign="MIDDLE" ><b>Payment Terms</b></td>
               <td class="border_cls1" colspan="2" align="CENTER" valign="MIDDLE" ><b>Required By Date</b></td>
            </tr>
            <tr>
               <td class="company" height="41" align="LEFT" bgcolor="#FFFFFF"><br></td>
               <td class="border_cls1" align="CENTER" valign="MIDDLE" bgcolor="#FFFFFF">By Road -Transport</td>
               <td class="border_cls" colspan="2" align="CENTER" valign="MIDDLE" bgcolor="#FFFFFF"><input type="text" class="form-control tbox_small" name="payment_terms" id="payment_terms" value="30 Days"></td>
               
			   <td class="border_cls" colspan="2" align="CENTER" valign="MIDDLE" bgcolor="#FFFFFF"><input type="date" class="form-control tbox_small" name="required_date" id="required_date"  value="<?php echo date("Y-m-d"); ?>"></td>
            </tr>
           
			<tr><td colspan="6">
				<table id="order_item_body_1" class="table listhead table-bordered table-hover">
                      <tr>
               <td class="company" height="19" align="center" ><b>SL NO</b></td>
               <td align="CENTER"  ><b> Particular </b></td>
               <td align="CENTER" ><b>Dimensions</b></td>
               <td class="border_cls1" align="CENTER" ><b>Qty</b></td>
               <td class="border_cls1" align="CENTER" ><b>Rate</b></td>
               <td class="border_cls1" align="CENTER" ><b>Amount</b></td>
            </tr>
				<?php for($i=0;$i<7;$i++){ ?>
					<tr id="order_item_row_1_1" name="order_item_row_1_1">
					<td align="center"><label><?php echo $i+1; ?></label></td>					
					<td>  
						<select onchange="choose_product(this,<?php echo $i; ?>);" required class="form-control removes" name="name[<?php echo $i; ?>]" id="name_<?php echo $i; ?>">
						<option disabled selected value="">Select</option>		
						<?php if(!empty($product_list)){ foreach($product_list as $va=>$key){ ?>												
							<option value="<?php echo $key["id"]; ?>" dimen="<?php echo $key["dimension"]; ?>" price="<?php echo $key["product_price"]; ?>" ><?php echo $key["name"]; ?></option>
						<?php } } ?>
						</select>
					</td>
									
					<td>
                        <input type="text" class="form-control" name="dimension[<?php echo $i; ?>]" id="dimension_<?php echo $i; ?>" onblur="calc_amount(<?php echo $i; ?>)"  value="" required />
					</td>
					<td>
					<input type="number" min=1 class="form-control" name="qty[<?php echo $i; ?>]" id="qty_<?php echo $i; ?>" onblur="calc_amount(<?php echo $i; ?>)" value="" required />
					</td>
					<td>
                        <input type="text" class="form-control" name="price[<?php echo $i; ?>]" id="price_<?php echo $i; ?>" onblur="calc_amount(<?php echo $i; ?>)"  value="" required />
					</td>
					 
                    <td>
                        <input type="text" class="form-control" name="amount[]" id="amount_<?php echo $i; ?>" value="" required disabled />
                    </td>					 
					</tr>							
					<input type="hidden" value="1" id="order_item_row_index_1" name="order_item_row_index_1">	
			<?php } ?>
		</table>
		</td></tr>
          
            <tr>
               <!--<td class="company" height="16" colspan="2" align="LEFT" bgcolor="#FFFFFF"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Approved By:</b></td>-->
			   <td  colspan="4" ></td>
               <td class="border_cls3" bgcolor="#FFFFFF" align="center">Subtotal</td>
               <td class="border_cls3" align="CENTER" valign="MIDDLE" bgcolor="#FFFFFF" style="padding-top: 4px;padding-bottom: 4px;">
			   <div id="sub_total"><?php echo sprintf("%.2f",$total_amount); ?></div>
			   <input type="hidden" name="sub_totall" value="<?php echo $total_amount; ?>">
			   </td>
            </tr>
           <tr>
               <!-- <td class="company" colspan="2" height="93" align="CENTER" bgcolor="#FFFFFF"><b><br> X ________________________________</b></td> -->
			   <td  colspan="2" ></td>
               <td class="border_cls4" bgcolor="#FFFFFF" align="center" colspan="2" > 
			   <input type="text" placeholder="%" onblur="percentCalculation(this.value,"sgst_total")" class="form-control tbox_small"  name="sgst_percent" id="sgst_percent" value=""></td>
			   <td class="border_cls4" bgcolor="#FFFFFF" align="center" >SGST</td>
               <td class="border_cls4" align="CENTER" valign="MIDDLE" bgcolor="#FFFFFF" style="padding-top: 4px;padding-bottom: 4px;"> 
			   <input type="text" class="form-control tbox_small" name="sgst_total" id="sgst_total"> </td>
            </tr>
            <tr>
               <td class="border_cls5" colspan="2" rowspan="4" align="LEFT" bgcolor="#FFFFFF"><b><br></b></td>
			   <td class="border_cls4" bgcolor="#FFFFFF" align="center" colspan="2" > 
			   <input type="text" placeholder="%"  onblur="percentCalculation(this.value,"cgst_total")"  class="form-control tbox_small"  name="cgst_percent" id="cgst_percent" value=""></td>
               <td class="border_cls4"bgcolor="#FFFFFF" align="center"  >CGST</td>
               <td class="border_cls4" align="CENTER" valign="MIDDLE" bgcolor="#FFFFFF" style="padding-top: 4px;padding-bottom: 4px;"> 
			   <input type="text" class="form-control tbox_small" name="cgst_total" id="cgst_total"></td>
            </tr>
            <tr>
			   <td class="border_cls4" bgcolor="#FFFFFF" align="center" colspan="2" > 
			   <input type="text" placeholder="%" disabled onblur="percentCalculation(this.value,"igst_total")"  class="form-control tbox_small"  name="igst_percent" id="igst_percent" value=""></td>
               <td class="border_cls4" align="center" bgcolor="#FFFFFF">IGST</td>
			  
               <td class="border_cls4" align="CENTER" valign="MIDDLE" bgcolor="#FFFFFF" style="padding-top: 4px;padding-bottom: 4px;">
			   <input type="text" class="form-control tbox_small" disabled name="igst_total" id="igst_total"></td>
            </tr>
            <tr>
               <td colspan="2" rowspan="2" height="54" align="CENTER" bgcolor="#FFFFFF"><b><br></b>
				 
				</td>
               <td class="border_cls3" bgcolor="#FFFFFF" align="center"><b>Grand Total</b></td>
               <td class="border_cls3" align="CENTER" valign="MIDDLE"  style="padding-top: 4px;padding-bottom: 4px;"><b>
			  <div id="order_total"><?php echo $total_amount;?></div></b></td>
            </tr>
            <tr>
               <td class="border_cls3" align="LEFT" bgcolor="#FFFFFF"><br></td>
               <td class="border_cls3" align="LEFT" bgcolor="#FFFFFF"><br></td>
            </tr>
			<input type="hidden" name="orders" id="orders" value="<?php echo $total_amount;?>">
         </tbody>
      </table>
	  </div></div></div>
	  </form>
	  </div>
<div aria-hidden="true"  aria-labelledby="myModal" role="dialog" tabindex="-1" id="myModal" class="purchase modal fade">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
</div>
<div class="modal-body">
<iframe id="iframe_show_vehicle_data" frameborder="0" marginwidth="0" marginheight="0"  scrolling="no" style="width:100%;border:0px;">
</iframe>
</div>
</div>
</div>
</div>';
$mpdf=new mPDF($html);
$mpdf->WriteHTML($html);
$mpdf->Output("purchase.pdf" , "F");
header("location:purchase.pdf");
exit;
?>