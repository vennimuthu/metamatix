<?php
include("config/config.php");
if(!isset($_SESSION["user_id"])){ // checking whether the usertype is logged in
	header('Location: logout.php');        						//REdirection to logout.php
	exit;
}
include("core/class/db_query.php");                             // Class where query generetion is written
include("core/class/db_helper.php");                            // Class where table and feilds 
include("core/function/common.php");
                                                              // are mentioned to generate query
ob_start();														// to clear the internal output
$db_query=new db_query();  
	global $db_helper_obj;
	$db_helper_obj=new db_helper();
	$order_list=$db_helper_obj->order_list();
	$finished_list=$db_helper_obj->finished_list();
	$total_orders=0;
	$completed_orders=0;
	$inprogress_orders=0;
	foreach($finished_list as $va=>$key){
	if($key["status"]==3){	
		$products_approve=convert_array($key["products_approve"]);	
		foreach($products_approve as $va1=>$key1){
		$productsa=$db_helper_obj->product_edit($va1);
		foreach($productsa as $va2=>$key2){
			$comp_qty[$key2["sub_product_id"]]+=$key2["qty"]*$key1["approve_qty"];
		}
		 	
		}
	}
	}
	$comp_list=$comp_qty;
	foreach($order_list as $va=>$key){
		$products=convert_array($key["pdfinfo"]);	
		foreach($products["name"] as $va1=>$key1){		
			$total_product[$key["id"]][$key1]=$products["qty"][$va1];
			
		}
		$total_orders++;
	} 
	foreach($total_product as $va=>$key){
	foreach($key as $va1=>$key1){
		
	if($key1>$comp_qty[$va1]){
		$completd_orders[$va][$va1]=$key1-$comp_qty[$va1];	
	}else if($key1<=$comp_qty[$va1]){
		$completd_orders[$va][$va1]=0;
		$comp_qty[$va1]=$comp_qty[$va1]-$key1;	
	}
	}		
	}

	foreach($completd_orders as $va1=>$key1){	
	
	if(array_sum($key1)==0)
		$completed_orders++;
	else
		$inprogress_orders++; 
	}
	 
	$completed_percentage=($completed_orders/$total_orders)*100;
	$inprogress_percentage=($inprogress_orders/$total_orders)*100;
	//echo"<pre>";print_r($total_product);echo"</pre>";	
//	echo"<pre>";print_r($completd_orders);echo"</pre>";	
	
?>
<html>
<head>
<link rel="icon" type="images/png" href="">
  <link rel='stylesheet' href='css/font-awesome.css'>
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
   <link rel="stylesheet" href="css/responsive-menu.css">
  <link rel="stylesheet" href="css/style.css">
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap.js"></script>
  <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
  <link href="css/select2.min.css" rel="stylesheet" />
  <script src="js/select2.min.js"></script>
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" />
  <script src="js/modernizr-custom.js"></script>
  <script src="js/responsive-menu.js"></script>
</head>
<div class="listwrapper" id="height_id">

<div class="table-responsive"> 
<table class="table listhead table-bordered table-hover" align="center" width="50%" border="1" cellspacing="0" cellpadding="0">

<td align="center"><label>S No</label></td>
<td align="center"><label>Product Name</label></td>
<td align="center"><label>Order Qty</label></td>
<td align="center"><label>Inprogress Qty</label></td>
<td align="center"><label>Finished Qty</label></td>
</tr>
<?php
	//$completd_orders[$va][$va1]
 foreach($total_product as $va=>$key){ 
 if($va==$_GET["process_id"]){
 $count=1;
 foreach($key as $va1=>$key1){	
 ?>
<tr >
<td align="center"><?php echo $count; ?></td>
<td align="center" style="padding: 4px;">
<label><?php
$prod2=$db_helper_obj->total_product_edit($va1);
echo $prod2[0]["name"]; ?></label>
</td>
<td  align="center" style="padding: 4px;">
<label><?php echo $key1; ?></label>
</td>

<td  align="center" style="padding: 4px;">
<label><?php echo $completd_orders[$va][$va1]; ?></label>
</td>
<td  align="center"style="padding: 4px;">
<label><?php
echo $key1-$completd_orders[$va][$va1]; ?></label>
 </td>
</tr>
 <?php $count++; } } }?>
</table>
</div>
</div>
<script type="text/javascript">
	setTimeout(function(){ parent.$("#iframe_show_vehicle_data").attr("height",$("#height_id").height()+30+"px"); }, 100);
</script>
