<?php function main() {
	$db_query=new db_query();  
	global $db_helper_obj;
	$db_helper_obj=new db_helper();
	$supplier_list=$db_helper_obj->supplier_list();
	$material_list=$db_helper_obj->material_category();	
	$product_list=$db_helper_obj->total_product_list();
	 $openstock_list=$db_helper_obj->openstock_list();
	foreach($openstock_list as $va=>$key)
	{
		$openstock[]=$key["subproduct_id"];
		
	}
	 if(!empty($_POST)){
		$inserted=$db_helper_obj->openstock_insert();
		header('Location:  openstock_list.php'); //REdirection to master_list.php
	}else{
		unset($_SESSION["cost"]);
	}
?>
<script type="text/javascript">
function intializeSelect(){
    // this "initializes the boxes"
	$('.removes').each(function(box) {
      var value = $('.removes')[box].value;
      if (value) {
        $('.removes').not(this).find('option[value="' + value + '"]').hide();
      }
    });
};

// this is called every time a select box changes
$('.removes').on('change', function(event) {
$('.removes').find('option').show();
intializeSelect();
});
function add_order_row(order_row_id)
{	
	var order_item_index=$("#order_item_row_index_"+order_row_id).val();
	order_item_index++;
	var html_content;
	var options1=document.getElementById('product_select').innerHTML;
	
	var slect1='<select onchange="intializeSelect()" id="name_'+order_item_index+'" name="name['+order_item_index+']" class="form-control removes" >'+options1+'</select>';
	html_content='<tr id="order_item_row_'+order_row_id+"_"+order_item_index+'" name="order_item_row_'+order_row_id+"_"+order_item_index+'">'
+'<td>'+slect1+'</td>'
+'<td><input type="number" class="form-control" name="qty['+order_item_index+']" min="0" id="qty_'+order_item_index+'" value="" /></td>'
+'<td><input type="number" class="form-control" name="buffed_qty['+order_item_index+']" min="0" id="buffed_qty_'+order_item_index+'" value="" /></td>'
+'<td><input type="button" class="btn btn-danger" value="X" onclick="delete_order_item_row(this);"></td></tr>';
	
	div = document.getElementById('order_item_body_'+order_row_id);	
	div.insertAdjacentHTML( 'beforeend', html_content);
	$("#order_item_row_index_"+order_row_id).val(order_item_index);
	intializeSelect();
}

function delete_order_item_row(element)  
{
    var row = element.parentNode.parentNode;
    row.parentNode.removeChild(row);
}
function showmaterial(obj){
	if(obj.value==3)
		document.getElementById("material_id").style.display="none";
	else{
		document.getElementById("material_id").style.display="";
	}

}
function cost_add(obj){
var word=obj.id;
var number=word.replace('process_','');	
if(obj.checked==true)
	var check=1;
else if(obj.checked==false)	
	var check=0;
if(obj.checked==true&&document.getElementById('name_'+number).value!=''){
$.ajax({url: "master_costadd.php?sub_id="+number+"&check="+check+"",contentType:false, processData: false, success: function(result){
	var ser = document.getElementById('iframe_show_vehicle_data');
	ser.contentDocument.write(result);
	}
	});	
}else{ 
	$.ajax({url: "master_costadd.php?sub_id="+number+"&check="+check+"",contentType:false, processData: false, success: function(result){
	var ser = document.getElementById('iframe_show_vehicle_data');
	ser.contentDocument.write(result);
	}
	});	
}
}
function cancel_data(){
	var iframe = document.getElementById("iframe_show_vehicle_data");
	var html = "";
	iframe.contentWindow.document.open();
	iframe.contentWindow.document.write(html);
	iframe.contentWindow.document.close();
}

</script>


<style>
	.cost_form { }
	.cost_form input.form-control { margin-bottom:10px; }
	.tbox_vsmall {
     width:21%;
	}

</style>
<form class="form-horizontal" id="recruitment" name="recruitment" method="post">

	
  <div class="form-group">
  <h4  class="inline-heading">Open Stock add</h4>
	
  <section class="main_content">
	<div class="container-fluid">
    	
    	<div class="row">
		
        	<div class="col-sm-12">
            	
				<form name="master_add" id="master_add" method="post" target="iframe">
            	<div class="listwrapper">
			<div class="table-responsive"> 
                            <table id="order_item_table_1" class="table listhead table-bordered table-hover">
                                <thead>
                     <tr>
					<th>Sub Component Name </th>									
					<th>Non Buffed Qty</th>
					<th>Buffed Qty</th>						
					<th ></th>
                     </tr>
                                </thead>
								
                                <tbody  id="order_item_body_1"  name="order_item_row_1_1">
								
								<tr id="order_item_row_1_1" name="order_item_row_1_1">
								<div id="product_select" style="display:none;">				
					<option value="">Select	</option>
					<?php foreach($product_list as $va=>$key){ ?>
					<option value="<?php echo $key["id"]; ?>"><?php echo $key["name"]; ?></option>
					<?php }?>
					</div>
					
					<td>
					<select onchange="intializeSelect()" class="form-control removes" name="name[1]" required>
							<option value="">Select	</option>
							<?php foreach($product_list as $va1=>$key1){  if(!in_array($key["id"],$openstock)) { ?>
							<option <?php if($key["name"]==$key1["id"]){ ?> selected <?php } ?> value="<?php echo $key1["id"]; ?>"><?php echo $key1["name"]; ?></option>
							<?php } }?>
						</select>
					</td>
					
					<td>			 
					<input type="number" min=0 class="form-control" name="qty[1]" id="qty_1" value=""  />
					</td>
					<td>			 
					<input type="number" min=0 class="form-control" name="buffed_qty[1]" id="buffed_qty_1" value=""  />
					</td>								
					</tr>
				   </tbody>
				</table>
							
        <input type="hidden" value="1" id="order_item_row_index_1" name="order_item_row_index_1">
    </div>
	
	 </div>  <!------End--- Table Responsive -->
	 <br/> <br/>
	 <div class="addnew_wrapp clearfix">
		<div class="addnew" align="right">
		<a href="#" onclick="add_order_row(1)" class="" style="padding-top: 8px;padding-bottom: 8px;padding-left: 3px;padding-right: 3px;">ADD NEW</a>
	   </div>
	  </div>
	 </div>
	 
	 <div class="col-md-12">
			 <button type="button" onclick="window.location.href='openstock_list.php'" class="btn btn-primary">Cancel</button>
			 <button type="submit" class="btn btn-primary">Submit</button>
		</div>
				</form>
            </div>
        </div>

    </div>
	<div aria-hidden="true"  aria-labelledby="myModal" role="dialog" tabindex="-1" id="myModal" class="purchase modal fade">
                  <div class="modal-dialog">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" onclick="cancel_data()" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h3 class="modal-title text-center">Master View</h3>
                              
                          </div>
                          <div class="modal-body">
                          <iframe id="iframe_show_vehicle_data" 
frameborder="0" marginwidth="0" marginheight
="0"  scrolling="no"  style=
"width:100%;border:0px;">
				</iframe>
						
                          <div class="modal-footer">
                          </div>
                          </div>
                      </div>
                </div>
    </div>
  
	</section>



</form>
<?php } include("template.php"); ?>  