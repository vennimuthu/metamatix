<?php
include("config/config.php");
if(!isset($_SESSION["user_id"])){ //checking whether the usertype is logged in
	header('Location: logout.php');//REdirection to logout.php
	exit;
}
include("core/class/db_query.php");// Class where query generetion is written
include("core/class/db_helper.php");// Class where table and feilds 
include("core/function/common.php");// are mentioned to generate query
ob_start();                         // to clear the internal output
global $global_website_url;
global $db_helper_obj;
$db_query=new db_query();  
global $db_helper_obj;
$db_helper_obj=new db_helper();
$product_list=$db_helper_obj->material_products($_GET["material_type"]);
$unit_list=$db_helper_obj->unit_list();
?>
<div id="product_list" style="display:none;" >
<option disabled selected value="">Select</option>		
	<?php if(!empty($product_list)){ foreach($product_list as $va=>$key){ ?>												
		<option value="<?php echo $key["id"]; ?>" hsn="<?php echo $key["hsn"]; ?>" dimen="<?php echo $key["dimension"]; ?>" price="<?php echo $key["product_price"]; ?>" ><?php echo $key["name"]; ?></option>
	<?php } } ?>	
</div>


<?php 

if($_GET["from"]=='edit')
	$total_count=6;
else
	$total_count=2;
 for($count=1;$count<$total_count;$count++){

?>
<?php if($_GET["from"]=='edit'){ ?>
<tr>
<?php } ?>
<td align="center"><label><?php echo $count; ?></label></td>	
<td>  
	<select required onchange="choose_product(this,<?php echo $count; ?>);" class="form-control removes" name="name[<?php echo $count; ?>]" id="name_<?php echo $count; ?>">
	<option disabled selected value="">Select</option>		
	<?php if(!empty($product_list)){ foreach($product_list as $va=>$key){ ?> 
	<option value="<?php echo $key["id"]; ?>" hsn="<?php echo $key["hsn"]; ?>" dimen="<?php echo $key["dimension"]; ?>" price="<?php echo $key["product_price"]; ?>" ><?php echo $key["name"]; ?>
	</option>
	<?php } } ?>
	</select>
</td>
<td>
	<input type="text" class="form-control" name="hsn[<?php echo $count; ?>]" required id="hsn_<?php echo $count; ?>" onblur="calc_amount(<?php echo $count; ?>)"  value=""  />
</td>
<td>
	<input type="text" class="form-control" name="dimension[<?php echo $count; ?>]" id="dimension_<?php echo $count; ?>" required onblur="calc_amount(<?php echo $count; ?>)"  value=""  />
</td>
					
<td>
<input type="number" min=1 class="form-control" name="qty[<?php echo $count; ?>]" required id="qty_<?php echo $count; ?>" onblur="calc_amount(<?php echo $count; ?>)" value=""  />
</td>
<td>
<div style="display:none;" id="units_div">
<option disabled selected value="">Select</option>		
	<?php if(!empty($unit_list)){ foreach($unit_list as $va=>$key){ ?>
	<option value="<?php echo $key["id"]; ?>"  ><?php echo $key["unit_name"]; ?></option>
	<?php } } ?>
</div>
	<select style="width: 89px;" required class="form-control" name="units[<?php echo $count; ?>]" id="units_<?php echo $count; ?>">
	<option disabled selected value="">Select</option>		
	<?php if(!empty($unit_list)){ foreach($unit_list as $va=>$key){ ?>
		<option value="<?php echo $key["id"]; ?>"  ><?php echo $key["unit_name"]; ?></option>
	<?php } } ?>
	</select>
</td>
<td>
	<input type="text" class="form-control" name="price[<?php echo $count; ?>]" id="price_<?php echo $count; ?>" onblur="calc_amount(<?php echo $count; ?>)"  required  value=""  />
</td>
 
<td>
	<input type="text" class="form-control" name="amount[]" id="amount_<?php echo $count; ?>" value="" required disabled />
</td>	
 
<td style="text-align:left;">
	<input type="button" <?php if($count==1){ ?> disabled <?php } ?> class="btn btn-danger remove_class" value="X" onclick="delete_order_item_row(this);" />
</td>

<input type="hidden" name="suborder_id[<?php echo $count; ?>]" id="suborder_id" value="<?php echo $key["sub_order_id"]; ?>" />
<input type="hidden" value="1" id="order_item_row_index_1" name="order_item_row_index_1">	<?php if($_GET["from"]=='edit'){ ?>
</tr>
<?php } ?>
 <?php } ?>