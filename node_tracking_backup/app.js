var express = require('express');
var bodyparser = require('body-parser');
var expressValidator = require('express-validator');
global.connection = require('./connection');
var routes = require('./routes');
global.db_query = require('./helper/db_query');
global.fs = require('fs');
global.server_environment="staging";
global.base_url ="http://192.168.100.17";
 connection.init();
var app = express();
app.use(bodyparser.urlencoded({extended: false}));
app.use(expressValidator());
app.use(bodyparser.json()); 
routes.configure(app);

//Local
var server = app.listen(8050, function() {
	console.log('Server listening on port' + server.address().port);
});