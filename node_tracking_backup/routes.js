module.exports = {
	configure: function(app) {	

		app.post('/location',function(req,res){
			var info=require('./models/tracking');
			info.location(req, res);
	    });

		app.post('/location_insert',function(req,res){		
		var info=require('./models/tracking');		
		info.location_insert(req, res);	
		});
		app.post('/login',function(req,res){		
		var info=require('./models/tracking');		
		info.login(req, res);	
		});
		app.post('/checkin_status',function(req,res){		
		var info=require('./models/tracking');		
		info.checkin_status(req, res);	
		});
	}
};