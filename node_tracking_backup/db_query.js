var sql = require('mssql');
function db_query(){
	this.select_query= function (queryValue,callback,dd){
		//console.log(queryValue);
		global.connection.acquire(function(connection_error, con){
			var query_exec =con.query(queryValue,function(err, data)
			{
				if (err) 
				{
					self.genrate_log(err,queryValue);
					sql.close();
					callback(1, null);
				}
				else
				{
					sql.close();
					callback(0, data.recordset);
				}
			});
			if(dd==1)
			console.log(query_exec.sql);
		});
	}

	this.insert_query=function (insert_data,callback,dd){
		//console.log(insert_data);
		global.connection.acquire(function(connection_error, con) 
		{
			var query_exec =con.query(insert_data, function(err, data) 
			{
				if (err)
				{
					self.genrate_log(err,' '+insert_data);
					sql.close();
					callback(1, null);
				}
				else
				{
					var str = "SELECT @@IDENTITY AS 'identity'";
					con1 = new sql.Request();
					con1.query(str, function(suberr, subdata)
					{
						sql.close();
						callback(0, subdata.recordset[0]);
					});				
				}
			});
			if(dd==1)
			console.log(query_exec.sql);
		});
	}

	this.update_query= function (update_data,callback,dd){
		//console.log(update_data);
		global.connection.acquire(function(connection_error, con) 
		{
			var query_exec =con.query(update_data, function(err, data) 
			{
				if (err)
				{
					self.genrate_log(err,' '+update_data);
					sql.close();
					callback(1, null);
				}
				else
				{
					sql.close();
					callback(0, data.recordset);
				}
			});
			if(dd==1)
			console.log(query_exec.sql);
		});
	}

	 this.genrate_log=function (err,qry){
		var d = new Date();
		var error_message='\r\n\r\n LOG: '+Date();
		error_message +='\r\n ERROR TYPE: SELECT QUERY ';
		error_message +='\r\n ERROR: '+err;
		error_message +='\r\n query: '+qry;
		var month=d.getMonth()+1;
		global.fs.appendFile("error_log/query_error_log_"+d.getDate()+"_"+month+"_"+d.getFullYear()+".txt", error_message, function(err) 
		{
			if(err) 
			{
				console.log(err);
			}
		});
	} 
}
var self =module.exports = new db_query();