function mcpl() {
	this.login = function(req,res){
		console.log(req);
	var data_to_return={'status':0,'msg':'Invalid Data','data':{}};
	
	// setup validation
	req.assert('UserName', 'Invalid value').notEmpty();
	req.assert('Password', 'Invalid value').notEmpty();
	
	// validating errors
	var errors = req.validationErrors();
	if (errors) {
		data_to_return['data']['error_msg']=errors[0]['msg'];
		// returning the error
		res.send(data_to_return);
		return;
	}
	// data sanitisation  
	var UserName=req.sanitize('UserName').escape().trim();
	var Password=req.sanitize('Password').escape().trim();

	var query="select * from users where username='"+UserName+"' and password='"+Password+"' ";
	var user_select_qry_data=function(error, result){
	if(error==1){
		res.send({status: 500, msg: 'Query Failed'});
		return;
	}else{
		if (result==''){
			res.send({status: 400, msg: 'Invalid UserName Or Password'});
			return;
		}else{			
			data_to_return['status']=200;
			data_to_return['msg']="success";
			data_to_return['data']['user_id']=result[0].id;
			data_to_return['data']['username']=result[0].username;					
			res.send(data_to_return);
		}
	}
	}
	global.db_query.select_query(query,user_select_qry_data,1);
}

this.checkin_status = function(req,res){
	var data_to_return={'status':0,'msg':'Invalid Data','data':{}};
	
	// setup validation
	req.assert('user_id', 'Invalid User Id').notEmpty();
	 
	
	// validating errors
	var errors = req.validationErrors();
	if (errors) {
		data_to_return['data']['error_msg']=errors[0]['msg'];
		// returning the error
		res.send(data_to_return);
		return;
	}
	// data sanitisation  
	var user_id=req.sanitize('user_id').escape().trim();	 

	var query="select TOP 1 status from checkin_checkout where agent_name='"+user_id+"' order by id desc ";
	var user_select_qry_data=function(error, result){
	if(error==1){
		res.send({status: 500, msg: 'Query Failed'});
		return;
	}else{
		if (result==''){
			res.send({status: 400, msg: 'Invalid Status'});
			return;
		}else{			
			data_to_return['status']=200;
			data_to_return['msg']="success";
			data_to_return['data']['user_id']=result[0].id;	
			data_to_return['data']['status']=result[0].status;			
			res.send(data_to_return);
		}
	}
	}
	global.db_query.select_query(query,user_select_qry_data,1);
}



	this.location=function(req,res){
    console.log(req);

		var data_to_return={'status':0,'msg':'Invalid Data','data':{}};

		var user_id=req.sanitize('user_id').escape().trim();
       
		var Promise=require("promise");
		var promises = [];
		var async=require('async');
		var moment=require('moment');
		var date=moment(new Date(Date.now())).format('YYYY-MM-DD HH:mm:ss');
		for(var key in req.body){
			promises.push(checkValue(req.body[key],key));
		}		 

		Promise.all([promises])    
		.then(function(data){
			data_to_return['status']=200;
			data_to_return['msg']="success";
			data_to_return['data']['user_id']=user_id;
			res.send(data_to_return); 
		}).catch(function(err){ 
			console.log(err);  
		});


		function checkValue(value,key_val){
			console.log(value.batteryLevel);
			return new Promise(function(resolve, reject){
				var provider="";
				var speed="";
				var altitude="";
				var bearing="";
				var latitude="";
				var longitude="";
				var util=require('util');
				if(value.provider==undefined ||value.provider==""){
					value.provider="-";
				}
				if(value.speed==undefined ||value.speed==""){
					value.speed="-";
				}
				if(value.altitude==undefined ||value.altitude==""){
					value.altitude="-";
				}
				if(value.bearing==undefined ||value.bearing==""){
					value.bearing="-";
				}

				async.waterfall([
					function(callback){
						var location_query="select top 1  latitude,longitude,data_insertion_time,current_time_now from location_history  WHERE agent_name='"+user_id+"' and status='"+0+"'order by data_insertion_time desc";
					
						var location_data1=function(error1,result1){
							 
							
							if (error1==1){
								res.send({status: 500, msg: 'Query failed 1'});
								return;
							}
							else{
								if(result1==""){			
									callback(null,value.latitude,value.longitude,0,0,0);
								}
								else{
									var prev_lat=result1[0].latitude;
									var prev_lon=result1[0].longitude;
									var data_insertion_time=new Date(result1[0].data_insertion_time);
									var current_time_now=new Date(result1[0].current_time_now);
									var data_now=current_time_now.getTime();;
									var data=data_insertion_time.getTime();
									var diff=result1[0].current_time_now-result1[0].data_insertion_time;

									diff=Math.floor(diff/1000);									
									dist_travel=0.010055*diff;

									var dist=calc_distance(prev_lat,prev_lon,value.latitude,value.longitude);

									if(dist_travel<=dist){ 
 
										callback(null,value.latitude,value.longitude,dist,diff,1);
									}
									else{
										if(dist>0.15){
											callback(null,value.latitude,value.longitude,dist,diff,0);
										}
										else{
 
											callback(null,prev_lat,prev_lon,dist,diff,0);
										}
									}
								}
							}
						}
							global.db_query.select_query(location_query,location_data1,1);
					},
					function(arg2,arg3,arg4,arg5,arg6,callback){
						if(arg6==1){
							var location_insert_qry_data3=function(err3,result3){
								if (err3==1){
									res.send({status: 500, msg: 'Query failed 3'});
									return;
								}
								else{
									if(result3==""){
										res.send({status: 400, msg: 'No Result found 3'});
									}
									else{
										var count=0;
										for(let val in result3){
											var dist=calc_distance(result3[val].latitude,result3[val].longitude,value.latitude,value.longitude);
											//var data_insertion_time=new Date(result3[val].data_insertion_time);
											var diff=value.time-result3[val].data_insertion_time;
 											diff=Math.floor(diff/1000);
											dist_travel=0.018055*diff;

											if(dist_travel>dist){
												
												count++;
												if(count>=3){
													
													callback(null,arg2,arg3,arg4,arg5,0);
													return;
												}
												else if(val==result3.length-1){
													callback(null,arg2,arg3,arg4,arg5,1);
													return;
												}
											}
										}
									}
								}
							}
							var location_query1="select top 5 latitude,longitude,data_insertion_time from location_history  WHERE agent_name='"+user_id+"' and status='"+0+"'order by data_insertion_time desc";
							global.db_query.select_query(location_query1,location_insert_qry_data3,1);
						}
						else{
							callback(null,arg2,arg3,arg4,arg5,arg6);
						}
					},
					function(arg2,arg3,arg4,arg5,arg6,callback){
						
						latitude=arg2;
						longitude=arg3;
						var location_insert_qry_data3=function(err3,result3){
							if (err3==1){
								res.send({status: 500, msg: 'Query failed 4'});
								return;
							}
							else{
								if(result3==""){
									res.send({status: 400, msg: 'No Result found 4'});
								}
								else{
									
									//if(arg6==1){
									//	callback('ERROR',result3.insertId);
									//}
									//else{
										callback(null,arg2,arg3);
									//}
								}
							}
						}
						var date_insert=moment(new Date(Date.now())).format('YYYY-MM-DD HH:mm:ss');
						var current_time_now=new Date(Date.now());
						var data_now=current_time_now.getTime();
						var location_insert_query = "INSERT INTO location_history (agent_name,latitude,actual_latitude,longitude,actual_longitude,battery_percentage,data_insertion_time,current_time_now,accuracy,distance,time_interval,provider,altitude,bearing,status) VALUES ";
						location_insert_query+=util.format("('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',%d)",user_id,arg2,value.latitude,arg3,value.longitude,value.batteryLevel,value.time,data_now,value.accuracy,arg4,arg5,value.provider,value.altitude,value.bearing,arg6);
						
						global.db_query.insert_query(location_insert_query,location_insert_qry_data3,1);
					},
					 function(args1,args2,callback){
 
						var current_location_query="select top 1 agent_name,lat_message,lon_message from current_location  WHERE agent_name='"+user_id+"' order by data_insertion_time desc";
						var location_insert_qry_data=function(err,result){
							if (err==1){
								res.send({status: 500, msg: 'Query failed'});
								return;
							}
							else{  
								
								if(result==""){
									callback(null,0,args1,args2);
								}
								else{
									callback(null,1,args1,args2)
								}
							}
						}
						global.db_query.select_query(current_location_query,location_insert_qry_data,1);
					},
					 function(args3,args4,args5,callback){
						
						var location_insert_qry_data1=function(err1,result1){
							if (err1==1){
								res.send({status: 500, msg: 'Query failed 1'});
								return;
							}
							else{
								if(result1=="")
								{
									res.send({status: 400, msg: 'No Result found 1'});
								}
								else{
									callback(null,10)
								}
							}
						}
						 var date_insert=moment(new Date(Date.now())).format('YYYY-MM-DD HH:mm:ss');
						var current_time_now=new Date(Date.now());
						var data_now=current_time_now.getTime();	
						if(args3==0){
							
							//var current_location_insert_query = "INSERT INTO current_location (user_id,lat_message,prev_lat_message,lon_message,prev_lon_message,battery_percentage,data_insertion_time) VALUES ";
							//current_location_insert_query+=util.format("(%d,'%s','%s','%s','%s','%s','%s')",user_id,args4,args4,args5,args5,value.batteryLevel,data_now);

							var current_location_insert_query = "INSERT INTO current_location (agent_name,lat_message,prev_lat_message,lon_message,prev_lon_message,data_insertion_time) VALUES ";
							current_location_insert_query+=util.format("('%s','%s','%s','%s','%s','%s')",user_id,args4,args4,args5,args5,data_now);  

							global.db_query.insert_query(current_location_insert_query,location_insert_qry_data1,1);
						}
						else if(args3==1){
							//var current_location_update_query = "update current_location set user_id="+util.format("(%d)",user_id)+",lat_message="+util.format("('%s')",args4)+",prev_lat_message="+util.format("('%s')",args4)+",lon_message="+util.format("('%s')",args5)+",prev_lon_message="+util.format("(%d)",args5)+",data_insertion_time="+util.format("('%s')",data_now)+",battery_percentage="+util.format("('%s')",value.batteryLevel);
							var current_location_update_query = "update current_location set agent_name="+util.format("('%s')",user_id)+",lat_message="+util.format("('%s')",args4)+",prev_lat_message="+util.format("('%s')",args4)+",lon_message="+util.format("('%s')",args5)+",prev_lon_message="+util.format("(%d)",args5)+",data_insertion_time="+util.format("('%s')",data_now);



							current_location_update_query+=" where agent_name="+util.format("('%s')",user_id);
						global.db_query.update_query(current_location_update_query,location_insert_qry_data1,1);
						}		 
					}
				],function(errors,results){
					console.log(results);
				});
			});	 
		}
	}

	function calc_distance(from_lat,from_lng,to_lat,to_lng){
		var theta = from_lng- to_lng;
		var dist = Math.sin(from_lat*(Math.PI/180)) * Math.sin(to_lat*(Math.PI/180)) +  Math.cos(from_lat*(Math.PI/180)) * Math.cos(to_lat*(Math.PI/180)) * Math.cos(theta*(Math.PI/180));
		dist = Math.acos(dist);
		dist = dist*(180/Math.PI);
		var miles = dist * 60 * 1.1515;
		miles=(miles * 1.609344).toFixed(2);
		return miles;
	}

	this.location_insert = function(req,res){
		console.log(req);
		var data_to_return={'status':0,'msg':'Invalid Data','data':{}};

		req.assert('user_id', 'Invalid value user_id').notEmpty();
		//req.assert('latitude', 'Invalid value latitude').notEmpty();
		//req.assert('longitude', 'Invalid value longitude').notEmpty();

		var errors = req.validationErrors();
		if (errors) {
			data_to_return['data']['error_msg']=errors[0]['msg'];
			res.send(data_to_return);
			return;
		}
		
		var data_send={};
		var data1_send={};
		var user_id=req.sanitize('user_id').escape().trim();
		data_send['user_id']=user_id;
		var latitude=req.body[0].latitude;
		//var latitude=req.sanitize('latitude').escape().trim();
		data_send['latitude']=latitude;
		
		//var longitude=req.sanitize('longitude').escape().trim();
		var longitude=req.body[0].longitude;
		data_send['longitude']=longitude;
		var status_checkin=req.sanitize('status_checkin').escape().trim();
		data_send['status']=status_checkin;
		var date=new Date(Date.now());
		var moment=require("moment");
		//var datetime=req.sanitize('time').escape().trim();
		//var datetime=req.body[0].time;
		var current_time_now=new Date(Date.now());
		var datetime=current_time_now.getTime();
		
		var util=require('util'); 
		data_send['data_insertion_time']=datetime;
		var checkin_insert_query = "INSERT INTO checkin_checkout (agent_name,latitude,longitude,data_insertion_time,status) VALUES ";
			checkin_insert_query+=util.format("('%s','%s','%s','%s',%d)",user_id,latitude,longitude,data_send['data_insertion_time'],status_checkin);
		var location_insert_qry_data=function(err , result){
			if(err==1){
				res.send({status:500,msg:'Query Failed'});		
			}
			else{
				if (result==''){
					res.send({status: 400, msg: 'empty result'});
				return;}else{
					data_to_return['status']=200;
					data_to_return['msg']="success";
					data_to_return['data']['user_id']=user_id;
					res.send(data_to_return); 
				}
			}
			
		}
		console.log(checkin_insert_query);
			global.db_query.insert_query(checkin_insert_query,location_insert_qry_data);
	}
}
var self =module.exports = new mcpl();