
var exports=module.exports = {};

//CHECK AUTO FENCH
exports.check_auto_fench= function (){
	
	//GLOBAL VARIABLES
	var order_info_array={};
	var order_info_array_size=0;
	var order_info_array_index=0;
	var order_info_array_index_check=1;
	
	//DISTANCE TO CHECK THE VEHICLE IN & OUT DISTANCE IN AUTOFENCH
	var auto_check_distance=global.autofench_distance;		
	
	var select_query= " SELECT oai.oa_id AS oai_oa_id,oai.trip_status as oai_trip_status, " +
	" oai.vehicle_id as oai_vehicle_id,  oai.device_id as oai_device_id, " +" dp.device_type as device_type, " +
	" dp.lat_message as device_lat,dp.lon_message as device_lon, " +
	" oai.geo_address_lat,oai.geo_address_lng, " +
	" ( CASE WHEN (oai.trip_status=2)  THEN ROUND((( 3959 * ACOS( COS( RADIANS(dp.lat_message) ) * COS( RADIANS( oai.geo_address_lat ) ) * COS( RADIANS( oai.geo_address_lng ) - RADIANS(dp.lon_message) ) + SIN( RADIANS(dp.lat_message) ) * SIN( RADIANS( oai.geo_address_lat ) ) ) )*1609.34) ,2)  ELSE '0'  END ) AS distance " +
	" FROM milk_order_aggregate_info oai  " +
	" JOIN device_port dp ON dp.device_id=oai.device_id " +
	" WHERE oai.trip_status='2' AND oai.is_deleted=0 " ;
	
	var d = new Date();
	var error_message='\r\n\r\n LOG: '+Date();
	error_message +='\r\n ERROR TYPE: SELECT QUERY ';
	error_message +='\r\n query: '+select_query;
	global.fs.appendFile("error_log/query_error_log"+d.getDate()+"_"+d.getMonth()+"_"+d.getFullYear()+".txt", error_message, function(err) 
	{
		if(err) 
		{
			console.log(err);
		}
	});
	
	global.db_query.custom_select_query(
			select_query,
			AfterOrderInfoResultFetch,
			"1"
	);	
	
	function AfterOrderInfoResultFetch(err , result){
		if (err==1){
			
		}
		else if(!result.length>0){
			
		}
		else{
			order_info_array = result.slice();
			order_info_array_size=result.length;
			IterateOrderInfoArray();
		}
	}
	
	function IterateOrderInfoArray(){
		if(order_info_array_index==order_info_array_index_check)
			order_info_array_index++;
		order_info_array_index_check=order_info_array_index;
		
		if(order_info_array_index<order_info_array_size){
			//check destination in
			if(order_info_array[order_info_array_index]["oai_trip_status"]==2){
				CheckForActiveOrder();
			}
			else{
				IterateOrderInfoArray();
			}				
		}
		else{
			console.log("came to end");
		}
	}
	
	function CheckForActiveOrder()
	{
		global.db_query.select_query(
			" milk_order_info",
			" count(*) as order_count ",
			" oa_id='"+order_info_array[order_info_array_index]["oai_oa_id"]+"' AND is_deleted='0' AND trip_status IN(1,0) ",
			"",
			function(err , result) 
			{			
				if(err==1){
					IterateOrderInfoArray();
				}
				else if (!result.length>0)
				{
					IterateOrderInfoArray();
				}
				else{
					if(result[0]["order_count"]==0){
						CheckDestinationInn();
					}
					else
						IterateOrderInfoArray();
				}
			}
		);
	}
	
	function CheckDestinationInn(){
		
		//console.log("destination In Order id="+order_info_array[order_info_array_index]['oi_order_id']);
		if(order_info_array[order_info_array_index]["oai_trip_status"]==2 && order_info_array[order_info_array_index]["distance"]<auto_check_distance){
			var data_to_update={};
			data_to_update['trip_status']="3";
			data_to_update['trip_completed_datetime']=global.common_method.Get_Date_Time();
											
			global.db_query.update_query(
				"milk_order_aggregate_info",
				"oa_id='"+order_info_array[order_info_array_index]["oai_oa_id"]+"' AND trip_status='2' ",
				data_to_update,
				function(err , upresult){	
					if (err==1){
						IterateOrderInfoArray();
					}
					else{
						if(upresult["affectedRows"]>0){		
							global.sms_method.send_pod_link_to_consignee(order_info_array[order_info_array_index]["oi_order_id"]);
							UpdateVehicleDetail();
						}
						else{
							IterateOrderInfoArray();
						}
					}
				}
			);
		}
		else
			IterateOrderInfoArray();
	}
	
	//CHECK AND REMOVE THE VEHICLE FORM TRIP AND UNAMRY THE DEVICE
	function UpdateVehicleDetail()
	{
		var data_to_update={};
		data_to_update['otrip_status']="0";
		data_to_update['oid']="0";
		if(order_info_array[order_info_array_index]["device_type"]==0)
		data_to_update['device_id']="0";
		
		global.db_query.update_query(
				"vehicle_details",
				"id='"+order_info_array[order_info_array_index]["oai_vehicle_id"]+"' AND oid='"+order_info_array[order_info_array_index]["oai_oa_id"]+"' ",
				data_to_update,
				function(err , result){	
					if (err==1){
						IterateOrderInfoArray();
					}
					else if (!result.length>0){
						IterateOrderInfoArray();
					}
					else{
						if(result["affectedRows"]>0){		
							IterateOrderInfoArray();
						}
						else{
							IterateOrderInfoArray();
						}
					}
				}
			);
	}
};
