<?php
//error_reporting(0);

include("config/config.php");
if(!isset($_SESSION["user_id"])){ // checking whether the usertype is logged in
	header('Location: logout.php');        						//REdirection to logout.php
	exit;
}
include("core/class/db_query.php");                       // Class where query generetion is written
include("core/class/db_helper.php");                            // Class where table and feilds 
include("core/function/common.php"); 
$db_query=new db_query();  

global $db_helper_obj;
$db_helper_obj=new db_helper();

$list=$db_helper_obj->deliverymo_edit($_GET["process_id"]);

$supplier_list=$db_helper_obj->supplier_list();

//$product_name=$db_helper_obj->product_name($_GET["order_id"]);
$_POST["products"]=convert_array($list[0]["products"]);

$shipto=$db_helper_obj->shipto(2);
if($_POST["po_from"]=='po_edit'){
$old_arr=convert_array($_POST["products"]);// convert_array() is store it to an object directly
$var_count=0;
foreach($old_arr as $va=>$key){
$old_arr[$va]["qty"]=$_POST["qty"][$var_count];	
$old_arr[$va]["rate"]=$_POST["rate"][$var_count];	
$old_arr[$va]["amount"]=$old_arr[$va]["qty"]*$old_arr[$va]["rate"];	
$var_count++;	
}
$_POST["products"]=base64_encode(json_encode($old_arr));
}	


$count=1;
function Amount_find($num1,$num2){
return 	$num1*$num2;;
}
$amount=array_map("Amount_find",$_POST["qty"],$_POST["rate"]);
$arr="";
$arrays=$_POST["products"];
//echo"<pre>"; print_r($arrays["product_id"]); echo"</pre>"; exit;
foreach($arrays["product_id"] as $va1=>$key1){
$product_name=$db_helper_obj->total_product_edit($key1);

$final_array[$key1]["product_id"]=$product_name[0]["name"];
$final_array[$key1]["qty"]=$arrays["qty"][$va1];
$final_array[$key1]["rate"]=$arrays["rate"][$va1];
$final_array[$key1]["amount"]=$arrays["qty"][$va1]*$arrays["rate"][$va1];
}
//echo"<pre>"; print_r($final_array); echo"</pre>"; exit;
foreach($final_array as $va2=>$key2){

$arr.='<tr >
   <td  class="border_cls" height="19" align="CENTER" >'.$count.'</td>
   <td colspan="3" class="border_cls" align="CENTER" ><font size="3">'.$key2["product_id"].' </font></td>
   <td class="border_cls" align="CENTER" ><font size="3">'.$key2["qty"].' NOS</font></td>
   <td class="border_cls" align="CENTER" ><font size="3">'.$key2["rate"].'</font></td>
   <td colspan="1" style="border-left: 1px solid #000000;" class="border_cls" align="CENTER" ><font size="3">'.sprintf("%.2f",$key2["amount"]).'</font></td></tr>';
$count++;
   }

  $nature=$db_helper_obj->nature_edit($arrays["nature"][0]);
  //echo"<pre>"; print_r($nature[0]["nature"]); echo"</pre>";  exit;
//echo"<pre>"; print_r(); echo"</pre>";  exit;
$supplier_list=$db_helper_obj->supplier_edit($_POST["products"]["supplier"]);
foreach($supplier_list as $va3=>$key3){
$reg=explode(",",$key3["address"]);
$supplier_list[$va3]["addr1"]=$reg[0].",".$reg[1];
if(isset($reg[2])||isset($reg[3]))
$supplier_list[$va3]["addr2"]=$reg[2].",".$reg[3];	
}


$ship_purchase.=' 
  <tr>
    <td class="company" style=
    "padding-left: 5px;border-bottom: 1px solid #ffffff;border-left: 1px solid #000000;border-right:1px solid #000000;"
    height="19" colspan="4" align="left" valign="middle" bgcolor=
    "#FFFFFF" >
      <label id=
      "pur_add1"><font size="3">'.strtoupper($supplier_list[0]["supplier_name"]).'</font></label>
    </td>

    <td align="left" bgcolor="#FFFFFF" colspan=
    "2" valign="middle">
      <label id=
      "supp_addr1"><b><font size="3"> Nature of Processing </font></b></label>
     </td><td style="border-right:1px solid #000000;border-left:1px solid #ffffff"><b><font size="3">'.$nature[0]["nature"].'</font></b></td>
	
  </tr>
  <tr>
    <td class="company" style=
    "padding-left: 5px;border-bottom: 1px solid #ffffff;" height=
    "15" colspan="4" align="left" bgcolor="#FFFFFF" valign=
    "middle">
      <label id=
      "pur_add2"><font size="3">'.strtoupper($supplier_list[0]["addr1"]).'</font></label>
    </td>
    
    <td  align="left" bgcolor="#FFFFFF" colspan=
    "2" valign="middle">
      <label id=
      "supp_addr2"><b><font size="3">Despatched through:</font></b></label>
	  </td><td style="border-right:1px solid #000000;border-left:1px solid #ffffff"><b><font size="3">'.$_POST["products"]["delivery_through"].'</font></b></td>
    </td>
  </tr>
  <tr>
    <td class="company" style=
    "padding-left: 5px;border-bottom:1px solid #ffffff;" height=
    "19" colspan="4" align="left" bgcolor="#FFFFFF" valign=
    "middle">
      <label id=
      "pur_add3"><font size="3">'.strtoupper($supplier_list[0]["addr2"]).'</font></label>
    </td>
  
    <td align="left" bgcolor="#FFFFFF" style=
    "border-right:1px solid #ffffff;border-bottom: 1px solid #ffffff;border-left: 1px solid #ffffff;"
    colspan="2" valign="middle">
      <label id=
      "supp_addr3"><b><font size="3">Duration of Process:</font></b></label>
    </td>
	<td style="border-right:1px solid #000000;border-left:1px solid #ffffff"><b><font size="3">'.$_POST["products"]["duration"].'</font></b></td>
  </tr>
  <tr>
    <td class="company" height="19" colspan="4" align="left"
    bgcolor="#FFFFFF"  style="border-bottom:1px solid  #000000;" valign="middle">
      <label id=
      "pur_add4"><font size="3">'.strtoupper($supplier_list[0]["city"]).'</font></label>
    </td>
   
    <td align="left" bgcolor="#FFFFFF" style=
    "border-right:1px solid #000000;border-bottom: 1px solid #000000;border-left: 1px solid #ffffff;"
    colspan="3" valign="middle">
      <label id=
      "supp_addr4"><font size="3"></font></label>
    </td>
  </tr>';
			


if($_POST["products"]["igst"]!=''&&$_POST["products"]["igst_total"]!=''){
$val='
            <tr>
			          
			   
			   <td class="" bgcolor="#FFFFFF" align="right" colspan="2"  valign="middle" style="border-bottom:1px solid #ffffff;"> 
			  <font size="3"> '.$_POST["products"]["igst"].'%</font>
			  </td>
               <td class="border_cls4" bgcolor="#FFFFFF" align="center" valign="middle" style="border-left:1px solid #000000;border-right:0px solid #000000;"><font size="3">IGST</font>
			   </td>
               <td class="border_cls4" align="CENTER" valign="MIDDLE" bgcolor="#FFFFFF" style="padding-top: 3px;padding-bottom: 4px;"> <font size="3">
			   '.$_POST["products"]["igst_total"].'</font></td>
            </tr>';	
}
else 
{
$val='<tr><td class="company" style="border-right:0px solid #000000;" align="CENTER" bgcolor="#FFFFFF"><b><br> </b></td>
               <td class="" bgcolor="#FFFFFF" align="right" colspan="1" valign="middle"><font size="3">
			   '.$_POST["products"]["sgst"].'%</font></td>
			   <td class="border_cls4" bgcolor="#FFFFFF" align="center" valign="middle">SGST</td>
               <td class="border_cls4" align="CENTER" valign="MIDDLE" bgcolor="#FFFFFF" style="padding-top: 4px;padding-bottom: 4px;"> 
			   '.$_POST["products"]["sgst_total"].' </td>
            </tr>
            <tr>
               <td class="border_cls5" style="border-left:1px solid #ffffff;border-right:0px solid #000000;" colspan="1" rowspan="4" align="LEFT" bgcolor="#FFFFFF"><b><br></b></td>
			   <td class="" bgcolor="#FFFFFF" align="right"  valign="middle"><font size="3">
			   '.$_POST["products"]["cgst"].'%</font></td>
               <td class="border_cls4" bgcolor="#FFFFFF" align="center" valign="middle"><font size="3">CGST</font></td>
               <td class="border_cls4" align="CENTER" valign="MIDDLE" bgcolor="#FFFFFF" style="padding-top: 4px;padding-bottom: 4px;"> <font size="3">
			   '.$_POST["products"]["cgst_total"].'</font></td>
            </tr>'	;
}

$total_qty=array_sum($qtys);
$total_amount=array_sum($amount);

//$purno=$db_helper_obj->purchase_list();
foreach($purno as $va3=>$key3){
	$purnos[]=$key3["po_number"];
}

//if(!in_array($_POST["po_number"],$purnos))	
//	$supplierss=$db_helper_obj->order_insert();
?>
<?php
ob_clean();

// create some HTML content
$html1 = '<style>

	   .pur_head{
		border: 1px solid #000000;
		-webkit-text-fill-color:black;
	  }
	  .company{
		border-left: 1px solid #000000;
		border-right: 1px solid #000000;
		border-bottom:1px solid #ffffff;
		 border-spacing: 2px;
	  }

	   .border_cls{
			border-left: 1px solid #000000; border-right: 1px solid #000000;
			border-bottom: 1px solid #ffffff;
	  }
	  .border_cls1{
		border-right: 1px solid #000000 ;
		border-left: 1px solid #ffffff;
		border-bottom: 1px solid #ffffff;
	  }
	  .border_cls2{
		border-bottom: 1px solid #ffffff; border-left: 1px solid #000000 ;

	  }
	  .border_cls3{
		border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;  

	  }
	  .border_cls4{
		border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;  

	  }
	  .border_cls5{
		border-bottom: 1px solid #ffffff; border-right: 1px solid #ffffff;
	
	  } 
	 
	  td{
		   
			height: 10px;
			valign:"center"; 			
		
	  }
	  </style><table align="center" style="border-collapse:collapse;font-size:	15px;">
         <colgroup>
            <col width="10">
            <col width="10">
            <col width="133">
            <col width="119">
            <col width="111">
            <col width="244">
         </colgroup>
         <tbody>
            <tr>
			
               <td class="pur_head" colspan="7" align="center" valign="MIDDLE" >
			   <b>
			   JOB WORK OUT ORDER</b></td>
            </tr>
			 <tr>
               <td class="company" style="-webkit-text-fill-color:black;padding-left: 0px;border-right:1px solid #ffffff;" valign="MIDDLE" align="LEFT" rowspan="4" >
				<img style="width:170px;max-height: 40px;" src="images/Metamatix_logo.png" alt="Team Metamatix" />
			   </td> 
				<td style="padding-left: 0px;padding-right:5px;" valign="bottom" align="LEFT" colspan="3">
				<b><font size="3">METAMATIX SOLUTIONS PRIVATE LIMITED</font></b>
			   </td>
               
               <td  align="LEFT" valign="bottom" style="-webkit-text-fill-color:black;border-bottom: 1px solid #ffffff;border-left:1px solid #000000;" >
			 <font size="3">  <b>Voucher No:</b></font>
			   </td>
               <td colspan="2" class="border_cls1" style="-webkit-text-fill-color:black;border-bottom: 1px solid #ffffff;" valign="bottom"  align="LEFT" >
			   <font size="3"><b>'.$_POST['products']['challan_no'].'</b></font>
			   </td>			   
            </tr>
          
            <tr>
               <td class="company" colspan="3" style="-webkit-text-fill-color:black;padding-left: 0px;" valign="bottom" align="LEFT" >
			   <font size="3">SHED NO:27, SIDCO INDUSTRIAL ESTATE,</font>
			   </td>
               
               <td colspan="2"align="LEFT" valign="bottom" style="-webkit-text-fill-color:black;border-bottom: 1px solid #ffffff;border-top:1px solid #000000;border-left:1px solid #000000;padding-left:1px;">
			   <b><font size="3">Dated:</font>
			   </td>
               <td colspan="2" class="border_cls1" style="-webkit-text-fill-color:black;border-bottom: 1px solid #ffffff;" align="LEFT" valign="bottom" >
			  
              <font size="3"><b>'.date('d-M-Y',strtotime($_POST["products"]["dated"])).'</b></font>
               </td>
            </tr>
            <tr>
               <td class="company" style="-webkit-text-fill-color:black;padding-left: 2px;" colspan="3" align="LEFT" valign="bottom" >
			   <font size="3">KURICHI</font>
			   </td>
               
               <td  colspan="2"align="LEFT" style="-webkit-text-fill-color:black;border-bottom: 1px solid #ffffff;border-left: 1px solid #ffffff"  valign="bottom" >
			  <font size="2"> <b>Supplier Ref./Order No</b></font>
			   </td>
               <td colspan="2" class="border_cls1" style="-webkit-text-fill-color:black;border-bottom: 1px solid #ffffff;" align="LEFT" valign="bottom" >
			   <font size="3"><b>'.$_POST["products"]["challan_no"].'</b></font>
			   </td>
            </tr>
            <tr>
               <td class="company" style="-webkit-text-fill-color:black;padding-left: 2px;border-bottom: 1px solid #000000;" colspan="3" valign="bottom" align="LEFT" >
			   <p><font size="3">COIMBATORE</font></p>
			   <p><font size="3">Tamil Nadu - 641 021</font></p>
			   <p><font size="3">GSTIN/UIN: 33AAKCM7413N1Z5</font></p>
			   <p><font size="3">CIN: U28990TZ2016PTC028310</font></p>
			   </td>
              
               <td  colspan="2" align="LEFT" style="border-bottom: 1px solid #000000;border-left: 1px solid #ffffff;"><font size="3"><b>Mode/Terms of Payment</b></font>
			   <br>
			   </td>
               <td class="border_cls1" align="LEFT" style="border-bottom: 1px solid #000000;" >'.$_POST["products"]["payment_term"].'
			   <br>
			   </td>
            </tr>
			<tr>
			 
            <tr>
               <td class="company" colspan="4"  align="LEFT"	style="padding-left: 4px;padding-right:1px; border-bottom: 1px solid #ffffff;border-left:1px solid #000000;border-top:1px solid #000000;border-right:1px solid #000000;" valign="middle">
			   <font size="3"> <b>GSTIN/UIN: </b>'.$_POST["products"]["gst_number"].' </font>
			  
				<font size="3"><p></p><b>Supplier:</b></font>
			   </td>              
               <td  align="LEFT" colspan="2" valign="middle" style="border-bottom: 1px solid #ffffff;border-left: 1px solid #ffffff;border-top:1px solid #000000;">
			   <font size="3"><b>Destination:</b></font>
			   </td>
			   
			 <td style="border-right:1px solid #000000;"><b><font size="3">'.$_POST["products"]["destination"].'</font></b></td><br>/br>
			 
			 <td  align="LEFT" colspan="2" valign="middle" style="border-bottom: 1px solid #ffffff;border-left: 1px solid #ffffff;border-top:1px solid #000000;">
			   <font size="3"><b></b></font>
			   </td>
			   
			 
            </tr>
            
           '.$ship_purchase.'
           
            <tr>
               <td class="company" style="border-bottom: 1px solid #000000;" align ="CENTER"    bgcolor="#C3D69B" valign="middle"><font size="3"><b>SL NO</b><font></td>
               <td colspan="3" align="CENTER" bgcolor="#C3D69B" style="border-right:1px solid #000000;border-bottom: 1px solid #000000;" valign="middle" ><font size="3" align="CENTER" ><b> Description of Goods </b></font></td>
			             
               <td class="border_cls1" style="border-bottom: 1px solid #000000;" align="CENTER" bgcolor="#C3D69B" valign="middle"><font size="3"><b>Quantity</b></font></td>
               <td class="border_cls1" style="border-bottom: 1px solid #000000;" align="CENTER" bgcolor="#C3D69B" valign="middle"><font size="3"><b>Rate</b></font></td>
			    <td colspan="1" class="border_cls1" style="border-bottom: 1px solid #000000;" align="CENTER" bgcolor="#C3D69B" valign="middle"><font size="3"><b>Amount</b></font></td>
				
			</tr>
            <tr >
               <td colspan="1"  class="company" height="19" align="RIGHT" bgcolor="#FFFFFF"><br></td>
               <td colspan="3"  class="border_cls" align="LEFT" bgcolor="#FFFFFF"><br></td>
			   <td class="border_cls" align="LEFT" bgcolor="#FFFFFF"><br></td>
               <td class="border_cls" align="LEFT" bgcolor="#FFFFFF"><br></td>
               <td  colspan="1" class="border_cls" align="LEFT" bgcolor="#FFFFFF"><br></td>
               
            </tr>
		      '.$arr.'  
			
            <tr>
               <td style="border-bottom: 1px solid #000000; border-left: 1px solid #000000" height="19" align="LEFT" bgcolor="#FFFFFF"><br></td>
               <td colspan="3" class="border_cls3" align="LEFT" bgcolor="#FFFFFF"><br></td>
               <td class="border_cls3" align="LEFT" bgcolor="#FFFFFF"><br></td>
               <td class="border_cls3" align="LEFT" bgcolor="#FFFFFF"><br></td>
               <td colspan="1" class="border_cls3" align="LEFT" bgcolor="#FFFFFF"><br></td>
            </tr>
            <tr>
               <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 1px solid #000000;border-left: 1px solid #000000;" height="10" align="RIGHT" bgcolor="#FFFFFF"></td>
               <td style="border-top: 1px solid #000000;border-bottom: 1px solid #000000;border-right: 1px solid #000000;border-left: 1px solid #000000;" align="CENTER" bgcolor="#FFFFFF">Total<br></td>
               <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 1px solid #000000" colspan="3"  align="CENTER" bgcolor="#FFFFFF"><b>'.$_POST["products"]["sub_totallqty"].'   NOS <br></b></td>
               <td style="border-top: 1px solid #000000; border-left: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 1px solid #000000;" align="CENTER" bgcolor="#FFFFFF"><br></td>
               <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000;border-left: 1px solid #000000; border-right: 1px solid #000000;" align="CENTER" bgcolor="#FFFFFF">'.$_POST["products"]["sub_totall"].' <br></td>
            </tr>
			
             <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
			 		  
			
				 
				------------
			
				------
				
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;">Amount Chargeable(in words)</td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;">
				<b>INR '.$_POST["products"]["words"].'</b></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				  <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				  <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="3" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="4" style="border-top:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;"><font size="2" align="MIDDLE"> for METAMATIX SOLUTIONS PRIVATE LIMITED</font></td>		
				</tr>
				
				 <tr>
				<td colspan="3" style="border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;"><font size="3">Company`s PAN   :<b>&nbsp;&nbsp;&nbsp;&nbsp;		AAKCM7413N</b><p></p><u></font>
				
				</td>		
				
				<td colspan="3" style="border-bottom:1px solid #000000;border-left:1px solid #ffffff;"></td>
				<td align="right" style="border-bottom:1px solid #000000;border-right:1px solid #000000;"><font size="2">Authorised Signatory</font>			
				</tr>
					
				        
			
			
           </tbody>
		
      </table><P align="CENTER"> This is a Computer Generated Document</p>';
	  
//echo"<pre>";print_r($html1);echo"</pre>";exit;
include("MPDF57/mpdf.php");
$mpdf=new mPDF('en-GB-x','A4','','',12,12,12,12,8,4); 
$mpdf->SetDisplayMode('fullpage');
$mpdf->list_indent_first_level = 0;	// 1 or 0 - whether to indent the first level of a list
// LOAD a stylesheet
$stylesheet = file_get_contents('mpdfstyletables.css');
$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
$mpdf->WriteHTML($html1);
$mpdf->Output();
exit;
//==============================================================
//==============================================================
//==============================================================
?>