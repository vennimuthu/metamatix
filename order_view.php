<?php 
include("config/config.php");
if(!isset($_SESSION["user_id"])){ // checking whether the usertype is logged in
	header('Location: logout.php');        						//REdirection to logout.php
	exit;
}

include("core/class/db_query.php");                             // Class where query generetion is written
include("core/class/db_helper.php");                            // Class where table and feilds 
                                                                // are mentioned to generate query
ob_start();                                                     // to clear the internal output
global $db_helper_obj;
global $global_website_url;
$db_helper_obj=new db_helper();  // Creating object to get data from table
$current_page = basename($_SERVER['PHP_SELF']); // to get the page name from url
global $month_name_arr;
$month_name_arr=array('1'=>'Jan','2'=>'Feb','3'=>'Mar','4'=>'Apr','5'=>'May','6'=>'Jun','7'=>'Jul','8'=>'Aug','9'=>'Sep','10'=>'Oct','11'=>'Nov','12'=>'Dec');
?>
 
	<link rel="icon" type="images/png" href="images/favicon.png">
	<link rel='stylesheet' href='css/font-awesome.css'>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/jquery.mCustomScrollbar.min.css">
	<link rel="stylesheet" href="css/responsive-menu.css">
	<link rel="stylesheet" href="css/style.css">
	<script src="js/jquery.js"></script>
<?php 

	$db_query=new db_query();  
	global $db_helper_obj;
	$db_helper_obj=new db_helper();

	if(!empty($_REQUEST)){
	$master=$db_helper_obj->order_edit($_REQUEST["product_id"]);
	}


	?>
	
<style>
	.cost_form { }
	.cost_form input.form-control { margin-bottom:10px; }

	</style>
							  
<div class="table-responsive " id="orderview_id"> 
<table class="table listhead table-bordered table-hover">
	<thead>
		<tr>
    <th>Purchase Order Number</th>
    <td><?php echo $master[0]["po_number"] ?></td>
  </tr>
		<tr>
			<th>Po Date</th>
			
			<td><?php echo date('d-m-Y',strtotime($master[0]["po_date"]));?></th>
		</tr>
		
		<?php 

	 if(!empty($master[0]["supplier_id"])){foreach($master as $va=>$key){ }?>   
		<tr> <th>Supplier</th>
			<td><?php 
			$master2=$db_helper_obj->supplier_edit($key["supplier_id"]);
			echo $master2[0]["supplier_name"]; ?>	
			</td>
			
		</tr>
	  <?php }  ?> 
	  
		<?php 

	 if(!empty($master[0]["shipto_id"])){foreach($master as $va=>$key){ }?>   
		<tr> <th>Ship To</th>
			<td><?php 
			$master3=$db_helper_obj->shipto($key["shipto_id"]);
			echo $master3[0]["ship_name"]; ?>
			</td>
			
		</tr>
	  <?php }  ?>
		<tr><th>Despatch Through</th>
		<td>By-Road Transport</td></tr>
		<tr>
			<th>Payment Terms</th>
			<td><?php echo $master[0]["payment_terms"]; ?></td>
		</tr>
		<tr>	
			<th>Required Date</th>
			<td><?php echo date('d-m-Y',strtotime($master[0]["required_date"])); ?></td>	
		</tr>
		<tr>	
			<th>Material Type</th>
			<td><?php if($master[0]["material_type"]==1) echo "Raw Materials"; else if($master[0]["material_type"]==2) echo "Steel Materials"; ?></td>	
		</tr>
	</thead>
	<tbody>
		<tr>
			
					
		</tr>
	</tbody>
</table>

<table align="center"  class="table listhead table-bordered table-hover">
	<thead>
	<p></p>
		<tr>
			<th style="text-align: center;">S.No</th>
			<th style="text-align: center;">Item Name</th>
			<th style="text-align: center;"> HSN </th>
			<th style="text-align: center;">Qty</th>
			<th style="text-align: center;">Unit</th>
			<th style="text-align: center;width: 11%;">Rate</th>
			<th style="text-align: center;width: 15%;">Amount</th>
		</tr>
		
		</thead>
		
	<tbody>
	 <?php $count=1;
	 $total_amount=0;
	 if(!empty($master)){foreach($master as $va=>$key){ ?>   
		<tr>
			<td align="center"><?php echo $count; ?></td>
			<td align="center"><?php 
			$master1=$db_helper_obj->total_product_edit($key["product_id"]);
			echo $master1[0]["name"]; ?></td>	
			<td align="center"><?php echo $key["hsn"];?></td>
			<td align="center"><?php echo $key["qty"]; ?></td>
			<td align="center"><?php $unit=$db_helper_obj->unit_edit($key["unit"]);
					echo $unit[0]["unit_name"]; ?></td>
			<td align="right"><?php echo $key["price"];?></td>	
			<td align="right"><?php $total_amount+=$key["amount"]; echo $key["amount"]; ?></td>	
			
		</tr>
	  <?php $count++;} } ?>  
	<?php if($master[0]["sgst_percent"]){ ?>  
	<tr>
		<td colspan="4"></td>
		<td align="center"><b>SGST</b></td>
		<td align="right"><?php echo $master[0]["sgst_percent"]; ?>%</td>
		<td align="right"><?php $total_amount+=$master[0]["sgst_total"]; echo $master[0]["sgst_total"];?></td>
	</tr>
	<?php } ?> 
	<?php if($master[0]["cgst_percent"]){ ?> 
		<tr>
		<td colspan="4"></td>
		<td align="center"><b>CGST</b></td>
		<td align="right"><?php echo $master[0]["cgst_percent"]; ?>%</td>
		<td align="right"> <?php $total_amount+=$master[0]["cgst_total"]; echo $master[0]["cgst_total"];?></td>
		</tr>
	<?php } if($master[0]["igst_percent"]){ ?> 
		<tr>
		<td  colspan="4"></td>
		<td align="center"><b>IGST</b></td>
		<td align="right"><?php echo $master[0]["igst_percent"]; ?>%</td>
		<td align="right"> <?php $total_amount+=$master[0]["igst_total"]; echo $master[0]["igst_total"]; ?></td>
		</tr>
	<?php } ?>
		 
		<tr>
			<td  colspan="5"></td>
			<td><b>Grand Total</b></td>
			<td align="right"><b><?php echo $total_amount; ?></b></td>		
		</tr>
	</tbody>
	
</table>
</div>                 
<script>
setTimeout(function(){ parent.$("#iframe_show_vehicle_data").attr("height",$("#orderview_id").height()+30+"px"); }, 100);
</script>				