<?php function main(){
	$db_query=new db_query();  
	global $db_helper_obj;
	$db_helper_obj=new db_helper();
	if(!empty($_POST)){
		$inserted=$db_helper_obj->total_product_update($_GET["product_id"]);
		header('Location:  category_list.php'); //REdirection to master_list.php
	}else{
		unset($_SESSION["cost"]);
	}
	$material_list=$db_helper_obj->material_category();	
	$master_list=$db_helper_obj->total_product_edit($_GET["product_id"]);

?>
<style>
	.cost_form { }
	.cost_form input.form-control { margin-bottom:10px; }
	.tbox_vsmall {
     width:21%;
	}

</style>
<script type="text/javascript">
	$(document).ready(function(){
		show_hide('<?php echo $master_list[0]["material"];  ?>');
	});
	function show_hide(type){
		if(type==5){
			$('.hidden_id').hide();
			$('.hidden_kg').show();
		}else{
			$('.hidden_id').show();
			$('.hidden_kg').hide();
		}
	}
</script>
<form class="form-horizontal" id="recruitment" name="recruitment" method="post">

  <div class="form-group">
	
  <section class="main_content">
	<div class="container-fluid">
    	
    	<div class="row">
        	<div class="col-sm-12">
            	
				<form name="master_add" id="master_add" method="post" target="iframe">
            	<div class="listwrapper">
			<div class="table-responsive"> 
                            <table id="order_item_table_1" class="table listhead table-bordered table-hover">
                                <thead>
                     <tr>
					<th>Sub Products </th>	
					<th>HSN/SAC</th>
					<!--<th>Qty</th>-->
					<th>Material</th>	
					<th>Dimension</th>						
					<th class="hidden_id">Buffing Price / Hour</th>
					<th class="hidden_id">Machining Price </th>
					<th>Price <label class="hidden_kg"> / KG </label></th>
					<th class="hidden_id">Process Involve</th>
					<th class="hidden_id">Machining Involve</th>
					 
                     </tr>
                                </thead>
                                <tbody  id="order_item_body_1"  name="order_item_row_1_1">
								<tr id="order_item_row_1_1" name="order_item_row_1_1">
								
								<div id="material_select" style="display:none;">				
					<option value="" selected disabled >Select</option>
					<?php if(!empty($material_list)) foreach($material_list as $va=>$key){
					?>
					<option value="<?php echo $key["id"];?>"><?php echo $key["category"];?></option>
					<?php  } ?>
					</div>
					<td>
					<input type="text" class="form-control" name="name[1]" id="name_1" value="<?php echo $master_list[0]["name"] ?>" />
					</td>
					<td>
					<input type="text" class="form-control" name="hsn[1]" id="hsn1" value="<?php echo $master_list[0]["hsn"] ?>" />
					</td>
					<td  style="padding: 4px;" >
					<select id="material_1" name="material[1]" class="form-control" onchange="showmaterial(this);" >
					<option value="" selected disabled >Select</option>
					<?php if(!empty($material_list)) foreach($material_list as $va=>$key){
					?>
					<option <?php if($master_list[0]["material"]==$key["id"]) { ?>selected<?php } ?> value="<?php echo $key["id"];?>"><?php echo $key["category"];?></option>
					<?php  } ?>
					</select>
					</td>
					<td>			 
					<input type="text" class="form-control" name="dimension[1]" id="dimension_1" value="<?php echo $master_list[0]["dimension"];?>"  />
					</td>
					
					<td class="hidden_id">
					<input type="text" class="form-control" name="price[1]" id="price_1" value="<?php echo $master_list[0]["price"] ?>" />
					</td>
					<td class="hidden_id">
<input type="text" class="form-control" name="machining_price[1]" id="machining_price_1" value="<?php /*if($master_list[0]["update_machinprice"]=='') { echo $master_list[0]["machining_price"]; } else{ echo  base64_decode($master_list[0]["update_machinprice"]); } */  
 echo $master_list[0]["machining_price"]; 
?>" />
					</td>
					
					<td>
					<input type="text" class="form-control" name="product_price[1]" id="product_price_1" value="<?php echo $master_list[0]["product_price"] ?>"  />
					</td>
					<td class="hidden_id" align="center">
<input type="checkbox" name="process[1]" id="process_1" value="1" <?php if($master_list[0]["process"]==1){ ?>checked<?php } ?>  data-toggle="modal" >
					</td>
					<td class="hidden_id" align="center">
						<input type="checkbox" name="machining[1]" id="machining_1" value="1" <?php if($master_list[0]["machining"]==1){ ?>checked<?php } ?>  >
					</td>				
						</tr>
							</tbody>
                            </table>
							
        <input type="hidden" value="1" id="order_item_row_index_1" name="order_item_row_index_1">
    </div>
	
	 </div>  <!------End--- Table Responsive -->
	 </div>
	 
	 <div class="col-md-12">
			 <button type="button" onclick="window.location.href='category_list.php'" class="btn btn-primary">Cancel</button>
			 <button type="submit" class="btn btn-primary">Submit</button>
		</div>
				</form>
            </div>
        </div>

    </div>
	<div aria-hidden="true"  aria-labelledby="myModal" role="dialog" tabindex="-1" id="myModal" class="purchase modal fade">
                  <div class="modal-dialog">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" onclick="cancel_data()" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h3 class="modal-title text-center">Master View</h3>
                              
                          </div>
                          <div class="modal-body">
							<iframe id="iframe_show_vehicle_data" frameborder="0" marginwidth="0" marginheight="0"  scrolling="no" style="width:100%;border:0px;">
							</iframe>
						
                          <div class="modal-footer">                              
                          </div>                          
                          </div>
                         
                      </div>
                </div>
    </div>
  
	</section>



</form>
<?php } include("template.php"); ?>  