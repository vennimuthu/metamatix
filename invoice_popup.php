<?php 

include("config/config.php");

if(!isset($_SESSION["user_id"])){
	header('Location: logout.php');
	exit;
}

include("core/class/db_query.php");
include("core/class/db_helper.php");

ob_start();
global $db_helper_obj;
global $global_website_url;
$db_helper_obj=new db_helper();
if($_POST["report_ids"]=='Invoice Generate'){
	$inward_list=$db_helper_obj->invoice_generation();
	header('Location:  invoice_list.php');
}
//echo"<pre>";print_r($_POST);echo"</pre>";
//exit; ?>	 
	<link rel='stylesheet' href='css/font-awesome.css'>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/jquery.mCustomScrollbar.min.css">
	<link rel="stylesheet" href="css/responsive-menu.css">
	<link rel="stylesheet" href="css/style.css">
	<script src="js/jquery.js"></script>
<form id="invoice_popup" name="invoice_popup" method="post"  action="invoice_popup.php">
            <div class="listwrapper">
			<div class="table-responsive"  id="master_viewid"> 
                <table id="order_item_table_1" class="table listhead table-bordered table-hover">
                    <thead>
                    <tr>
						<th style="text-align: center;">Invoice No</th>	
						<th style="text-align: center;">Invoice Date</th>
                    </tr>
                    </thead>
								
					<tbody  id="order_item_body_1"  name="order_item_row_1_1">
					<tr id="order_item_row_1_1" name="order_item_row_1_1">
					 
					<td align="center">
						<input type="text" name="inv_no" id="inv_no" class="form-control tbox_small">
					</td>
					<td  align="center">			 
						<input type="date" name="inv_date" id="inv_date" class="form-control tbox_small" value="<?php echo date("Y-m-d"); ?>">
					</td>			
					   
					</tr>
					<tr>
						<td align="center" colspan="3"><input type="submit" name="report_ids" id="report_ids" class="btn btn-primary pull-center" value="Invoice Generate" onclick="parent.location.reload();">
					<input type="hidden" name="inward_id" id="inward_id" value="<?php echo implode(",",$_POST["approve"]);  ?>">	
						</td>
					</tr>
				   </tbody>
				</table>
			
</div>
</div>
 </form>
<script>
function weight_calculate(){
	var vehicle_empty=document.getElementById("vehicle_empty").value;
	var vehicle_load=document.getElementById("vehicle_load").value;
	var rate_kg=document.getElementById("rate_kg").value;
	document.getElementById("net_weight").innerHTML=Number(vehicle_load-vehicle_empty).toFixed(3);
	document.getElementById("total_amount").innerHTML=Number(document.getElementById("net_weight").innerHTML*rate_kg).toFixed(3);
}
function pdf_generate(){
	//parent.document.getElementById("wavement").value=document.getElementById("wavement_no").value;	
	parent.document.getElementById("vehicle").value=document.getElementById("vehicle_no").value;	
	parent.document.getElementById("creation").value=document.getElementById("creation_date").value;
	
	parent.document.getElementById("empty").value=document.getElementById("vehicle_empty").value;
	parent.document.getElementById("load").value=document.getElementById("vehicle_load").value;
	parent.document.getElementById("price").value=document.getElementById("rate_kg").value;	
	
	parent.document.getElementById("receipt_list").submit();
	setTimeout(function(){parent.location.reload()},200);	
}
setTimeout(function(){ parent.$("#iframe_show_vehicle_data").attr("height",$("#order_item_table_1").height()+50+"px"); }, 100);
</script>	