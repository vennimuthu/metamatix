<?php
include("config/config.php");
include("core/class/db_query.php");                             // Class where query generetion is written
include("core/class/db_helper.php");                            // Class where table and feilds 
include("core/function/common.php");   	
                                                                // are mentioned to generate query
ob_start();                                                     // to clear the internal output
global $db_helper_obj;
global $global_website_url;
$db_helper_obj=new db_helper();  

$master=$db_helper_obj->subproduct_edit($_GET["product_id"]); 
//echo"<pre>";print_r($product_id);echo"</pre>";

?>
<div class="container-fluid">
<div class="row">
<div class="col-sm-12">
<div class="listwrapper">
<div class="table-responsive"> 
<table id="order_item_table_1" class="table listhead table-bordered table-hover">
<thead>
<tr>
<th>Sub Products</th>
<th>Open Stock</th>					
<th>Price</th>
<th>Process Involve</th>
<th>Approve</th>
<th></th>
</tr>
</thead>
<tbody id="order_item_body_<?php echo count($master) ?>">
<?php $count=1; if(!empty($master)){foreach($master as $va=>$key){ 
$_SESSION["cost"][$count]=convert_array($key["piece_rate"]);
//echo"<pre>";print_r($_SESSION["cost"][$count]);echo"</pre>";
?>
<tr id="order_item_row_<?php echo count($master) ?>_<?php echo $count; ?>" name="order_item_row_<?php echo count($master) ?>_<?php echo $count; ?>">
<td>
<input type="text" class="form-control" name="name[<?php echo $count; ?>]" id="name_<?php echo $count; ?>" value="<?php echo $key["name"]; ?>" required />
</td>

<td>
<input type="number" class="form-control" name="qty[<?php echo $count; ?>]" id="qty_<?php echo $count; ?>" value="<?php echo $key["qty"]; ?>" required />
</td>

<td>
<input type="text" class="form-control" name="price[<?php echo $count; ?>]" id="price_<?php echo $count; ?>" value="<?php echo $key["price"]; ?>" required />
</td>

<td align="center">
<input type="checkbox" name="process[<?php echo $count; ?>]" id="process_<?php echo $count; ?>" <?php if($key["process"]==1){ ?>checked<?php } ?> value="1">
</td>
<td align="center">
<input type="checkbox" name="approve[<?php echo $count; ?>]" id="approve_<?php echo $count; ?>"  value="1">
</td>
<td style="text-align:left;">
<input type="button" class="btn btn-danger" disabled value="X" onclick="delete_order_item_row(this);"/>
</td>	


<input type="hidden" name="product_id" id="product_id"	value="<?php echo $key["id"]; ?>"/>
</tr>
<input type="hidden" name="sub_product_id[<?php echo $count; ?>]" id="sub_product_id<?php echo $count; ?>"	value="<?php echo $key["sub_product_id"]; ?>"/>
<?php $count++;} } ?>
</tbody>
</table>

<input type="hidden" value="<?php echo count($master) ?>" id="order_item_row_index_<?php echo count($master) ?>" name="order_item_row_index_<?php echo count($master) ?>">
</div>
<div class="addnew_wrapp clearfix">
<div class="addnew" align="right">       
<a href="#" onclick="add_order_row(<?php echo count($master) ?>)" class="" style="padding-top: 8px;padding-bottom: 8px;padding-left: 3px;padding-right: 3px;"> ADD NEW</a>
</div></div>
<br/>
<br/>
</div>
</div>
<div class="col-md-12">
<button type="button" onclick="window.location.href='master_list.php'" class="btn btn-primary">Cancel</button>
<button type="submit" class="btn btn-primary">Submit</button>
</div>
</div>
</div>


