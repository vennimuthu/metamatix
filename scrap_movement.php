<?php function main(){
	$db_query=new db_query();  
	global $db_helper_obj;
$db_helper_obj=new db_helper();

$steel_history=$db_helper_obj->steel_history('history');
 
foreach($steel_history as $va=>$key){	
	//if($key["po_number"]!=0){
		//$scrap[$key["steel_number"]]["rm_weight"]+=$key["rm_weight"];
		$scrap[$key["steel_number"]]["created_date"]=$key["created_date"];
		$scrap[$key["steel_number"]]["rm_wastage"]+=$key["rm_wastage"];
		//$scrap[$key["steel_number"]]["total_weight"]+=$key["total_weight"];
		$scrap[$key["steel_number"]]["qty_approved"]+=$key["qty_approved"];
		///$scrap[$key["po_number"]][$key["steel_number"]][$key["sub_product_id"]]["id"]=$key["sub_product_id"];
	//}
}

?>
<form id="receipt_list" name="receipt_list" method="post" action="scrap_generation.php"  target="_blank">
	<div class="container-fluid">    	
    	<div class="row">
        	<div class="col-sm-12">
			 
            	<!--<h4 class="inline-heading">Scrap Movement</h4>-->
				<table id="contractor_tb" class="table listhead table-bordered table-hover" align="center" cellspacing="0" cellpadding="0">
	<tr>
	<td align="center" ><label>From Date</label></td>
	<td align="center"><input type="date" class="form-control" name="from_date" id="from_date" value="<?php echo date("Y-m-d"); ?>"></td>
	<td align="center"><label>To Date</label></td>
	<td align="center"><input type="date" class="form-control"  name="to_date" id="to_date" value="<?php echo date("Y-m-d"); ?>"></td>
	<td align="center"><input type="button" onclick="fillter_date()" name="report_ids" id="report_ids" class="btn btn-primary" value="Filter"></td>
	</tr>
	</table>
				<input  onclick="generate()"  type="button" name="report_ids" id="report_ids" class="btn btn-primary pull-right" value="Generate"><br><br>
				<div class="listwrapper" id="fillter_id">
                	<div class="table-responsive" id="table_append"> 
                            <table class="table listhead table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>S No</th>                                         
										<th>STN No</th>
										<th>Date</th>
										<th>Qty</th>										 
										<th>RM Wastage (G)</th>
										<th>Total Wastage (KG)</th>
										<th>Report</th>
									</tr>
                                </thead>
                                <tbody>
	<?php $count=1; $num=0;
		if(!empty($scrap)){
		foreach($scrap as $va1=>$key1){ 
	?>
	<tr id="product_tr_<?php echo $num; ?>" >	
	<td align="center" ><?php echo $count; ?></td>
	<td align="center"> 
	<?php echo $va1; ?></td>
	<td align="center"> 
	<?php echo date("d-m-Y",$key1["created_date"]); ?></td>
	<td align="center"><?php  echo $key1["qty_approved"]; ?></td>
	<td align="center"><?php  echo $key1["rm_wastage"]; ?></td>	  
	<td><?php $total_scrap+=number_format($key1["qty_approved"]*$key1["rm_wastage"],3); echo number_format($key1["qty_approved"]*$key1["rm_wastage"],3); ?></td>	 
	<td>
	<input type="checkbox" name="approve[]" price="<?php echo number_format($key1["qty_approved"]*$key1["rm_wastage"],3); ?>" id="approve_<?php echo $num; ?>" value="<?php echo $va1; ?>" >
	</td>	 
	</tr>
	<?php $count++; $num++;  }}  ?>
	<tr>
	<td align="right" colspan="6"><b>Total Scrap</b></td>
	<td><b><?php echo $total_scrap; ?></b>
	<input type="hidden" name="total_scrap" id="total_scrap" value="<?php echo $total_scrap; ?>" />
	</td>
	</tr>
<div aria-hidden="true" aria-labelledby="myModal" role="dialog" tabindex="-1" id="myModal" class="purchase modal fade">
<div class="modal-dialog">
  <div class="modal-content">
	  <div class="modal-header">
		  <button type="button" class="close"  onclick="cancel_data()"  data-dismiss="modal" aria-hidden="true">&times;</button>
		  <h3 class="modal-title text-center">Scrap Movement</h3>		  
	  </div>
	  <div class="modal-body">
<iframe id="iframe_show_vehicle_data" frameborder="0" marginwidth="0" marginheight="0"  scrolling="yes"  style="width:100%;border:0px;">
</iframe>
	  </div>                          
  </div>
</div>
</div>
	<input type="hidden" name="wavement" id="wavement" value="" />
	<input type="hidden" name="vehicle" id="vehicle" value="" />
	<input type="hidden" name="creation" id="creation" value="" />
	<input type="hidden" name="empty" id="empty" value="" />
	<input type="hidden" name="load" id="load" value="" />
	<input type="hidden" name="price" id="price" value="" />
	</tbody>
                            </table>
                        </div>  <!------End--- Table Responsive -->
						
                </div>
				
            </div>
        </div>
    </div>
	
</form>

<?php } include("template.php"); ?>  
<script type="text/javascript">
 
function show_order_data(selected_wastage){
	var total_wastage=document.getElementById("total_scrap").value;	
	$.ajax({url: "scrap_popup.php?selected_wastage="+selected_wastage.toFixed(3)+"&total_wastage="+total_wastage+"",contentType:false, processData: false, success: function(result){
		var ser = document.getElementById('iframe_show_vehicle_data');
		ser.contentDocument.write(result);
	}
	});
}

function cancel_data(){
var iframe = document.getElementById("iframe_show_vehicle_data");
var html = "";
iframe.contentWindow.document.open();
iframe.contentWindow.document.write(html);
iframe.contentWindow.document.close();
}
	function calculate_price(){
	  var obj=document.getElementsByName("processed[]");
	  var len=obj.length;
	  var total=0;
	  for(var i=0;i<len;i++){  
		  if(document.getElementById(obj[i].id).checked==true){
	 
		  total+=Number(document.getElementById(obj[i].id).getAttribute('price'));
		  }
	  }
	  document.getElementById("payable_id").innerHTML=total;  
	  document.getElementById("balance_id").innerHTML=Number(document.getElementById("totalpayable_id").innerHTML)-Number(total);
	 document.getElementById("pay").value=total; 
	 document.getElementById("balances").value=document.getElementById("balance_id").innerHTML;
	  }

function generate(){
  var obj=document.getElementsByName("approve[]");
  var checked=0;
   var selected_wastage=0;
  for(var i=0;i<obj.length;i++){
	if(document.getElementById("approve_"+i).checked==true){
  	checked++;
	selected_wastage+=Number(document.getElementById("approve_"+i).getAttribute('price'));
	}
  }
  if(checked==0){
	  alert("Please Select atleast one row");
  }else{
	  $('#myModal').modal('show'); 
	  show_order_data(selected_wastage);
  }
}
function fillter_date()
{
	var form = document.getElementById("receipt_list");
	 $.ajax({url: "scrap_fillter.php", type: 'POST',data: new FormData(form),contentType:false, processData: false, success: function(result){
	$("#fillter_id").html(result);
	//alert(result);
	}
}); 
}
</script>

