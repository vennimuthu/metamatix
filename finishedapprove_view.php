<?php
include("config/config.php");

if(!isset($_SESSION["user_id"])){ // checking whether the usertype is logged in
	header('Location: logout.php');        						//REdirection to logout.php
	exit;
}

include("core/class/db_query.php");                             // Class where query generetion is written
include("core/class/db_helper.php");                            // Class where table and feilds 
include("core/function/common.php");   	
                                                                // are mentioned to generate query
ob_start();                                                     // to clear the internal output
global $db_helper_obj;
global $global_website_url;
$db_helper_obj=new db_helper();  
//echo"<pre>";print_r($_GET["process_id"]);echo"</pre>";
$finished_view=$db_helper_obj->finished_view($_GET["process_id"]);
//echo"<pre>";print_r($buffing_view);echo"</pre>";

foreach($finished_view as $va=>$key){
	$receipt=convert_array($key["products"]);
	$receipt_apprve=convert_array($key["products_approve"]);
	//echo"<pre>";print_r($receipt);echo"</pre>";
}


?>
<html>
<head>
<link rel="icon" type="images/png" href="">
  <link rel='stylesheet' href='css/font-awesome.css'>
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
   <link rel="stylesheet" href="css/responsive-menu.css">
  <link rel="stylesheet" href="css/style.css">
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap.js"></script>
  <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
  <link href="css/select2.min.css" rel="stylesheet" />
  <script src="js/select2.min.js"></script>
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" />
  <script src="js/modernizr-custom.js"></script>
  <script src="js/responsive-menu.js"></script>
</head>
<div class="listwrapper" id="height_id">

<div class="table-responsive"> 
<table class="table listhead table-bordered table-hover" align="center" width="50%" border="1" cellspacing="0" cellpadding="0">
<tr>
<td><label>Mov NO : </label></td>
<td colspan="2">
<label><?php 
echo $finished_view[0]["mov_no"];
 ?></label>
</td>
<td></td>
<td><label>Mov Date</label></td>
<td  colspan="4">
<label><?php 
	echo date("d-m-Y",$finished_view[0]["mov_date"]);
 ?></label>
 </td>
</tr>


<tr>
<td align="center"><label>S No</label></td>
<td align="center"><label>Product Name</label></td>
<td align="center"><label>Series</label></td>
<td align="center"><label>Stock in hand</label></td>
<td align="center"><label>Qty Produced</label></td>
<td align="center"><label>Qty Approved</label></td>
<td align="center"><label>Qty Rejected</label></td>
<td align="center"><label>Remarks</label></td>
</tr>
<?php 
$count=1;
foreach($receipt as $va=>$key){
?>
<tr >
<td align="center"><?php echo $count; ?></td>
<td style="padding: 4px;">
<label><?php $prod=$db_helper_obj->product_edit($key["product_id"]);
 echo $prod[0]["product_name"]; ?></label>
</td>
<td  align="center"style="padding: 4px;">
<label><?php echo $prod[0]["series"]; ?></label>
</td>
<td  align="center"style="padding: 4px;">
<label><?php echo $key["stock_hand"]; ?></label>
</td>
<td  align="center"style="padding: 4px;">
<label><?php echo $key["produced_qty"]; ?></label>
</td>
<td  align="center"style="padding: 4px;">
<label><?php echo $receipt_apprve[$key["product_id"]]["approve_qty"]; ?></label>
</td>
<td  align="center"style="padding: 4px;">
<label><?php echo $key["produced_qty"]-$receipt_apprve[$key["product_id"]]["approve_qty"]; ?></label>
</td>
<td  align="center"style="padding: 4px;">
<label><?php echo $key["remarks"]; ?></label>
</td>
</tr>
	<?php $tl_qty+=$key["produced_qty"];  $count++; }?>
<!--<tr><td align="center"><?php echo $count; ?></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
<tr>-->
<td rowspan="2" colspan="2">
<label>Pass By:</label><label><?php echo $finished_view[0]["pass_by"]; ?></label>
</td>
<td rowspan="2" >
<label>Ins By:</label><label><?php echo $finished_view[0]["ins_by"]; ?></label>
</td>
<td rowspan="2">
<label>Approved By:</label><label><?php echo $finished_view[0]["approve_by"]; ?></label>
</td>
<td align="center"><label>Total Qty:-</label></td>
<td align="right" colspan="3"> <label><?php echo $tl_qty; ?></label></td>
</tr>
</table>
</div>
</div>

<script type="text/javascript">
	setTimeout(function(){ parent.$("#iframe_show_vehicle_data").attr("height",$("#height_id").height()+30+"px"); }, 100);
</script>