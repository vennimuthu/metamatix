<?php
$css = "
table tr td th{
	width:auto;
	height: 40px;

},
table.myFormat tr td th{ font-size: 13px; }
}";

//error_reporting(0);
// Include the main TCPDF library (search for installation path).
//require_once('tpdf/TCPDF-master/tcpdf_import.php');
include("config/config.php");
include("core/class/db_query.php");                             // Class where query generetion is written
include("core/class/db_helper.php");                            // Class where table and feilds 
include("core/function/common.php"); 
require_once('HtmlExcel.php');
$db_helper_obj=new db_helper();

	$report_list=$db_helper_obj->report_list();
	$inward_approve_list=$db_helper_obj->inward_approve_list();
	$buffing_list1=$db_helper_obj->buffing_list();
	
	if($_GET["from"]=='stock_register')
		$product_list=$db_helper_obj->totalproduct_list2($_POST["mate"]);
	else
		$product_list=$db_helper_obj->totalproduct_list($_POST["mate"]);
	//echo"<pre>";print_r($product_list);echo"</pre>";
	$delivery_list=$db_helper_obj->deliverymi_list();
	$price_historylist=$db_helper_obj->price_historylist();
	 
	
	foreach($delivery_list as $va=>$key){
		$material=convert_array($key["products"]);
		foreach($material["product_id"] as $va1=>$key1){
			$material_inqty[$key1]=$material["qty"][$va1];
		}
	}
	
	foreach($buffing_list1 as $va=>$key){
		if($key["admin_approved"]==1){
			$buffing_list[]=$key;	
		}
	}
	foreach($buffing_list as $va1=>$key1){
		$products=convert_array($key1["products_approve"]);
		foreach($products as $va2=>$key2){
			$approve_arry[$key2["product_id"]]["qty"]+=$key2["approve"];	
		}
	}
    $order_list=$db_helper_obj->order_qty();
	
	foreach($order_list as $va=>$key){
		$order_list1[$key["product_id"]]["qty"]+=$key["qty"];
		$order_ids[]=$key["product_id"];
		$order_date=$db_helper_obj->order_dates($key["order_id"]);
	}
	 
	$inward_view=$db_helper_obj->inward_approve_admin();
	 
	if(empty($buffing_list)){
	foreach($inward_view as $va=>$key){
		$receipt[$key["id"]]=convert_array($key["products"]);		 
		foreach($receipt[$key["id"]] as $va1=>$key1){
			$price_history=$db_helper_obj->price_history($key1["product_id"]);
			$qty[$key1["product_id"]]+=$key1["qty_approve"];
			$price[$key1["product_id"]]+=$key1["qty_approve"]*$key1["product_price"]["price"];
		}
	}
	}
	
	foreach($price as $va1=>$key1){
		$actual_price[$va1]=$key1/$qty[$va1];
	}
	 
	$final_arry=array();
	foreach($product_list as $va=>$key){
	
		$products=$db_helper_obj->openstock_edit1($key["id"]);		
		$totalproduct=$db_helper_obj->total_product_edit($key["id"]);
		if(!empty($order_list1)){
			$final_arry[$key["id"]]["name"]=$key["name"];
			if($products[0]["processed_qty"]>$approve_arry[$key["id"]]["qty"]){
			
			if($totalproduct[0]["process"]==1)
				$final_arry[$key["id"]]["qty"]=$products[0]["processed_qty"];
			else
				$final_arry[$key["id"]]["qty"]=$products[0]["processed_qty"]-$products[0]["finished_qty"];
			$final_arry[$key["id"]]["buff"]=($products[0]["buffed_qty"]+$approve_arry[$key["id"]]["qty"])-($products[0]["finished_qty"]);
			 
			$final_arry[$key["id"]]["notbuff"]=$products[0]["processed_qty"];
			}else{
			$final_arry[$key["id"]]["qty"]=$approve_arry[$key["id"]]["qty"];	
		    
			$final_arry[$key["id"]]["buff"]=($products[0]["buffed_qty"]+$approve_arry[$key["id"]]["qty"]);
			$final_arry[$key["id"]]["notbuff"]=$products[0]["processed_qty"];
			}
		
		}
		else{
		
		$final_arry[$key["id"]]["name"]=$key["name"];
		$final_arry[$key["id"]]["qty"]=$products[0]["processed_qty"]-$approve_arry[$key["id"]]["qty"];
	 
		if(!empty($approve_arry))		
			$final_arry[$key["id"]]["buff"]=($products[0]["buffed_qty"]+$approve_arry[$key["id"]]["qty"]);	 
		else
			$final_arry[$key["id"]]["buff"]=$products[0]["buffed_qty"];
		
			$final_arry[$key["id"]]["notbuff"]=$products[0]["processed_qty"];
		}
		$final_arry[$key["id"]]["total_qty"]=($final_arry[$key["id"]]["buff"]+$final_arry[$key["id"]]["notbuff"]);
		$final_arry[$key["id"]]["finished"]=$products[0]["finished_qty"];
		$final_arry[$key["id"]]["price"]=$key["price"];
		$final_arry[$key["id"]]["product_price"]=$key["product_price"];
		$final_arry[$key["id"]]["machining_price"]=$key["machining_price"];
		$final_arry[$key["id"]]["update_machinprice"]=$key["update_machinprice"];
		$final_arry[$key["id"]]["updated_price"]=$key["updated_price"];
		$final_arry[$key["id"]]["updated_product_price"]=$key["updated_product_price"];
		$final_arry[$key["id"]]["machining"]=$key["machining"];
		//maching qty
		$final_arry[$key["id"]]["maching_qty"]=$products[0]["maching_qty"];
	}

$buffing_list=$db_helper_obj->buffing_list1();
$final_array=array();
	foreach($buffing_list as $va=>$key){
		$bpl_no[$key["name"]]["name"]=$key["bpl_no"];
		$key["dated"]=date("d-m-Y",$key["buffing_date"]);	
		$approval_list[$key["name"]][$key["dated"]][]=$key;
		foreach($approval_list[$key["name"]][$key["dated"]] as $va1=>$key1){
			$products=convert_array($key1["products"]);	
			$products_approve=convert_array($key1["products_approve"]);
			$final_array1[$key["name"]][$key["dated"]][$key["bpl_no"]]["countss"]=0;
		foreach(convert_array($key1["products"]) as $va2=>$key2){
			$final_array[$key["name"]][$key["dated"]][$key["bpl_no"]][$key2["product_id"]]["emp_type"]=$key["emp_type"];
			$final_array[$key["name"]][$key["dated"]][$key["bpl_no"]][$key2["product_id"]]["bpl_no"]=$key["bpl_no"];
			$final_array[$key["name"]][$key["dated"]][$key["bpl_no"]][$key2["product_id"]]["product_id"]=$key2["product_id"];
			$final_array[$key["name"]][$key["dated"]][$key["bpl_no"]][$key2["product_id"]]["stocks"]=$key2["stocks"];
			$final_array[$key["name"]][$key["dated"]][$key["bpl_no"]][$key2["product_id"]]["buffed_qty"]=$key2["buffed_qty"];
			$final_array[$key["name"]][$key["dated"]][$key["bpl_no"]][$key2["product_id"]]["balance"]=$key2["stocks"]-$key2["buffed_qty"];
			$final_array[$key["name"]][$key["dated"]][$key["bpl_no"]][$key2["product_id"]]["price"]=$key2["price"];
			$final_array1[$key["name"]][$key["dated"]][$key["bpl_no"]]["countss"]++;
		}
		foreach($products_approve as $va3=>$key3){
			$final_array[$key["name"]][$key["dated"]][$key["bpl_no"]][$key3["product_id"]]["approve"]=$key3["approve"];
			$final_array[$key["name"]][$key["dated"]][$key["bpl_no"]][$key3["product_id"]]["reject"]=$key3["reject"];
			$final_array[$key["name"]][$key["dated"]][$key["bpl_no"]][$key3["product_id"]]["amount"]=$final_array[$key["name"]][$key["dated"]][$key["bpl_no"]][$key3["product_id"]]["price"]*$key3["approve"];
		}	
		}
	}
	$couns=0;
foreach($inward_approve_list as $va=>$key){
	$products_appr=convert_array($key["products"]); 
}
foreach($products_appr as $va=>$key){
	$product_idsappr[]=$key["product_id"];
}
$return_list=$db_helper_obj->return_list();	
	foreach($return_list as $va=>$key){
		$product_ids[]=$key["product_id"];
	}
	foreach($return_list as $va2=>$key2){
		
		if(!in_array($key["product_id"],$product_idsappr)){
		if($final_arry[$key2["product_id"]]["notbuff"]==0){
			$final_arry[$key2["product_id"]]["notbuff"]=0;
		}
		else{
			$final_arry[$key2["product_id"]]["notbuff"]+=$key2["qty"];
		}
}
	
	}
	foreach($product_list as $va4=>$key4){
		$final_arry1[$key4["id"]]["id"]=$key4["id"];	
		$inward_approve_list=$db_helper_obj->inward_approve_list($key4["id"]);
		$inwardaddproveproduct=convert_array($inward_approve_list[$key4["products"]]);
		
		foreach($inward_approve_list as $va5=>$key5){		 
			$final_arry2[$key5["id"]]["products"]=convert_array($key5["products"]);
		}
	}

ob_start();
//if($_POST["mate"]==1)
include "stock_exelhtml.php";
$myvar = ob_get_clean();
$xls = new HtmlExcel();
$xls->setCss($css);
$xls->addSheet("Stock Consolidation Report",$myvar);
$xls->headers("Stockeport_".date('d-M-y').".xls");

echo $xls->buildFile();
?>