<?php
include("config/config.php");
include("core/class/db_query.php");                             // Class where query generetion is written
include("core/class/db_helper.php");                            // Class where table and feilds 
include("core/function/common.php");   	
                                                                // are mentioned to generate query
ob_start();                                                     // to clear the internal output
global $db_helper_obj;
global $global_website_url;
$db_helper_obj=new db_helper();
$product_list=$db_helper_obj->product_list();
$finished_list=$db_helper_obj->finished_list();
$fgreport=$db_helper_obj->report_filter();

foreach($fgreport as $va=>$key){
$products=convert_array($key["products"]);
foreach($products["list"] as $va1=>$key1){
$billing_qty[$va1]+=$key1["billing_qty"];
}
}
foreach($product_list as $va=>$key){	
	foreach($finished_list as $va1=>$key1){
		if($key1["status"]==3){
		$products_approve=convert_array($key1["products_approve"]);
		
		$product_list[$va]["open_qty"]+=$products_approve[$key["id"]]["approve_qty"];
		
		}
	}
	$product_list[$va]["open_qty"]=$product_list[$va]["open_qty"]-$billing_qty[$key["id"]];
	
	
}

?>
	<div class="table-responsive" id="table_append"> 
	<table class="table listhead table-bordered table-hover" align="center" cellspacing="0" cellpadding="0">
	<tr>
	<td colspan="8" align="center" valign="middle"><label>Finished Goods Report</label></td>
	</tr>
	<tr>
	<td colspan="8">
	<table id="contractor_tb" style="width: 100%;" class="table listhead table-bordered table-hover" align="center" cellspacing="0" cellpadding="0">
	<thead>
	<th align="center">SL No</th>
	<th align="center">PART NAME</th>
	<th align="center">SERIES</th>
	<th align="center">QTY IN STOCK</th>
	<th align="center">QTY TO BILLING</th>
	<th align="center">RATE</th>
	<th align="center">AMOUNT</th>	
	<th align="center">Report</th>
	</thead>
	<?php $count=1; $num=0;
	if(!empty($product_list)){
	foreach($product_list as $va1=>$key1){
	if($key1["open_qty"]!=0){	
	?>
	<tr id="product_tr_<?php echo $num; ?>" >	
	<td align="center" ><?php echo $count; ?></td>
	<td style="padding: 4px;" align="center"><label>
	<?php  echo $key1["product_name"]; ?></label></td>
	<td align="center" style="padding: 4px;"><label><?php echo  $key1["series"]; ?></label></td>
	<td align="center" style="padding: 4px;"><label><?php echo $key1["open_qty"]; ?></label></td>
	<td><input type="number" min="1" onblur="calculate_price(this,<?php echo $key1["id"]; ?>)" oninput="validity.valid||find_max('<?php echo $key1["open_qty"]; ?>',this);" max="<?php echo $key1["open_qty"]; ?>" name="billing_qty[]" id="billing_qty_<?php echo $count; ?>" class="form-control tbox_small"></td>
	<td><label id="price_<?php echo $key1["id"]; ?>"><?php echo $key1["price"]; ?></label></td>
	<td><label id="amount_<?php echo $key1["id"]; ?>"></label></td>
	<td align="right"><input type="checkbox" name="processed[]" onclick="find_pay(<?php echo $key1["id"]; ?>)" id="processed_<?php echo $key1["id"]; ?>" value="<?php echo $key1["id"]; ?>" ></td>
	</tr>
	<?php  $count++; 
	 $total_price+=$key1["open_qty"]*$key1["price"];
	} }  } else{  ?>
	 <tr>
         <td align="center" colspan="8">There are no Reports found</td>
	 </tr>
	<?php } ?>	
	</table>
	</td>
	</tr>
	</table>
	<table id="contractor_tb" style="width: 100%;" class="table listhead table-bordered table-hover" align="center" cellspacing="0" cellpadding="0">
	<thead>
	<th align="center" >SL No</th>
	<th align="center">TOTAL PAYABLE AMOUNT</th>
	<th align="center">PAYMENT RELEASED</th>
	<th align="center">INVOICE NO</th>
	<th align="center">DATE</th>
	<th align="center">BALANCE PAYABLE</th>
	</thead>
     <tr>
	 <td>1</td>
	 <td>
	 <input type="hidden" name="totalpay" id="totalpay" value="<?php echo $total_price; ?>">
	 <label id="totalpayable_id">	 
	 <?php echo $total_price; ?></label></td>
	 <input type="hidden" name="pay" id="pay" value="">
	 <td><label id="payable_id">0</label></td>
	 <td><input type="text" name="inst_no" id="inst_no" value="" class="form-control tbox_small"></td>
	 <td><input type="date" name="inst_date" id="inst_date" value="<?php echo date("Y-m-d"); ?>" class="form-control"></td>
	 <td><input type="hidden" name="balances" id="balances"><label id="balance_id"><?php echo $totalamount+$totalamount1; ?></label></td>
	 </tr> 
	</table>
	</div>