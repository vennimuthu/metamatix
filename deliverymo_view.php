<?php
include("config/config.php");

if(!isset($_SESSION["user_id"])){ // checking whether the usertype is logged in
	header('Location: logout.php');        						//REdirection to logout.php
	exit;
}

include("core/class/db_query.php");                             // Class where query generetion is written
include("core/class/db_helper.php");                            // Class where table and feilds 
include("core/function/common.php");
                                                               // are mentioned to generate query
ob_start();                                          			// to clear the internal output
global $db_helper_obj;
global $global_website_url;
$db_helper_obj=new db_helper();  

$deliverymo_edit=$db_helper_obj->deliverymo_edit($_GET["process_id"]);

?>
<html>
<head>
<link rel="icon" type="images/png" href="">
  <link rel='stylesheet' href='css/font-awesome.css'>
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
   <link rel="stylesheet" href="css/responsive-menu.css">
  <link rel="stylesheet" href="css/style.css">
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap.js"></script>
  <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
  <link href="css/select2.min.css" rel="stylesheet" />
  <script src="js/select2.min.js"></script>
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" />
  <script src="js/modernizr-custom.js"></script>
  <script src="js/responsive-menu.js"></script>
</head>
<div class="listwrapper" id="height_id">

<div class="table-responsive"> 
<table class="table listhead table-bordered table-hover" align="center" width="50%" border="1" cellspacing="0" cellpadding="0">
<tr>	
<td ><label>Date:</label></td>
<td  >
<label><?php 
 echo date('d-m-Y',strtotime($deliverymo_edit[0]["dated"]));
 ?></label>
</td>
<td><label>Challan NO:</label></td>
<td colspan="2">
<label><?php echo $deliverymo_edit[0]["challan_no"]; ?></label>
 </td>
</tr>
<tr>
</td>
<td><label>Supplier Name:</label></td>
<td colspan="4">
<label><?php 
$prod=$db_helper_obj->supplier_edit($deliverymo_edit[0]["supplier_id"]);
echo $prod[0]["supplier_name"]; ?></label>
</td>
</tr>

<tr>
<td><label>Nature of Operation</label></td>
<td  >
<label><?php 
$nature=$db_helper_obj->nature_edit($deliverymo_edit[0]["nature"]);
echo $nature[0]["nature"]; ?><?php 
 ?></label>
</td>
<td><label>Duration of Process:</label></td>
<td colspan="2">
<label><?php echo $deliverymo_edit[0]["duration"]; ?></label>
 </td></tr>
 <tr>
<td><label>Vehicle No</label></td>
<td>
<label><?php echo $deliverymo_edit[0]["vehicle_no"]; ?></label>
</td>
<td ><label>Delivery through:</label></td>
<td colspan="2"><label><?php echo $deliverymo_edit[0]["delivery_through"]; ?></label>
 </td>
</tr>
<td align="center"><label>SL No</label></td>
<td align="center"><label>Item Name</label></td>
<td align="center"><label>Quantity</label></td>
<td align="center" style="width:90px;"><label>Rate</label></td>
<td align="center"><label>Amount</label></td>
</tr>
<?php $count=1;
if(!empty($deliverymo_edit)) {
 foreach($deliverymo_edit as $va=>$key){
	 ?>
<tr>
<td align="center"><?php echo $count; ?></td>
<td  align="center"	style="padding: 4px;">
<label><?php $prod2=$db_helper_obj->total_product_edit($key["product_id"]);
 echo $prod2[0]["name"]; ?></label>
</td>
<td  align="center"	style="padding: 4px;">
<label><?php echo $key["qty"]; ?></label>
</td>
<td  align="center"style="padding: 4px;">
<label><?php $prod2=$db_helper_obj->total_product_edit($key["product_id"]);
 echo $prod2[0]["machining_price"]; ?></label>
 </td>
<td  align="center"style="padding: 4px;">
<label><?php echo $key["qty"]*$prod2[0]["machining_price"];  ?></label>
</td>
</tr>
	<?php $count++; } }?>
<?php //echo"<pre>total_product_edit"; print_r($prod2); echo"</pre>"; ?>
</table>
</div>
</div>
<script type="text/javascript">
	setTimeout(function(){ parent.$("#iframe_show_vehicle_data").attr("height",$("#height_id").height()+30+"px"); }, 100);
</script>
