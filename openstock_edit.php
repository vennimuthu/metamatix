<?php function main(){
	$db_query=new db_query();  
	global $db_helper_obj;
	$db_helper_obj=new db_helper();
	$supplier_list=$db_helper_obj->supplier_list();
	$material_list=$db_helper_obj->material_category();	
	$product_list=$db_helper_obj->total_product_list();
	 
	if(!empty($_POST)){
		$inserted=$db_helper_obj->openstock_update($_GET["product_id"]);
		header('Location:openstock_list.php'); 
		//REdirection to master_list.php
	}else{
		unset($_SESSION["cost"]);
	}
	$master=$db_helper_obj->openstock_edit($_GET["product_id"]);
	$subproduct=$db_helper_obj->total_product_edit($_GET["product_id"]); 
	?>
<script type="text/javascript">

function add_order_row(order_row_id)
{	
	var order_item_index=$("#order_item_row_index_"+order_row_id).val();
	order_item_index++;
	var html_content;
	var options1=document.getElementById('product_select').innerHTML;
	
	var slect1='<select id="name_'+order_item_index+'" name="name['+order_item_index+']" class="form-control" >'+options1+'</select>';
	html_content='<tr id="order_item_row_'+order_row_id+"_"+order_item_index+'" name="order_item_row_'+order_row_id+"_"+order_item_index+'">'
+'<td>'+slect1+'</td>'
+'<td><input type="number" class="form-control" name="qty['+order_item_index+']" min="0" id="qty_'+order_item_index+'" value="" /></td>'
+'<td><input type="number" class="form-control" name="buffed_qty['+order_item_index+']" min="0" id="buffed_qty_'+order_item_index+'" value="" /></td>'
+'<td><input type="button" class="btn btn-danger" value="X" onclick="delete_order_item_row(this);"></td></tr>';
	
	div = document.getElementById('order_item_body_'+order_row_id);	
	div.insertAdjacentHTML( 'beforeend', html_content);
	$("#order_item_row_index_"+order_row_id).val(order_item_index);
}

function delete_order_item_row(element)  
{
    var row = element.parentNode.parentNode;
    row.parentNode.removeChild(row);
}
function showmaterial(obj){
	if(obj.value==3)
		document.getElementById("material_id").style.display="none";
	else{
		document.getElementById("material_id").style.display="";
	}

}
function cost_add(obj){
var word=obj.id;
var number=word.replace('process_','');	
if(obj.checked==true)
	var check=1;
else if(obj.checked==false)	
	var check=0;
if(obj.checked==true&&document.getElementById('name_'+number).value!=''){
$.ajax({url: "master_costadd.php?sub_id="+number+"&check="+check+"",contentType:false, processData: false, success: function(result){
	var ser = document.getElementById('iframe_show_vehicle_data');
	ser.contentDocument.write(result);
	}
	});	
}else{ 
	$.ajax({url: "master_costadd.php?sub_id="+number+"&check="+check+"",contentType:false, processData: false, success: function(result){
	var ser = document.getElementById('iframe_show_vehicle_data');
	ser.contentDocument.write(result);
	}
	});	
}
}
function cancel_data(){
	var iframe = document.getElementById("iframe_show_vehicle_data");
	var html = "";
	iframe.contentWindow.document.open();
	iframe.contentWindow.document.write(html);
	iframe.contentWindow.document.close();
}

</script>

<style>
	.cost_form { }
	.cost_form input.form-control { margin-bottom:10px; }
	.tbox_vsmall {
     width:21%;
	}
</style>

<form class="form-horizontal" id="recruitment" name="recruitment" method="post">

	
  <div class="form-group">
	
 
	<div class="container-fluid">
    	
    	<div class="row">
        	<div class="col-sm-12">
            	
				<form name="master_add" id="master_add" method="post" target="iframe">
            	<div class="listwrapper">
			<div class="table-responsive"> 
                    <table id="order_item_table_1" class="table listhead table-bordered table-hover">
                    <thead>
						<tr>
							<th>Sub&nbsp;Component&nbsp;Name </th>
							<?php if($subproduct[0]["material"]!=5){ ?>							
							<th>Rm Weight</th>
							<th>Rm Wastage</th>							
							<?php } ?>
							<th><?php if($subproduct[0]["material"]==1||$subproduct[0]["material"]==5) echo "Non&nbsp;Buffed&nbsp;Qty"; else echo "General&nbsp;Qty";   ?></th>
							<th>Non&nbsp;Buffed&nbsp;Rate <?php if($subproduct[0]["material"]==5){?>
							/ KG
							<?php } ?></th>
							<?php if($subproduct[0]["material"]!=5){ ?>
							<th>Maching Qty</th>
							<th>Maching Rate</th>
							<th>Buffed Qty</th>	
							<th>Buffed Rate</th>
							<?php } ?>
						</tr>
                    </thead>
                    <tbody  id="order_item_body_1"  name="order_item_row_1_1">
								
					<tr id="order_item_row_1_1" name="order_item_row_1_1">
					<div id="product_select" style="display:none;">				
					<option value="">Select	</option>
					<?php foreach($product_list as $va=>$key){
					if($key["id"]==$master[0]["subproduct_id"]){
						$master[0]["non_buffed_price"]=$key["product_price"];
						$master[0]["maching_price"]=$key["machining_price"];
						$master[0]["buffed_price"]=$key["price"];
					}
					?>
					<option  value="<?php echo $key["id"]; ?>"><?php echo $key["name"]; ?></option>
					<?php } ?>
					</div>
					
					<td>
					<select disabled="disabled" class="form-control" name="name[1]" required>
							<option value="">Select	</option>
							<?php foreach($product_list as $va1=>$key1){ ?>
							<option <?php if($master[0]["subproduct_id"]==$key1["id"]){ ?> selected <?php } ?> value="<?php echo $key1["id"]; ?>"><?php echo $key1["name"]; ?></option>
							<?php } ?>
						</select>
					</td>
					<input type="hidden" name="name[1]" value="<?php echo $master[0]["subproduct_id"]; ?>">
					<?php if($subproduct[0]["material"]!=5){ ?>	
					<td>			 
						<input type="number" step="0.001" min=0 class="form-control" name="rm_weight[1]" id="rm_weight_1" value="<?php echo $master[0]["rm_weight"]; ?>"  />
					</td>
					
					<td>			 
						<input type="number" step="0.001" class="form-control" name="rm_wastage[1]" id="rm_wastage_1" value="<?php echo $master[0]["rm_wastage"]; ?>"  />
					</td>
					<!--<td>			 
					<input type="number" min=0 class="form-control" name="general_qty[1]" id="general_qty_1" value="<?php echo $master[0]["general_qty"]; ?>"  />
					</td>-->
					<?php } ?>
					
					
					<td>			 
					<input type="number" min=0 class="form-control" name="qty[1]" id="qty_1" value="<?php echo $master[0]["qty"]; ?>"  />
					</td>
					
					<td>			 
					<input type="number" min=0 class="form-control" name="nonbuffed_price[1]" step="0.01" id="non_buffed_price_1" value="<?php echo $master[0]["non_buffed_price"]; ?>"  />
					</td>
				<?php if($subproduct[0]["material"]!=5){ ?>
					<td>			 
					<input type="number" min=0 class="form-control" name="maching_qty[1]" id="maching_qty_1" value="<?php echo $master[0]["maching_qty"]; ?>"  />
					</td>
					
					<td>			 
					<input type="number" min=0 class="form-control" name="maching_price[1]" step="0.01" id="maching_price_1" value="<?php echo $master[0]["maching_price"]; ?>"  />
					</td>
					
					<td>
					<input type="number" min=0 class="form-control" name="buffed_qty[1]" id="buffed_qty_1" value="<?php echo $master[0]["buffed_qty"]; ?>"  />
					</td>

					<td>			 
					<input type="number" min=0 class="form-control" name="buffed_price[1]" step="0.01" id="buffed_1" value="<?php echo $master[0]["buffed_price"]; ?>" />
					</td>
					<?php } ?>					
					</tr>
				   </tbody>
				</table>
							
        <input type="hidden" value="1" id="order_item_row_index_1" name="order_item_row_index_1">
    </div>
	
	 </div>  <!------End--- Table Responsive -->
	  
	 </div>
	 
	 <div class="col-md-12">
			 <button type="button" onclick="window.location.href='openstock_list.php'" class="btn btn-primary">Cancel</button>
			 <button type="submit" class="btn btn-primary">Submit</button>
		</div>
				</form>
            </div>
        </div>

    </div>
	<div aria-hidden="true"  aria-labelledby="myModal" role="dialog" tabindex="-1" id="myModal" class="purchase modal fade">
                  <div class="modal-dialog">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" onclick="cancel_data()" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h3 class="modal-title text-center">Master View</h3>
                              
                          </div>
                          <div class="modal-body">
                          <iframe id="iframe_show_vehicle_data" frameborder="0" marginwidth="0" marginheight="0"  scrolling="no"  style="width:100%;border:0px;">
				</iframe>
						
                          <div class="modal-footer">
                              
                          </div>
                          
                          </div>
                          
                          
                          
                      </div>
                </div>
    </div>
   



</form>
<?php } include("template.php"); ?>  