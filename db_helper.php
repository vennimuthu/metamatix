<?php
class db_helper extends db_query{
	function login($arr){
		return $this->select_query("users","id,name,password,user_type","name=? and password=?",$arr);
	}
	
	function product_insert(){
		extract($_POST);
		if($material!=3){
		$insr_array["product_name"]=$title;
		$insr_array["series"]=$series;
		$insr_array["price"]=$product_price;
		$insr_array["open_qty"]=$open_qty;
		}
		$insr_array["material"]=$material;
		$insr_array["creation_time"]=time();
		$insert_id=$this->insert_query("products",$insr_array);
		foreach($name as $va=>$key){
			$insr_array1["name"]=$key;
			$insr_array1["qty"]=$qty[$va];
			$insr_array1["processed_qty"]=$qty[$va];
			$insr_array1["price"]=$price[$va];
			$insr_array1["product_id"]=$insert_id;			
			if(isset($process[$va])&&$process[$va]==1)
				$insr_array1["process"]=1;
			else
				$insr_array1["process"]=0;
			//$insr_array1["piece_rate"]=base64_encode(json_encode($_SESSION["cost"][$va]));
			$insert=$this->insert_query("sub_products",$insr_array1);
		}
		return "success";
	}
	
	function report_generation(){
		extract($_POST);
		
		$insr_array["bpl_no"]=base64_encode(json_encode($_POST["processed"]));
		
		foreach($_POST["processed"] as $va=>$key){
		$insr_array7["report"]=1;
		$where="bpl_no=:inwardid";
		$where_arr= array('inwardid'=>$key);
		$update1=$this->update_query("buffing",$insr_array7,$where,$where_arr);	
		}
		
		
		$report=$this->report_list();
		if(count($report)>0)
		$report_no="MMSPL-REP-".sprintf("%03s",count($report)+1);
		else
		$report_no="MMSPL-REP-001";
		
		$insr_array["report_no"]=$report_no;
		$insr_array["pays"]=base64_encode(json_encode($_POST));
		$insr_array["creation_time"]=time();
		$insr_array["created_by"]=$_SESSION["user_id"];
		$insert_id=$this->insert_query("report",$insr_array);
		
		return "success";
	}
	function report_list(){ 
			return $this->select_query("report","*","id!=''");
	}
	function order_insert(){
		extract($_POST);
		$insr_array["po_number"]=$title;
		$insr_array["creation_time"]=time();
		$insert_id=$this->insert_query("orders",$insr_array);
		foreach($name as $va=>$key){
			$insr_array1["product_name"]=$key;
			$insr_array1["qty"]=$qty[$va];
			$insr_array1["price"]=$price[$va];
			$insr_array1["amount"]=$qty[$va]*$price[$va];
			$insr_array1["order_id"]=$insert_id;
			$insert=$this->insert_query("order_sub",$insr_array1);
		}
		return "success";
	}
	function inward_insert(){
		extract($_POST);
		$insr_array1["dated"]=strtotime($date);
		$insr_array1["inv_no"]=$inv_no;
		$insr_array1["supplier"]=$supplier;
		$insr_array1["inv_date"]=strtotime($inv_date);
		$insr_array1["material"]=$material;
		$final_arry=array();
		foreach($product_id as $va=>$key){
			$final_arry[$va]["product_id"]=$key;
			$final_arry[$va]["specification"]=$specification[$va];	
			$final_arry[$va]["order_qty"]=$order_qty[$va];	
			$final_arry[$va]["qty"]=$qty[$va];	
		}
		$final_arry1=array();
		$final_arry1["total_order"]=$total_order;
		$final_arry1["total_received"]=$total_received;	
		$final_arry1["total_balance"]=$total_balance;	

		
		$insr_array1["products"]=base64_encode(json_encode($final_arry));
		$insr_array1["qty_list"]=base64_encode(json_encode($final_arry1));
		$insr_array1["comments"]=$comments;
		$insr_array1["received_by"]=$received_by;
		$insr_array1["created_by"]=$_SESSION["user_id"];
		$insr_array1["creation_time"]=time();
		$insr_array1["inward_no"]=$inward_no;
		
		$insr_array1["status"]=1;
		$insert8=$this->insert_query("inward",$insr_array1);
		return "success";
	}
	function inwardappr_insert(){
		extract($_POST);
		$insr_array1["dated"]=strtotime($grn_date);
		$insr_array1["inward_no"]=$grn_no;			
		$insr_array1["inv_no"]=$inv_no;
		$insr_array1["supplier"]=$supplier;
		$insr_array1["ins_date"]=strtotime($ins_date);
		$insr_array1["ins_by"]=$ins_by;
		$insr_array1["received_by"]=$recei_by;
		$insr_array1["inv_date"]=strtotime($inv_date);
		$final_arry=array();
					$total_received=0;
					$total_approve=0;
					$total_reject=0;
		foreach($product_id as $va=>$key){
			$final_arry[$va]["product_id"]=$key;
			$final_arry[$va]["qty_received"]=$qty_received[$va];
			$final_arry[$va]["qty_approve"]=$qty_approve[$va];
			$final_arry[$va]["qty_rejected"]=$qty_rejected[$va];
			$final_arry[$va]["ins_remarks"]=$ins_remarks[$va];	
			
			$total_received+=$qty_received[$va];
			$total_approve+=$qty_approve[$va];
			$total_reject+=$qty_rejected[$va];
			
		}
		$final_arry1["total_received"]=$total_received;
		$final_arry1["total_approve"]=$total_approve;
		$final_arry1["total_reject"]=$total_reject;
		$insr_array1["products"]=base64_encode(json_encode($final_arry));
		$insr_array1["qty_list"]=base64_encode(json_encode($final_arry1));
		$insr_array1["comments"]=$comments;
		$insr_array1["created_by"]=$_SESSION["user_id"];
		$insr_array1["creation_time"]=time();
		$insr_array1["inward_id"]=$inward_id;
		
		
		

		$insert1s=$this->insert_query("inward_approve",$insr_array1);
		$insr_array["status"]=2;
		$where="id=:inwardid";
		$where_arr= array('inwardid'=>$inward_id);
		$update=$this->update_query("inward",$insr_array,$where,$where_arr);
		
		
		
		return "success";
	}
	function employee_insert(){ 
		extract($_POST);		
		$insr_array["name"]=$employee_name;
		$insr_array["role"]=$employee_role;
		$insr_array["price"]=$price;
		$insr_array["creation_time"]=time();
		$insert_id=$this->insert_query("employee",$insr_array);
		return "success";
	}
	function inventory_update(){ 
		extract($_POST);		
		$insr_array["admin_approve"]=$approve;
		$insr_array["admin_comment"]=$comments;
		$insr_array["admin_processed"]=$processed;	
		$insr_array["admin_appr_time"]=time();
		$where="id=:sub_product_id";
		$where_arr= array('sub_product_id'=>$_GET["id"]);
		$update=$this->update_query("inward_approve",$insr_array,$where,$where_arr);
		return "success";
	}
	function received_insert(){
		extract($_POST);		
		$insr_array["product_id"]=$product_id;
		$insr_array["qty"]=$qty;
		$insr_array["price"]=$price;
		$insr_array["supplier_id"]=$supplier_id;
		$insr_array["received_date"]=$received_date;
		$insr_array["creation_time"]=time();
		$insert_id=$this->insert_query("received_products",$insr_array);
		return "success";
	}
	function random(){
	$characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $result = '';
    for ($i = 0; $i < 5; $i++){
        $result .= $characters[mt_rand(0, 26)];
	}
	return $result;
	}
	function buffing_insert(){
		
		extract($_POST);
		//echo"<pre>";print_r($_POST);echo"</pre>";
		$insr_array["name"]=$name;
		$insr_array["supervisor_name"]=$supervisor_name;
		
		//$date = date("d-m-Y", strtotime($date));		
		$insr_array["buffing_date"]=strtotime($grn_date);
		$insr_array["bpl_no"]=$bpl_no;
		$insr_array["creation_time"]=time();
				
		$insr_array["emp_type"]=$emp_type;
		
		$inward_appr_list=$this->inward_approve_admin();
		$subproduct_list=$this->subproduct_pinvolve();

		foreach($subproduct_list as $va=>$key){
		if(in_array($key["id"],$product_id)){
		$product_ids[]=$key["id"];
		$open_stock[$key["id"]]=$key["processed_qty"];
		$po_name=$this->product_edit($key["product_id"]);
		$product_name[$key["id"]]=$po_name[0]["product_name"];
		
		$price[$key["id"]]=$key["price"];
		}
		}

		foreach($inward_appr_list as $va=>$key){
			$products=convert_array($key["products"]);		
			$qtys=convert_array($key["qty_list"]);	
			foreach($products as $va1=>$key1){
				if(in_array($key1["product_id"],$product_ids)){
					$total_qty[$key1["product_id"]]["qty_approve"]+=$key1["qty_approve"];
					$total_qty[$key1["product_id"]]["product_id"]=$key1["product_id"];
				}
			}
		}
		foreach($total_qty as $va1=>$key1){
		$total_qty[$va1]["stocks"]=$key1["qty_approve"]+$open_stock[$key1["product_id"]];
		$total_qty[$va1]["product_name"]=$product_name[$key1["product_id"]];
		$total_qty[$va1]["price"]=$price[$key1["product_id"]];
		}
		foreach($subproduct_list as $va=>$key){
if(!isset($total_qty[$key["id"]]["product_id"])){
if($key["qty"]!=0){
$total_qty[$key["id"]]["product_id"]=$key["id"];
$total_qty[$key["id"]]["stocks"]=$key["processed_qty"];
$total_qty[$key["id"]]["product_name"]=$product_name[$key["id"]];
$total_qty[$key["id"]]["price"]=$key["price"];
}	
}	
}

		if($emp_type!=1){
		if(!empty($aprv_over_time))
			$insr_array["aprve_time_before"] =$aprv_over_time;
		if(!empty($approve))
			$insr_array["approved"] =$approve[0];
		$insr_array["comments"]=$comments_regular;	
		}else{
		$insr_array["comments"]=$comments;
		
		}
		 $final_arry=array();
		if($emp_type==1){	
		foreach($product_id as $va=>$key){
		if($qty[$va]!=0)	{
		 $final_arry[$va]["product_id"] =$total_qty[$key]["product_id"];
		 
		 $qtys=$this->sub_pro_name($total_qty[$key]["product_id"]);
		 if($qtys[0]["processed_qty"]>=$qty[$va])			 
		 $updatearr["processed_qty"]=$qtys[0]["processed_qty"]-$qty[$va];
	     else if($qtys[0]["processed_qty"]<$qty[$va])
		  $updatearr["processed_qty"]=0;
			$where2="id=:product_id ";
			$where_arr2= array('product_id'=>$total_qty[$key]["product_id"]);
	
		$insert_ids=$this->update_query("sub_products",$updatearr,$where2,$where_arr2);
	  
	  
		 $final_arry[$va]["stocks"] =$total_qty[$key]["stocks"];
		 $final_arry[$va]["product_name"] =$total_qty[$key]["product_name"];
		 $final_arry[$va]["price"] =$total_qty[$key]["price"];
		 $final_arry[$va]["buffed_qty"] =$qty[$va];
		 $final_arry[$va]["amount"] =$qty[$va]*$total_qty[$key]["price"];
		}
		}
		}
		if($emp_type!=1){
		
		foreach($regular_price as $va=>$key){
		
		 $final_arry[$va]["regular_time"] =$regular_time;
		 $final_arry[$va]["regular_price"] =$key;
		 $final_arry[$va]["regular_amount"] =$regular_time*$key;
		 if(!empty($approve)){
		 $final_arry[$va]["over_time"] ='';
		 $final_arry[$va]["overtime_price"] =$overtime_price[$va];
		 $final_arry[$va]["overtime_amount"] ='';	
		 }
		}	
		
		}

		$insr_array["products"]=base64_encode(json_encode($final_arry));		
		$insr_array["created_by"]=$_SESSION["user_id"];
		$insr_array["status"]=1;
		$insert_id=$this->insert_query("buffing",$insr_array);
		
		return "success";
	}
	
	function supplier_insert(){
		extract($_POST);
		$insr_array["supplier_name"]=$supplier_name;
		$insr_array["vendor_id"]=$vendor_id;
		$insr_array["address"]=$address;
		$insr_array["city"]=$city;
		$insr_array["state"]=$state;	
		$insr_array["gst_no"]=$gst_no;
		$insr_array["pincode"]=$pincode;
		$insr_array["payment_terms"]=$payment_terms;
		$insr_array["creation_time"]=time();
		$insr_array["created_by"]=$_SESSION["user_id"];
		$insert_id=$this->insert_query("suppliers",$insr_array);
		return "success";
	}
	function product_update(){
		extract($_POST);
		if($material!=3){		
		$insr_array["product_name"]=$title;
		$insr_array["series"]=$series;
		$insr_array["price"]=$product_price;
		$insr_array["open_qty"]=$open_qty;
		}
		$insr_array["material"]=$material;
		$insr_array["creation_time"]=time();
		
		$where="id=:product_id ";
	 	$where_arr= array('product_id'=>$product_id);
	
		$insert_id=$this->update_query("products",$insr_array,$where,$where_arr);
		//$total_count=$order_item_row_index_1;
		foreach($sub_product_id as $va=>$key){
			$insr_array1["name"]=$name[$va];
			$insr_array1["qty"]=$qty[$va];
			$insr_array1["processed_qty"]=$qty[$va];			
			$insr_array1["price"]=$price[$va];	
			$insr_array1["product_id"]=$product_id;	
			if(isset($process[$va]))
				$insr_array1["process"]=1;
			else
				$insr_array1["process"]=0;
			if(!empty($name[$va])){ 
				$where="id=:sub_product_id AND  product_id=:product_id";
				$where_arr= array('sub_product_id'=>$key,'product_id'=>$product_id);
				$update=$this->update_query("sub_products",$insr_array1,$where,$where_arr);
				
			}else{
				//$insert=$this->insert_query("sub_products",$insr_array1);
				$where1="id=:sub_product_id AND  product_id=:product_id";
				$where_arr1= array('sub_product_id'=>$key,'product_id'=>$product_id);
			   //echo 'deleteids'.$sub_product_id[$va].'<br/>';
			$delete=$this->delete_query("sub_products",$where1,$where_arr1);
			}
		}
		foreach($_POST["name_new"] as $va1=>$key1){
			$insr_array2["name"]=$key1;
			$insr_array2["qty"]=$_POST["qty_new"][$va1];
			$insr_array2["processed_qty"]=$_POST["qty_new"][$va1];			
			$insr_array2["price"]=$_POST["price_new"][$va1];	
			$insr_array2["product_id"]=$product_id;	
			if(isset($process_new[$va1]))
				$insr_array2["process"]=1;
			else
				$insr_array2["process"]=0;
			$insert=$this->insert_query("sub_products",$insr_array2);
		}

		return "success";
	}
	
	function product_update_new($pid){
		extract($_POST);
		foreach($sub_product_id as $va=>$key){
			$insr_array1["name"]=$name[$va];
			$insr_array1["qty"]=$qty[$va];
			$insr_array1["processed_qty"]=$qty[$va];			
			$insr_array1["price"]=$price[$va];	
			$insr_array1["product_id"]=$product_id;	
			if(isset($process[$va]))
				$insr_array1["process"]=1;
			else
				$insr_array1["process"]=0;
			if(!empty($name[$va])){ 
				$where="id=:sub_product_id AND  product_id=:product_id";
				$where_arr= array('sub_product_id'=>$key,'product_id'=>$product_id);
				$update=$this->update_query("sub_products",$insr_array1,$where,$where_arr);
				
			}
		}
		foreach($_POST["name_new"] as $va1=>$key1){
			$insr_array2["name"]=$key1;
			$insr_array2["qty"]=$_POST["qty_new"][$va1];
			$insr_array2["processed_qty"]=$_POST["qty_new"][$va1];			
			$insr_array2["price"]=$_POST["price_new"][$va1];	
			$insr_array2["product_id"]=$pid;
			if(isset($approve_new[$va1]))
				$insr_array2["approved"]=1;
			else
				$insr_array2["approved"]=0;			
			if(isset($process_new[$va1]))
				$insr_array2["process"]=1;
			else
				$insr_array2["process"]=0;
			$insert=$this->insert_query("sub_products",$insr_array2);
		}

		return "success";
	}
	function order_update(){ 
		extract($_POST);
		
		$insr_array["product_name"]=$title;
		$insr_array["series"]=$series;
		$where="id=:order_id ";
	 	$where_arr= array('order_id'=>$_GET["order_id"]);
	
		$insert_id=$this->update_query("orders",$insr_array,$where,$where_arr);
		//$total_count=$order_item_row_index_1;
		foreach($suborder_id as $va=>$key){
			$insr_array1["product_name"]=$name[$va];
			$insr_array1["qty"]=$qty[$va];
			$insr_array1["price"]=$price[$va];
			$insr_array1["amount"]=$qty[$va]*$price[$va];
			$insr_array1["order_id"]=$_GET["order_id"];
			if(!empty($name[$va])){
				$where1="id=:sub_order_id AND  order_id=:order_id";
				$where_arr1= array('sub_order_id'=>$key,'order_id'=>$_GET["order_id"]);
				$update=$this->update_query("order_sub",$insr_array1,$where1,$where_arr1);				
			}else{
				$where2="id=:sub_order_id AND  order_id=:order_id";
				$where_arr2= array('sub_order_id'=>$key,'order_id'=>$_GET["order_id"]);
				$delete=$this->delete_query("order_sub",$where2,$where_arr2);
			}
		}
		foreach($_POST["name_new"] as $va1=>$key1){
			$insr_array2["product_name"]=$name_new[$va1];
			$insr_array2["qty"]=$_POST["qty_new"][$va1];
			$insr_array2["price"]=$_POST["price_new"][$va1];
			$insr_array2["amount"]=$_POST["qty_new"][$va1]*$_POST["price_new"][$va1];			
			$insr_array2["order_id"]=$_GET["order_id"];
			$insert=$this->insert_query("order_sub",$insr_array2);
		}

		return "success";
	}
	
	
	
	function employee_update(){ 
		extract($_POST);
		$insr_array["name"]=$employee_name;
		$insr_array["role"]=$employee_role;
		$insr_array["price"]=$price;
		$insr_array["creation_time"]=time();
		
		$where="id=:emp_id ";
	 	$where_arr= array('emp_id'=>$emp_id);
	
		$insert_id=$this->update_query("employee",$insr_array,$where,$where_arr);
	}	
	function received_update(){ 
		extract($_POST);

		$insr_array["product_id"]=$product_id;
		$insr_array["qty"]=$qty;
		$insr_array["price"]=$price;
		$insr_array["supplier_id"]=$supplier_id;
		$insr_array["received_date"]=$received_date;
		$insr_array["creation_time"]=time();
		
		$where="id=:recei_id ";
	 	$where_arr= array('recei_id'=>$recei_id);
	
		$insert_id=$this->update_query("received_products",$insr_array,$where,$where_arr);
	}	
	/* function payment_update($pay,$buffid){
		$insr_array["payment"]=$pay;
		$where="id=:buffid ";
		$where_arr= array('buffid'=>$buffid);	
		$insert_id=$this->update_query("buffing",$insr_array,$where,$where_arr);	
	} */
	
	function process_insert(){
		extract($_POST);
		$insr_array["process"]=$process;
		$insr_array["po_number"]=$po_number;
		//$insr_array["inward_id"]=$purchase;
		
		$insr_array["name"]=$name;
		$insr_array["supervisor_name"]=$supervisor_name;
		$insr_array["receipt_date"]=$date;
		$insr_array["pur_id"]=$purchase;
		$insr_array["approved"]=$approved;
		$insr_array["vf_supervisor"]=$vf_supervisor;
		$final_arry=array();
		$val=0;
		foreach($_POST["check_name"] as $va=>$key){
			
		$final_arry["list"][$va]["name"]=$_POST["subproduct"][$va];	
		$final_arry["list"][$va]["qty"]=$_POST["qty"][$va];
		$final_arry["list"][$va]["price"]=$_POST["price"][$va];
		$final_arry["list"][$va]["amount"]=$_POST["amount"][$va];
		$val++;
		}
		$insr_array["sub_products"]=base64_encode(json_encode($final_arry));		
		$insr_array["created_by"]=$_SESSION["user_id"];
		$insr_array["creation_time"]=time();
		$insert_id=$this->insert_query("inward_process",$insr_array);
		return "success";
	}
	function process_update($id){
		extract($_POST);
		$insr_array["process"]=$process;
		$insr_array["complete_qty"]=$qtys;
		$insr_array["complete_date"]=$date;
		$insr_array["po_number"]=$po_number;
		$insr_array["inward_id"]=$purchase;
		$insr_array["creation_time"]=time();
		$insr_array["created_by"]=$_SESSION["user_id"];
		$where="id=:process_id ";
	 	$where_arr= array('process_id'=>$id);
	
		$insert_id=$this->update_query("inward_process",$insr_array,$where,$where_arr);
		return "success";
	}
	
	function supplier_update(){ 
		extract($_POST);
		
		$insr_array["supplier_name"]=$supplier_name;
		$insr_array["vendor_id"]=$vendor_id;
		$insr_array["address"]=$address;
		$insr_array["gst_no"]=$gst_no;
		$insr_array["pincode"]=$pincode;
		$insr_array["payment_terms"]=$payment_terms;
		$insr_array["creation_time"]=time();
		$insr_array["city"]=$city;
		$insr_array["state"]=$state;
		$insr_array["created_by"]=$_SESSION["user_id"];
		$where="id=:supplier_id";
	 	$where_arr= array('supplier_id'=>$_GET["supplier_id"]);
	
		$insert_id=$this->update_query("suppliers",$insr_array,$where,$where_arr);
		return "success";
	}
	
	function forwards($ids){ 
		extract($_POST);
		$insr_array["forward_approved"]=1;
		$where="id=:ids";
	 	$where_arr= array('ids'=>$ids);	
		$insert_id=$this->update_query("buffing",$insr_array,$where,$where_arr);	

		return "success";
	}
	function approve($ids){ 
		extract($_POST);
		
		
		$already=$this->buffing_edit($buffing_id);
		foreach($already as $va=>$key){
$products=convert_array($key["products"]);
//echo"<pre>";print_r($products);echo"</pre>";
}

		$products[0]["over_time"]=$aprv_over_time;
		$products[0]["overtime_amount"]=$aprv_over_time*$products[0]["overtime_price"];
		$insr_array["products"]=base64_encode(json_encode($products));
		$insr_array["admin_approved"]=1;
		
		$where="id=:ids";
	 	$where_arr= array('ids'=>$buffing_id);	
		$insert_id=$this->update_query("buffing",$insr_array,$where,$where_arr);	

		return "success";
	}
	function forwards_admin($ids){ 
		extract($_POST);
		$insr_array["admin_approved"]=1;
		$where="id=:ids";
	 	$where_arr= array('ids'=>$ids);	
		$insert_id=$this->update_query("buffing",$insr_array,$where,$where_arr);	

		return "success";
	}
	function receivedqty_approve(){ 
		extract($_POST);
		$insr_array["admin_approve"]=1;
		$insr_array["qty_approve"]=$qty_approve;
		$insr_array["reason"]=$reason_reject;
		$where="id=:ids";
	 	$where_arr= array('ids'=>$recei_id);	
		$insert_id=$this->update_query("received_products",$insr_array,$where,$where_arr);	

		return "success";
	}	
	function buffingappr_insert($id){ 
		extract($_POST);
		$insr_array["qc_approved"]=1;
		foreach($product_id as $va=>$key){
		$finalarry[$va]["product_id"]=$key;
		$finalarry[$va]["approve"]=$approve[$va];
		$finalarry[$va]["reject"]=$reject[$va];
		}
		$insr_array["products_approve"]=base64_encode(json_encode($finalarry));
		
		$insr_array["qc_comments"]=$comments;
		$insr_array["ins_by"]=$ins_by;		
		$where="id=:ids";
	 	$where_arr= array('ids'=>$id);	
		$insert_id=$this->update_query("buffing",$insr_array,$where,$where_arr);	

		return "success";
	}	
	function payment_update($id){ 
		extract($_POST);
		$insr_array["admin_approved"]=1;		
		$insr_array["admin_comments"]=$comments;
		$insr_array["admin_processed"]=$admin_processed;
		$where="id=:ids";
	 	$where_arr= array('ids'=>$id);	
		$insert_id=$this->update_query("buffing",$insr_array,$where,$where_arr);	

		return "success";
	}	
	function bufreject_approve($list){ 
		extract($_POST);
		$insr_array["buffing_id"]=$list["id"];
		$insr_array["product_id"]=$list["product_id"];
		$insr_array["return_by"]=$_SESSION["user_id"];
		$insr_array["qty"]=$list["reject"];		
		$insr_array["creation_time"]=time();
		$insert_id=$this->insert_query("return_qty",$insr_array);	

		return "success";
	}	
	function reject_approve($list){ 
		extract($_POST);
		$insr_array["inward_approve"]=$list["id"];
		$insr_array["debit_no"]=$list["debit_no"];
		$insr_array["product_id"]=$list["product_id"];
		$insr_array["removed_by"]=$_SESSION["user_id"];
		$insr_array["removed_qty"]=$list["reject"];		
		$insr_array["removed_time"]=time();
		//$where="id=:ids";
	 	//$where_arr= array('ids'=>$list["id"]);	
		$insert_id=$this->insert_query("rejection",$insr_array);	

		return "success";
	}	
	function dashboard_list($fillter){
	 if($_GET["fillter"]=='today'){
	$date=strtotime('today');
	$where="`buffing_date`= '".$date."' ";
	 }else if($_GET["fillter"]=='week'){ 
		 
	$date=strtotime('-1 week');
	$where="  `buffing_date` >= '".$date."' ";
	 }else if($_GET["fillter"]=='month'){
		$date=strtotime('-1 month');
	$where=" `buffing_date` >= '".$date."'";
	 }else
		 $where="`id`!=''";
			return $this->select_query("buffing","id,name,supervisor_name,buffing_date,products,created_by,comments,creation_time,approved,payment,forward_approved,admin_approved",$where);
	}
	function report_filter(){
	extract($_POST);
	$where=" `buffing_date` >= '".strtotime($from_date)."' AND `buffing_date` <= '".strtotime($to_date)."' AND report=0 ";
	return $this->select_query("buffing","*",$where,array()," order by emp_type ASC");
	}
	function product_list(){ 
			return $this->select_query("products","*","id!=''");
	}
	function subproduct_list($mate){ 
	if($mate=='')		
			return $this->select_query("sub_products","*","id!=''");
	else
			return $this->select_query("products a,sub_products b","*","a.id=b.product_id AND a.material=".$mate."");

	}
	function subproduct_pinvolve(){ 
			return $this->select_query("sub_products","*","id!='' and process=1");
	}
	function inward_list(){
			return $this->select_query("inward","*","id!=''");
	}
	function inward_edit($inward){
			return $this->select_query("inward","*","id=".$inward."");
	}
	function inwardapprove_edit($inward){
			return $this->select_query("inward_approve","*","id=".$inward."");
	}
	function inward_approve_list(){
			return $this->select_query("inward_approve","*","id!=''");
	}
	function inward_approve_admin(){
			return $this->select_query("inward_approve","*","id!='' and admin_approve=1");
	}
	function material_category(){ 
			return $this->select_query("material_category","*","id!=''");
	}
	function material_edit($id){ 
			return $this->select_query("material_category","*","id=".$id."");
	}
	function order_list(){ 
			return $this->select_query("orders","id,po_number,creation_time","id!=''");
	}
	function return_list(){ 
			return $this->select_query("return_qty","*","id!=''");
	}
	function order_edit($order_id){
			return $this->select_query("orders a,order_sub b","a.id,a.po_number,a.creation_time,b.id as sub_order_id,b.product_name as product_id,b.qty,b.price,b.amount,b.order_id","a.id=b.order_id AND b.order_id=".$order_id."");
	}
	function order_qty(){ 
			return $this->select_query("order_sub","id,product_name as product_id,qty","id!=''");
	}
	
	function employee_list(){ 
			return $this->select_query("employee","id,name,price,(select role_name from roles where employee.role=roles.id) as role,creation_time","id!=''");
	}
	
	function buffing_list(){ 
			return $this->select_query("buffing","*","id!=''",array()," order by emp_type ASC");
	}
	function report_history(){
			return $this->select_query("report","*","id!=''");
	}
	function report_edit($id){
			return $this->select_query("report","*","id=".$id."");
	}
	function buffing_list1(){ 
			return $this->select_query("buffing","*","id!='' and report=0",array()," order by emp_type ASC");
	}
	function buffing_edit($id){ 
			return $this->select_query("buffing","*","id=".$id."");
	}
	function buffing_view($id){ 
			return $this->select_query("buffing","*","id=".$id."");
	}
	function inward_view($id){ 
			return $this->select_query("inward","*","id=".$id."");
	}
	function inwardappr_view($id){ 
			return $this->select_query("inward_approve","*","inward_id=".$id."");
	}
	function rejected($id,$product_id){ 
			return $this->select_query("rejection","*","inward_approve=".$id." and product_id=".$product_id."");
	}
	function received_list(){ 
			return $this->select_query("received_products","id,qty,price,received_date,(select supplier_name from suppliers where received_products.supplier_id=suppliers.id) as supplier_name,(select product_name from products where received_products.product_id=products.id) as product_name,creation_time,product_id,admin_approve,qty_approve","id!=''");
	}
	function employee_edit($id){ 
		return $this->select_query("employee","id,name,role,price","id=".$id."");
	}
	function received_edit($id){ 
		return $this->select_query("received_products","id,qty,price,received_date,product_id,supplier_id,admin_approve,qty_approve","id=".$id."");
	}
	function received_edit1($id){ 
		return $this->select_query("received_products","id,qty,price,received_date,product_id,supplier_id,admin_approve,qty_approve","product_id=".$id."");
	}
	function roles(){ 
			return $this->select_query("roles","id,role_name","id!=''");
	}
	function roles_edit($role_id){ 
			return $this->select_query("roles","id,role_name","id=".$role_id."");
	}	
	function emp_delete($emp_edit){
		extract($_POST);
		$insert_array["reason"]=$reason_delete;
		$insert_array["name"]=$emp_edit[0]["name"];
		$insert_array["role"]=$emp_edit[0]["role"];
		$insert_array["delete_time"]=time();
		$insert_array["delete_by"]=$_SESSION["user_id"];
		$insert_id=$this->insert_query("employee_deletelog",$insert_array);
		$where1="id=:emp_id";
		$where_arr1= array('emp_id'=>$emp_edit[0]["id"]);	
		$delete=$this->delete_query("employee",$where1,$where_arr1);
	}
	
	function supplier_list(){
			return $this->select_query("suppliers","id,supplier_name,vendor_id,address,city,state,gst_no,pincode,payment_terms,creation_time","id!=''");
	}

	function master_request(){ 
	
		$where="a.id=b.product_id";
		return $this->select_query("products a,sub_products b","a.id,a.product_name,a.series,a.open_qty,a.price as product_price,a.creation_time,b.name,b.qty,b.price,b.process,b.id as sub_product_id",$where);
	
	}
	function product_edit($product_id){
		$where="a.id=b.product_id AND a.id=".$product_id;
		return $this->select_query("products a,sub_products b","a.id,a.material,b.approved,a.product_name,a.series,a.open_qty,a.price as product_price,a.creation_time,b.name,b.qty,b.price,b.process,b.id as sub_product_id",$where);
	}
	function sub_pro_name($product_id){
		$where="id=".$product_id."";
		return $this->select_query("sub_products","*",$where);	
	}

	function subproduct_edit($product_id){	
    	$where="a.id=".$product_id." AND a.id=b.product_id";
		return $this->select_query("products a,sub_products b","*",$where);	
	}
		function find_product($series){
			return $this->select_query("products","*","id!='' and series LIKE ('".$series."')");
	}
	function get_productid($product_id){	
    	$where="product_name LIKE ('".$product_id."')";
		return $this->select_query("products","*",$where);	
	}
	function product_delete($product_id){
		$where="id=:productid";
		$where1="product_id=:product_id";
		$where_arr=array("productid"=>$product_id);
		$where_arr1=array("product_id"=>$product_id);
		$this->delete_query("products",$where,$where_arr);
		$this->delete_query("sub_products",$where1,$where_arr1);
		return 'deleted';
	}
	function supplier_delete($supplier_id){
		$where="id=:supplier_id";
		
		$where_arr=array("supplier_id"=>$supplier_id);

		$this->delete_query("suppliers",$where,$where_arr);

		return 'deleted';
	}
	function supplier_edit($supplier_id){ 
		$where="id=".$supplier_id;
		return $this->select_query("suppliers","id,supplier_name,city,state,gst_no,pincode,payment_terms,creation_time,vendor_id,address",$where);
	}
} ?>
