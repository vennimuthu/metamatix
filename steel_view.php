<?php
include("config/config.php");

if(!isset($_SESSION["user_id"])){ // checking whether the usertype is logged in
	header('Location: logout.php');        						//REdirection to logout.php
	exit;
}

include("core/class/db_query.php");                             // Class where query generetion is written
include("core/class/db_helper.php");                            // Class where table and feilds 
include("core/function/common.php");
                                                               // are mentioned to generate query
ob_start();														// to clear the internal output
global $db_helper_obj;
global $global_website_url;
$db_helper_obj=new db_helper();  

$steel_view=$db_helper_obj->steel_view($_GET["process_id"]);

foreach($steel_view as $va=>$key){
	$steel_convert[$key["order_product_id"]]["product_id"]=$key["order_product_id"];
	$steel_convert[$key["order_product_id"]]["qty_received"]=$key["qty_received"];
	$steel_convert[$key["order_product_id"]]["unit"]=$key["unit"];
	$steel_convert[$key["order_product_id"]]["sub_product"][$key["sub_product_id"]]["sub_product_id"]=$key["sub_product_id"];
	$steel_convert[$key["order_product_id"]]["sub_product"][$key["sub_product_id"]]["qty_produced"]=$key["qty_produced"];
	$steel_convert[$key["order_product_id"]]["sub_product"][$key["sub_product_id"]]["qty_approved"]=$key["qty_approved"];
	$steel_convert[$key["order_product_id"]]["sub_product"][$key["sub_product_id"]]["rm_weight"]=$key["rm_weight"];
	$steel_convert[$key["order_product_id"]]["sub_product"][$key["sub_product_id"]]["rm_wastage"]=$key["rm_wastage"];
	$steel_convert[$key["order_product_id"]]["sub_product"][$key["sub_product_id"]]["total_weight"]=$key["total_weight"];
	$steel_convert[$key["order_product_id"]]["sub_product"][$key["sub_product_id"]]["storage_location"]=$key["storage_location"];
}
//echo"<pre>";print_r($steel_convert);echo"</pre>";
?>
<html>
<head>
<link rel="icon" type="images/png" href="">
  <link rel='stylesheet' href='css/font-awesome.css'>
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
   <link rel="stylesheet" href="css/responsive-menu.css">
  <link rel="stylesheet" href="css/style.css">
  <script src="js/jquery.js"></script>
  <script src="js/bootstrap.js"></script>
  <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
  <link href="css/select2.min.css" rel="stylesheet" />
  <script src="js/select2.min.js"></script>
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" />
  <script src="js/modernizr-custom.js"></script>
  <script src="js/responsive-menu.js"></script>
</head>
<div class="listwrapper" id="height_id">

<div class="table-responsive"> 
<table class="table listhead table-bordered table-hover" align="center" width="50%" border="1" cellspacing="0" cellpadding="0">
<tr>	
<td><label>Converted From:</label></td>
<td>
<label><?php 
if($steel_view[0]["converted_from"]==1) echo "Purchase Order"; else echo "Stock In Hand";
 ?></label>
</td>
<td><label>SCN NO:</label></td>
<td>
<label><?php echo $steel_view[0]["steel_number"]; ?></label>
 </td>
</tr>

<?php if($steel_view[0]["converted_from"]==1){ ?>
<tr>
<td><label>Supplier Name</label></td>
<td>
<label><?php 
$prod=$db_helper_obj->supplier_edit($steel_view[0]["supplier"]);
echo $prod[0]["supplier_name"]; ?></label>
</td>
<td width="70px"><label>Po Number</label></td>
<td>
<label><?php 
$po=$db_helper_obj->get_po_number($steel_view[0]["po_number"]);
echo $po[0]["po_number"]; ?></label>
</td>
</tr>
<?php } ?>

<?php
$count=1;
foreach($steel_convert as $va=>$key){	
?>
<tr>
<td align="center"><label>S No</label></td>
<td align="center"><label>Part Name</label></td>
<td align="center"><label>Qty Received</label></td>
<td align="center" style="width:90px;"><label>Unit</label></td> 
</tr>
<tr>
<td align="center"><?php echo $count; ?></td>
<td align="center" style="padding: 4px;">
<label><?php $prod2=$db_helper_obj->total_product_edit($key["product_id"]);
 echo $prod2[0]["name"]; ?></label>
</td>
<td  align="center" style="padding: 4px;">
<label><?php echo $key["qty_received"]; ?></label>
</td>
<td align="center" style="padding: 4px;">
<label><?php echo "KG"; ?></label>
 </td>
</tr>

<tr><td colspan="4">
<table class="table listhead table-bordered table-hover" align="center" width="50%" border="1" cellspacing="0" cellpadding="0" >
<tr>
<td align="center"><label>S No</label></td>
<td align="center"><label>Sub Parts</label></td>
<td align="center"><label>Qty Produced</label></td>
<td align="center"><label>Rm Weight</label></td> 
<td align="center"><label>Rm Wastage</label></td> 
<td align="center"><label>Total Weight</label></td> 
<td align="center"><label>Storage Location	</label></td>
</tr>

<?php
$count=1;
$counts=1;
foreach($key["sub_product"] as $va1=>$key1){
if($va1==$key1["sub_product_id"]){
?>
<tr>
<td align="center"><?php echo $counts; ?></td>
<td align="center" style="padding: 4px;">
<label><?php $prod2=$db_helper_obj->total_product_edit($key1["sub_product_id"]);
echo $prod2[0]["name"]; ?></label>
</td>
<td  align="center" style="padding: 4px;">
<label><?php echo $key1["qty_produced"]; ?></label>
</td>
 
<td  align="center" style="padding: 4px;">
<label><?php echo $key1["rm_weight"]; ?></label>
</td>
<td  align="center" style="padding: 4px;">
<label><?php echo $key1["rm_wastage"]; ?></label>
</td>
<td  align="center" style="padding: 4px;">
<label><?php echo $key1["total_weight"]; ?></label>
</td>
<td align="center" style="padding: 4px;">
<label><?php
if($key1["storage_location"]==1)
	echo "Raw Material Stock";
else if($key1["storage_location"]==2)	
	echo "Machined Stock";
else if($key1["storage_location"]==3)
	echo "Buffed Stock";
?></label>
</td>
</tr>

<?php } $counts++; } ?>

</table>
</td></tr>
 <?php $count++;	} ?>
</table>
</div>
</div>
<script type="text/javascript">
setTimeout(function(){ parent.$("#iframe_show_vehicle_data").attr("height",$("#height_id").height()+30+"px"); }, 100);
</script>
