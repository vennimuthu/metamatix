
<?php 
include("config/config.php");
include("core/class/db_query.php");                       // Class where query generetion is written
include("core/class/db_helper.php");                            // Class where table and feilds 
include("core/function/common.php"); 
$db_query=new db_query();  

global $db_helper_obj;
$db_helper_obj=new db_helper();
$deliverymo_edit=$db_helper_obj->deliverymo_edit($_GET["process_id"]);
//$_POST["products"]=convert_array($deliverymo_edit[0]["products"]);
$nature_list=$db_helper_obj->nature_list();
$deliverymo_list=$db_helper_obj->deliverymo_list();

if(count($deliverymo_list)>0)
	$inward_no="MMSPL-CHN-".sprintf("%03s",count($deliverymo_list)+1);
else
	$inward_no="MMSPL-CHN-001";
 
if(!empty($_POST)){
	$process=$db_helper_obj->deliverymo_insert();	
	header('Location:  deliverymo_list.php');
}


$employee_list=$db_helper_obj->employee_list();
$product_list=$db_helper_obj->total_product_list();
$inward_appr_list=$db_helper_obj->inward_approve_admin();
$subproduct_list=$db_helper_obj->totalpr_minvolve();

foreach($subproduct_list as $va=>$key){
	$product_ids[]=$key["id"];
	$po_qty=$db_helper_obj->openstock_edit1($key["id"]);
    $open_stock[$key["id"]]=$po_qty[0]["processed_qty"];
	$subproduct_list[$va]["qty"]=$po_qty[0]["processed_qty"];
	$product_name[$key["id"]]=$key["name"];
	$price[$key["id"]]=$key["price"];
	$product_price[$key["id"]]=$key["product_price"];
}

foreach($inward_appr_list as $va=>$key){
	$products=convert_array($key["products"]);		
	$qtys=convert_array($key["qty_list"]);	
	foreach($products as $va1=>$key1){
		if(in_array($key1["product_id"],$product_ids)){
			$total_qty[$key1["product_id"]]["qty_approve"]+=$key1["qty_approve"];
			$total_qty[$key1["product_id"]]["product_id"]=$key1["product_id"];
		}
	}
}
foreach($total_qty as $va1=>$key1){
$total_qty[$va1]["stocks"]=$open_stock[$key1["product_id"]];
$total_qty[$va1]["product_name"]=$product_name[$key1["product_id"]];
$total_qty[$va1]["price"]=$price[$key1["product_id"]];
$total_qty[$va1]["product_price"]=$product_price[$key1["product_id"]];
}
foreach($subproduct_list as $va=>$key){
if(!isset($total_qty[$key["id"]]["product_id"])){
if($key["qty"]!=0){
$total_qty[$key["id"]]["product_id"]=$key["id"];
$total_qty[$key["id"]]["stocks"]=$key["qty"];
$total_qty[$key["id"]]["product_name"]=$key["name"];
$total_qty[$key["id"]]["price"]=$key["price"];
$total_qty[$key["id"]]["product_price"]=$key["product_price"];
}	
}	
}

foreach($total_qty as $va2=>$key2){
$subproduct=$db_helper_obj->total_product_edit($key2["product_id"]);	
if($subproduct[0]["machining"]==1){
	$total_qty1[$va2]=$key2;
	$id_machin[]=$key2["product_id"];
}
}

$supplier_list=$db_helper_obj->supplier_edit($_POST["products"]["supplier"]);
foreach($supplier_list as $va3=>$key3){
$reg=explode(",",$key3["address"]);
$supplier_list[$va3]["addr1"]=$reg[0].",".$reg[1];
if(isset($reg[2])||isset($reg[3]))
$supplier_list[$va3]["addr2"]=$reg[2].",".$reg[3];	
}
foreach($shipto_list as $va3=>$key3){
$reg=explode(",",$key3["ship_address"]);
$shipto_list[$va3]["addr1"]=$reg[0].",".$reg[1];
$shipto_list[$va3]["addr2"]=$reg[2].",".$reg[3];	
}
$reg=explode(",",$shipto_list["pdfinfo"]["ship_address"]);
$add1=$reg[0]." ".$reg[1];
$add2=$reg[2].",".$reg[3];

$reg=implode(",",$supplier_list[0]["address"]);

	$add1=$reg[0]." ".$reg[1];
if(isset($reg[2])||isset($reg[3]))
	$add2=$reg[2].",".$reg[3];
$ship=implode(",",$shipto_list[0]["ship_address"]);
	$add3=$ship[0]." ".$ship[1];
if(isset($ship[2])||isset($ship[3]))
	$add4=$ship[2].",".$ship[3];

	
$ship_purchase.=' 
  <tr>
    <td class="company" style=
    "padding-left: 5px;border-bottom: 1px solid #ffffff;border-left: 1px solid #000000;border-right:1px solid #000000;"
    height="19" colspan="4" align="left" valign="middle" bgcolor=
    "#FFFFFF" >
      <label id=
      "pur_add1"><font size="3">'.strtoupper($supplier_list[0]["supplier_name"]).'</font></label>
    </td>

    <td class="border_cls1" align="left" bgcolor="#FFFFFF" colspan=
    "3" valign="middle">
      <label id=
      "supp_addr1"><font size="3">'.strtoupper($shipto_list[0]["ship_name"]).'</font></label>
    </td>
  </tr>
  <tr>
    <td class="company" style=
    "padding-left: 5px;border-bottom: 1px solid #ffffff;" height=
    "15" colspan="4" align="left" bgcolor="#FFFFFF" valign=
    "middle">
      <label id=
      "pur_add2"><font size="3">'.strtoupper($supplier_list[0]["addr1"]).'</font></label>
    </td>
    
    <td class="border_cls1" align="left" bgcolor="#FFFFFF" colspan=
    "3" valign="middle">
      <label id=
      "supp_addr2"><font size="3">'.strtoupper($shipto_list[0]["addr1"]).'</font></label>
    </td>
  </tr>
  <tr>
    <td class="company" style=
    "padding-left: 5px;border-bottom:1px solid #ffffff;" height=
    "19" colspan="4" align="left" bgcolor="#FFFFFF" valign=
    "middle">
      <label id=
      "pur_add3"><font size="3">'.strtoupper($supplier_list[0]["addr2"]).'</font></label>
    </td>
  
    <td align="left" bgcolor="#FFFFFF" style=
    "border-right:1px solid #000000;border-bottom: 1px solid #ffffff;border-left: 1px solid #ffffff;"
    colspan="3" valign="middle">
      <label id=
      "supp_addr3"><font size="3">'.strtoupper($shipto_list[0]["addr2"]).'</font></label>
    </td>
  </tr>
  <tr>
    <td class="company" height="19" colspan="4" align="left"
    bgcolor="#FFFFFF"  style="border-bottom:1px solid  #000000;" valign="middle">
      <label id=
      "pur_add4"><font size="3">'.strtoupper($supplier_list[0]["city"]).'</font></label>
    </td>
   
    <td align="left" bgcolor="#FFFFFF" style=
    "border-right:1px solid #000000;border-bottom: 1px solid #000000;border-left: 1px solid #ffffff;"
    colspan="3" valign="middle">
      <label id=
      "supp_addr4"><font size="3">'.strtoupper($shipto_list[0]["ship_city"]).'</font></label>
    </td>
  </tr>';
?>
<?php
ob_clean();

// create some HTML content
$html1 = '<style>

	   .pur_head{
		border: 1px solid #000000;
		-webkit-text-fill-color:black;
	  }
	  .company{
		border-left: 1px solid #000000;
		border-right: 1px solid #000000;
		border-bottom:1px solid #ffffff;
		 border-spacing: 2px;
	  }

	   .border_cls{
			border-left: 1px solid #000000; border-right: 1px solid #000000;
			border-bottom: 1px solid #ffffff;
	  }
	  .border_cls1{
		border-right: 1px solid #000000 ;
		border-left: 1px solid #ffffff;
		border-bottom: 1px solid #ffffff;
	  }
	  .border_cls2{
		border-bottom: 1px solid #ffffff; border-left: 1px solid #000000 ;

	  }
	  .border_cls3{
		border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;  

	  }
	  .border_cls4{
		border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;  

	  }
	  .border_cls5{
		border-bottom: 1px solid #ffffff; border-right: 1px solid #ffffff;
	
	  } 
	 
	  td{
		   
			height: 10px;
			valign:"center"; 			
		
	  }
	  </style><table align="center" style="border-collapse:collapse;font-size:	15px;">
         <colgroup>
            <col width="10">
            <col width="10">
            <col width="133">
            <col width="119">
            <col width="111">
            <col width="244">
         </colgroup>
         <tbody>
            <tr>
			
               <td class="pur_head" colspan="7" align="center" valign="MIDDLE" >
			   <b>
			   DELIVERY CHALLAN (Material In)</b></td>
            </tr>
			 <tr>
               <td class="company" style="-webkit-text-fill-color:black;padding-left: 0px;border-right:1px solid #ffffff;" valign="MIDDLE" align="LEFT" rowspan="4" >
				<img style="width:170px;max-height: 40px;" src="images/Metamatix_logo.png" alt="Team Metamatix" />
			   </td> 
				<td style="padding-left: 0px;padding-right:5px;" valign="bottom" align="LEFT" colspan="4">
				<b><font size="3">METAMATIX SOLUTIONS PRIVATE LIMITED</font></b>
			   </td>
               
               <td align="LEFT" valign="bottom" style="-webkit-text-fill-color:black;border-bottom: 1px solid #ffffff;border-left:1px solid #000000;" >
			 <font size="3"><b>Delivery Challan No:</b></font>
			   </td>
               <td class="border_cls1" style="-webkit-text-fill-color:black;border-bottom: 1px solid #ffffff;" valign="bottom"  align="RIGHT" >
			   <font size="3"><b>'.$deliverymo_edit[0]["challan_no"].'</b></font>
			   </td>			   
            </tr>
          
            <tr>
               <td class="company" colspan="4" style="-webkit-text-fill-color:black;padding-left: 0px;" valign="bottom" align="LEFT" >
			   <font size="3">SHED NO:27, SIDCO INDUSTRIAL ESTATE,</font>
			   </td>
               
               <td align="LEFT" valign="bottom" style="-webkit-text-fill-color:black;border-bottom: 1px solid #ffffff;padding-left:1px;">
			   <b><font size="3">Date:</font></b>
			   </td>
               <td class="border_cls1" style="-webkit-text-fill-color:black;border-bottom: 1px solid #ffffff;" align="LEFT" valign="bottom" >
			  
              <font size="3"><b>'.date('d-M-Y',strtotime($deliverymo_edit[0]["dated"])).'</b></font>
               </td>
            </tr>
            <tr>
               <td class="company" style="-webkit-text-fill-color:black;padding-left: 2px;" colspan="4" align="LEFT" valign="bottom" >
			   <font size="3">KURICHI</font>
			   </td>
               
               <td align="LEFT" style="-webkit-text-fill-color:black;border-bottom: 1px solid #ffffff;border-left: 1px solid #ffffff"  valign="bottom" >
			  <font size="3"> <b>Supplier ID:</b></font>
			   </td>
               <td class="border_cls1" style="-webkit-text-fill-color:black;border-bottom: 1px solid #ffffff;" align="LEFT" valign="bottom"  >
			   <font size="3"><b>'.$_POST["products"]["supplier"].'</b></font>
			   </td>
            </tr>
            <tr>
               <td class="company" style="-webkit-text-fill-color:black;padding-left: 2px;border-bottom: 1px solid #000000;" colspan="4" valign="bottom" align="LEFT" >
			   <p><font size="3">COIMBATORE</font></p>
			   <p><font size="3">Tamil Nadu - 641 021</font></p>
			   <p><font size="3">GSTIN/UIN: 33AAKCM7413N1Z5</font></p>
			   <p><font size="3">CIN: U28990TZ2016PTC028310</font></p>
			   </td>
              
               <td align="LEFT" style="border-bottom: 1px solid #000000;border-left: 1px solid #ffffff;">
			   <br>
			   </td>
               <td class="border_cls1" align="LEFT" style="border-bottom: 1px solid #000000;" >
			   <br>
			   </td>
            </tr>
			<tr>
			 
            <tr>
               <td class="company" colspan="4"  align="LEFT"	style="padding-left: 4px;padding-right:1px; border-bottom: 1px solid #000000;border-left:1px solid #000000;border-top:1px solid #000000;border-right:1px solid #000000;" valign="middle">
			   <font size="3"> <b>GSTIN/UIN: </b>'.$_POST["pdfinfo"]["gst_number"].' </font>
			  
				<font size="3"><p></p><b>Supplier:</b></font>
			   </td>              
               <td align="LEFT" colspan="3" valign="middle" style="border-bottom: 1px solid #000000;border-left: 1px solid #ffffff;border-top:1px solid #000000;border-right:1px solid #000000;">
			   <font size="3"><b>Destination:</b></font>
			   </td>
			 
            </tr>
            
           '.$deliverymo_edit[0]["supplier"].'
                      
  <tr style="border-bottom: 1px solid #000000;">
               <td colspan="3" class="company" style="border-bottom: 1px solid #000000;border-top: 1px solid #000000;padding-left:0px;" align ="CENTER"  width="2%"  bgcolor="#C3D69B" valign="middle"><font size="3"><b>Sl No</b><font></td>
               <td  align="CENTER" bgcolor="#C3D69B" style="border-right:1px solid #000000;border-bottom: 1px solid #000000;" valign="middle" ><font size="3"><b> Description </b>'.strtoupper($list[0]["name"]).'</font></td>
               <td class="border_cls1" style="border-bottom: 1px solid #000000;" align="CENTER" bgcolor="#C3D69B" valign="middle"><font size="3"><b>Qty</b></font></td>
               <td class="border_cls1" style="border-bottom: 1px solid #000000;" align="CENTER" bgcolor="#C3D69B" valign="middle"><font size="3"><b>Rate</b></font></td>
               <td class="border_cls1" style="border-bottom: 1px solid #000000;" align="CENTER" bgcolor="#C3D69B" valign="middle"><font size="3"><b>Amount</b></font></td>
            </tr>
          	
            
             <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				
				  <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 
				------------
				 
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				
				  <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				  <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				------
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr> <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="2" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;">
			    </td>
				
				<td colspan="1" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;border-right:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;">
				<b>'.$_POST["pdfinfo"]["words"].'</b></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				  <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="8" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				  <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="7" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
				</tr>
				 <tr>
				<td colspan="3" style="border-bottom:1px solid #ffffff;border-left:1px solid #000000;"></td>
							
				<td colspan="4" style="border-top:1px solid #ffffff;border-left:1px solid #ffffff;border-right:1px solid #000000;"><font size="2" align="MIDDLE"> </font></td>		
				</tr>
				
				 <tr>
				<td colspan="4" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;padding-bottom:50px;padding-right:50px;"><font size="3"><b>Received Goods ins Condition:</b></font>
				
				</td>		
				<td colspan="3" align="CENTER" style="border-top:1px solid #000000;border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;padding-top:30px;"><font align="CENTER" size="2"><b>for METAMATIX SOLUTIONS PRIVATE LIMITED</b></font>
				
				</td>
					
				</tr>
           </tbody>
      </table>';
	  
//echo"<pre>";print_r($html1);echo"</pre>";exit;
include("MPDF57/mpdf.php");
$mpdf=new mPDF('en-GB-x','A4','','',12,12,12,12,8,4); 
$mpdf->SetDisplayMode('fullpage');
$mpdf->list_indent_first_level = 0;	// 1 or 0 - whether to indent the first level of a list
// LOAD a stylesheet
$stylesheet = file_get_contents('mpdfstyletables.css');
$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
$mpdf->WriteHTML($html1);
$mpdf->Output();
exit;
//==============================================================
//==============================================================
//==============================================================
?>