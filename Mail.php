<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mail extends MY_Controller{
	
	public function __construct(){
		parent::__construct();
		user_validate(); // session validation
		user_type_validate(array(1,2));
		$this->mail_data_arr[1]=array('id'=>1,'disp_name'=>"Reference",'col_name'=>"reference",'type'=>'textarea');
		$this->mail_data_arr[2]=array('id'=>2,'disp_name'=>"Description",'col_name'=>"description",'type'=>'textarea');
		$this->mail_data_arr[3]=array('id'=>3,'disp_name'=>"Price",'col_name'=>"price",'type'=>'text');
		$this->mail_data_arr[4]=array('id'=>4,'disp_name'=>"Height",'col_name'=>"height",'type'=>'text');
		$this->mail_data_arr[5]=array('id'=>5,'disp_name'=>"Width",'col_name'=>"width",'type'=>'text');
		$this->mail_data_arr[6]=array('id'=>6,'disp_name'=>"Quantity",'col_name'=>"quantity",'type'=>'text');
	}
	 public function set_header($header, $value)
  {
    $this->_headers[ $header ] = $value;
  }
	public function index(){
		if(!$this->session->userdata('mail_data_to_sent')){
			$this->load->model('mail_model');
			$prod_list=$this->mail_model->get_products();
			foreach ($prod_list->result() as $va=>$key){
				$ses_arr[$key->id]=array('data'=>array(1=>1,2=>1,3=>1,4=>1,5=>1,6=>1),'file'=>array(1=>array(),2=>array(),3=>array(),5=>array(),6=>array()));
			}
			$this->session->set_userdata('mail_data_to_sent',$ses_arr);
		}
		$this->data["selected_value"]=$this->session->userdata('mail_data_to_sent');
		//$this->data["selected_prod"]=array();
		//print_r($this->data["selected_value"]);
		$this->data["selected_prod"]=$this->session->userdata('mail_prod_to_sent');
		$this->middle = 'product_mail_detail'; // passing middle to function.
		$this->layout();
	}
	public function product_datas(){
		foreach($_FILES["others"]["tmp_name"] as $va=>$key){
			//echo "<pre>";print_r($key);echo "</pre>";	
		}
		echo base64_encode(json_encode($_FILES["others"]));exit;
		$image = $_FILES["others"]["tmp_name"][0];
		$imageData = base64_encode(file_get_contents($image));
		$src = 'data: '.mime_content_type($image).';base64,'.$imageData;
		//echo '<img class="img-thumbnail img-check ng-scope"  src="' . $src . '">';
		exit;
	}
	public function product_data($id){
		$this->load->model('mail_model');
		$data=(array)$this->mail_model->get_product($id);
		$data["list"]=$data;
		$ses_arr=$this->session->userdata('mail_data_to_sent');
		
		foreach($this->mail_data_arr as $va=>$key){
			if(isset($data[$key["col_name"]])){
				$data["list"]["data"][]=array('id'=>$key["id"],'name'=>$key["disp_name"],'value'=>$data[$key["col_name"]],'type'=>$key['type']);
					$ses_arr[$id]["data_value"][$key["id"]]=$data[$key["col_name"]];
				}
			}
		$this->session->set_userdata('mail_data_to_sent',$ses_arr);
		$file=$this->mail_model->get_file($id,"1,2,3,5,6");
		//$data["list"]["file"]=array(1,2);
		foreach($file->result() as $va=>$key){
			$data["list"]["file"][$key->reference_id][]=array('path'=>site_url('getfiledata/get_image/'.$key->id),'file_id'=>$key->id);
		}
		echo json_encode($data["list"]);
		//$this->load->view('json/product_data',$data); 
	}
	public function product_data1($id){
		$this->load->model('mail_model');
		$data=(array)$this->mail_model->get_product($id);

		$data["list"]=$data;
		$ses_arr=$this->session->userdata('mail_data_to_sent');
		foreach($this->mail_data_arr as $va=>$key){
			$data["list"]["data"][]=array('id'=>$key["id"],'name'=>$key["disp_name"],'value'=>$data[$key["col_name"]],'type'=>$key['type']);
			$data[$id]["data_value"][$key["id"]]=$data[$key["col_name"]];
		}
		//echo "<pre>";print_r($ses_arr);echo "</pre>";
		$this->session->set_userdata('mail_data_to_sent',$ses_arr);
		$file=$this->mail_model->get_file($id,"1,2,3,5,6");
		//$data["list"]["file"]=array(1,2);
		foreach($file->result() as $va=>$key){
			$data["list"]["file"][$key->reference_id][]=array('path'=>site_url('getfiledata/get_image/'.$key->id),'file_id'=>$key->id);
		}
		return $data;
		//$this->load->view('json/product_data',$data); 
	}
	public function set_mail_data_value($product_id,$col_name,$assign_value){
		$ses_arr=$this->session->userdata('mail_data_to_sent');
		$ses_arr[$product_id]["data_value"][$col_name]=urldecode($assign_value);
		$this->session->set_userdata('mail_data_to_sent',$ses_arr);
	}
	public function set_mail_data($product_id,$data_type,$col_name,$assign_value){
		$ses_arr=$this->session->userdata('mail_data_to_sent');
		$ses_arr[$product_id][$data_type][$col_name]=$assign_value;
		$this->session->set_userdata('mail_data_to_sent',$ses_arr);
	}
	public function set_mail_data_file($product_id,$data_type,$col_name,$assign_value,$ref_id){
		$ses_arr=$this->session->userdata('mail_data_to_sent');
		$ses_arr[$product_id][$data_type][$ref_id][$col_name]=$assign_value;
		$this->session->set_userdata('mail_data_to_sent',$ses_arr);
		$data=$this->session->userdata('mail_data_to_sent');
	}
	public function set_prod_mail($product_id,$p_name){
		$ses_arr=$this->session->userdata('mail_prod_to_sent');
		$ses_arr[$product_id]=array('id'=>urldecode($product_id),'p_name'=>urldecode($p_name));
		$this->session->set_userdata('mail_prod_to_sent',$ses_arr);
	}
	public function remove_prod_mail($product_id){
		$ses_arr=$this->session->userdata('mail_prod_to_sent');
		unset($ses_arr[$product_id]);
		$this->session->set_userdata('mail_prod_to_sent',$ses_arr);
	}
	public function get_products(){
		$ses_arr=$this->session->userdata('mail_prod_to_sent');
		$id=array(0);
		if(count($ses_arr)>0)
		foreach($ses_arr as $va=>$key){
			$id[]=$key["id"];
		}
		$this->load->model('mail_model');
		$prod_list=$this->mail_model->get_non_mail_products($id);
		$data["prod_list"]=array();
		if($prod_list->num_rows()>0){
			foreach ($prod_list->result() as $va=>$key){
				$data["prod_list"][]=(array)$key;
			}
		}
		echo json_encode($data["prod_list"]);
	}
	function file_conet($var){
	$_POST=$var;
	ob_start();
	//include("E:/xampp/htdocs/lisus/application/views/quote_email1.php");
	include(dirname(dirname(__FILE__)).'/views/quote_email1.php');
	
	$ob = ob_get_clean();	
	return 	$ob ;
	}
function create_zip($files = array(),$destination = '',$overwrite = false) {
	//if the zip file already exists and overwrite is false, return false
	if(file_exists($destination) && !$overwrite) { return false; }
	//vars
	$valid_files = array();
	//if files were passed in...
	if(is_array($files)) {
		//cycle through each file
		foreach($files as $file) {
			//make sure the file exists
			if(file_exists($file)) {
				$valid_files[] = $file;
			}
		}
	}
	//if we have good files...
	if(count($valid_files)) {
		//create the archive
		$zip = new ZipArchive();
		if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
			return false;
		}
	
		//add the files
		/* foreach($valid_files as $file) {
			//echo"<pre>";print_r($file);echo"</pre>";
				$files = scandir($file);
				$val=explode('/',$file);
				$val1=end($val);
			
				foreach($files as $va1=>$key1){
				if($key1!='.'||$key1!='..')	{
				echo "E:\\xampp\htdocs\lisus\application/views/pdf/".$val1.'/'.$key1;
				$paths=str_replace("E:\\xampp\htdocs\lisus\application/views/pdf/","",$file)."<br/>";
				$zip->addFile("E:\\xampp\htdocs\lisus\application/views/pdf/".$val1.'/'.$key1,$key1);	
				}
				}
        
		} */
		foreach($valid_files as $file) {
		$zip->addFile($file,basename($file));	
		}
		//debug
		//echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;
		
		//close the zip -- done!
		$zip->close();
		
		//check to make sure the file exists
		return file_exists($destination);
	}
	else
	{ 
		return false;
	}
}
function my_folder_delete($path) {
    if(!empty($path) && is_dir($path) ){
        $dir  = new RecursiveDirectoryIterator($path, RecursiveDirectoryIterator::SKIP_DOTS); //upper dirs are not included,otherwise DISASTER HAPPENS :)
        $files = new RecursiveIteratorIterator($dir, RecursiveIteratorIterator::CHILD_FIRST);
        foreach ($files as $f) {if (is_file($f)) {unlink($f);} else {$empty_dirs[] = $f;} } if (!empty($empty_dirs)) {foreach ($empty_dirs as $eachDir) {rmdir($eachDir);}} rmdir($path);
    }
}
	public function send_mail(){ error_reporting(0);	

      //   echo dirname(dirname(__FILE__)).'/views/pdf/';	exit;

			
		$files=$this->file_conet($_POST);		$file_path=dirname(dirname(__FILE__)).'/views/pdf/pdf_'.time().'.pdf';
		$data = array('html' => base64_encode($files),'path' => $file_path);
		$this->load->helper('pdf_helper');
		$this->load->view('pdfreport', $data);    
		
		$err=0;
		if(isset($_POST["send_mail"]) && ($_POST["send_mail"]=="Send Quotes") && isset($_POST["email"]) && $_POST["email"]!=""){
			$this->load->helper('form');
			$this->load->library(array('form_validation'));
			$this->form_validation->set_rules('email','Email Data','required');
			if($this->form_validation->run()){
				$this->load->helper('common_helper');
				$mail_to=$_POST["email"];
				$prod_to_sent=$this->session->userdata('mail_prod_to_sent');

				
				
				
				$prod_data_to_sent=$this->session->userdata('mail_data_to_sent');
			
				foreach(array_filter($prod_to_sent) as $va=>$key){
					$prod_id[]=$key["id"];
					$pname[]=$key["p_name"];
				}
				
				
				$p_id_data=implode(",",$prod_id);	
				$this->load->model('mail_model');
				$disp_file=$this->mail_model->get_mail_file($p_id_data,"1,2,3,5,6");
				
				$file_path_arr=array();

				foreach($disp_file->result() as $va_file=>$data_file){
					$file_path_arr[$data_file->id]=FILE_UPLOAD_DIRECTORY.$data_file->file_path;
				}				

					$ref_file_array=array(1=>'Product Image',2=>'Technical Document',3=>'Testing Document','5'=>'Patent Document');
				
					foreach($prod_to_sent as $va_prod=>$data_prod){
					$data_to_store[$data_prod['id']]=array();
					$disp_data=$prod_data_to_sent[$data_prod["id"]];	
		
					foreach(array_filter($disp_data["data"])  as $va=>$key){
						if($key==1){
							$data_to_store[$data_prod['id']]["data"][]=$va;					
						}
					}
					
					
					//$ref_file_array=array(1=>'Product Image',2=>'Technical Document',3=>'Testing Document','5'=>'Patent Document');
					
					foreach($disp_data["file"]  as $va_ref=>$key_ref){
						foreach($key_ref  as $va=>$key){
							if($key==1){
								$data_to_store[$data_prod['id']]["file"][]=$va;
								$image_file[$data_prod['p_name']][$ref_file_array[$va_ref]][]=$file_path_arr[$va];
							}
						}
					}
				}
				//echo"<pre>";print_r($data_to_store);echo"</pre>";exit;	
				}

			$pdf_file="";
			if(isset($image_file) && count($image_file)>0){
				$this->load->library('Fpdf_lib');
				$pdf_file=$this->fpdf_lib->load($image_file);
			}
			/* $this->load->library('email');
			$config['mailpath']    = '/usr/sbin/postfix';
			$config['protocol']    = 'smtp';
			$config['smtp_host']    = 'smtp.gmail.com';

			$config['smtp_port']    = '25';

			$config['smtp_timeout'] = '7';

			$config['smtp_user']    = 'admn.jtech@gmail.com';

			$config['smtp_pass']    = 'J@j123456';

			$config['charset']    = 'utf-8';

			$config['newline']    = "\r\n";

			$config['mailtype'] = 'text'; // or html

			$config['validation'] = TRUE; // bool whether to validate email or not      

			$this->email->initialize($config);


			$this->email->from('admn.jtech@gmail.com');
			$this->email->to('vennimuthu1992@gmail.com'); 


			$this->email->subject('Email Test');

			$this->email->message('Testing the email class.');  

			$this->email->send();

			echo $this->email->print_debugger();

			exit; */
					
			
			$this->load->library('parser');
			
			$this->load->library('encrypt');
			
			$this->load->library('email');
			
			$otherfile=array();
			foreach($prod_id as $vas=>$keys){
			  $va="others_".$keys;
			  foreach($_FILES[$va]["name"] as $vas1=>$keys1){
					//mkdir(dirname(dirname(__FILE__)).'/views/pdf/'.$pname[$vas]);
					$otherfile[$va][]=dirname(dirname(__FILE__)).'/views/pdf/'.$pname[$vas];
					$otherfile1[]=dirname(dirname(__FILE__)).'/views/pdf'.'/'.time().'_'.$keys1;
					//move_uploaded_file($_FILES[$va]["tmp_name"][$vas1],dirname(dirname(__FILE__)).'/views/pdf/'.$pname[$vas].'/'.time().'_'.$keys1);	
					move_uploaded_file($_FILES[$va]["tmp_name"][$vas1],dirname(dirname(__FILE__)).'/views/pdf'.'/'.time().'_'.$keys1);	
			  }
			} 

			$uploadfile=dirname(dirname(__FILE__)).'/views/zip/miscellaneous_'.time().'.zip';
			//if true, good; if false, zip creation failed
			$result = $this->create_zip($otherfile1,$uploadfile);
			
			foreach($otherfile1 as $va1=>$key1){
			//foreach($key1 as $va2=>$key2){
			//$this->my_folder_delete($key2);
			//}
			unlink($key1);
			}
			
			//$file_path;
			$pdf_file1=dirname(dirname(__FILE__)).'/views/pdf/'.$pdf_file;
			$file_path_nw= dirname(dirname(__FILE__)).'/views/pdf/'.basename($file_path);
			$pdf_file1_nw= dirname(dirname(__FILE__)).'/views/pdf/'.basename($pdf_file1);
			$pdf_file3_nw= dirname(dirname(__FILE__)).'/views/zip/'.basename($uploadfile);
			
			

			//$data_file=array($file_path_nw,$pdf_file1_nw,$pdf_file3_nw);
			$table.="</body></html>";
			
			$this->email->set_newline("\r\n");
			$this->email->from('admn.jtech@gmail.com', 'FROM LISUS');			
			$this->email->set_mailtype("html");
			$this->email->to($mail_to);
			$subject='Lisus Quotation';
			$this->email->subject($subject);
			$file_pat= 'http://13.229.109.84/lisus/application/views/pdf/'.basename($file_path);
			$pdf_fil= 'http://13.229.109.84/lisus/application/views/pdf/'.basename($pdf_file1);
			$pdf_fil10= 'http://13.229.109.84/lisus/application/views/zip/'.basename($uploadfile);
			$data_file=array($file_pat,$pdf_fil,$pdf_fil10); 
			$content="This is the quotation from lisus."."<br/>";
			$content.="<br /><a href='".$file_pat."' target='_blank'>click here to view quotation</a>
			<br /><a href='".$pdf_fil."' target='_blank'>click here to view the products documents</a>
			<br /><a href='".$pdf_fil10."' target='_blank'>click here to view Other documents</a>";
			//foreach($data_file as $va=>$key){
			//	$this->email->attach($key);
			//}
			$this->email->message($content); 
			$this->email->send();
			$this->load->model('emailhistory_model');
			//echo"<pre>";print_r($data_to_store);echo"</pre>";exit;	
			$mail_data=array();
			$mail_data["to"]=$mail_to;
			$mail_data["subject"]=$subject;
			$session_data = $this->session->userdata();
			$mail_data["created_by"]=$session_data["user_id"];
			$mail_data["created_datetime"]=date("Y-m-d H:i:s",time());
			$mail_id=$this->emailhistory_model->record_mail($mail_data);
			
			foreach($data_to_store as $va=>$key){
				$mail_data_to_store=array();
				$mail_data_to_store["product_id"]=$va;
				$mail_data_to_store["mail_id"]=$mail_id;
				if(isset($key["data"]) && $key["data"]!=""){
					$mail_data_to_store["data_id"]=implode(",",$key["data"]);
					$disp_data=$prod_data_to_sent[$va];
					foreach($key["data"] as $va_data=>$data_id_value){
						$this->mail_data_arr[$data_id_value]['col_name'];
						$mail_data_to_store[$this->mail_data_arr[$data_id_value]['col_name']]=$disp_data["data_value"][$data_id_value];
					}
				}
				if(isset($key["file"]) && $key["file"]!="")
					$mail_data_to_store["file_id"]=implode(",",$key["file"]);
				$this->emailhistory_model->record_mail_data($mail_data_to_store,$data_file);
			}
			$this->session->set_userdata('mail_prod_to_sent',array());
			$this->session->set_userdata('mail_data_to_sent',array());
			echo "Email has been sent sucessfully"; 
			redirect('mailhistory','refresh');
			}
			if($err==0)
			return $this->index();
		}
		
	
	
	public function resend_mail(){
		
		$file_id=array();
		$prod_to_sent=array();
		$this->load->model('emailhistory_model');
	
		$this->load->model('mail_model');
		$prod_data_to_sent1=$this->emailhistory_model->get_mail_details($_POST["mailid"]);
		
		foreach($prod_data_to_sent1 as $va=>$key){
		$prod_to_sent[$key->product_id]=$this->mail_model->get_mail_productsnew(array($key->product_id));
		$prod_data_to_sent[$key->product_id]=$key;
		}

		
		$prod1=$this->emailhistory_model->get_mail_details($_POST["mailid"]);
		$this->load->model('mail_model');
		$maillist=$this->mail_model->get_mail_id($_POST["mailid"]);
		$err=0;
		$this->load->helper('common_helper');
		$mail_to=$maillist->to;
			
		$data_file=json_decode(base64_decode($prod1[0]->paths));
		//echo"<pre>";print_r($data_file);echo"</pre>";exit;	
		$data_store=json_decode(json_encode($prod1),true);	
			$this->load->library('parser');			
			$this->load->library('encrypt');			
			$this->load->library('email');
			$this->email->set_newline("\r\n");
			$this->email->from('shankar.jtech@gmail.com', 'FROM LISUS');			
			$this->email->set_mailtype("html");
			$this->email->to($mail_to);
			$subject='Re:Lisus Quotation';
			$this->email->subject($subject);
			 
			
			//$data_file=array($file_pat,$pdf_fil,$pdf_fil10); 
			$content="This is the quotation from lisus."."<br/>";
			$content.="<br /><a href='".$data_file[0]."' target='_blank'>click here to view quotation</a>
			<br /><a href='".$data_file[1]."' target='_blank'>click here to view the products documents</a>
			<br /><a href='".$data_file[2]."' target='_blank'>click here to view Other documents</a>";

			//foreach($data_file as $va=>$key){
			//	$this->email->attach($key);
			//}
			$this->email->message($content); 
			$this->email->send();
			$this->load->model('emailhistory_model');
			$mail_data=array();
			$mail_data["to"]=$mail_to;
			$mail_data["subject"]=$subject;
			$session_data = $this->session->userdata();
			$mail_data["created_by"]=$session_data["user_id"];
			$mail_data["created_datetime"]=date("Y-m-d H:i:s",time());
			$mail_id=$this->emailhistory_model->record_mail($mail_data); 
			foreach($data_store as $va=>$key){
				$mail_data_to_store=array();
				$mail_data_to_store["product_id"]=$key["product_id"];
				$mail_data_to_store["reference"]=$key["reference"];
				$mail_data_to_store["description"]=$key["description"];
				$mail_data_to_store["height"]=$key["height"];
				$mail_data_to_store["width"]=$key["width"];
				$mail_data_to_store["price"]=$key["price"];
				$mail_data_to_store["data_id"]=$key["data_id"];
				$mail_data_to_store["file_id"]=$key["file_id"];
				$mail_data_to_store["mail_id"]=$mail_id;
				$mail_data_to_store["paths"]=$key["paths"];
				
				$this->emailhistory_model->record_mail_data($mail_data_to_store,array());
			}
			
			$this->session->set_userdata('mail_prod_to_sent',array());
			$this->session->set_userdata('mail_data_to_sent',array());
			echo "Email has been re sent sucessfully"; 
			redirect('mailhistory','refresh');
			
		
		if($err==0)
			return $this->index();
	}

}